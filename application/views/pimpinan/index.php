<?php echo $header ?>

<header id="topbar">
	<div class="topbar-left">
		<ol class="breadcrumb">
			<li class="crumb-active">
				<a href="#">Dashboard</a>
			</li>
			<li class="crumb-trail">
				<span class="glyphicon glyphicon-home"></span>
			</li>
		</ol>
	</div>
</header>
<section id="content" class="animated fadeIn">
	<div class="row">
		<div class="col-md-3">
			<div class="col-md-12">
				<div class="panel panel-tile text-primary br-b bw5 br-primary-light of-h mb10">
					<div class="panel-body pl20 p5">
						<div class="icon-bg">
							<i class="fa fa-flask"></i>
						</div>
						<h2 class="mt15 lh15">
							<b><?= $pn->num_rows() ?></b>
						</h2>
						<h5 class="text-muted">Data Penelitian</h5>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="panel panel-tile text-warning br-b bw5 br-warning-light of-h mb10">
					<div class="panel-body pl20 p5">
						<div class="icon-bg">
							<i class="fa fa-leaf"></i>
						</div>
						<h2 class="mt15 lh15">
							<b><?= $pg->num_rows() ?></b>
						</h2>
						<h5 class="text-muted">Data Pengabdian</h5>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="panel panel-tile text-danger br-b bw5 br-danger-light of-h mb10">
					<div class="panel-body pl20 p5">
						<div class="icon-bg">
							<i class="fa fa-warning"></i>
						</div>
						<h2 class="mt15 lh15">
							<b><?= $tanggungan->num_rows() ?></b>
						</h2>
						<h5 class="text-muted">Data Tanggungan</h5>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php echo $footer ?>