<?php echo $header ?>

<style>
	.table-bordered > thead > tr > th, 
	.table-bordered > tbody > tr > th, 
	.table-bordered > tfoot > tr > th, 
	.table-bordered > thead > tr > td, 
	.table-bordered > tbody > tr > td, 
	.table-bordered > tfoot > tr > td {
		border: 1px solid #aaa;
	}

	.table-bordered {
		border: 1px solid #aaa;
	}

	#alert-danger { 
		display: none; 
	}
</style>

<header id="topbar">
	<div class="topbar-left">
		<ol class="breadcrumb">
			<li class="crumb-active">
				<a href="#">Tambah Pengabdian</a>
			</li>
			<li class="crumb-trail">
				<span class="glyphicon glyphicon-home"></span>
			</li>
		</ol>
	</div>
</header>
<section id="content" class="animated fadeIn">
	<div class="row">
		<div class="col-md-12">
            <a class="btn btn-primary" href="<?php echo site_url('pengabdian') ?>">
                <i class="fa fa-arrow-left"></i> Kembali
            </a>
			<div class="alert alert-danger light alert-dismissable mt10" id="alert-danger"></div>
			<div class="panel panel-default mn mt10">
				<form class="form-horizontal" method="post" action="<?php echo site_url('pengabdian/edit_proses/'.$this->uri->segment(3)) ?>">
					<div class="panel-heading">
						<span class="panel-title">Edit Pengabdian</span>
					</div>
					<div class="panel-body">
						<?php
							foreach($prop as $row) {
								echo "
									<div class='form-group'>
										<label class='col-lg-3 control-label'>".$row['label']."</label>
										<div class='col-lg-7'>
											".$row['form']."
										</div>
									</div>
								";
							}
						?>
						<div class="form-group">
							<label class="col-lg-3 control-label">Program Studi</label>
							<div class="col-lg-8">
								<select id="prodi" multiple="multiple" name="prodi[]">
									<?php foreach($prodi as $row) { ?> 
										<option value="<?= $row->Kode_Prodi ?>"><?= $row->Nama_Prodi ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
					</div>
					<div class="panel-footer">
						<button class="btn btn-primary">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>

<?php echo $footer ?>

<script>
	jQuery(document).ready(function() {
		<?php
			$stts_input = $this->session->flashdata("status");
			$msg_input = $this->session->flashdata("message");
			
			if($stts_input == "gagal") { ?>
				$("#alert-danger").text("<?= $msg_input ?>");
				$('#alert-danger').fadeToggle();
				$("#alert-danger").delay(4000).fadeOut();
		<?php } ?>

		$('#waktu_pelaksanaan').daterangepicker();

		$("#jenis_pen").on("change", function() {
			$("#skimpengabdian").remove();
			let val = $(this).val();
			$.ajax({
				url: "<?php echo site_url('pengabdian/get_skim_pengabdian/') ?>"+val,
				dataType: "JSON",
				success: function(data) {
					$("#jenis_pen").after(data.output);
				}
			});
		});
		
		let prodi = "<?= $pg->prodi ?>";
		let splitProdi = prodi.split(",");

		$('#prodi').multiselect({
			includeSelectAllOption: true
		});
		$("#prodi").val(splitProdi).multiselect("refresh");
	});
</script>