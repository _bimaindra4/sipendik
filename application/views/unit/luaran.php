<?php echo $header ?>

<style>
	#alert-danger-edit-luaran, 
	#alert-danger-hps-luaran,
	#alert-success,
	.hilang {
		display: none;
	}
</style>

<header id="topbar">
	<div class="topbar-left">
		<ol class="breadcrumb">
			<li class="crumb-active">
				<a href="#">Data Luaran</a>
			</li>
			<li class="crumb-trail">
				<span class="glyphicon glyphicon-home"></span>
			</li>
		</ol>
	</div>
</header>
<section id="content" class="animated fadeIn">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default mn mb10">
				<div class="panel-heading">
					<span class="panel-title">Filter Luaran</span>
				</div>
				<div class="panel-body">
					<form method="post" id="filter-luaran" class="form-horizontal">
						<div class="form-body">
							<div class="form-group">
								<label class="control-label col-md-2">Periode</label>
								<div class="col-md-4">
									<select class="form-control" name="periode" id="periode">
										<option value="">-- Semua Periode --</option>
										<?php foreach($periode as $row) { ?> 
											<option value="<?= $row->id ?>"><?= $row->nama ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2">Pilih Kegiatan</label>
								<div class="col-md-4">
									<select id="kegiatan" class="form-control">
										<option value="">-- Pilih Kegiatan --</option>
										<option value="penelitian">Penelitian</option>
										<option value="pengabdian">Pengabdian</option>
									</select>
								</div>
								<div class="col-md-4 hidden" id="luaran-group">
									<select id="luaran-input" class="form-control">
										<option value="">-- Pilih Luaran --</option>
									</select>
								</div>
							</div>
							<div class="col-md-offset-2 col-md-2" style="padding-left: 5px">
								<button class="btn btn-primary " id="filter_luaran">Filter</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
        <div class="col-md-12">
			<div class="alert alert-success light alert-dismissable" id="alert-success"></div>
            <div class="panel panel-default mn">
				<div class="panel-heading">
					<span class="panel-title">Data Luaran</span>
				</div>
				<div class="panel-body">
					<table id="luaran" class="table table-striped table-hover table-bordered">
						<thead>
							<tr>
								<th>Data</th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
        </div>
	</div>

	<div id="modalDetailLuaran" class="popup-basic bg-none mfp-with-anim mfp-hide" style="max-width: 550px">
		<div class="panel">
			<div class="panel-heading">
				<span class="panel-title"> Detail Luaran</span>
			</div>
			<div class="panel-body" id="luaran-detail"></div>
		</div>
	</div>

	<div id="modalEditLuaran" class="popup-basic bg-none mfp-with-anim mfp-hide" style="max-width: 550px">
		<div class="panel">
			<div class="panel-heading">
				<span class="panel-icon"><i class="fa fa-plus"></i>
				</span>
				<span class="panel-title"> Edit Luaran</span>
			</div>
			<form class="form-horizontal" id="form_edit_luaran" method="post" enctype="multipart/form-data">
				<div class="alert alert-danger light alert-dismissable" id="alert-danger-edit-luaran"></div>
				<div class="panel-body">
					<input type="hidden" name="id_luaran" id="id_luaran">
					<div id="edit_luaran_isian"></div>
				</div>
				<div class="panel-footer text-right">
					<button class="btn btn-primary btn-sm" type="button" id="btn-edit-luaran">Edit</button>
				</div>
			</form>
		</div>
	</div>

	<div id="modalHapusLuaran" class="popup-basic bg-none mfp-with-anim mfp-hide" style="max-width: 500px">
		<div class="panel panel-danger">
			<div class="panel-heading">
				<span class="panel-icon"><i class="fa fa-trash"></i>
				</span>
				<span class="panel-title"> Hapus Luaran</span>
			</div>
			<form class="form-horizontal" method="post">
				<div class="panel-body">
					<div class="alert alert-danger light alert-dismissable" id="alert-danger-hps-luaran"></div>
					<input type="hidden" name="luaran_hapus" id="luaran_hapus">
					<input type="hidden" name="jenis_luaran_hapus" id="jenis_luaran_hapus">
					<p>Apakah Anda yakin akan menghapus data luaran ini?</p>
				</div>
				<div class="panel-footer text-right">
					<button class="btn btn-danger btn-sm" type="button" id="btn-hapus-luaran">Hapus</button>
				</div>
			</form>
		</div>
	</div>
</section>

<?php echo $footer ?>

<script>
    jQuery(document).ready(function() {
		$('#luaran').dataTable();

		$("#kegiatan").on("change", function() {
			if($(this).val() == "penelitian") {
				let luaran = "";

				<?php foreach($luaran_pn as $row) { ?>
					luaran += "<option value='<?= $row->id ?>'><?= $row->luaran ?></option>";
				<?php } ?>

				$("#luaran-group").removeClass("hidden");
				$("#luaran-input").html('<option>-- Luaran --</option>'+luaran);
			} else if($(this).val() == "pengabdian") {
				let luaran = "";

				<?php foreach($luaran_pg as $row) { ?>
					luaran += "<option value='<?= $row->id ?>'><?= $row->luaran ?></option>";
				<?php } ?>

				$("#luaran-group").removeClass("hidden");
				$("#luaran-input").html('<option>-- Luaran --</option>'+luaran);
			} else {
				$("#luaran-group").addClass("hidden");
				$("#luaran-input").html('');
			}
		});

		$("#filter_luaran").on("click", function(e) {
			e.preventDefault();
			let kegiatan = $("#kegiatan").val();
			let luaran = $("#luaran-input").val();
			let periode = $("#periode").val();

			$.ajax({
				url: "<?php echo site_url('luaran/show_filter_luaran_') ?>"+kegiatan+"/"+luaran,
				dataType: "JSON",
				success: function(data) {
					$('#luaran').dataTable().fnDestroy();
					$("#luaran").html(data.output);
					$('#luaran').DataTable({
						"ajax": {
							url: "<?php echo site_url('luaran/get_data_luaran/') ?>"+luaran+"/semua/"+periode,
							type: 'GET'
						},
						responsive: true
					});
				},

				error: function (jqXHR, textStatus, errorThrown) {
					$("#luaran").html('');
				}
			});
		});

		$('.detail-luaran').on('click', function(e) {
			let kegiatan = $("#kegiatan").val();
			let luaran = $("#luaran-input").val();
			
			$('.detail-luaran').find('button').removeClass('active-animation');
			$(this).addClass('active-animation item-checked');

			$.magnificPopup.open({
				removalDelay: 500,
				items: {
					src: "#modalDetailLuaran"
				},
				callbacks: {
					beforeOpen: function(e) {
						var Animation = $(".detail-luaran").attr('data-effect');
						this.st.mainClass = Animation;
					}
				},
				midClick: true
			});
			
			$.ajax({
				url: "<?php echo site_url('luaran/detail_luaran_') ?>"+kegiatan+"/"+luaran,
				dataType: "JSON",
				success: function(data) {
					$("#judul-luaran").text(kegiatan+" "+luaran);
				},

				error: function (jqXHR, textStatus, errorThrown) {}
			});
		});

		$("#btn-edit-luaran").on("click", function() {
			let id = $("#id_luaran").val();

			$.ajax({
				url: "<?php echo site_url('luaran/edit_luaran/') ?>"+id,
				type: "POST",
				dataType: "JSON",
				data: $("#form_edit_luaran").serialize(),
				success: function(data) {
					if(data.status == true) {
						$("#form_edit_luaran")[0].reset();
						$("#modalTambahLuaran").magnificPopup("close");
						$("#alert-success").text(data.message);
						$('#alert-success').fadeToggle();
						$("#alert-success").delay(4000).fadeOut();

						$('#data_luaran').dataTable().fnDestroy();
						$('#data_luaran').DataTable({
							"ajax": {
								url: "<?php echo site_url('luaran/get_data_luaran/') ?>"+data.jenis,
								type: 'GET'
							}
						});
					} else if(data.status == false) {
						$("#alert-danger-edit-luaran").text(data.message);
						$('#alert-danger-edit-luaran').fadeToggle();
						$("#alert-danger-edit-luaran").delay(4000).fadeOut();
					}
				}
			});
		});

		$("#btn-hapus-luaran").on("click", function() {
			let luaran = $("#luaran_hapus").val();
			$.ajax({
				url: "<?php echo site_url('luaran/hapus_luaran/') ?>"+luaran,
				type: "POST",
				dataType: "JSON",
				success: function(data) {
					if(data.status == false) {
						$("#alert-danger-hps-luaran").text(data.message);
						$('#alert-danger-hps-luaran').fadeToggle();
                		$("#alert-danger-hps-luaran").delay(4000).fadeOut();
					} else if(data.status == true) {
						$("#modalHapusLuaran").magnificPopup("close");
						$("#alert-success").text(data.message);
						$('#alert-success').fadeToggle();
						$("#alert-success").delay(4000).fadeOut();

						$('#data_luaran').dataTable().fnDestroy();
						$('#data_luaran').DataTable({
							"ajax": {
								url: "<?php echo site_url('luaran/get_data_luaran/') ?>"+data.jenis,
								type: 'GET'
							}
						});
					}
				}
			});
		});
	});

	function detailLuaran(val) {
		$.ajax({
			url: "<?php echo site_url('luaran/get_detail_luaran/') ?>"+val,
			dataType: "JSON",
			success: function(data) {
				$("#luaran-detail").html(data.output);
				$.magnificPopup.open({
					removalDelay: 500,
					items: {
						src: "#modalDetailLuaran"
					},
					callbacks: {
						beforeOpen: function(e) {
							var Animation = "mfp-flipInX";
							this.st.mainClass = Animation;
						}
					},
					midClick: true
				});
			},

			error: function (jqXHR, textStatus, errorThrown) {
				alert("Gagal mengambil data");
			}
		});
	}

	function editLuaran(val) {
		$.magnificPopup.open({
			removalDelay: 500,
			items: {
				src: "#modalEditLuaran"
			},
			callbacks: {
				beforeOpen: function(e) {
					var Animation = "mfp-flipInX";
					this.st.mainClass = Animation;
				}
			},
			midClick: true
		});

		$.ajax({
			url: "<?php echo site_url('luaran/get_luaran_edit/') ?>"+val,
			dataType: "JSON",
			success: function(data) {
				$("#edit_luaran_isian").html(data.output);
				$("#id_luaran").val(val);
			}
		});
	}

	function hapusLuaran(val) {
		$.magnificPopup.open({
			removalDelay: 500,
			items: {
				src: "#modalHapusLuaran"
			},
			callbacks: {
				beforeOpen: function(e) {
					var Animation = "mfp-flipInX";
					this.st.mainClass = Animation;
				}
			},
			midClick: true
		});

		$("#luaran_hapus").val(val);
	}
</script>