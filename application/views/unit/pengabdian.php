<?php echo $header ?>

<style>
	#alert-success, #alert-danger { display: none; }
</style>

<header id="topbar">
	<div class="topbar-left">
		<ol class="breadcrumb">
			<li class="crumb-active">
				<a href="#">Data Pengabdian</a>
			</li>
			<li class="crumb-trail">
				<span class="glyphicon glyphicon-home"></span>
			</li>
		</ol>
	</div>
	<div class="topbar-right">
		<div class="ml15 ib va-m" id="toggle_sidemenu_r">
			<a href="#" class="pl5"><i class="fa fa-filter fs20 text-primary"></i></a>
		</div>
	</div>
</header>
<section id="content" class="animated fadeIn">
	<div class="row">
		<div class="col-md-12">
            <a class="btn btn-primary" href="<?php echo site_url('pengabdian/tambah') ?>">
                <i class="fa fa-plus"></i> Tambah Data
            </a>
			<div class="alert alert-success light alert-dismissable mt10" id="alert-success"></div>
			<div class="alert alert-danger light alert-dismissable mt10" id="alert-danger"></div>
			<div class="panel panel-default mn mt10">
				<div class="panel-heading">
					<span class="panel-title">Data Pengabdian</span>
				</div>
				<div class="panel-body">
					<div id="tabel_pengabdian">
						<table id="pengabdian" class="table table-striped table-hover table-bordered">
							<thead>
								<tr>
									<th>No</th>
									<th>Periode</th>
									<th width="250">Judul Kegiatan</th>
									<th>Jenis</th>
									<th>Tanggal</th>
									<th>Biaya</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<?php 
									$no=1; 
									foreach($pengabdian->result() as $row) { 
										$config = json_decode($row->config); 
								?>
									<tr>
										<td class="<?= $config->label ?>"><?= $no++ ?></td>
										<td><?= $row->periode ?></td>
										<td><?= $row->judul ?></td>
										<td><?= strtoupper(substr($row->jenis, 11)); ?></td>
										<td>
											<?php
												if($row->tgl_mulai_berlaku == $row->tgl_selesai_berlaku) {
													echo $this->AppModel->DateIndo($row->tgl_mulai_berlaku);
												} else {
													echo $this->AppModel->DateIndo($row->tgl_mulai_berlaku)." s/d ".$this->AppModel->DateIndo($row->tgl_selesai_berlaku);
												}
											?>
										</td>
										<td>Rp. <?= ($row->dana == "" || $row->dana == NULL ? "0" : number_format($row->dana)) ?></td>
										<td>
											<div class="btn-group">
												<i class="glyphicon glyphicon-cog dropdown-toggle" data-toggle="dropdown" aria-expanded="false"></i>
												<ul class="dropdown-menu pull-right" role="menu">
													<li>
														<a href="<?php echo site_url('pengabdian/detail/'.$row->id) ?>">Detail</a>
													</li>
													<li>
														<a href="<?php echo site_url('pengabdian/edit/'.$row->id) ?>">Edit</a>
													</li>
													<li>
														<a href="#" class="hapus-data" data-effect="mfp-flipInX" data-id="<?= $row->id ?>">Hapus</a>
													</li>
												</ul>
											</div>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<aside id="sidebar_right" class="nano">
	<div class="sidebar_right_content nano-content">
		<div class="tab-block sidebar-block br-n" style="height: 90%">
			<div class="tab-content br-n" style="height: 90%">
				<div id="sidebar-right-tab1" class="tab-pane active" style="height: 90%">
					<h5 class="title-divider text-muted mb20"> Filter Data</h5>
					<form method="post" id="filter_pengabdian">
						<div class="form-body">
							<div class="form-group">
								<label for="" class="control-label mb10">Pilihan Waktu</label>
								<div class="radio-custom mb5">
									<input type="radio" id="periode" name="pilihan_waktu" value="periode" onclick="pilihanWaktu(this.value)">
									<label for="periode">Periode</label>
									<input type="radio" id="tanggal" name="pilihan_waktu" value="tanggal" onclick="pilihanWaktu(this.value)">
                                    <label for="tanggal">Tanggal</label>
								</div>
							</div>
							<div class="form-group hidden" id="input-periode">
								<label for="" class="control-label">Periode</label>
								<select class="form-control" name="periode" id="periode_val">
									<option value="">-- Pilih Periode --</option>
									<?php foreach($periode as $row) { ?> 
										<option value="<?= $row->id ?>"><?= $row->nama ?></option>	
									<?php } ?>
								</select>
							</div>
							<div class="form-group hidden" id="input-tanggal">
								<label for="" class="control-label">Tanggal Pelaksanaan</label>
								<input type="text" class="form-control pull-right" name="tanggal" id="tanggal_pelaksanaan">
							</div>
							<div class="form-group">
								<label for="" class="control-label mt15">Jenis Pengabdian</label>
								<select class="form-control" name="jenis_pengabdian" id="jenis_pengabdian" onchange="getChildPengabdian(this.value)">
									<option value="semua">-- Semua Jenis --</option>
									<?php foreach($jenis_pg as $row) { ?>
										<option value="<?= $row->id ?>"><?= $row->value ?></option>
									<?php } ?>
								</select>
							</div>
							<div id="skim"></div>
							<div class="form-group">
								<label for="" class="control-label">Status Pengabdian</label>
								<select class="form-control" name="status_pengabdian" id="status_pengabdian">
									<option value="semua">-- Semua Status --</option>
									<?php foreach($tahapan as $row) { ?> 
										<option value="<?= $row->id ?>"><?= $row->tahapan ?></option>
									<?php } ?>
								</select>
							</div>
							<div class="form-group">
								<label class="control-label">Program Studi</label>
								<br>
								<select id="prodi" multiple="multiple" id="prodi[]">
									<?php foreach($prodi as $row) { ?> 
										<option value="<?= $row->Kode_Prodi ?>"><?= $row->Nama_Prodi ?></option>
									<?php } ?>
								</select>
							</div>
							<div class="form-group">
								<div class="checkbox-custom mt20">
									<input type="checkbox" name="mhs" id="checkboxDefault3" value="mhs">
									<label for="checkboxDefault3">Mahasiswa Terlibat</label>
								</div>
							</div>
							<div class="form-group">
								<button class="btn btn-primary" type="submit">Filter</button>
							</div>
						</div>
					</form>	
				</div>
			</div>
			<!-- end: .tab-content -->
		</div>
	</div>
</aside>

<div id="modalHapusData" class="popup-basic bg-none mfp-with-anim mfp-hide">
	<div class="panel panel-danger">
		<div class="panel-heading">
			<span class="panel-icon"><i class="fa fa-plus"></i>
			</span>
			<span class="panel-title"> Hapus Data</span>
		</div>
		<form class="form-horizontal" id="formHapusPengabdian" action="" method="post">
			<div class="panel-body">
				<p>Apakah anda yakin akan menghapus data ini?</p>
			</div>
			<div class="panel-footer text-right">
				<button class="btn btn-danger btn-sm" type="submit">Hapus</button>
				<button class="btn btn-default btn-sm tutup" type="button">Batal</button>
			</div>
		</form>
	</div>
</div>

<?php echo $footer ?>

<script>
	jQuery(document).ready(function() {
		<?php
			$stts_input = $this->session->flashdata("status");
			$msg_input = $this->session->flashdata("message");
			
			if($stts_input == "sukses") { ?>
				$("#alert-success").text("<?= $msg_input ?>");
				$('#alert-success').fadeToggle();
				$("#alert-success").delay(4000).fadeOut();
		<?php } else if($stts_input == "gagal") { ?>
			$("#alert-danger").text("<?= $msg_input ?>");
			$('#alert-danger').fadeToggle();
			$("#alert-danger").delay(4000).fadeOut();
		<?php } ?>

		$('.hapus-data').on('click', function() {
			$('.hapus-data').find('a').removeClass('active-animation');
			$(this).addClass('active-animation item-checked');

			$.magnificPopup.open({
				removalDelay: 500,
				items: {
					src: "#modalHapusData"
				},
				callbacks: {
					beforeOpen: function(e) {
						var Animation = $(".hapus-data").attr('data-effect');
						this.st.mainClass = Animation;
					}
				},
				midClick: true
			});

			let id = $(this).attr('data-id');
			$("#formHapusPengabdian").attr("action", "<?php echo site_url('pengabdian/hapus/') ?>"+id);
		});

		$('#jenis_pen').multiselect();
		$('#tanggal_pelaksanaan').daterangepicker();
		$('#prodi').multiselect({
			includeSelectAllOption: true
		});

		$('#pengabdian').dataTable({
            dom: 'Bfrtip',
			buttons: [
				{ extend: 'excel', className: 'btn btn-primary mb20' },
				{ extend: 'pdf', className: 'btn btn-primary mb20', exportOptions: { columns: [0,1,2,3,4,5] } },
				{ extend: 'print', className: 'btn btn-primary mb20' }
			]
		});

		$('#filter_pengabdian').submit(function(e) {
			e.preventDefault();

			$.ajax({
				url: "<?= site_url('pengabdian/get_filter_pengabdian') ?>",
				type: "POST",
				dataType: "JSON",
				data: $("#filter_pengabdian").serialize(),
				success: function(data) {
					$("#tabel_pengabdian").html(data.output);
					$("#pengabdian").DataTable({
						dom: 'Bfrtip',
						buttons: [
							{ extend: 'excel', className: 'btn btn-primary mb20' },
							{ extend: 'pdf', className: 'btn btn-primary mb20', exportOptions: { columns: [0,1,2,3,4,5] } },
							{ extend: 'print', className: 'btn btn-primary mb20' }
						]
					});
				}
			});
		});
	});

	function pilihanWaktu(val) {
		if(val == "periode") {
			$("#input-periode").removeClass("hidden");
			$("#input-tanggal").addClass("hidden");
		} else if(val == "tanggal") {
			$("#input-periode").addClass("hidden");
			$("#input-tanggal").removeClass("hidden");
		}
	}

	function getChildPenngabdian(val) {
		$.ajax({
			url: "<?php echo site_url('pengabdian/get_child_pengabdian/') ?>"+val,
			dataType: "JSON",
			success: function(data) {
				if(data.status) {
					$("#skim").html(data.output);
				} else {
					$("#skim").html("");
				}
			}
		});
	}
</script>