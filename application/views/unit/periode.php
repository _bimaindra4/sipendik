<?php echo $header ?>

<style>
	#alert-danger, #alert-success {
		display: none;
	}
</style>

<header id="topbar">
	<div class="topbar-left">
		<ol class="breadcrumb">
			<li class="crumb-active">
				<a href="#">Periode</a>
			</li>
			<li class="crumb-trail">
				<span class="glyphicon glyphicon-home"></span>
			</li>
		</ol>
	</div>
</header>
<section id="content" class="animated fadeIn">
    <div class="row">
		<div class="col-md-12">
			<div class="alert alert-success light alert-dismissable" id="alert-success"></div>
			<div class="alert alert-danger light alert-dismissable" id="alert-danger"></div>
			<a class="btn btn-primary" data-effect="mfp-flipInX" id="tambah-periode"><i class="fa fa-plus"></i> Tambah Periode</a>
			<div class="panel panel-default mn mt10">
				<div class="panel-heading">
					<span class="panel-title">Data Periode</span>
				</div>
				<div class="panel-body">
					<table id="periode" width="100%" class="table table-striped table-hover table-bordered table-responsive">
						<thead>
							<tr>
								<th>No</th>
								<th>Periode</th>
								<th>Status Aktif</th>
								<th>Tanggal Mulai</th>
								<th>Tanggal Selesai</th>
								<th width="50"></th>
							</tr>
						</thead>
						<tbody></tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>

<div id="modalTambahPeriode" class="popup-basic bg-none mfp-with-anim mfp-hide" style="max-width: 550px">
	<div class="panel">
		<div class="panel-heading">
			<span class="panel-icon"><i class="fa fa-plus"></i>
			</span>
			<span class="panel-title"> Tambah Periode</span>
		</div>
		<form class="form-horizontal" id="form-tambah-periode" method="post">
			<div class="panel-body">
				<div class="form-group">
					<label for="inputStandard" class="control-label col-md-3">Nama Periode</label>
					<div class="col-md-8">
						<input type="text" name="nama" class="form-control">
					</div>
				</div>
				<div class="form-group">
					<label for="inputStandard" class="control-label col-md-3">Masa Berlaku</label>
					<div class="col-md-8">
						<input type="text" class="form-control" name="masa_berlaku" id="masa_berlaku" autocomplete="off">
					</div>
				</div>
			</div>
			<div class="panel-footer text-right">
				<button class="btn btn-primary btn-sm" id="btn-tambah-periode">Tambah</button>
			</div>
		</form>
	</div>
</div>

<div id="modalEditPeriode" class="popup-basic bg-none mfp-with-anim mfp-hide" style="max-width: 550px">
	<div class="panel">
		<div class="panel-heading">
			<span class="panel-icon"><i class="fa fa-plus"></i>
			</span>
			<span class="panel-title"> Edit Periode</span>
		</div>
		<form class="form-horizontal" id="form-edit-periode" method="post">
			<div class="panel-body">
				<div class="form-group">
					<label for="inputStandard" class="control-label col-md-3">Nama Periode</label>
					<div class="col-md-8">
						<input type="hidden" name="id_periode_ed" id="id_periode_ed" class="form-control">
						<input type="text" name="nama_ed" id="nama_ed" class="form-control">
					</div>
				</div>
				<div class="form-group">
					<label for="inputStandard" class="control-label col-md-3">Masa Berlaku</label>
					<div class="col-md-8">
						<input type="text" class="form-control" name="masa_berlaku_ed" id="masa_berlaku_ed" autocomplete="off">
					</div>
				</div>
			</div>
			<div class="panel-footer text-right">
				<button class="btn btn-primary btn-sm" id="btn-edit-periode">Edit</button>
			</div>
		</form>
	</div>
</div>

<div id="modalHapusPeriode" class="popup-basic bg-none mfp-with-anim mfp-hide" style="max-width: 550px">
	<div class="panel panel-danger">
		<div class="panel-heading">
			<span class="panel-icon"><i class="fa fa-plus"></i>
			</span>
			<span class="panel-title"> Hapus Periode</span>
		</div>
		<form class="form-horizontal" method="post">
			<div class="panel-body">
				<input type="hidden" name="id_periode" id="id_periode">
				<p>Apakah anda yakin akan menghapus data periode?</p>
			</div>
			<div class="panel-footer text-right">
				<button class="btn btn-danger btn-sm" id="btn-hapus-periode">Hapus</button>
			</div>
		</form>
	</div>
</div>

<div id="modalNonaktivPeriode" class="popup-basic bg-none mfp-with-anim mfp-hide" style="max-width: 550px">
	<div class="panel panel-danger">
		<div class="panel-heading">
			<span class="panel-icon"><i class="fa fa-plus"></i>
			</span>
			<span class="panel-title"> Nonaktif Periode</span>
		</div>
		<form class="form-horizontal" method="post">
			<div class="panel-body">
				<input type="hidden" name="id_periode_non" id="id_periode_non">
				<p>Apakah anda yakin akan menonaktifkan periode?</p>
			</div>
			<div class="panel-footer text-right">
				<button class="btn btn-danger btn-sm" id="btn-nonaktiv-periode">Nonaktif</button>
			</div>
		</form>
	</div>
</div>

<div id="modalAktivasiPeriode" class="popup-basic bg-none mfp-with-anim mfp-hide" style="max-width: 550px">
	<div class="panel panel-success">
		<div class="panel-heading">
			<span class="panel-icon"><i class="fa fa-plus"></i>
			</span>
			<span class="panel-title"> Aktiv Periode</span>
		</div>
		<form class="form-horizontal" method="post">
			<div class="panel-body">
				<input type="hidden" name="id_periode_aktiv" id="id_periode_aktiv">
				<p>Apakah anda yakin akan mengaktifkan data periode?</p>
			</div>
			<div class="panel-footer text-right">
				<button class="btn btn-success btn-sm" id="btn-aktiv-periode">Aktifkan</button>
			</div>
		</form>
	</div>
</div>

<?php echo $footer ?>

<script>
	jQuery(document).ready(function() {
		$('#tambah-periode').on('click', function() {
			$('#tambah-periode').find('button').removeClass('active-animation');
			$(this).addClass('active-animation item-checked');

			$.magnificPopup.open({
				removalDelay: 500, //delay removal by X to allow out-animation,
				items: {
					src: "#modalTambahPeriode"
				},
				callbacks: {
					beforeOpen: function(e) {
						var Animation = $("#tambah-periode").attr('data-effect');
						this.st.mainClass = Animation;
					}
				},
				midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
			});

			$('#masa_berlaku').daterangepicker();
		});

		$("#aktivasi-periode").on('click', function(e) {
			$('#aktivasi-periode').find('button').removeClass('active-animation');
			$(this).addClass('active-animation item-checked');

			$.magnificPopup.open({
				removalDelay: 500, //delay removal by X to allow out-animation,
				items: {
					src: "#modalAktivasiPeriode"
				},
				callbacks: {
					beforeOpen: function(e) {
						var Animation = $("#aktivasi-periode").attr('data-effect');
						this.st.mainClass = Animation;
					}
				},
				midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
			});

			$.ajax({
				url: "<?php echo site_url('periode/get_periode_aktiv') ?>",
				dataType: "JSON",
				success: function(data) {
					$("#periode_aktiv").val(data.result);
				}
			});
		});

		$("#btn-tambah-periode").on('click', function(e) {
			e.preventDefault();
			$.ajax({
				url: "<?php echo site_url('periode/tambah_periode') ?>",
				type: "POST",
				dataType: "JSON",
				data: $("#form-tambah-periode").serialize(),
				success: function(data) {
					if(data.status) {
						$("#form-tambah-periode")[0].reset();
						$("#modalTambahPeriode").magnificPopup("close");
						$('#periode').DataTable().ajax.reload();

						$("#alert-success").text(data.message);
						$('#alert-success').fadeToggle();
                		$("#alert-success").delay(4000).fadeOut();
					} else {
						$("#alert-danger").text(data.message);
						$('#alert-danger').fadeToggle();
                		$("#alert-danger").delay(4000).fadeOut();
					}
				}
			});
		});

		$("#btn-edit-periode").on('click', function(e) {
			let id = $("#id_periode_ed").val();

			e.preventDefault();
			$.ajax({
				url: "<?php echo site_url('periode/edit_periode/') ?>"+id,
				type: "POST",
				dataType: "JSON",
				data: $("#form-edit-periode").serialize(),
				success: function(data) {
					if(data.status) {
						$("#form-edit-periode")[0].reset();
						$("#modalEditPeriode").magnificPopup("close");
						$('#periode').DataTable().ajax.reload();

						$("#alert-success").text(data.message);
						$('#alert-success').fadeToggle();
                		$("#alert-success").delay(4000).fadeOut();
					} else {
						$("#alert-danger").text(data.message);
						$('#alert-danger').fadeToggle();
                		$("#alert-danger").delay(4000).fadeOut();
					}
				}
			});
		});

		$("#btn-hapus-periode").on('click', function(e) {
			e.preventDefault();

			let id = $("#id_periode").val();

			$.ajax({
				url: "<?php echo site_url('periode/hapus_periode/') ?>"+id,
				dataType: "JSON",
				success: function(data) {
					if(data.status) {
						$("#modalHapusPeriode").magnificPopup("close");
						$('#periode').DataTable().ajax.reload();

						$("#alert-success").text(data.message);
						$('#alert-success').fadeToggle();
                		$("#alert-success").delay(4000).fadeOut();
					} else {
						$("#alert-danger").text(data.message);
						$('#alert-danger').fadeToggle();
                		$("#alert-danger").delay(4000).fadeOut();
					}
				}
			});
		});

		$("#btn-nonaktiv-periode").on('click', function(e) {
			e.preventDefault();

			let id = $("#id_periode_non").val();

			$.ajax({
				url: "<?php echo site_url('periode/nonaktiv_periode/') ?>"+id,
				dataType: "JSON",
				success: function(data) {
					if(data.status) {
						$("#modalNonaktivPeriode").magnificPopup("close");
						$('#periode').DataTable().ajax.reload();

						$("#alert-success").text(data.message);
						$('#alert-success').fadeToggle();
                		$("#alert-success").delay(4000).fadeOut();
					} else {
						$("#alert-danger").text(data.message);
						$('#alert-danger').fadeToggle();
                		$("#alert-danger").delay(4000).fadeOut();
					}
				}
			});
		});

		$("#btn-nonaktiv-periode").on('click', function(e) {
			e.preventDefault();

			let id = $("#id_periode_non").val();

			$.ajax({
				url: "<?php echo site_url('periode/nonaktif_periode/') ?>"+id,
				dataType: "JSON",
				success: function(data) {
					if(data.status) {
						$("#modalNonaktivPeriode").magnificPopup("close");
						$('#periode').DataTable().ajax.reload();

						$("#alert-success").text(data.message);
						$('#alert-success').fadeToggle();
                		$("#alert-success").delay(4000).fadeOut();
					} else {
						$("#alert-danger").text(data.message);
						$('#alert-danger').fadeToggle();
                		$("#alert-danger").delay(4000).fadeOut();
					}
				}
			});
		});

		$("#btn-aktiv-periode").on('click', function(e) {
			e.preventDefault();

			let id = $("#id_periode_aktiv").val();

			$.ajax({
				url: "<?php echo site_url('periode/aktivasi_periode/') ?>"+id,
				dataType: "JSON",
				success: function(data) {
					if(data.status) {
						$("#modalAktivasiPeriode").magnificPopup("close");
						$('#periode').DataTable().ajax.reload();

						$("#alert-success").text(data.message);
						$('#alert-success').fadeToggle();
						$("#alert-success").delay(4000).fadeOut();
					} else {
						$("#alert-danger").text(data.message);
						$('#alert-danger').fadeToggle();
						$("#alert-danger").delay(4000).fadeOut();
					}
				}
			});
		});

		$('#periode').DataTable({
			"ajax": {
				url: "<?php echo site_url('periode/get_data_periode/') ?>",
				type: 'GET'
			}
		});

		$.magnificPopup.instance._onFocusIn = function(e) {
			if($(e.target).hasId('masa_berlaku')) {
				return true;
			}

			$.magnificPopup.proto._onFocusIn.call(this,e);
		};
	});

	function editPeriode(val) {
		$('.edit-periode').find('button').removeClass('active-animation');
		$(this).addClass('active-animation item-checked');
		
		$.magnificPopup.open({
			removalDelay: 500,
			items: {
				src: "#modalEditPeriode"
			},
			callbacks: {
				beforeOpen: function(e) {
					var Animation = "mfp-flipInX";
					this.st.mainClass = Animation;
				}
			},
			midClick: true
		});

		$.magnificPopup.instance._onFocusIn = function(e) {
			if($(e.target).hasId('masa_berlaku_ed')) {
				return true;
			}

			$.magnificPopup.proto._onFocusIn.call(this,e);
		};

		$.ajax({
			url: "<?php echo site_url('periode/get_detail_periode/') ?>"+val,
			dataType: "JSON",
			success: function(data) {
				$("#id_periode_ed").val(data.id);
				$("#nama_ed").val(data.nama);
				$("#masa_berlaku_ed").val(data.tanggal);
				$('#masa_berlaku_ed').daterangepicker({
					parentEl: "#modalEditPeriode .panel-body",
					"startDate": data.tgl_mulai,
					"endDate": data.tgl_selesai
				});
				
			}
		});
	}

	function hapusPeriode(val) {
		$.magnificPopup.open({
			removalDelay: 500,
			items: {
				src: "#modalHapusPeriode"
			},
			callbacks: {
				beforeOpen: function(e) {
					var Animation = "mfp-flipInX";
					this.st.mainClass = Animation;
				}
			},
			midClick: true
		});

		$("#id_periode").val(val);
	}

	function nonaktivPeriode(val) {
		$.magnificPopup.open({
			removalDelay: 500,
			items: {
				src: "#modalNonaktivPeriode"
			},
			callbacks: {
				beforeOpen: function(e) {
					var Animation = "mfp-flipInX";
					this.st.mainClass = Animation;
				}
			},
			midClick: true
		});

		$("#id_periode_non").val(val);
	}

	function aktivasiPeriode(val) {
		$.magnificPopup.open({
			removalDelay: 500,
			items: {
				src: "#modalAktivasiPeriode"
			},
			callbacks: {
				beforeOpen: function(e) {
					var Animation = "mfp-flipInX";
					this.st.mainClass = Animation;
				}
			},
			midClick: true
		});

		$("#id_periode_aktiv").val(val);
	}
</script>