<?php echo $header ?>

<style>
	.table-bordered > thead > tr > th, 
	.table-bordered > tbody > tr > th, 
	.table-bordered > tfoot > tr > th, 
	.table-bordered > thead > tr > td, 
	.table-bordered > tbody > tr > td, 
	.table-bordered > tfoot > tr > td {
		border: 1px solid #aaa;
	}

	.table-bordered {
		border: 1px solid #aaa;
	}

	#alert-danger { 
		display: none; 
	}
</style>

<header id="topbar">
	<div class="topbar-left">
		<ol class="breadcrumb">
			<li class="crumb-active">
				<a href="#">Edit Penelitian</a>
			</li>
			<li class="crumb-trail">
				<span class="glyphicon glyphicon-home"></span>
			</li>
		</ol>
	</div>
</header>
<section id="content" class="animated fadeIn">
	<div class="row">
		<div class="col-md-12">
            <a class="btn btn-primary" href="<?php echo site_url('penelitian') ?>">
                <i class="fa fa-arrow-left"></i> Kembali
            </a>
			<div class="alert alert-danger light alert-dismissable mt10" id="alert-danger"></div>
			<div class="panel panel-default mn mt10">
				<div class="panel-heading">
					<span class="panel-title">Edit Penelitian</span>
				</div>
				<form class="form-horizontal" method="post" action="<?php echo site_url('penelitian/edit_proses/'.$this->uri->segment(3)) ?>">
					<div class="panel-body">
						<?php
							foreach($prop as $row) {
								echo '
									<div class="form-group">
										<label class="col-lg-3 control-label">'.$row['label'].'</label>
										<div class="col-lg-7">
											'.$row['form'].'
										</div>
									</div>
								';
							}
						?>
						<div class="form-group">
							<label class="col-lg-3 control-label">Program Studi</label>
							<div class="col-lg-8">
								<select id="prodi" multiple="multiple" name="prodi[]">
									<?php foreach($prodi as $row) { ?> 
										<option value="<?= $row->Kode_Prodi ?>"><?= $row->Nama_Prodi ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
					</div>
					<div class="panel-footer">
						<button class="btn btn-primary" type="submit">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>

<?php echo $footer ?>

<script>
	jQuery(document).ready(function() {
		<?php
			$stts_input = $this->session->flashdata("status");
			$msg_input = $this->session->flashdata("message");
			
			if($stts_input == "gagal") { ?>
				$("#alert-danger").text("<?= $msg_input ?>");
				$('#alert-danger').fadeToggle();
				$("#alert-danger").delay(4000).fadeOut();
		<?php } ?>

		$('.status_anggota').multiselect();
		$('#waktu_pelaksanaan').daterangepicker();

		$("#jenis_pen").on("change", function() {
			$("#skimpenelitian").remove();
			let val = $(this).val();
			$.ajax({
				url: "<?php echo site_url('penelitian/get_skim_penelitian/') ?>"+val,
				dataType: "JSON",
				success: function(data) {
					$("#jenis_pen").after(data.output);
				}
			});
		});
		
		$("#bidpen").on("change", function() {
			$("#subbidpen").remove();
			let val = $(this).val();
			$.ajax({
				url: "<?php echo site_url('penelitian/get_subbidang_penelitian/') ?>"+val,
				dataType: "JSON",
				success: function(data) {
					$("#bidpen").after(data.output);
				}
			});
		});

		$("#tujuan_sosnom").on("change", function() {
			$("#subtujuan").remove();
			let val = $(this).val();
			$.ajax({
				url: "<?php echo site_url('penelitian/get_subtujuan_sosial/') ?>"+val,
				dataType: "JSON",
				success: function(data) {
					$("#tujuan_sosnom").after(data.output);
				}
			});
		});

		let prodi = "<?= $pn->prodi ?>";
		let splitProdi = prodi.split(",");

		$('#prodi').multiselect({
			includeSelectAllOption: true
		});
		$("#prodi").val(splitProdi).multiselect("refresh");
	});

	$(document).ready(function() {
		let jenis_pen = $("#jenis_pen").val();
		let bidpen = $("#bidpen").val();
		let tujuan_sosnom = $("#tujuan_sosnom").val();

		$.ajax({
			url: "<?php echo site_url('penelitian/get_skim_penelitian/') ?>"+jenis_pen,
			dataType: "JSON",
			success: function(data) {
				$("#jenis_pen").after(data.output);
				$("#skimpenelitian").val("<?= $pn->id_skim ?>");
			}
		});

		$.ajax({
			url: "<?php echo site_url('penelitian/get_subbidang_penelitian/') ?>"+bidpen,
			dataType: "JSON",
			success: function(data) {
				$("#bidpen").after(data.output);
				$("#subbidpen").val("<?= $pn->subbid ?>");
			}
		});

		$.ajax({
			url: "<?php echo site_url('penelitian/get_subtujuan_sosial/') ?>"+tujuan_sosnom,
			dataType: "JSON",
			success: function(data) {
				$("#tujuan_sosnom").after(data.output);
				$("#subtujuan").val("<?= $pn->subtujuan ?>");
			}
		});
	});
</script>