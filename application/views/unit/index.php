<?php echo $header ?>

<header id="topbar">
	<div class="topbar-left">
		<ol class="breadcrumb">
			<li class="crumb-active">
				<a href="#">Dashboard</a>
			</li>
			<li class="crumb-trail">
				<span class="glyphicon glyphicon-home"></span>
			</li>
		</ol>
	</div>
</header>
<section id="content" class="animated fadeIn">
	<div class="row">
		<div class="col-md-3">
			<div class="col-md-12">
				<div class="panel panel-tile text-primary br-b bw5 br-primary-light of-h mb10">
					<div class="panel-body pl20 p5">
						<div class="icon-bg">
							<i class="fa fa-flask"></i>
						</div>
						<h2 class="mt15 lh15">
							<b><?= $pn->num_rows() ?></b>
						</h2>
						<h5 class="text-muted">Data Penelitian</h5>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="panel panel-tile text-warning br-b bw5 br-warning-light of-h mb10">
					<div class="panel-body pl20 p5">
						<div class="icon-bg">
							<i class="fa fa-leaf"></i>
						</div>
						<h2 class="mt15 lh15">
							<b><?= $pg->num_rows() ?></b>
						</h2>
						<h5 class="text-muted">Data Pengabdian</h5>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-9">
			<div class="panel mb20">
				<div class="panel-heading">
					<span class="panel-title">Permohonan Validasi</span>
				</div>
				<div class="panel-body">
					<table id="validasi" class="table table-striped table-hover table-bordered">
						<thead>
							<tr>
								<th>No</th>
								<th>Judul Kegiatan</th>
								<th>Jenis</th>
								<th>Tanggal</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<?php $no=1; foreach($usulan as $row) { ?>
								<tr>
									<td><?= $no++ ?></td>
									<td><?= $row->judul ?></td>
									<td><?= $row->jenis ?></td>
									<td><?= $this->AppModel->DateIndo(date("Y-m-d", strtotime($row->tgl_pengajuan))) ?></td>
									<td>
										<button data-effect="mfp-flipInX" class="btn btn-info btn-xs detail-usulan" onclick="detailUsulan('<?= $row->id ?>')">Detail</button>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
			<h3 class="mb10">Pengumuman</h3>
			<?php foreach($pengumuman as $row) { ?>
				<div class="panel-group accordion" id="pengumuman<?= $row->id ?>" style="margin-bottom: 0">
					<div class="panel">
						<div class="panel-heading">
							<div class="row">
								<div class="col-md-9">
									<a class="accordion-toggle accordion-icon link-unstyled collapsed" data-toggle="collapse" data-parent="#pengumuman<?= $row->id ?>" href="#isi_pg<?= $row->id ?>">
										<?= $row->judul ?>
									</a>
								</div>
								<div class="col-md-3 text-right"><?= date("d F Y", strtotime($row->Created_Date)) ?></div>
							</div>
						</div>
						<div id="isi_pg<?= $row->id ?>" class="panel-collapse collapse" style="height: auto;">
							<div class="panel-body"><?= $row->isi ?></div>
						</div>
					</div>
				</div>
			<?php } ?>
		</div>
	</div>

	<div id="modalDetailUsulan" class="popup-basic bg-none mfp-with-anim mfp-hide" style="max-width: 550px">
		<div class="panel">
			<div class="panel-heading">
				<span class="panel-title"> Detail Usulan</span>
			</div>
			<form class="form-horizontal" id="form_usulan" method="post">
				<div class="panel-body">
					<div id="data_detail"></div>
					<hr style="margin-bottom: 10px">
					<div class="form-group">
						<label for="inputStandard" class="control-label col-md-3">Status</label>
						<div class="col-md-1"></div>
						<div class="col-md-4">
							<select name="status" class="form-control" onchange="statusForm(this.value)">
								<option value="1">Terima</option>
								<option value="2">Revisi</option>
								<option value="3">Tolak</option>
							</select>
						</div>
					</div>
					<div id="form_revisi"></div>
				</div>
				<div class="panel-footer text-right">
					<button class="btn btn-primary btn-sm">Submit</button>
				</div>
			</form>
		</div>
	</div>
</section>

<?php echo $footer ?>

<script>
    $(document).ready( function () {
        $('#validasi').dataTable({
            "columnDefs": [
                {"orderable": false, "targets": 3}
            ]
		});
    });

	function detailUsulan(val) {
		$.magnificPopup.open({
			removalDelay: 500, //delay removal by X to allow out-animation,
			items: {
				src: "#modalDetailUsulan"
			},
			callbacks: {
				beforeOpen: function(e) {
					var Animation = "mfp-flipInX";
					this.st.mainClass = Animation;
				}
			},
			midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
		});

		$.ajax({
			url: "<?php echo site_url('usulan/get_data_usulan/') ?>"+val,
			dataType: "JSON",
			success: function(data) {
				$("#data_detail").html(data.output);
				$("#form_usulan").attr("action", "<?php echo site_url('usulan/ubah_status_usulan/') ?>"+val);
			}
		});
	}

	function statusForm(val) {
		if(val == 2) {
			$("#form_revisi").html('<div class="form-group">'+
				'<label for="inputStandard" class="control-label col-md-3">Revisi</label>'+
				'<div class="col-md-1"></div>'+
				'<div class="col-md-8">'+
					'<textarea name="revisi" class="form-control"></textarea>'+
				'</div>'+
			'</div>');
		} else {
			$("#form_revisi").html('');
		}
	}
</script>