<?php  echo $header; if($pn != NULL) { ?>

<style>
	th, td {
		border-top: 1px solid #eeeeee !important;
	}

	ul.list-inline > li {
		color: #666 !important;
	}

	#alert-danger-tbh-personil, 
	#alert-danger-hps-personil,
	#alert-danger-tbh-luaran,
	#alert-danger-edit-luaran,
	#alert-danger-hps-luaran, 
	#alert-danger-tbh-revisi,
	#alert-danger-edit-sk,
	#alert-danger-edit-status,
	#alert-danger-dokumen,
	#alert-danger-hps-dokumen,
	#alert-success,
	#alert-success-tbh-penelitian,
	.hilang {
		display: none;
	}
</style>

<?php 
	// EDITABLE CONFIG
	$editable = false;
	if(isset($kontributor)) {
		if($kontributor == "Ketua" || $kontributor == "LPPM") {
			if($pn->status == "Usulan" || $pn->status == "On Going") {
				$editable = true;
			}
		}
	} else {
		if($pn->id_inisiator == $userid) {
			if($pn->status == "Usulan") {
				$editable = true;
			}
		}
	}

	// COLOUR CONFIG
	if($pn->status == "Usulan") {
		$color_txt = "primary";
	} else if($pn->status == "On Going") {
		$color_txt = "warning";
	} else if($pn->status == "Selesai") {
		$color_txt = "success";
	} else if($pn->status == "Gagal") {
		$color_txt = "danger";
	}

	$jenis = strtoupper(substr($pn->jenis, 11));

	// FILES OUTPUT CONFIG
	if($pn->usulan == NULL) { 
		$color_us = "danger";
		$icon_us = "fa-times";
	} else {
		$color_us = "success";
		$icon_us = "fa-check";
	}

	if($pn->surat_tugas == NULL) { 
		$color_st = "danger";
		$icon_st = "fa-times";
	} else {
		$color_st = "success";
		$icon_st = "fa-check";
	}

	if($pn->lpj == NULL) { 
		$color_lpj = "danger";
		$icon_lpj = "fa-times";
	} else {
		$color_lpj = "success";
		$icon_lpj = "fa-check";
	}
?>

<header id="topbar">
	<div class="topbar-left">
		<ol class="breadcrumb">
			<li class="crumb-active">
				<a href="#">Detail Penelitian</a>
			</li>
			<li class="crumb-trail">
				<span class="glyphicon glyphicon-home"></span>
			</li>
		</ol>
	</div>
</header>
<section id="content" class="animated fadeIn">
	<div class="row mb10">
		<div class="col-md-12">
			<a class="btn btn-primary" href="<?php echo site_url('penelitian') ?>">
				<i class="fa fa-arrow-left"></i> Kembali
			</a>
		</div>
	</div>
	<div class="alert alert-success light alert-dismissable" id="alert-success-tbh-penelitian"></div>
	<div class="row">
		<div class="col-md-3">
			<ul class="nav tabs-left mb10" style="width: 100%">
				<li class="active">
					<a href="#informasi" data-toggle="tab">
						<i class="glyphicons glyphicons-list mr10"></i> Informasi
					</a>
				</li>
				<li>
					<a href="#personil" data-toggle="tab">
						<i class="glyphicons glyphicons-user mr10"></i> Personil
					</a>
				</li>
				<li>
					<a href="#luaran" data-toggle="tab">
						<i class="glyphicons glyphicons-cogwheels mr10"></i> Luaran
					</a>
				</li>
				<li>
					<a href="#dokumen" data-toggle="tab">
						<i class="glyphicons glyphicons-file mr10"></i> Dokumen
					</a>
				</li>
				<li>
					<a href="#revisi" data-toggle="tab">
						<i class="glyphicons glyphicons-edit mr10"></i> Revisi
					</a>
				</li>
			</ul>
			<?php if($role == 1) { ?>
				<button class="btn btn-primary btn-block" id="ubah-status" data-effect="mfp-flipInX">UBAH STATUS</button>
				<?php if($editable) { ?>
					<button class="btn btn-danger btn-block" id="tambah-revisi" data-effect="mfp-flipInX">REVISI</button>
				<?php } ?>
			<?php } ?>
		</div>
		<div class="col-md-9">
			<h2 style="margin-top: 0px"><?= $pn->judul ?></h2>
			<ul class="list-inline mb20 fs12" style="color: #aaa">
				<li id="status-pn"><span class="label label-md bg-<?= $color_txt ?>"><?= strtoupper($pn->status) ?></span></li>
				<li><?= $pn->periode ?></li>
				<li>PENELITIAN</li>
				<li><?= $jenis ?></li>
			</ul>
			<div class="alert alert-success light alert-dismissable" id="alert-success"></div>
			<div class="alert alert-danger light alert-dismissable" id="alert-danger-tbh-revisi"></div>
			<div class="tab-content">
				<div id="informasi" class="tab-pane active">
					<div class="row">
						<div class="col-md-7">
							<div class="panel panel-default mn mt10">
								<div class="panel-heading">
									<i class="glyphicons glyphicons-list ml5"></i> 
									<span class="panel-title">Informasi Penelitian</span>
								</div>
								<div class="panel-body">
									<table class="table">
										<tr>
											<th width="200">Jenis Penelitian</th>
											<td><?= $pn->jenis ?></td>
										</tr>
										<?php if($pn->skim != NULL) { ?>
											<tr>
												<th width="200">Skim Penelitian</th>
												<td><?= $pn->skim ?></td>
											</tr>
										<?php } ?>
										<tr>
											<th>Pelaksanaan Penelitian</th>
											<td><?= $this->AppModel->DateIndo($pn->tgl_mulai_berlaku)." - ".$this->AppModel->DateIndo($pn->tgl_selesai_berlaku) ?></td>
										</tr>
										<tr>
											<th>Bidang Penelitian</th>
											<td><?= $pn->bidang_penelitian ?></td>
										</tr>
										<tr>
											<th>Tujuan Sosial Ekonomi</th>
											<td><?= $pn->tujuan_penelitian ?></td>
										</tr>
										<tr>
											<th>Sumber Dana</th>
											<td><?= $pn->sumber_dana ?></td>
										</tr>
										<tr>
											<th>Institusi Sumber Dana</th>
											<td><?= $pn->institusi_sumber_dana ?></td>
										</tr>
										<tr>
											<th>Jumlah Dana</th>
											<td>Rp. <?= number_format(($pn->dana == "" || NULL ? 0 : $pn->dana)) ?></td>
										</tr>
									</table>
								</div>
							</div>
						</div>
						<div class="col-md-5">
							<div class="panel panel-default mn mt10">
								<div class="panel-heading">
									<i class="glyphicons glyphicons-list ml5"></i> 
									<span class="panel-title">Status Penelitian</span>
								</div>
								<div class="panel-body">
									<?php if($editable && $kontributor == "LPPM") { ?>
										<button class="btn btn-sm btn-primary mb15" id="tambah-status" data-effect="mfp-flipInX">
											Tambah Status
										</button>
									<?php } ?>
									<table class="table table-bordered table-hover">
										<thead>
											<tr>
												<th>Periode</th>
												<th>Tahapan</th>
												<th></th>
											</tr>
										</thead>
										<tbody>
											<?php 
												foreach($riset_tahap->result() as $row) { 
													if($row->tahapan == "Usulan") {
														$tahap = "primary";
													} else if($row->tahapan == "On Going") {
														$tahap = "warning";
													} else if($row->tahapan == "Selesai") {
														$tahap = "success";
													} else if($row->tahapan == "Gagal") {
														$tahap = "danger";
													}
											?>
												<tr>
													<td><?= $row->nama ?></td>
													<td>
														<span class="label label-md bg-<?= $tahap ?>">
															<?= strtoupper($row->tahapan) ?>
														</span>
													</td>
													<td>
														<!-- <button class="btn btn-xs btn-warning edit-status" data-id="<?= $row->id ?>" data-effect="mfp-flipInX">
															<i class="glyphicons glyphicons-edit"></i>
														</button> -->
													</td>
												</tr>
											<?php } ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="personil" class="tab-pane">
					<div class="row mb10">
						<div class="col-md-2">
							<div class="panel panel-tile text-center">
								<div class="panel-body bg-default">
									<h1 class="fs35 mbn" id="count-dosen"><?= $person_dosen->num_rows() ?></h1>
									<h6 class="text-black">DOSEN</h6>
								</div>
							</div>
						</div>
						<div class="col-md-2">
							<div class="panel panel-tile text-center">
								<div class="panel-body bg-default">
									<h1 class="fs35 mbn" id="count-mhs"><?= $person_mhs->num_rows() ?></h1>
									<h6 class="text-black">MAHASISWA</h6>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="panel">
								<div class="panel-heading">
									<ul class="nav panel-tabs-border panel-tabs panel-tabs-left">
										<li class="active">
											<a href="#dosen" data-toggle="tab">Dosen</a>
										</li>
										<li>
											<a href="#mhs" data-toggle="tab">Mahasiswa</a>
										</li>
									</ul>
								</div>
								<div class="panel-body">
									<div class="tab-content pn br-n">
										<div id="dosen" class="tab-pane active">
											<?php if($editable) { ?> 
												<button class="btn btn-sm btn-primary mb15 tambah-personil" data-role="dosen" data-effect="mfp-flipInX">
													Tambah Dosen
												</button>
											<?php } ?>
											<table id="p_dosen" class="table table-striped table-hover table-bordered" width="100%">
												<thead>
													<tr>
														<th>No</th>
														<th>Nama</th>
														<th>Prodi</th>
														<th>Status</th>
														<?php if($editable) { ?> 
															<th></th>
														<?php } ?>
													</tr>
												</thead>
												<tbody></tbody>
											</table>
										</div>
										<div id="mhs" class="tab-pane">
											<?php if($editable) { ?> 
												<button class="btn btn-sm btn-primary mb15 tambah-personil" data-role="mhs" data-effect="mfp-flipInX">
													Tambah Mahasiswa
												</button>
											<?php } ?>
											<table id="p_mahasiswa" class="table table-striped table-hover table-bordered" width="100%">
												<thead>
													<tr>
														<th>No</th>
														<th>NRP</th>
														<th>Nama Mahasiswa</th>
														<th>Prodi</th>
														<?php if($editable) { ?> 
															<th></th>
														<?php } ?>
													</tr>
												</thead>
												<tbody></tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="luaran" class="tab-pane">
					<div class="row">
						<div class="col-md-12">
							<select name="luaran_filter" id="luaran_filter_table" class="form-control mt10 mb10" style="width: 30%" onchange="tabelLuaran(this.value)">
								<option value="">-- Luaran --</option>
								<?php foreach($luaran_penelitian as $row) { ?> 
									<option value="<?= $row->id ?>"><?= $row->luaran ?></option>
								<?php } ?>
							</select>
							<div class="panel panel-default mn">
								<div class="panel-heading">
									<i class="glyphicons glyphicons-cogwheels ml5"></i>
									<span class="panel-title">Data Luaran</span>
								</div>
								<div class="panel-body">
									<?php if($editable) { ?> 
										<button class="btn btn-sm btn-primary mb15" id="tambah-luaran" data-effect="mfp-flipInX">Tambah Luaran</button>
									<?php } ?>
									<table id="data_luaran" class="table table-striped table-hover table-bordered">
										<thead>
											<tr>
												<th>Data</th>
											</tr>
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="dokumen" class="tab-pane">
					<div class="row">
						<div class="col-md-12">
							<div class="panel">
								<div class="panel-heading">
									<i class="glyphicons glyphicons-file ml5"></i>
									<span class="panel-title">Dokumen</span>
								</div>
								<div class="panel-body">
									<?php if($editable) { ?> 
										<button class="btn btn-sm btn-primary mb15" id="tambah-dokumen" data-effect="mfp-flipInX">Tambah Dokumen</button>
									<?php } ?>
									<div class="row">
										<div class="col-sm-3 col-md-3">
											<div class="panel panel-tile text-center br-a" id="usulan">
												<div class="panel-heading hidden">
													<span class="panel-title"><i class="fa fa-pencil"></i> Title</span>
												</div>
												<div class="panel-body" style="border: 1px solid #DDD">
													<h1 class="fs60 mbn mtn text-<?= $color_us ?>"><i class="fa <?= $icon_us ?>"></i></h1>
													<h6 class="fs12">PROPOSAL PENELITIAN</h6>
												</div>
												<?php if($pn->usulan == NULL) { ?> 
													<?php if($editable) { ?>
														<div class="panel-footer bg-danger light br-n p12 upload-proposal" style="cursor: pointer">
															<span class="fs11"><i class='fa fa-arrow-up mr5'></i> <b>UPLOAD PROPOSAL</b></span>
														</div>
													<?php } ?>
												<?php } else { ?>
													<div class="row">
														<?php if($editable) { $col = 6; $style = "padding-left: 0px"; ?>
															<div class="col-md-6" style="padding-right: 0px">
																<div class="panel-footer bg-primary light br-n p12 upload-proposal" style="cursor: pointer">
																	<span class="fs11"><b>EDIT</b></span>
																</div>
															</div>
														<?php } else { $col = 12; $style = ""; } ?>
														<div class="col-md-<?= $col ?>" style="<?= $style ?>">
															<div class="panel-footer bg-success light br-n p12" style="cursor: pointer" onclick="downloadFile('<?= $pn->usulan ?>')">
																<span class="fs11"><b>DOWNLOAD</b></span>
															</div>
														</div>
													</div>
												<?php } ?>
											</div>
										</div>
										<div class="col-sm-3 col-md-3">
											<div class="panel panel-tile text-center br-a" id="surat-tugas">
												<div class="panel-heading hidden">
													<span class="panel-title"><i class="fa fa-pencil"></i> Title</span>
												</div>
												<div class="panel-body" style="border: 1px solid #DDD">
													<h1 class="fs60 mbn mtn text-<?= $color_st ?>"><i class="fa <?= $icon_st ?>"></i></h1>
													<h6 class="fs12">SURAT KEPUTUSAN</h6>
												</div>
												<?php if($pn->surat_tugas == NULL) { ?> 
													<?php if($editable) { ?>
														<div class="panel-footer bg-danger light br-n p12 sk-detail" style="cursor: pointer">
															<span class="fs11"><i class='fa fa-arrow-up mr5'></i> <b>UPLOAD SK</b></span>
														</div>
													<?php } ?>
												<?php } else { ?>
													<div class="row">
														<?php if($editable) { $col = 6; $style = "padding-left: 0px"; ?>
															<div class="col-md-6" style="padding-right: 0px">
																<div class="panel-footer bg-primary light br-n p12 sk-detail" style="cursor: pointer">
																	<span class="fs11"><b>EDIT</b></span>
																</div>
															</div>
														<?php } else { $col = 12; $style = ""; } ?>
														<div class="col-md-<?= $col ?>" style="<?= $style ?>">
															<div class="panel-footer bg-success light br-n p12" style="cursor: pointer" onclick="downloadFile('<?= $pn->surat_tugas ?>')">
																<span class="fs11"><b>DOWNLOAD</b></span>
															</div>
														</div>
													</div>
												<?php } ?>
											</div>
										</div>
										<div class="col-sm-3 col-md-3">
											<div class="panel panel-tile text-center br-a" id="lpj">
												<div class="panel-heading hidden">
													<span class="panel-title"><i class="fa fa-pencil"></i> Title</span>
												</div>
												<div class="panel-body" style="border: 1px solid #DDD">
													<h1 class="fs60 mbn mtn text-<?= $color_lpj ?>"><i class="fa <?= $icon_lpj ?>"></i></h1>
													<h6 class="fs12">LAPORAN AKHIR</h6>
												</div>
												<?php if($pn->lpj == NULL) { ?>
													<?php if($editable) { ?> 
														<div class="panel-footer bg-danger light br-n p12 upload-laporan" style="cursor: pointer">
															<span class="fs12">
																<span class="fs11"><i class='fa fa-arrow-up mr5'></i> <b>UPLOAD LAPORAN</b></span>
															</span>
														</div>
													<?php } ?>
												<?php } else { ?> 
													<div class="row">
														<?php if($editable) { $col = 6; $style = "padding-left: 0px"; ?>
															<div class="col-md-6" style="padding-right: 0px">
																<div class="panel-footer bg-primary light br-n p12 upload-laporan" style="cursor: pointer">
																	<span class="fs11"><b>EDIT</b></span>
																</div>
															</div>
														<?php } else { $col = 12; $style = ""; } ?>
														<div class="col-md-<?= $col ?>" style="<?= $style ?>">
															<div class="panel-footer bg-success light br-n p12" style="cursor: pointer" onclick="downloadFile('<?= $pn->lpj ?>')">
																<span class="fs11"><b>DOWNLOAD</b></span>
															</div>
														</div>
													</div>
												<?php } ?>
											</div>
										</div>

										<!-- Additional Document -->
										<div id="additional-document">
											<?php foreach($dokumen->result() as $row) { ?>
												<div class="col-sm-3 col-md-3">
													<div class="panel panel-tile text-center br-a">
														<div class="panel-heading hidden">
															<span class="panel-title"><i class="fa fa-pencil"></i> Title</span>
														</div>
														<div class="panel-body" style="border: 1px solid #DDD">
															<h1 class="fs60 mbn mtn text-success"><i class="fa fa-check"></i></h1>
															<h6 class="fs11"><?= strtoupper($row->nama) ?></h6>
														</div>
														<div class="row">
															<?php if($editable) { ?> 
																<div class="col-md-4" style="padding-right: 0px">
																	<div class="panel-footer bg-primary light br-n p12" style="cursor: pointer" onclick="editDokumen('<?= $row->id ?>')">
																		<span class="fs11"><b><i class="fa fa-edit"></i></b></span>
																	</div>
																</div>
															<?php } ?>
															<?php if($editable) { $col = 4; $style = "padding: 0px"; $text = ""; } else { $col = 12; $style = ""; $text = "DOWNLOAD"; } ?> 
															<div class="col-md-<?= $col ?>" style="<?= $style ?>">
																<div class="panel-footer bg-success light br-n p12" style="cursor: pointer" onclick="downloadFile('<?= $row->file ?>')">
																	<span class="fs11"><b><i class="fa fa-arrow-down"></i> <?= $text ?></b></span>
																</div>
															</div>
															<?php if($editable) { ?> 
																<div class="col-md-4" style="padding-left: 0px">
																	<div class="panel-footer bg-danger light br-n p12" style="cursor: pointer" onclick="hapusDokumen('<?= $row->id ?>')">
																		<span class="fs11"><b><i class="fa fa-times"></i></b></span>
																	</div>
																</div>
															<?php } ?>
														</div>
													</div>
												</div>
											<?php } ?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="revisi" class="tab-pane">
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-default mn">
								<div class="panel-heading">
									<i class="glyphicons glyphicons-edit ml5"></i>
									<span class="panel-title">Histori Revisi</span>
								</div>
								<div class="panel-body">
									<table id="tabel_revisi" width="100%" class="table table-striped table-hover table-bordered">
										<thead>
											<tr>
												<th>No</th>
												<th>Keterangan</th>
												<th>Tanggal</th>
											</tr>
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="modalTambahRevisi" class="popup-basic bg-none mfp-with-anim mfp-hide" style="max-width: 500px">
		<div class="panel panel-danger">
			<div class="panel-heading">
				<span class="panel-icon"><i class="fa fa-plus"></i>
				</span>
				<span class="panel-title"> Tambah Revisi</span>
			</div>
			<form class="form-horizontal" id="form_revisi" method="post">
				<div class="panel-body">
					<div class="form-group">
						<label for="inputStandard" class="control-label col-md-3">Deskripsi</label>
						<div class="col-md-8">
							<textarea name="deskripsi" class="form-control"></textarea>
						</div>
					</div>
				</div>
				<div class="panel-footer text-right">
					<button class="btn btn-danger btn-sm" type="button" id="btn-tambah-revisi">Tambah</button>
				</div>
			</form>
		</div>
	</div>

	<div id="modalTambahPersonil" class="popup-basic bg-none mfp-with-anim mfp-hide" style="max-width: 500px">
		<div class="panel">
			<div class="panel-heading">
				<span class="panel-icon"><i class="fa fa-plus"></i>
				</span>
				<span class="panel-title"> Tambah Personil</span>
			</div>
			<form class="form-horizontal" id="form_personil" method="post">
				<div class="panel-body">
					<div class="alert alert-danger light alert-dismissable" id="alert-danger-tbh-personil"></div>
					<div id="personil_isian">
						<div id="personil_dosen" class="hidden">
							<?php
								foreach($person_dosen_prop as $row) {
									echo '
										<div class="form-group">
											<label class="col-lg-3 control-label">'.$row['label'].'</label>
											<div class="col-lg-8">
												'.$row['form'].'
											</div>
										</div>
									';
								}
							?>
						</div>
						<div id="personil_mhs" class="hidden">
							<?php
								foreach($person_mhs_prop as $row) {
									echo '
										<div class="form-group">
											<label class="col-lg-3 control-label">'.$row['label'].'</label>
											<div class="col-lg-8">
												'.$row['form'].'
											</div>
										</div>
									';
								}
							?>
						</div>
					</div>
				</div>
				<div class="panel-footer text-right">
					<button class="btn btn-primary btn-sm" type="button" id="btn-tambah-personil">Tambah</button>
				</div>
			</form>
		</div>
	</div>

	<div id="modalTambahLuaran" class="popup-basic bg-none mfp-with-anim mfp-hide" style="max-width: 550px">
		<div class="panel">
			<div class="panel-heading">
				<span class="panel-icon"><i class="fa fa-plus"></i>
				</span>
				<span class="panel-title"> Tambah Luaran</span>
			</div>
			<form class="form-horizontal" id="form_luaran" method="post">
				<div class="panel-body">
					<div class="alert alert-danger light alert-dismissable" id="alert-danger-tbh-personil"></div>
					<div class="form-group">
						<label for="inputStandard" class="control-label col-md-3">Luaran</label>
						<div class="col-md-8">
							<select name="luaran_filter" id="luaran_filter" class="form-control" onchange="formLuaran(this.value)">
								<option value="">-- Luaran --</option>
								<?php foreach($luaran_penelitian as $row) { ?> 
									<option value="<?= $row->id ?>"><?= $row->luaran ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div id="luaran_isian"></div>
				</div>
				<div class="panel-footer text-right">
					<button class="btn btn-primary btn-sm hilang" type="submit" id="btn-tambah-luaran">Tambah</button>
				</div>
			</form>
		</div>
	</div>

	<div id="modalEditLuaran" class="popup-basic bg-none mfp-with-anim mfp-hide" style="max-width: 550px">
		<div class="panel">
			<div class="panel-heading">
				<span class="panel-icon"><i class="fa fa-plus"></i>
				</span>
				<span class="panel-title"> Edit Luaran</span>
			</div>
			<form class="form-horizontal" id="form_edit_luaran" method="post" enctype="multipart/form-data">
				<div class="alert alert-danger light alert-dismissable" id="alert-danger-edit-luaran"></div>
				<div class="panel-body">
					<input type="hidden" name="id_luaran" id="id_luaran">
					<div id="edit_luaran_isian"></div>
				</div>
				<div class="panel-footer text-right">
					<button class="btn btn-primary btn-sm" type="submit" id="btn-edit-luaran">Edit</button>
				</div>
			</form>
		</div>
	</div>

	<div id="modalTambahDokumen" class="popup-basic bg-none mfp-with-anim mfp-hide" style="max-width: 550px">
		<div class="panel">
			<div class="panel-heading">
				<span class="panel-icon"><i class="fa fa-plus"></i>
				</span>
				<span class="panel-title"> Tambah Dokumen</span>
			</div>
			<form class="form-horizontal" id="form_dokumen" method="post" enctype="multipart/form-data">
				<div class="panel-body">
					<div class="form-group">
						<label for="inputStandard" class="control-label col-md-3">Nama Dokumen</label>
						<div class="col-md-8">
							<input type="text" name="nama" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<label for="inputStandard" class="control-label col-md-3">Dokumen</label>
						<div class="col-md-8">
							<input type="file" name="dokumen" class="form-control">
						</div>
					</div>
				</div>
				<div class="panel-footer text-right">
					<button class="btn btn-primary btn-sm" type="submit">Tambah</button>
				</div>
			</form>
		</div>
	</div>

	<div id="modalEditDokumen" class="popup-basic bg-none mfp-with-anim mfp-hide" style="max-width: 550px">
		<div class="panel">
			<div class="panel-heading">
				<span class="panel-icon"><i class="fa fa-plus"></i>
				</span>
				<span class="panel-title"> Edit Dokumen</span>
			</div>
			<form class="form-horizontal" id="form_dokumen_edit" method="post">
				<div class="panel-body">
					<div class="form-group">
						<label for="inputStandard" class="control-label col-md-3">Nama Dokumen</label>
						<div class="col-md-8">
							<input type="hidden" name="id_dokumen_ed" id="id_dokumen_ed">
							<input type="text" name="nama_dokumen_ed" id="nama_dokumen_ed" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<label for="inputStandard" class="control-label col-md-3">Dokumen</label>
						<div class="col-md-8">
							<input type="file" name="dokumen_ed" class="form-control">
						</div>
					</div>
				</div>
				<div class="panel-footer text-right">
					<button class="btn btn-primary btn-sm" type="submit">Edit</button>
				</div>
			</form>
		</div>
	</div>

	<div id="modalHapusDokumen" class="popup-basic bg-none mfp-with-anim mfp-hide" style="max-width: 500px">
		<div class="panel panel-danger">
			<div class="panel-heading">
				<span class="panel-icon"><i class="fa fa-trash"></i>
				</span>
				<span class="panel-title"> Hapus Dokumen</span>
			</div>
			<form class="form-horizontal" method="post">
				<div class="panel-body">
					<input type="hidden" name="id_dokumen_hps" id="id_dokumen_hps">
					<p>Apakah Anda yakin akan menghapus data dokumen ini?</p>
				</div>
				<div class="panel-footer text-right">
					<button class="btn btn-danger btn-sm" type="button" id="btn-hapus-dokumen">Hapus</button>
				</div>
			</form>
		</div>
	</div>
	
	<div id="modalSKDetail" class="popup-basic bg-none mfp-with-anim mfp-hide" style="max-width: 900px">
		<div class="panel">
			<div class="panel-heading">
				<span class="panel-icon"><i class="fa fa-plus"></i>
				</span>
				<span class="panel-title"> Surat Keputusan</span>
			</div>
			<form class="form-horizontal" id="form_sk" method="post">
				<div class="panel-body">
					<div class="alert alert-danger light alert-dismissable" id="alert-danger-edit-sk"></div>
					<div class="form-group">
						<label for="inputStandard" class="control-label col-md-3">No SK</label>
						<div class="col-md-8">
							<input type="hidden" name="id_sk_riset">
							<input type="hidden" name="jenis_keg" id="jenis_keg" value="penelitian">
							<input type="text" name="no_sk" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<label for="inputStandard" class="control-label col-md-3">Tentang</label>
						<div class="col-md-8">
							<textarea name="tentang" class="form-control"></textarea>
						</div>
					</div>
					<div class="form-group">
						<label for="inputStandard" class="control-label col-md-3">Tanggal Ditetapkan</label>
						<div class="col-md-8">
							<input type="date" name="tgl_ditetapkan" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<label for="inputStandard" class="control-label col-md-3">Masa Berlaku</label>
						<div class="col-md-8">
							<input type="text" class="form-control" name="masa_berlaku" id="masa_berlaku" autocomplete="off">
						</div>
					</div>
					<div class="form-group">
						<label for="inputStandard" class="control-label col-md-3">Penerbit</label>
						<div class="col-md-8">
							<select name="penerbit" class="form-control">
								<option value="">-- Pilih Penerbit --</option>
								<?php foreach($unit as $row) { ?> 
									<option value="<?= $row->unit_id ?>"><?= $row->nama_unit ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="inputStandard" class="control-label col-md-3">Ditetapkan Oleh</label>
						<div class="col-md-8">
							<select name="pegawai" class="form-control">
								<option value="">-- Pilih Penerbit --</option>
								<?php foreach($pegawai as $row) { ?> 
									<option value="<?= $row->nip ?>"><?= $row->nama ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="inputStandard" class="control-label col-md-3">Upload SK</label>
						<div class="col-md-8">
							<input type="file" name="file" class="form-control">
						</div>
					</div>
				</div>
				<div class="panel-footer text-right">
					<button class="btn btn-primary btn-sm" type="submit">Tambah</button>
				</div>
			</form>
		</div>
	</div>

	<div id="modalHapusPerson" class="popup-basic bg-none mfp-with-anim mfp-hide" style="max-width: 500px">
		<div class="panel panel-danger">
			<div class="panel-heading">
				<span class="panel-icon"><i class="fa fa-trash"></i>
				</span>
				<span class="panel-title"> Hapus Personil</span>
			</div>
			<form class="form-horizontal" method="post">
				<div class="panel-body">
					<div class="alert alert-danger light alert-dismissable" id="alert-danger-hps-personil"></div>
					<input type="hidden" name="person_hapus" id="person_hapus">
					<p>Apakah Anda yakin akan menghapus data personil ini?</p>
				</div>
				<div class="panel-footer text-right">
					<button class="btn btn-danger btn-sm" type="button" id="btn-hapus-personil">Hapus</button>
				</div>
			</form>
		</div>
	</div>

	<div id="modalHapusLuaran" class="popup-basic bg-none mfp-with-anim mfp-hide" style="max-width: 500px">
		<div class="panel panel-danger">
			<div class="panel-heading">
				<span class="panel-icon"><i class="fa fa-trash"></i>
				</span>
				<span class="panel-title"> Hapus Luaran</span>
			</div>
			<form class="form-horizontal" method="post">
				<div class="panel-body">
					<div class="alert alert-danger light alert-dismissable" id="alert-danger-hps-luaran"></div>
					<input type="hidden" name="luaran_hapus" id="luaran_hapus">
					<input type="hidden" name="jenis_luaran_hapus" id="jenis_luaran_hapus">
					<p>Apakah Anda yakin akan menghapus data luaran ini?</p>
				</div>
				<div class="panel-footer text-right">
					<button class="btn btn-danger btn-sm" type="button" id="btn-hapus-luaran">Hapus</button>
				</div>
			</form>
		</div>
	</div>

	<div id="modalDetailLuaran" class="popup-basic bg-none mfp-with-anim mfp-hide" style="max-width: 550px">
		<div class="panel">
			<div class="panel-heading">
				<span class="panel-title"> Detail Luaran</span>
			</div>
			<div class="panel-body" id="luaran-detail"></div>
		</div>
	</div>

	<div id="modalTambahStatus" class="popup-basic bg-none mfp-with-anim mfp-hide" style="max-width: 550px">
		<div class="panel">
			<div class="panel-heading">
				<span class="panel-title"> Tambah Status</span>
			</div>
			<form class="form-horizontal" method="post" id="form-tambah-status">
				<div class="panel-body">
					<div class="alert alert-danger light alert-dismissable" id="alert-danger-edit-status"></div>
					<div class="form-group">
						<label for="inputStandard" class="control-label col-md-3">Periode</label>
						<div class="col-md-8">
							<select name="periode" class="form-control">
								<?php foreach($periode_after as $row) { ?>
									<option value="<?= $row->id ?>"><?= $row->nama ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="inputStandard" class="control-label col-md-3">Ubah Status</label>
						<div class="col-md-8">
							<select name="status" class="form-control">
								<?php foreach($tahapan as $row) { ?> 
									<option value="<?= $row->id ?>"><?= $row->tahapan ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
				</div>
				<div class="panel-footer text-right">
					<button class="btn btn-primary btn-sm" type="button" id="btn-tambah-status">Tambah</button>
				</div>
			</form>
		</div>
	</div>

	<div id="modalUbahStatus" class="popup-basic bg-none mfp-with-anim mfp-hide" style="max-width: 550px">
		<div class="panel">
			<div class="panel-heading">
				<span class="panel-title"> Ubah Status Penelitian</span>
			</div>
			<form class="form-horizontal" method="post" id="form-ubah-status">
				<div class="panel-body">
					<div class="alert alert-danger light alert-dismissable" id="alert-danger-edit-status"></div>
					<div class="form-group">
						<label for="inputStandard" class="control-label col-md-3">Periode</label>
						<div class="col-md-8">
							<input type="hidden" id="id_tahap" value="<?= $this->uri->segment(3) ?>">
							<select name="periode" class="form-control">
								<?php foreach($periode_riset as $row) { ?>
									<option value="<?= $row->id ?>"><?= $row->nama ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="inputStandard" class="control-label col-md-3">Ubah Status</label>
						<div class="col-md-8">
							<select name="status" class="form-control">
								<?php foreach($tahapan as $row) { ?> 
									<option value="<?= $row->id ?>"><?= $row->tahapan ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
				</div>
				<div class="panel-footer text-right">
					<button class="btn btn-primary btn-sm" type="button" id="btn-ubah-status">Ubah</button>
				</div>
			</form>
		</div>
	</div>

	<div id="modalUploadFileLPJ" class="popup-basic bg-none mfp-with-anim mfp-hide" style="max-width: 550px">
		<div class="panel">
			<div class="panel-heading">
				<span class="panel-title" id="judul-upload-file"></span>
			</div>
			<form class="form-horizontal" method="post" id="form_upload_file_lpj">
				<div class="panel-body">
					<div class="form-group">
						<label for="inputStandard" class="control-label col-md-3">Upload File</label>
						<div class="col-md-8">
							<input type="hidden" name="jenis_file" id="jenis_file">
							<input type="hidden" name="kategori" id="kategori" value="penelitian">
							<input type="file" name="file" class="form-control">
						</div>
					</div>
				</div>
				<div class="panel-footer text-right">
					<button class="btn btn-primary btn-sm" type="submit">Upload</button>
				</div>
			</form>
		</div>
	</div>

	<div id="modalUploadFileProposal" class="popup-basic bg-none mfp-with-anim mfp-hide" style="max-width: 550px">
		<div class="panel">
			<div class="panel-heading">
				<span class="panel-title">Upload File Proposal Penelitian</span>
			</div>
			<form class="form-horizontal" method="post" id="form_upload_file_proposal">
				<div class="panel-body">
					<div class="form-group">
						<label for="inputStandard" class="control-label col-md-3">Upload File</label>
						<div class="col-md-8">
							<input type="hidden" name="kategori" id="kategori_prop" value="penelitian">
							<input type="file" name="file" class="form-control">
						</div>
					</div>
				</div>
				<div class="panel-footer text-right">
					<button class="btn btn-primary btn-sm" type="submit">Upload</button>
				</div>
			</form>
		</div>
	</div>
</section>

<?php } echo $footer ?>

<script>
	jQuery(document).ready(function() {
		<?php
			$stts_input = $this->session->flashdata("status");
			$msg_input = $this->session->flashdata("message");
			
			if($stts_input == "sukses") { ?>
				$("#alert-success-tbh-penelitian").text("<?= $msg_input ?>");
				$('#alert-success-tbh-penelitian').fadeToggle();
				$("#alert-success-tbh-penelitian").delay(4000).fadeOut();
		<?php } ?>
		
		$('#tambah-revisi').on('click', function() {
			$('#tambah-revisi').find('button').removeClass('active-animation');
			$(this).addClass('active-animation item-checked');

			$.magnificPopup.open({
				removalDelay: 500, //delay removal by X to allow out-animation,
				items: {
					src: "#modalTambahRevisi"
				},
				callbacks: {
					beforeOpen: function(e) {
						var Animation = $("#tambah-revisi").attr('data-effect');
						this.st.mainClass = Animation;
					}
				},
				midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
			});
		});

		$('.tambah-personil').on('click', function() {
			$('.tambah-personil').find('button').removeClass('active-animation');
			$(this).addClass('active-animation item-checked');

			$.magnificPopup.open({
				removalDelay: 500, //delay removal by X to allow out-animation,
				items: {
					src: "#modalTambahPersonil"
				},
				callbacks: {
					beforeOpen: function(e) {
						var Animation = $(".tambah-personil").attr('data-effect');
						this.st.mainClass = Animation;
					}
				},
				midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
			});

			reset_form_personil();
			let role = $(this).attr("data-role");
			$("#personil_"+role).removeClass("hidden");
			$("#btn-tambah-personil").attr('data-ref', role);

			if(role == "dosen") {
				$("#modalTambahPersonil").find(".panel-title").text("Tambah Dosen");
			} else if(role == "mhs") {
				$("#modalTambahPersonil").find(".panel-title").text("Tambah Mahasiswa");
			}
		});

		$('#tambah-luaran').on('click', function() {
			$('#tambah-luaran').find('button').removeClass('active-animation');
			$(this).addClass('active-animation item-checked');

			$.magnificPopup.open({
				removalDelay: 500, //delay removal by X to allow out-animation,
				items: {
					src: "#modalTambahLuaran"
				},
				callbacks: {
					beforeOpen: function(e) {
						var Animation = $("#tambah-luaran").attr('data-effect');
						this.st.mainClass = Animation;
					}
				},
				midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
			});
		});

		$("#tambah-dokumen").on('click', function() {
			$('#tambah-dokumen').find('button').removeClass('active-animation');
			$(this).addClass('active-animation item-checked');

			$.magnificPopup.open({
				removalDelay: 500, //delay removal by X to allow out-animation,
				items: {
					src: "#modalTambahDokumen"
				},
				callbacks: {
					beforeOpen: function(e) {
						var Animation = $("#tambah-dokumen").attr('data-effect');
						this.st.mainClass = Animation;
					}
				},
				midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
			});
		});

		$("#ubah-status").on('click', function() {
			$('#ubah-status').find('button').removeClass('active-animation');
			$(this).addClass('active-animation item-checked');

			$.magnificPopup.open({
				removalDelay: 500, //delay removal by X to allow out-animation,
				items: {
					src: "#modalUbahStatus"
				},
				callbacks: {
					beforeOpen: function(e) {
						var Animation = $("#ubah-status").attr('data-effect');
						this.st.mainClass = Animation;
					}
				},
				midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
			});
		});

		$("#tambah-status").on('click', function() {
			$('#tambah-status').find('button').removeClass('active-animation');
			$(this).addClass('active-animation item-checked');

			$.magnificPopup.open({
				removalDelay: 500, //delay removal by X to allow out-animation,
				items: {
					src: "#modalTambahStatus"
				},
				callbacks: {
					beforeOpen: function(e) {
						var Animation = $("#tambah-status").attr('data-effect');
						this.st.mainClass = Animation;
					}
				},
				midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
			});
		});

		$(".upload-proposal").on("click", function() {
			$('.upload-proposal').find('button').removeClass('active-animation');
			$(this).addClass('active-animation item-checked');

			$.magnificPopup.open({
				removalDelay: 500, //delay removal by X to allow out-animation,
				items: {
					src: "#modalUploadFileProposal"
				},
				callbacks: {
					beforeOpen: function(e) {
						var Animation = "mfp-flipInX";
						this.st.mainClass = Animation;
					}
				},
				midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
			});
		});

		$(".sk-detail").on("click", function() {
			$('.sk-detail').find('button').removeClass('active-animation');
			$(this).addClass('active-animation item-checked');

			$.magnificPopup.open({
				removalDelay: 500, //delay removal by X to allow out-animation,
				items: {
					src: "#modalSKDetail"
				},
				callbacks: {
					beforeOpen: function(e) {
						var Animation = "mfp-flipInX";
						this.st.mainClass = Animation;
					}
				},
				midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
			});

			$.magnificPopup.instance._onFocusIn = function(e) {
				if($(e.target).hasId('masa_berlaku')) {
					return true;
				}

				$.magnificPopup.proto._onFocusIn.call(this,e);
			};

			$.ajax({
				url: "<?php echo site_url('penelitian/get_data_sk/'.$this->uri->segment(3)) ?>",
				dataType: "JSON",
				success: function(data) {
					$("[name=id_sk_riset]").val(data.id);
					$("[name=no_sk]").val(data.no_sk);
					if(data.tentang != null) {
						$("[name=tentang]").text(data.tentang);
					}
					$("[name=tgl_ditetapkan]").val(data.tgl_ditetapkan);
					$("[name=masa_berlaku]").val(data.tanggal);
					$('#masa_berlaku').daterangepicker({
						parentEl: "#modalSKDetail .panel-body",
						"startDate": data.tgl_mulai_berlaku,
						"endDate": data.tgl_selesai_berlaku
					});

					if(data.penerbit != null) {
						$("[name=penerbit]").val(data.penerbit);
					}

					if(data.pejabat_ttd != null) {
						$("[name=pegawai]").val(data.pejabat_ttd);
					}
				},
				error: function (jqXHR, textStatus, errorThrown) {
					alert("Gagal mengambil data");
				}
			});
		});

		$(".upload-laporan").on("click", function() {
			$('.upload-laporan').find('button').removeClass('active-animation');
			$(this).addClass('active-animation item-checked');

			$.magnificPopup.open({
				removalDelay: 500, //delay removal by X to allow out-animation,
				items: {
					src: "#modalUploadFileLPJ"
				},
				callbacks: {
					beforeOpen: function(e) {
						var Animation = "mfp-flipInX";
						this.st.mainClass = Animation;
					}
				},
				midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
			});

			$("#judul-upload-file").text("Upload File Laporan Akhir");
			$("#jenis_file").val("lpj");
		});

		$("#nip").on("keyup", function(e) {
			if(e.key == "Enter" || e.keyCode == "13") {
				let nip = $(this).val();
				$.ajax({
					url: "<?php echo site_url('dosen/get_data_dosen/') ?>"+nip,
					dataType: "JSON",
					success: function(data) {
						$("#nama_dosen").val(data.nama);
						$("#prodi_dosen").val(data.prodi)
					},
					error: function (jqXHR, textStatus, errorThrown) {
						alert("Gagal mengambil data");
					}
				});
			}
		});

		$("#nrp").on("keyup", function(e) {
			if(e.key == "Enter" || e.keyCode == "13") {
				let nrp = $(this).val();
				$.ajax({
					url: "<?php echo site_url('mahasiswa/get_data_mahasiswa/') ?>"+nrp,
					dataType: "JSON",
					success: function(data) {
						$("#nama_mhs").val(data.Nama_Mhs);
						$("#prodi_mhs").val(data.prodi)
					},
					error: function (jqXHR, textStatus, errorThrown) {
						alert("Gagal mengambil data");
					}
				});
			}
		});

		$("#btn-tambah-personil").on('click', function() {
			let ref = $("#btn-tambah-personil").attr("data-ref");
			let url = "";
			if(ref == "dosen") {
				url = "<?php echo site_url('penelitian/tambah_personil_dosen/'.$this->uri->segment(3)) ?>";
			} else if(ref == "mhs") {
				url = "<?php echo site_url('penelitian/tambah_personil_mhs/'.$this->uri->segment(3)) ?>";
			}

			$.ajax({
				url: url,
				type: "POST",
				dataType: "JSON",
				data: $("#form_personil").serialize(),
				success: function(data) {
					if(data.status == false) {
						$("#alert-danger-tbh-personil").text(data.message);
						$('#alert-danger-tbh-personil').fadeToggle();
                		$("#alert-danger-tbh-personil").delay(4000).fadeOut();
					} else if(data.status == true) {
						$("#form_personil")[0].reset();
						$("#modalTambahPersonil").magnificPopup("close");
						$("#alert-success").text(data.message);
						$('#alert-success').fadeToggle();
                		$("#alert-success").delay(4000).fadeOut();

						if(ref == "dosen") {
							$('#p_dosen').DataTable().ajax.reload();
							$("#count-dosen").text(data.person_count);
						} else if(ref == "mhs") {
							$('#p_mahasiswa').DataTable().ajax.reload();
							$("#count-mhs").text(data.person_count);
						}
					}
				}
			});
		});

		$("#btn-hapus-personil").on("click", function() {
			let person = $("#person_hapus").val();
			$.ajax({
				url: "<?php echo site_url('penelitian/hapus_personil/'.$this->uri->segment(3).'/') ?>"+person,
				type: "POST",
				dataType: "JSON",
				data: $("#form_personil").serialize(),
				success: function(data) {
					if(data.status == false) {
						$("#alert-danger-hps-personil").text(data.message);
						$('#alert-danger-hps-personil').fadeToggle();
                		$("#alert-danger-hps-personil").delay(4000).fadeOut();
					} else if(data.status == true) {
						$("#form_personil")[0].reset();
						$("#modalHapusPerson").magnificPopup("close");
						$("#alert-success").text(data.message);
						$('#alert-success').fadeToggle();
                		$("#alert-success").delay(4000).fadeOut();

						if(data.person_ref == "Pegawai") {
							$('#p_dosen').DataTable().ajax.reload();
							$("#count-dosen").text(data.person_count);
						} else if(data.person_ref == "Mahasiswa") {
							$('#p_mahasiswa').DataTable().ajax.reload();
							$("#count-mhs").text(data.person_count);
						}
					}
				}
			});
		});

		$("#btn-hapus-luaran").on("click", function() {
			let luaran = $("#luaran_hapus").val();
			$.ajax({
				url: "<?php echo site_url('luaran/hapus_luaran/') ?>"+luaran,
				type: "POST",
				dataType: "JSON",
				success: function(data) {
					if(data.status == false) {
						$("#alert-danger-hps-luaran").text(data.message);
						$('#alert-danger-hps-luaran').fadeToggle();
                		$("#alert-danger-hps-luaran").delay(4000).fadeOut();
					} else if(data.status == true) {
						$("#modalHapusLuaran").magnificPopup("close");
						$("#alert-success").text(data.message);
						$('#alert-success').fadeToggle();
						$("#alert-success").delay(4000).fadeOut();
						
						$('#data_luaran').dataTable().fnDestroy();
						$('#data_luaran').DataTable({
							"ajax": {
								url: "<?php echo site_url('luaran/get_data_luaran/') ?>"+data.jenis+"/"+<?= $this->uri->segment(3) ?>,
								type: 'GET'
							}
						});
					}
				}
			});
		});

		$("#btn-edit-sk").on("click", function() {
			$.ajax({
				url: "<?php echo site_url('penelitian/edit_sk') ?>",
				type: "POST",
				dataType: "JSON",
				data: $("#form_sk").serialize(),
				success: function(data) {
					if(data.status == false) {
						$("#alert-danger-edit-sk").text(data.message);
						$('#alert-danger-edit-sk').fadeToggle();
                		$("#alert-danger-edit-sk").delay(4000).fadeOut();
					} else if(data.status == true) {
						$("#modalSKDetail").magnificPopup("close");
						$("#alert-success").text(data.message);
						$('#alert-success').fadeToggle();
						$("#alert-success").delay(4000).fadeOut();
					}
				}
			});
		});

		$("#btn-tambah-revisi").on("click", function() {
			$.ajax({
				url: "<?php echo site_url('penelitian/tambah_revisi/'.$this->uri->segment(3)) ?>",
				type: "POST",
				dataType: "JSON",
				data: $("#form_revisi").serialize(),
				success: function(data) {
					if(data.status == true) {
						$("#form_revisi")[0].reset();
						$("#modalTambahRevisi").magnificPopup("close");
						$("#alert-success").text(data.message);
						$('#alert-success').fadeToggle();
                		$("#alert-success").delay(4000).fadeOut();

						$('#tabel_revisi').DataTable().ajax.reload();
					} else if(data.status == false) {
						$("#alert-danger-tbh-revisi").text(data.message);
						$('#alert-danger-tbh-revisi').fadeToggle();
                		$("#alert-danger-tbh-revisi").delay(4000).fadeOut();
					}
				}
			});
		});

		$("#btn-tambah-status").on("click", function() {
			let id = "<?= $this->uri->segment(3) ?>";
			$.ajax({
				url: "<?php echo site_url('penelitian/tambah_status_penelitian/') ?>"+id,
				type: "POST",
				dataType: "JSON",
				data: $("#form-tambah-status").serialize(),
				success: function(data) {
					if(data.status) {
						$("#alert-success").text(data.message);
						$('#alert-success').fadeToggle();
						$("#alert-success").delay(1000).fadeOut();

						$("#modalStatus").magnificPopup("close");

						setTimeout(function () {
							window.location.href = "<?= site_url('penelitian/detail/'.$this->uri->segment(3)) ?>";
						}, 1500);
					} else {
						$("#alert-danger-tambah-status").text(data.message);
						$('#alert-danger-tambah-status').fadeToggle();
						$("#alert-danger-tambah-status").delay(4000).fadeOut();
					}
				}
			});
		});

		$("#btn-ubah-status").on('click', function() {
			let id = $("#id_tahap").val();
			$.ajax({
				url: "<?php echo site_url('penelitian/edit_status_penelitian/') ?>"+id,
				type: "POST",
				dataType: "JSON",
				data: $("#form-ubah-status").serialize(),
				success: function(data) {
					if(data.status) {
						$("#alert-success").text(data.message);
						$('#alert-success').fadeToggle();
						$("#alert-success").delay(1000).fadeOut();

						$("#modalStatus").magnificPopup("close");

						setTimeout(function () {
							window.location.href = "<?php site_url('penelitian/detail/'.$this->uri->segment(3)) ?>";
						}, 1500);
					} else {
						$("#alert-danger-edit-status").text(data.message);
						$('#alert-danger-edit-status').fadeToggle();
						$("#alert-danger-edit-status").delay(4000).fadeOut();
					}
				}
			});
		});

		$("#form_dokumen").submit(function(e) {
			e.preventDefault();

			$.ajax({
				url: "<?= site_url('penelitian/tambah_dokumen/'.$this->uri->segment(3)) ?>",
				dataType: "JSON",
				type: "POST",
				data: new FormData(this),
				processData: false,
				contentType: false,
				cache: false,
				async: false,
				success: function(data) {
					if(data.status) {
						$("#form_dokumen")[0].reset();
						$("#modalTambahDokumen").magnificPopup("close");
						$("#alert-success").text(data.message);
						$('#alert-success').fadeToggle();
						$("#alert-success").delay(4000).fadeOut();

						$("#additional-document").html("");
						$("#additional-document").html(data.output);
					} else {
						$("#alert-danger-dokumen").text(data.message);
						$('#alert-danger-dokumen').fadeToggle();
						$("#alert-danger-dokumen").delay(4000).fadeOut();
					}
				}
			});
		});

		$("#form_dokumen_edit").submit(function(e) {
			e.preventDefault();
			let id = $("#id_dokumen_ed").val();

			$.ajax({
				url: "<?= site_url('penelitian/edit_dokumen/') ?>"+id,
				dataType: "JSON",
				type: "POST",
				data: new FormData(this),
				processData: false,
				contentType: false,
				cache: false,
				async: false,
				success: function(data) {
					if(data.status) {
						$("#form_dokumen_edit")[0].reset();
						$("#modalEditDokumen").magnificPopup("close");
						$("#alert-success").text(data.message);
						$('#alert-success').fadeToggle();
						$("#alert-success").delay(4000).fadeOut();

						$("#additional-document").html("");
						$("#additional-document").html(data.output);
					} else {
						$("#alert-danger-dokumen").text(data.message);
						$('#alert-danger-dokumen').fadeToggle();
						$("#alert-danger-dokumen").delay(4000).fadeOut();
					}
				}
			});
		});

		$("#btn-hapus-dokumen").on('click', function() {
			let id = $("#id_dokumen_hps").val();

			$.ajax({
				url: "<?= site_url('penelitian/hapus_dokumen/') ?>"+id,
				dataType: "JSON",
				success: function(data) {
					if(data.status) {
						$("#modalHapusDokumen").magnificPopup("close");
						$("#alert-success").text(data.message);
						$('#alert-success').fadeToggle();
						$("#alert-success").delay(4000).fadeOut();

						$("#additional-document").html("");
						$("#additional-document").html(data.output);
					} else {
						$("#alert-danger-dokumen").text(data.message);
						$('#alert-danger-dokumen').fadeToggle();
						$("#alert-danger-dokumen").delay(4000).fadeOut();
					}
				}
			});
		});

        $('#p_dosen').DataTable({
			"ajax": {
				url: "<?php echo site_url('penelitian/get_personil_penelitian/'.$this->uri->segment(3).'/Pegawai') ?>",
				type: 'GET'
			}
		});

		$('#p_mahasiswa').DataTable({
			"ajax": {
				url: "<?php echo site_url('penelitian/get_personil_penelitian/'.$this->uri->segment(3).'/Mahasiswa') ?>",
				type: 'GET'
			}
		});

		$('#tabel_revisi').DataTable({
			"ajax": {
				url: "<?php echo site_url('penelitian/get_data_revisi/'.$this->uri->segment(3)) ?>",
				type: 'GET'
			}
		});

		$(".applyBtn").on("click", function(e) {
			e.preventDefault();
		});

		$(".cancelBtn").on("click", function(e) {
			e.preventDefault();
		});
	});

	function reset_form_personil() {
		$("#personil_isian").children().addClass("hidden");
	}

	function reset_form_luaran() {
		$("#luaran_isian").children().addClass("hilang");
	}
	
	function tabelLuaran(val) {
		$.ajax({
			url: "<?php echo site_url('luaran/show_filter_luaran_penelitian/') ?>"+val,
			dataType: "JSON",
			success: function(data) {
				$('#data_luaran').dataTable().fnDestroy();
				$("#data_luaran").html(data.output);
				$('#data_luaran').DataTable({
					"ajax": {
						url: "<?php echo site_url('luaran/get_data_luaran/') ?>"+val+"/"+<?= $this->uri->segment(3) ?>,
						type: 'GET'
					}
				});
			},

			error: function (jqXHR, textStatus, errorThrown) {
				$("#data_luaran").html('');
			}
		});
	}

	function detailLuaran(val) {
		$.ajax({
			url: "<?php echo site_url('luaran/get_detail_luaran/') ?>"+val,
			dataType: "JSON",
			success: function(data) {
				$("#luaran-detail").html(data.output);
				$.magnificPopup.open({
					removalDelay: 500,
					items: {
						src: "#modalDetailLuaran"
					},
					callbacks: {
						beforeOpen: function(e) {
							var Animation = "mfp-flipInX";
							this.st.mainClass = Animation;
						}
					},
					midClick: true
				});
			},

			error: function (jqXHR, textStatus, errorThrown) {
				alert("Gagal mengambil data");
			}
		});
	}

	function formLuaran(val) {
		$.ajax({
			url: "<?php echo site_url('luaran/show_form_luaran/') ?>"+val,
			dataType: "JSON",
			success: function(data) {
				$("#luaran_isian").html(data.output);
				$("#btn-tambah-luaran").removeClass("hilang");
			},

			error: function (jqXHR, textStatus, errorThrown) {
				$("#luaran_isian").html('');
			}
		});
	}

	function hapusPersonil(val) {
		$.magnificPopup.open({
			removalDelay: 500,
			items: {
				src: "#modalHapusPerson"
			},
			callbacks: {
				beforeOpen: function(e) {
					var Animation = "mfp-flipInX";
					this.st.mainClass = Animation;
				}
			},
			midClick: true
		});

		$("#person_hapus").val(val);
	}

	function editLuaran(val) {
		$.magnificPopup.open({
			removalDelay: 500,
			items: {
				src: "#modalEditLuaran"
			},
			callbacks: {
				beforeOpen: function(e) {
					var Animation = "mfp-flipInX";
					this.st.mainClass = Animation;
				}
			},
			midClick: true
		});

		$.ajax({
			url: "<?php echo site_url('luaran/get_luaran_edit/') ?>"+val,
			dataType: "JSON",
			success: function(data) {
				$("#edit_luaran_isian").html(data.output);
				$("#id_luaran").val(val);
			}
		});
	}

	function hapusLuaran(val) {
		$.magnificPopup.open({
			removalDelay: 500,
			items: {
				src: "#modalHapusLuaran"
			},
			callbacks: {
				beforeOpen: function(e) {
					var Animation = "mfp-flipInX";
					this.st.mainClass = Animation;
				}
			},
			midClick: true
		});

		$("#luaran_hapus").val(val);
	}

	function editDokumen(val) {
		$.magnificPopup.open({
			removalDelay: 500,
			items: {
				src: "#modalEditDokumen"
			},
			callbacks: {
				beforeOpen: function(e) {
					var Animation = "mfp-flipInX";
					this.st.mainClass = Animation;
				}
			},
			midClick: true
		});

		$.ajax({
			url: "<?php echo site_url('pengabdian/get_dokumen_edit/'.$this->uri->segment(3).'/') ?>"+val,
			dataType: "JSON",
			success: function(data) {
				$("#id_dokumen_ed").val(val)
				$("#nama_dokumen_ed").val(data.nama);
			}
		});
	}

	function hapusDokumen(val) {
		$.magnificPopup.open({
			removalDelay: 500,
			items: {
				src: "#modalHapusDokumen"
			},
			callbacks: {
				beforeOpen: function(e) {
					var Animation = "mfp-flipInX";
					this.st.mainClass = Animation;
				}
			},
			midClick: true
		});

		$("#id_dokumen_hps").val(val);
	}
</script>