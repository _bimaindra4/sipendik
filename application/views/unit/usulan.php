<?php echo $header ?>

<style>
	#alert-danger, #alert-success {
		display: none;
	}
</style>

<header id="topbar">
	<div class="topbar-left">
		<ol class="breadcrumb">
			<li class="crumb-active">
				<a href="#">Usulan Kegiatan</a>
			</li>
			<li class="crumb-trail">
				<span class="glyphicon glyphicon-home"></span>
			</li>
		</ol>
	</div>
</header>
<section id="content" class="animated fadeIn">
	<div class="row">
		<div class="col-md-12">
			<div class="alert alert-success light alert-dismissable" id="alert-success"></div>
			<div class="alert alert-danger light alert-dismissable" id="alert-danger"></div>
			<div class="panel panel-default mn">
				<div class="panel-heading">
					<span class="panel-title">Data Usulan </span>
				</div>
				<div class="panel-body">
					<table id="usulan" width="100%" class="table table-striped table-hover table-bordered table-responsive">
						<thead>
							<tr>
								<th>No</th>
								<th>Tanggal Pengajuan</th>
								<th>Judul Usulan</th>
								<th>Inisiator</th>
								<th>Jenis Usulan</th>
								<th>Pelaksanaan</th>
								<th>File Pendukung</th>
								<th width="50"></th>
							</tr>
						</thead>
						<tbody>
							<?php $no=1; foreach($usulan as $row) { ?>
								<tr>
									<td><?= $no++ ?></td>
									<td><?= $this->AppModel->DateIndo(date("Y-m-d", strtotime($row->tgl_pengajuan))) ?></td>
									<td><?= $row->judul ?></td>
									<td><?= $row->inisiator ?></td>
									<td><?= $row->jenis ?></td>
									<td>
										<?php
											if($row->tgl_mulai_berlaku == $row->tgl_selesai_berlaku) {
												echo $this->AppModel->DateIndo($row->tgl_mulai_berlaku);
											} else {
												echo $this->AppModel->DateIndo($row->tgl_mulai_berlaku)." s/d ".$this->AppModel->DateIndo($row->tgl_selesai_berlaku);
											}
										?>
									</td>
									<td>
										<button type="button" class="btn btn-rounded btn-success btn-xs" onclick="downloadFile('<?= $row->usulan ?>'); return false;">
											Download
										</button>
									</td>
									<td>
										<button data-effect="mfp-flipInX" class="btn btn-info btn-xs detail-usulan" onclick="detailUsulan('<?= $row->id ?>')">Detail</button>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	
	<div id="modalDetailUsulan" class="popup-basic bg-none mfp-with-anim mfp-hide" style="max-width: 550px">
		<div class="panel">
			<div class="panel-heading">
				<span class="panel-title"> Detail Usulan</span>
			</div>
			<form class="form-horizontal" id="form_usulan" method="post">
				<div class="panel-body">
					<div id="data_detail"></div>
					<hr style="margin-bottom: 10px">
					<div class="form-group">
						<label for="inputStandard" class="control-label col-md-3">Status</label>
						<div class="col-md-1"></div>
						<div class="col-md-4">
							<select name="status" class="form-control" onchange="statusForm(this.value)">
								<option value="1">Terima</option>
								<option value="2">Revisi</option>
								<option value="3">Tolak</option>
							</select>
						</div>
					</div>
					<div id="form_revisi"></div>
				</div>
				<div class="panel-footer text-right">
					<button class="btn btn-primary btn-sm">Submit</button>
				</div>
			</form>
		</div>
	</div>
</section>

<?php echo $footer ?>

<script>
	jQuery(document).ready(function() {
		<?php
			$stts_input = $this->session->flashdata("status");
			$msg_input = $this->session->flashdata("message");
			
			if($stts_input == "sukses") { ?>
				$("#alert-success").text("<?= $msg_input ?>");
				$('#alert-success').fadeToggle();
				$("#alert-success").delay(4000).fadeOut();
			<?php } else if($stts_input == "gagal") { ?>
				$("#alert-danger").text("<?= $msg_input ?>");
				$('#alert-danger').fadeToggle();
				$("#alert-danger").delay(4000).fadeOut();
			<?php } ?>

		$('#usulan').dataTable({
			"columnDefs": [
				{"orderable": false, "targets": 3}
			]
		});
	});

	function detailUsulan(val) {
		$.magnificPopup.open({
			removalDelay: 500, //delay removal by X to allow out-animation,
			items: {
				src: "#modalDetailUsulan"
			},
			callbacks: {
				beforeOpen: function(e) {
					var Animation = "mfp-flipInX";
					this.st.mainClass = Animation;
				}
			},
			midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
		});

		$.ajax({
			url: "<?php echo site_url('usulan/get_data_usulan/') ?>"+val,
			dataType: "JSON",
			success: function(data) {
				$("#data_detail").html(data.output);
				$("#form_usulan").attr("action", "<?php echo site_url('usulan/ubah_status_usulan/') ?>"+val);
			}
		});
	}

	function statusForm(val) {
		if(val == 2) {
			$("#form_revisi").html('<div class="form-group">'+
				'<label for="inputStandard" class="control-label col-md-3">Revisi</label>'+
				'<div class="col-md-1"></div>'+
				'<div class="col-md-8">'+
					'<textarea name="revisi" class="form-control"></textarea>'+
				'</div>'+
			'</div>');
		} else {
			$("#form_revisi").html('');
		}
	}
</script>