<?php 
	echo $header;
	if($role == 1) {
		$judul = "Tambah Penelitian";
	} else if($role == 2) {
		$judul = "Usulan Penelitian";
	}
?>

<header id="topbar">
	<div class="topbar-left">
		<ol class="breadcrumb">
			<li class="crumb-active">
				<a href="#"><?= $judul ?></a>
			</li>
			<li class="crumb-trail">
				<span class="glyphicon glyphicon-home"></span>
			</li>
		</ol>
	</div>
</header>
<section id="content" class="animated fadeIn">
	<div class="row">
		<div class="col-md-12">
            <a class="btn btn-primary" href="<?php echo site_url('penelitian') ?>">
                <i class="fa fa-arrow-left"></i> Kembali
            </a>
			<div class="panel panel-default mn mt10">
				<form class="form-horizontal" action="<?= site_url('penelitian/tambah_proses') ?>" method="post" enctype="multipart/form-data">
					<div class="panel-heading">
						<span class="panel-title"><?= $judul ?></span>
					</div>
					<div class="panel-body">
						<div class="form-group">
							<label class="col-lg-3 control-label">Periode</label>
							<div class="col-lg-3">
								<select name="periode" class="form-control">
									<?php 
										foreach($periode->result() as $row) { 
											$selected = ($row->isAktif == "YES" ? "selected" : "");
									?> 
										<option value="<?= $row->id ?>" <?= $selected ?>><?= $row->nama ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<?php
							foreach($prop as $row) {
								echo '
									<div class="form-group">
										<label class="col-lg-3 control-label">'.$row['label'].'</label>
										<div class="col-lg-7">
											'.$row['form'].'
										</div>
									</div>
								';
							}
						?>
						<div class="form-group">
							<label class="col-lg-3 control-label">Program Studi</label>
							<div class="col-lg-8">
								<select id="prodi" multiple="multiple" name="prodi[]">
									<?php foreach($prodi as $row) { ?> 
										<option value="<?= $row->Kode_Prodi ?>"><?= $row->Nama_Prodi ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-3 control-label">Proposal Penelitian</label>
							<div class="col-lg-7">
								<input type="file" name="proposal" class="form-control">
							</div>
						</div>
					</div>
					<div class="panel-footer">
						<button class="btn btn-primary">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>

<?php echo $footer ?>

<script>
	jQuery(document).ready(function() {
		$('#waktu_pelaksanaan').daterangepicker();

		$("#jenis_pen").on("change", function() {
			$("#skimpenelitian").remove();
			let val = $(this).val();
			$.ajax({
				url: "<?php echo site_url('penelitian/get_skim_penelitian/') ?>"+val,
				dataType: "JSON",
				success: function(data) {
					$("#jenis_pen").after(data.output);
				}
			});
		});
		
		$("#bidpen").on("change", function() {
			$("#subbidpen").remove();
			let val = $(this).val();
			$.ajax({
				url: "<?php echo site_url('penelitian/get_subbidang_penelitian/') ?>"+val,
				dataType: "JSON",
				success: function(data) {
					$("#bidpen").after(data.output);
				}
			});
		});

		$("#tujuan_sosnom").on("change", function() {
			$("#subtujuan").remove();
			let val = $(this).val();
			$.ajax({
				url: "<?php echo site_url('penelitian/get_subtujuan_sosial/') ?>"+val,
				dataType: "JSON",
				success: function(data) {
					$("#tujuan_sosnom").after(data.output);
				}
			});
		});

		$('#prodi').multiselect({
			includeSelectAllOption: true
		});
	});
</script>