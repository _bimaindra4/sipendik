<?php echo $header ?>
<style>
	h1.number {
		margin-top: 0;
	}
</style>

<header id="topbar">
	<div class="topbar-left">
		<ol class="breadcrumb">
			<li class="crumb-active">
				<a href="#">Data Dosen</a>
			</li>
			<li class="crumb-icon">
				<a href="<?php echo site_url() ?>">
					<span class="glyphicon glyphicon-home"></span>
				</a>
			</li>
		</ol>
	</div>
</header>
<section id="content" class="animated fadeIn">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default mn mb10">
				<div class="panel-heading">
					<span class="panel-title">Pencarian Dosen</span>
				</div>
				<div class="panel-body">
					<form method="post" id="cari-dosen" class="form-horizontal">
						<div class="form-body">
							<div class="form-group">
								<label for="" class="control-label col-md-2">Nama Dosen</label>
								<div class="col-md-5">
									<input type="text" class="form-control" name="nama">
								</div>
								<div class="col-md-2">
									<button class="btn btn-primary" id="btn-cari-dosen">Cari</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="row text-center" id="data-dosen">
		<?php foreach($dosen as $row) { ?>
			<div class="col-md-4 col-xs-6">
				<div class="panel panel-tile text-center br-a br-light">
					<div class="panel-body bg-light">
						<img src="<?php echo base_url() ?>assets/img/dosen/<?= $row['foto'] ?>" class="img-profile">
						<h1 class="fs15 mbn"><?= $row['nama'] ?></h1>
						<h6 class="fs10 mn mt5 mb10"><?= $row['nip']." / ".$row['nidn'] ?></h6>
						<span class="label label-md bg-success"><?= $row['prodi'] ?></span>
					</div>
					<div class="panel-footer bg-light dark br-t br-light p12">
						<div class="row">
							<div class="col-md-6">
								<h1 class="fs20 number"><?= $row['penelitian'] ?></h1>
								<h6 class="fs10 mn mt5">PENELITIAN</h6>
							</div>
							<div class="col-md-6">
								<h1 class="fs20 number"><?= $row['pengabdian'] ?></h1>
								<h6 class="fs10 mn mt5">PENGABDIAN</h6>
							</div>
						</div>
					</div>
					<a href="<?php echo site_url('dosen/detail/'.$row['nip']) ?>" class="btn btn-sm btn-info btn-block">Detail</a>
				</div>
			</div>
		<?php } ?>
	</div>
</section>

<?php echo $footer ?>

<script>
	$(document).ready(function() {
		$("#cari-dosen").submit(function(e) {
			e.preventDefault();

			$.ajax({
				url: "<?= site_url('dosen/cari_data_dosen') ?>",
				type: "POST",
				dataType: "JSON",
				data: $("#cari-dosen").serialize(),
				success: function(data) {
					$("#data-dosen").html(data.output);
				}
			});
		});
	});
</script>