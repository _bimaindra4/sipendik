<?php echo $header ?>

<header id="topbar">
	<div class="topbar-left">
		<ol class="breadcrumb">
			<li class="crumb-active">
				<a href="#">Data Report Periode</a>
			</li>
			<li class="crumb-trail">
				<span class="glyphicon glyphicon-home"></span>
			</li>
		</ol>
	</div>
</header>
<section id="content" class="animated fadeIn">
    <div class="row">
		<div class="col-md-3">
			<div class="panel panel-tile text-center" style="margin-bottom: 0px">
				<div class="panel-body bg-primary">
					<h1 class="fs20" style="margin: 0px">2018-2019 GENAP</h1>
				</div>
			</div>
			<ul class="nav tabs-left mb10" style="width: 100%">
				<li class="active">
					<a href="#penelitian" data-toggle="tab">
						<i class="glyphicons glyphicons-globe mr10"></i> Data Penelitian
					</a>
				</li>
				<li>
					<a href="#pengabdian" data-toggle="tab">
						<i class="glyphicons glyphicons-leaf mr10"></i> Data Pengabdian
					</a>
				</li>
				<li>
					<a href="#dosen" data-toggle="tab">
						<i class="glyphicons glyphicons-group mr10"></i> Data Dosen
					</a>
				</li>
			</ul>
		</div>
		<div class="col-md-9">
			<div class="tab-content">
				<div id="penelitian" class="tab-pane active">
					<h2 style="margin-top: 0px" class="mb20">Data Penelitian</h2>
					<div class="row">
						<div class="col-sm-4 col-md-4">
							<div class="panel panel-tile text-center">
								<div class="panel-heading hidden">
									<span class="panel-title"><i class="fa fa-pencil"></i> Title</span>
								</div>
								<div class="panel-body bg-warning">
									<h1 class="fs35 mbn"><?= $stts_pn->on_going ?></h1>
									<h6 class="text-white">ON GOING</h6>
								</div>
							</div>
						</div>
						<div class="col-sm-4 col-md-4">
							<div class="panel panel-tile text-center">
								<div class="panel-heading hidden">
									<span class="panel-title"><i class="fa fa-pencil"></i> Title</span>
								</div>
								<div class="panel-body bg-success">
									<h1 class="fs35 mbn"><?= $stts_pn->selesai ?></h1>
									<h6 class="text-white">SELESAI</h6>
								</div>
							</div>
						</div>
						<div class="col-sm-4 col-md-4">
							<div class="panel panel-tile text-center">
								<div class="panel-heading hidden">
									<span class="panel-title"><i class="fa fa-pencil"></i> Title</span>
								</div>
								<div class="panel-body bg-danger">
									<h1 class="fs35 mbn"><?= $stts_pn->gagal ?></h1>
									<h6 class="text-white">GAGAL</h6>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<div class="panel mobile-controls">
								<div class="panel-heading">
									<span class="panel-title text-info fw700"> Program Studi</span>
								</div>
								<div class="panel-body pn">
									<table class="table mbn tc-med-1 tc-bold-last">
										<thead>
											<tr class="hidden">
												<th>#</th>
												<th>First Name</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>Desain Komunikasi Visual</td>
												<td><?= $prodi_pn->dkv ?></td>
											</tr>
											<tr>
												<td>Teknik Informatika</td>
												<td><?= $prodi_pn->ti ?></td>
											</tr>
											<tr>
												<td>Manajemen Informatika</td>
												<td><?= $prodi_pn->mi ?></td>
											</tr>
											<tr>
												<td>Sistem Informasi</td>
												<td><?= $prodi_pn->si ?></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="panel mobile-controls">
								<div class="panel-heading">
									<span class="panel-title text-info fw700"> Institusi Sumber Dana Penelitian</span>
								</div>
								<div class="panel-body pn">
									<table class="table mbn tc-med-1 tc-bold-last">
										<thead>
											<tr class="hidden">
												<th>#</th>
												<th>First Name</th>
											</tr>
										</thead>
										<tbody>
											<?php foreach($dana_pn as $row) { ?> 
												<tr>
													<td><?= $row['nama'] ?></td>
													<td><?= $row['jumlah'] ?></td>
												</tr>
											<?php } ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="panel mobile-controls">
								<div class="panel-heading">
									<span class="panel-title text-info fw700"> Jenis Penelitian</span>
								</div>
								<div class="panel-body pn">
									<table class="table mbn tc-med-1 tc-bold-last">
										<thead>
											<tr class="hidden">
												<th>#</th>
												<th>First Name</th>
											</tr>
										</thead>
										<tbody>
											<?php foreach($jenis_pn as $row) { ?> 
												<tr>
													<td><?= $row['nama'] ?></td>
													<td><?= $row['jumlah'] ?></td>
												</tr>
											<?php } ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="pengabdian" class="tab-pane">
					<h2 style="margin-top: 0px" class="mb20">Data Pengabdian</h2>
					<div class="row">
						<div class="col-sm-4 col-md-4">
							<div class="panel panel-tile text-center">
								<div class="panel-heading hidden">
									<span class="panel-title"><i class="fa fa-pencil"></i> Title</span>
								</div>
								<div class="panel-body bg-warning">
									<h1 class="fs35 mbn"><?= $stts_pg->on_going ?></h1>
									<h6 class="text-white">ON GOING</h6>
								</div>
							</div>
						</div>
						<div class="col-sm-4 col-md-4">
							<div class="panel panel-tile text-center">
								<div class="panel-heading hidden">
									<span class="panel-title"><i class="fa fa-pencil"></i> Title</span>
								</div>
								<div class="panel-body bg-success">
									<h1 class="fs35 mbn"><?= $stts_pg->selesai ?></h1>
									<h6 class="text-white">SELESAI</h6>
								</div>
							</div>
						</div>
						<div class="col-sm-4 col-md-4">
							<div class="panel panel-tile text-center">
								<div class="panel-heading hidden">
									<span class="panel-title"><i class="fa fa-pencil"></i> Title</span>
								</div>
								<div class="panel-body bg-danger">
									<h1 class="fs35 mbn"><?= $stts_pg->gagal ?></h1>
									<h6 class="text-white">GAGAL</h6>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4">
							<div class="panel mobile-controls">
								<div class="panel-heading">
									<span class="panel-title text-info fw700"> Program Studi</span>
								</div>
								<div class="panel-body pn">
									<table class="table mbn tc-med-1 tc-bold-last">
										<thead>
											<tr class="hidden">
												<th>#</th>
												<th>First Name</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>Desain Komunikasi Visual</td>
												<td><?= $prodi_pg->dkv ?></td>
											</tr>
											<tr>
												<td>Teknik Informatika</td>
												<td><?= $prodi_pg->ti ?></td>
											</tr>
											<tr>
												<td>Manajemen Informatika</td>
												<td><?= $prodi_pg->mi ?></td>
											</tr>
											<tr>
												<td>Sistem Informasi</td>
												<td><?= $prodi_pg->si ?></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="panel mobile-controls">
								<div class="panel-heading">
									<span class="panel-title text-info fw700"> Sumber Dana Pengabdian</span>
								</div>
								<div class="panel-body pn">
									<table class="table mbn tc-med-1 tc-bold-last">
										<thead>
											<tr class="hidden">
												<th>#</th>
												<th>First Name</th>
											</tr>
										</thead>
										<tbody>
											<?php foreach($dana_pg as $row) { ?> 
												<tr>
													<td><?= $row['nama'] ?></td>
													<td><?= $row['jumlah'] ?></td>
												</tr>
											<?php } ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="panel mobile-controls">
								<div class="panel-heading">
									<span class="panel-title text-info fw700"> Jenis Pengabdian</span>
								</div>
								<div class="panel-body pn">
									<table class="table mbn tc-med-1 tc-bold-last">
										<thead>
											<tr class="hidden">
												<th>#</th>
												<th>First Name</th>
											</tr>
										</thead>
										<tbody>
											<?php foreach($jenis_pg as $row) { ?> 
												<tr>
													<td><?= $row['nama'] ?></td>
													<td><?= $row['jumlah'] ?></td>
												</tr>
											<?php } ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="dosen" class="tab-pane">
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-default mn">
								<div class="panel-heading">
									<span class="panel-title">Produktifitas Penelitian dan Pengabdian Masyarakat</span>
								</div>
								<div class="panel-body">
									<table class="table table-striped table-hover table-bordered table-responsive">
										<thead>
											<tr>
												<th>No</th>
												<th>Nama Dosen</th>
												<th class="text-center">Penelitian</th>
												<th class="text-center">Jumlah</th>
												<th class="text-center">Pengabdian</th>
												<th class="text-center">Jumlah</th>
											</tr>
										</thead>
										<tbody>
											<?php $no=1; foreach($produktifitas as $row) { ?>
												<tr>
													<td><?= $no++ ?></td>
													<td><?= $row['nama'] ?></td>
													<td class="text-center">
														<?php
															if($row['penelitian'] > 0) {
																echo '<span class="label label-md bg-success">ADA</span>';
															} else {
																echo '<span class="label label-md bg-danger">TIDAK ADA</span>';
															}
														?>
													</td>
													<td class="text-center"><?= $row['penelitian'] ?></td>
													<td class="text-center">
														<?php
															if($row['pengabdian'] > 0) {
																echo '<span class="label label-md bg-success">ADA</span>';
															} else {
																echo '<span class="label label-md bg-danger">TIDAK ADA</span>';
															}
														?>	
													</td>
													<td class="text-center"><?= $row['pengabdian'] ?></td>
												</tr>
											<?php } ?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php echo $footer ?>