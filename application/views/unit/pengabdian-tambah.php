<?php 
	echo $header;
	if($role == 1) {
		$judul = "Tambah Pengabdian";
	} else if($role == 2) {
		$judul = "Usulkan Pengabdian";
	}	
?>

<style>
	.table-bordered > thead > tr > th, 
	.table-bordered > tbody > tr > th, 
	.table-bordered > tfoot > tr > th, 
	.table-bordered > thead > tr > td, 
	.table-bordered > tbody > tr > td, 
	.table-bordered > tfoot > tr > td {
		border: 1px solid #aaa;
	}

	.table-bordered {
		border: 1px solid #aaa;
	}
</style>

<header id="topbar">
	<div class="topbar-left">
		<ol class="breadcrumb">
			<li class="crumb-active">
				<a href="#"><?= $judul ?></a>
			</li>
			<li class="crumb-trail">
				<span class="glyphicon glyphicon-home"></span>
			</li>
		</ol>
	</div>
</header>
<section id="content" class="animated fadeIn">
	<div class="row">
		<div class="col-md-12">
            <a class="btn btn-primary" href="<?php echo site_url('pengabdian') ?>">
                <i class="fa fa-arrow-left"></i> Kembali
            </a>
			<div class="panel panel-default mn mt10">
				<form class="form-horizontal" method="post" action="<?php echo site_url('pengabdian/tambah_proses') ?>" enctype="multipart/form-data">
					<div class="panel-heading">
						<span class="panel-title"><?= $judul ?></span>
					</div>
					<div class="panel-body">
						<div class="form-group">
							<label class="col-lg-3 control-label">Periode</label>
							<div class="col-lg-3">
								<select name="periode" class="form-control">
									<?php 
										foreach($periode->result() as $row) { 
											$selected = ($row->isAktif == "YES" ? "selected" : "");
									?> 
										<option value="<?= $row->id ?>" <?= $selected ?>><?= $row->nama ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<?php
							foreach($prop as $row) {
								echo '
									<div class="form-group">
										<label class="col-lg-3 control-label">'.$row['label'].'</label>
										<div class="col-lg-7">
											'.$row['form'].'
										</div>
									</div>
								';
							}
						?>
						<div class="form-group">
							<label class="col-lg-3 control-label">Program Studi</label>
							<div class="col-lg-8">
								<select id="prodi" multiple="multiple" name="prodi[]">
									<?php foreach($prodi as $row) { ?> 
										<option value="<?= $row->Kode_Prodi ?>"><?= $row->Nama_Prodi ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-3 control-label">Surat Pengantar</label>
							<div class="col-lg-7">
								<input type="file" name="pengantar" class="form-control">
							</div>
						</div>
					</div>
					<div class="panel-footer">
						<button class="btn btn-primary">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>

<?php echo $footer ?>

<script>
	jQuery(document).ready(function() {
		$('#waktu_pelaksanaan').daterangepicker();

		$("#jenis_pen").on("change", function() {
			$("#skimpengabdian").remove();
			let val = $(this).val();
			$.ajax({
				url: "<?php echo site_url('pengabdian/get_skim_pengabdian/') ?>"+val,
				dataType: "JSON",
				success: function(data) {
					$("#jenis_pen").after(data.output);
				}
			});
		});

		$('#prodi').multiselect({
			includeSelectAllOption: true
		});
	});
</script>