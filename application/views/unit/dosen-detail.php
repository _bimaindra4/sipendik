<?php echo $header ?>
<style>
	h1.number {
		margin-top: 0;
	}
</style>

<header id="topbar">
	<div class="topbar-left">
		<ol class="breadcrumb">
			<li class="crumb-active">
				<a href="#">Detail Dosen</a>
			</li>
			<li class="crumb-icon">
				<a href="<?= site_url('dashboard') ?>">
					<span class="glyphicon glyphicon-home"></span>
				</a>
			</li>
			<li class="crumb-link">
				<a href="<?= site_url('dosen') ?>">Data Dosen</a>
			</li>
			<li class="crumb-trail">Detail Dosen</li>
		</ol>
	</div>
</header>
<section id="content" class="pn animated fadeIn">
	<!-- <div class="p40 bg-background bg-topbar bg-psuedo-tp"> -->
	<div class="pv30 ph40 bg-light dark br-b br-grey posr">
		<div class="table-layout">
			<div class="w200 text-center pr30 hidden-xs">
				<img src="<?php echo base_url() ?>assets/img/dosen/<?= $dosen->foto ?>" class="responsive" style="width: 120px; height: 150px">
			</div>
			<div class="va-t m30">
				<h2 class=""> <?= $dosen->nama ?></h2>
				<p class="fs15 mb20"><?= $dosen->nip." / ".$dosen->nidn ?></p>
				<span class="label label-primary mr5 mb10 ib lh15"><?= $dosen->jabatan_akademik ?></span>
			</div>
		</div>
	</div>
	<div class="p25 pt35">
		<div class="row">
			<div class="col-md-3">
				<div class="panel">
					<div class="panel-heading">
						<span class="panel-icon"><i class="fa fa-user"></i></span>
						<span class="panel-title">Data Kegiatan Dosen</span>
					</div>
					<div class="panel-body pn">
						<table class="table mbn tc-icon-1 tc-med-2 tc-bold-last">
							<tbody>
								<tr>
									<td>
										<span class="fa fa-flask"></span>
									</td>
									<td>Penelitian</td>
									<td><?= $pn->num_rows() ?></td>
								</tr>
								<tr>
									<td>
										<span class="fa fa-leaf"></span>
									</td>
									<td>Pengabdian</td>
									<td><?= $pg->num_rows() ?></td>
								</tr>
								<tr>
									<td>
										<span class="fa fa-check text-success"></span>
									</td>
									<td>Selesai</td>
									<td><?= $selesai->num_rows() ?></td>
								</tr>
								<tr>
									<td>
										<span class="fa fa-spinner text-warning"></span>
									</td>
									<td>On Going</td>
									<td><?= $on_going->num_rows() ?></td>
								</tr>
								<tr>
									<td>
										<span class="fa fa-warning text-danger"></span>
									</td>
									<td>Tanggungan</td>
									<td><?= $tanggungan->num_rows() ?></td>
								</tr>
								<tr>
									<td>
										<span class="fa fa-edit text-primary"></span>
									</td>
									<td>Usulan</td>
									<td><?= $usulan->num_rows() ?></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="col-md-9">
			<div class="panel">
				<div class="panel-heading">
					<ul class="nav panel-tabs-border panel-tabs panel-tabs-left">
						<li class="active">
							<a href="#penelitian" data-toggle="tab" aria-expanded="true">
								<i class="fa fa-flask"></i> Penelitian
							</a>
						</li>
						<li class="">
							<a href="#pengabdian" data-toggle="tab" aria-expanded="false">
								<i class="fa fa-leaf"></i> Pengabdian
							</a>
						</li>
						<li>
							<a href="#tanggungan" data-toggle="tab">
								<i class="fa fa-warning"></i> Tanggungan
							</a>
						</li>
					</ul>
				</div>
				<div class="panel-body">
					<div class="tab-content pn br-n">
						<div id="penelitian" class="tab-pane active">
							<div class="row">
								<div class="col-md-4">
									<select name="periode" class="form-control mb15" id="filter_penelitian">
										<option value="semua">-- Semua Periode --</option>
										<?php foreach($periode as $row) { ?> 
										<option value="<?= $row->id ?>"><?= $row->nama ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<table id="penelitianTable" width="100%" class="table table-striped table-hover table-bordered">
										<thead>
											<tr>
												<th>No</th>
												<th>Judul Penelitian</th>
												<th>Jenis</th>
												<th>Pelaksanaan</th>
												<th class="text-center">ST</th>
												<th class="text-center">LPJ</th>
												<th>Status</th>
												<th></th>
											</tr>
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
						<div id="pengabdian" class="tab-pane">
							<div class="row">
								<div class="col-md-4">
									<select name="periode" class="form-control mb15" id="filter_pengabdian">
										<option value="semua">-- Semua Periode --</option>
										<?php foreach($periode as $row) { ?> 
										<option value="<?= $row->id ?>"><?= $row->nama ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<table id="pengabdianTable" width="100%" class="table table-striped table-hover table-bordered">
										<thead>
											<tr>
												<th>No</th>
												<th>Judul Pengabdian</th>
												<th>Jenis</th>
												<th>Pelaksanaan</th>
												<th>ST</th>
												<th>LPJ</th>
												<th>Status</th>
												<th></th>
											</tr>
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
						<div id="tanggungan" class="tab-pane">
							<div class="row">
								<div class="col-md-12">
									<table id="tanggunganTable" width="100%" class="table table-striped table-hover table-bordered">
										<thead>
											<tr>
												<th>No</th>
												<th>Periode</th>
												<th>Judul Kegiatan</th>
												<th>Kegiatan</th>
												<th>Jenis</th>
												<th></th>
											</tr>
										</thead>
										<tbody></tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
	</div>
</section>

<?php echo $footer ?>

<script>
    $(document).ready( function () {
        $('#penelitianTable').DataTable({
            "columnDefs": [
                {"orderable": false, "targets": 4},
                {"orderable": false, "targets": 5},
                {"orderable": false, "targets": 7}
            ],
			"scrollX": true,
			"ajax": {
				url: "<?php echo site_url('dosen/get_penelitian_dosen/'.$this->uri->segment(3)) ?>",
				type: 'GET'
			},
			dom: 'Bfrtip',
			buttons: [
				{ extend: 'excel', className: 'btn btn-primary mb20' },
				{ extend: 'pdf', className: 'btn btn-primary mb20', exportOptions: { columns: [0,1,2,3,6] } },
				{ extend: 'print', className: 'btn btn-primary mb20' }
			]
        });

        $('#pengabdianTable').DataTable({
            "columnDefs": [
                {"orderable": false, "targets": 4},
                {"orderable": false, "targets": 5},
                {"orderable": false, "targets": 7}
            ],
			"ajax": {
				url: "<?php echo site_url('dosen/get_pengabdian_dosen/'.$this->uri->segment(3)) ?>",
				type: 'GET'
			},
			dom: 'Bfrtip',
			buttons: [
				{ extend: 'excel', className: 'btn btn-primary mb20' },
				{ extend: 'pdf', className: 'btn btn-primary mb20', exportOptions: { columns: [0,1,2,3,6] } },
				{ extend: 'print', className: 'btn btn-primary mb20' }
			]
		});

        $('#tanggunganTable').DataTable({
			"ajax": {
				url: "<?php echo site_url('dosen/get_tanggungan_dosen/'.$this->uri->segment(3)) ?>",
				type: 'GET'
			},
			dom: 'Bfrtip',
			buttons: [
				{ extend: 'excel', className: 'btn btn-primary mb20' },
				{ extend: 'pdf', className: 'btn btn-primary mb20', exportOptions: { columns: [0,1,2,3,4] } },
				{ extend: 'print', className: 'btn btn-primary mb20' }
			]
		});

		$("#filter_penelitian").on("change", function() {
			let val = $(this).val();
			$('#penelitianTable').dataTable().fnDestroy();
			$('#penelitianTable').DataTable({
				"ajax": {
					url: "<?php echo site_url('dosen/get_penelitian_dosen/'.$this->uri->segment(3).'/periode/') ?>"+val,
					type: 'GET'
				}
			});
		});

		$("#filter_pengabdian").on("change", function() {
			let val = $(this).val();
			$('#pengabdianTable').dataTable().fnDestroy();
			$('#pengabdianTable').DataTable({
				"ajax": {
					url: "<?php echo site_url('dosen/get_pengabdian_dosen/'.$this->uri->segment(3).'/periode/') ?>"+val,
					type: 'GET'
				}
			});
		});
    });
</script>
