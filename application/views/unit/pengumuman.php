<?php echo $header ?>

<header id="topbar">
	<div class="topbar-left">
		<ol class="breadcrumb">
			<li class="crumb-active">
				<a href="#">Pengumuman</a>
			</li>
			<li class="crumb-trail">
				<span class="glyphicon glyphicon-home"></span>
			</li>
		</ol>
	</div>
</header>
<section id="content" class="animated fadeIn">
	<div class="row">
		<div class="col-md-12">
            <button class="btn btn-primary" id="btn-pengumuman" data-effect="mfp-flipInX">
                <i class="fa fa-plus"></i> Tambah Pengumuman
            </button>
			<div class="panel panel-default mn mt10">
				<div class="panel-heading">
					<span class="panel-title">Data Pengumuman</span>
				</div>
				<div class="panel-body">
					<table id="validasi" class="table table-striped table-hover table-bordered">
						<thead>
							<tr>
								<th>No</th>
								<th>Judul Pengumuman</th>
                                <th>Tanggal Pengumuman</th>
                                <th>Kategori</th>
                                <th>Dibuat Oleh</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<?php $no=1; foreach($pengumuman as $row) { ?>
								<tr>
									<td><?= $no++ ?></td>
									<td><?= $row->judul ?></td>
									<td><?= $this->AppModel->DateTimeIndo($row->Created_Date) ?></td>
									<td><?= $row->kategori ?></td>
									<td><?= $row->Created_By ?></td>
									<td>
										<button class="btn btn-warning btn-xs" onclick="editPengumuman('<?= $row->id ?>')">Edit</button>
										<button class="btn btn-danger btn-xs" onclick="hapusPengumuman('<?= $row->id ?>')">Hapus</button>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<div id="modal-panel" class="popup-basic bg-none mfp-with-anim mfp-hide" style="max-width: 650px">
		<div class="panel">
			<div class="panel-heading">
				<span class="panel-icon"><i class="fa fa-plus"></i>
				</span>
				<span class="panel-title"> Tambah Pengumuman</span>
			</div>
			<form role="form" method="post" action="<?php echo site_url('pengumuman/tambah') ?>">
				<div class="panel-body">
					<div class="form-group">
						<label for="inputStandard" class="control-label">Judul Pengumuman</label>
						<input type="text" class="form-control" name="judul" placeholder="Judul Pengumuman">
					</div>
					<div class="form-group">
						<label for="inputStandard" class="control-label">Isi Pengumuman</label>
						<textarea id="ckeditor1" name="isi" rows="12"></textarea>
					</div>
					<div class="form-group">
						<label for="inputStandard" class="control-label">Kategori</label>
						<input type="text" class="form-control" name="kategori" placeholder="Kategori">
					</div>
				</div>
				<div class="panel-footer text-right">
					<button class="btn btn-primary btn-sm" type="submit">Submit</button>
				</div>
			</form>
		</div>
	</div>

	<div id="modalEditPengumuman" class="popup-basic bg-none mfp-with-anim mfp-hide" style="max-width: 650px">
		<div class="panel">
			<div class="panel-heading">
				<span class="panel-icon"><i class="fa fa-plus"></i>
				</span>
				<span class="panel-title"> Edit Pengumuman</span>
			</div>
			<form role="form" method="post" id="form_edit_pengumuman">
				<div class="panel-body">
					<div class="form-group">
						<label for="inputStandard" class="control-label">Judul Pengumuman</label>
						<input type="text" class="form-control" name="judul" id="judul" placeholder="Judul Pengumuman">
					</div>
					<div class="form-group">
						<label for="inputStandard" class="control-label">Isi Pengumuman</label>
						<textarea id="ckeditor2" name="isi" rows="12"></textarea>
					</div>
					<div class="form-group">
						<label for="inputStandard" class="control-label">Kategori</label>
						<input type="text" class="form-control" name="kategori" id="kategori" placeholder="Kategori">
					</div>
				</div>
				<div class="panel-footer text-right">
					<button class="btn btn-primary btn-sm" type="submit">Submit</button>
				</div>
			</form>
		</div>
	</div>

	<div id="modalHapusPengumuman" class="popup-basic bg-none mfp-with-anim mfp-hide" style="max-width: 500px">
		<div class="panel panel-danger">
			<div class="panel-heading">
				<span class="panel-icon"><i class="fa fa-trash"></i>
				</span>
				<span class="panel-title"> Hapus Pengumuman</span>
			</div>
			<form class="form-horizontal" method="post" id="form_hapus_pengumuman">
				<div class="panel-body">
					<input type="hidden" name="id" id="id">
					<p>Apakah Anda yakin akan menghapus data pengumuman ini?</p>
				</div>
				<div class="panel-footer text-right">
					<button class="btn btn-danger btn-sm" type="submit">Hapus</button>
				</div>
			</form>
		</div>
	</div>
</section>

<?php echo $footer ?>

<script>
    jQuery(document).ready(function() {
        $('#validasi').dataTable({
            "columnDefs": [
                {"orderable": false, "targets": 3}
            ]
		});

		// Form Skin Switcher
		$('#btn-pengumuman').on('click', function() {
			$('#btn-pengumuman').find('button').removeClass('active-animation');
			$(this).addClass('active-animation item-checked');

			// Inline Admin-Form example 
			$.magnificPopup.open({
				removalDelay: 500, //delay removal by X to allow out-animation,
				items: {
					src: "#modal-panel"
				},
				// overflowY: 'hidden', // 
				callbacks: {
					beforeOpen: function(e) {
						var Animation = $("#btn-pengumuman").attr('data-effect');
						this.st.mainClass = Animation;
					}
				},
				midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
			});

			// Init Ckeditor
			CKEDITOR.replace('ckeditor1', {
				height: 210,
				on: {
					instanceReady: function(evt) {
						$('.cke').addClass('admin-skin cke-hide-bottom');
					}
				},
			});
		});

		
	});

	function editPengumuman(val) {
		$.magnificPopup.open({
			removalDelay: 500, //delay removal by X to allow out-animation,
			items: {
				src: "#modalEditPengumuman"
			},
			// overflowY: 'hidden', // 
			callbacks: {
				beforeOpen: function(e) {
					var Animation = "mfp-flipInX";
					this.st.mainClass = Animation;
				}
			},
			midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
		});

		CKEDITOR.replace('ckeditor2', {
			height: 210,
			on: {
				instanceReady: function(evt) {
					$('.cke').addClass('admin-skin cke-hide-bottom');
				}
			},
		});

		$.ajax({
			url: "<?= site_url('pengumuman/get_data_pengumuman/') ?>"+val,
			dataType: "JSON",
			success: function(data) {
				$("#judul").val(data.judul);
				$("#ckeditor2").val(data.isi);
				$("#kategori").val(data.kategori);
				$("#form_edit_pengumuman").attr("action", "<?= site_url('pengumuman/edit/') ?>"+val);
			}
		});
	}

	function hapusPengumuman(val) {
		$.magnificPopup.open({
			removalDelay: 500, //delay removal by X to allow out-animation,
			items: {
				src: "#modalHapusPengumuman"
			},
			// overflowY: 'hidden', // 
			callbacks: {
				beforeOpen: function(e) {
					var Animation = "mfp-flipInX";
					this.st.mainClass = Animation;
				}
			},
			midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
		});

		$("#id").val(val);
		$("#form_hapus_pengumuman").attr("action", "<?= site_url('pengumuman/hapus/') ?>"+val);
	}
</script>
