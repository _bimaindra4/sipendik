<?php 
	echo $header;
	$this->load->model("AppModel");
?>

<header id="topbar">
	<div class="topbar-left">
		<ol class="breadcrumb">
			<li class="crumb-active">
				<a href="#">Data Tanggungan Dosen</a>
			</li>
			<li class="crumb-trail">
				<span class="glyphicon glyphicon-home"></span>
			</li>
		</ol>
	</div>
</header>
<section id="content" class="animated fadeIn">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default mn mt10">
				<div class="panel-heading">
					<span class="panel-title">Data Tanggungan Dosen</span>
				</div>
				<div class="panel-body">
					<div id="tabel_tanggungan">
						<table id="tanggungan" class="table table-striped table-hover table-bordered">
							<thead>
								<tr>
									<th>No</th>
									<th>Periode</th>
									<th width="350">Judul Kegiatan</th>
									<th>Kegiatan</th>
									<th>Keterlambatan</th>
									<th>SK</th>
									<th>LPJ</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<?php 
									$no=1; 
									foreach($tanggungan->result() as $row) {  
										if($row->sk == NULL) {
											$sk = '<i class="fa fa-times text-danger"></i>';
										} else {
											$sk = '<i class="fa fa-check text-success"></i>';
										}
							
										if($row->lpj == NULL) {
											$lpj = '<i class="fa fa-times text-danger"></i>';
										} else {
											$lpj = '<i class="fa fa-check text-success"></i>';
										}
								?>
									<tr>
										<td><?= $no++ ?></td>
										<td><?= $row->periode ?></td>
										<td><?= $row->judul ?></td>
										<td><?= $row->kegiatan ?></td>
                                        <td><?= $this->AppModel->ConvertDay($row->keterlambatan) ?></td>
										<td><?= $sk ?></td>
										<td><?= $lpj ?></td>
										<td>
											<a href="<?= site_url(strtolower($row->kegiatan)."/detail/".$row->id) ?>" class="btn btn-info btn-xs">Detail</a>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php echo $footer ?>

<script>
    jQuery(document).ready(function() {
        $('#tanggungan').dataTable({
            dom: 'Bfrtip',
			buttons: [
				{ extend: 'excel', className: 'btn btn-primary mb20' },
				{ extend: 'pdf', className: 'btn btn-primary mb20', exportOptions: { columns: [0,1,2,3,4,5] } },
				{ extend: 'print', className: 'btn btn-primary mb20' }
			]
		});
    });
</script>