<?php echo $header ?>

<style>
</style>

<header id="topbar">
	<div class="topbar-left">
		<ol class="breadcrumb">
			<li class="crumb-active">
				<a href="#">Data Penelitian</a>
			</li>
			<li class="crumb-trail">
				<span class="glyphicon glyphicon-home"></span>
			</li>
		</ol>
	</div>
	<div class="topbar-right">
		<div class="ml15 ib va-m" id="toggle_sidemenu_r">
			<a href="#" class="pl5"><i class="fa fa-filter fs20 text-primary"></i></a>
		</div>
	</div>
</header>
<section id="content" class="animated fadeIn">
	<div class="row">
		<div class="col-md-12">
            <a class="btn btn-primary" href="<?php echo site_url('penelitian/tambah') ?>">
                <i class="fa fa-plus"></i> Usulkan Penelitian
            </a>
			<div class="panel panel-default mn mt10">
				<div class="panel-heading">
					<span class="panel-title">Data Penelitian</span>
				</div>
				<div class="panel-body">
					<div id="tabel_penelitian">
						<table id="penelitian" class="table table-striped table-hover table-bordered">
							<thead>
								<tr>
									<th>No</th>
									<th>Periode</th>
									<th width="250">Judul Kegiatan</th>
									<th>Jenis</th>
									<th>Tanggal</th>
									<th>Jumlah Dana</th>
									<th>Status Anggota</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								<?php 
									$no=1; 
									foreach($penelitian->result() as $row) { 
										$config = json_decode($row->config); 
								?>
									<tr>
										<td class="<?= $config->label ?>"><?= $no++ ?></td>
										<td><?= $row->periode ?></td>
										<td><?= $row->judul ?></td>
										<td><?= strtoupper(substr($row->jenis, 11)); ?></td>
										<td><?= $this->AppModel->DateIndo($row->tgl_mulai)." - ".$this->AppModel->DateIndo($row->tgl_selesai) ?></td>
										<td>Rp. <?= number_format($row->dana) ?></td>
										<td><?= $row->status_anggota ?></td>
										<td>
											<div class="btn-group">
												<i class="glyphicon glyphicon-cog dropdown-toggle" data-toggle="dropdown" aria-expanded="false"></i>
												<ul class="dropdown-menu pull-right" role="menu">
													<li>
														<a href="<?php echo site_url('penelitian/detail/'.$row->id) ?>">Detail</a>
													</li>
													<?php if($row->status_anggota == "Ketua") { ?>
														<?php if($row->tahapan == "Usulan" || $row->tahapan == "On Going") { ?>
															<li>
																<a href="<?php echo site_url('penelitian/edit/'.$row->id) ?>">Edit</a>
															</li>
														<?php } ?>
													<?php } ?>
												</ul>
											</div>
										</td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<aside id="sidebar_right" class="nano">
	<div class="sidebar_right_content nano-content">
		<div class="tab-block sidebar-block br-n" style="height: 90%">
			<div class="tab-content br-n" style="height: 90%">
				<div id="sidebar-right-tab1" class="tab-pane active" style="height: 90%">
					<h5 class="title-divider text-muted mb20"> Filter Data</h5>
					<form method="post" id="filter_penelitian">
						<div class="form-body">
							<div class="form-group">
								<label for="" class="control-label mb10">Pilihan Waktu</label>
								<div class="radio-custom mb5">
									<input type="radio" id="periode" name="pilihan_waktu" value="periode" onclick="pilihanWaktu(this.value)">
									<label for="periode">Periode</label>
									<input type="radio" id="tanggal" name="pilihan_waktu" value="tanggal" onclick="pilihanWaktu(this.value)">
                                    <label for="tanggal">Tanggal</label>
								</div>
							</div>
							<div class="form-group hidden" id="input-periode">
								<label for="" class="control-label">Periode</label>
								<select class="form-control" name="periode" id="periode_val">
									<option value="">-- Pilih Periode --</option>
									<?php foreach($periode as $row) { ?> 
										<option value="<?= $row->id ?>"><?= $row->nama ?></option>	
									<?php } ?>
								</select>
							</div>
							<div class="form-group hidden" id="input-tanggal">
								<label for="" class="control-label">Tanggal Pelaksanaan</label>
								<input type="text" class="form-control pull-right" name="tanggal" id="waktu_pelaksanaan">
							</div>
							<div class="form-group">
								<label for="" class="control-label mt15">Jenis Penelitian</label>
								<input type="hidden" id="nip" name="nip" value="<?= $userid ?>">
								<select class="form-control" name="jenis_penelitian" id="jenis_penelitian" onchange="getChildPenelitian(this.value)">
									<option value="semua">-- Semua Jenis --</option>
									<?php foreach($jenis as $row) { ?>
										<option value="<?= $row->id ?>"><?= $row->value ?></option>
									<?php } ?>
								</select>
							</div>
							<div id="skim"></div>
							<div class="form-group">
								<label for="" class="control-label">Status Penelitian</label>
								<select class="form-control" name="status_penelitian" id="status_penelitian">
									<option value="semua">-- Semua Status --</option>
									<?php foreach($tahapan as $row) { ?> 
										<option value="<?= $row->id ?>"><?= $row->tahapan ?></option>
									<?php } ?>
								</select>
							</div>
							<div class="form-group">
								<label for="" class="control-label">Status Anggota</label>
								<select class="form-control" name="status_anggota" id="status_anggota">
									<option value="semua">-- Semua Status --</option>
									<option value="Ketua">Ketua</option>
									<option value="Anggota">Anggota</option>
								</select>
							</div>
							<div class="form-group">
								<button class="btn btn-primary" type="submit">Filter</button>
							</div>
						</div>
					</form>	
				</div>
			</div>
			<!-- end: .tab-content -->
		</div>
	</div>
</aside>

<?php echo $footer ?>

<script>
	jQuery(document).ready(function() {
		$('#penelitian').dataTable({
            dom: 'Bfrtip',
			buttons: [
				{ extend: 'excel', className: 'btn btn-primary mb20' },
				{ extend: 'pdf', className: 'btn btn-primary mb20', exportOptions: { columns: [0,1,2,3,4,5] } },
				{ extend: 'print', className: 'btn btn-primary mb20' }
			]
		});
		$('#tanggal_pelaksanaan').daterangepicker();

		$('#jenis_pen').multiselect();
		$('#prodi').multiselect({
			includeSelectAllOption: true
		});

		$('#filter_penelitian').submit(function(e) {
			e.preventDefault();

			$.ajax({
				url: "<?= site_url('penelitian/get_filter_penelitian') ?>",
				type: "POST",
				dataType: "JSON",
				data: $("#filter_penelitian").serialize(),
				success: function(data) {
					$("#tabel_penelitian").html(data.output);
					$("#penelitian").DataTable({
						dom: 'Bfrtip',
						buttons: [
							{ extend: 'excel', className: 'btn btn-primary mb20' },
							{ extend: 'pdf', className: 'btn btn-primary mb20', exportOptions: { columns: [0,1,2,3,4,5] } },
							{ extend: 'print', className: 'btn btn-primary mb20' }
						]
					});
				}
			});
		});
	});

	function pilihanWaktu(val) {
		if(val == "periode") {
			$("#input-periode").removeClass("hidden");
			$("#input-tanggal").addClass("hidden");
		} else if(val == "tanggal") {
			$("#input-periode").addClass("hidden");
			$("#input-tanggal").removeClass("hidden");
		}
	}

	function getChildPenelitian(val) {
		$.ajax({
			url: "<?php echo site_url('penelitian/get_child_penelitian/') ?>"+val,
			dataType: "JSON",
			success: function(data) {
				if(data.status) {
					$("#skim").html(data.output);
				} else {
					$("#skim").html("");
				}
			}
		});
	}
</script>