<?= $header ?>

<style>
    #penelitian-data, #pengabdian-data, #tanggungan-data {
        margin-bottom: 0;
        border-radius: 2px 0;
    }

    #penelitian-link, #pengabdian-link, #tanggungan-link {
        border-radius: 0 2px;
        cursor: pointer;
    }
</style>

<header id="topbar">
	<div class="topbar-left">
		<ol class="breadcrumb">
			<li class="crumb-active">
				<a href="#">Dashboard</a>
			</li>
			<li class="crumb-trail">
				<span class="glyphicon glyphicon-home"></span>
			</li>
		</ol>
	</div>
</header>
<section id="content" class="animated fadeIn">
	<div class="row">
		<div class="col-md-3">
            <div class="panel panel-tile text-center br-a br-light">
				<div class="panel-body bg-light">
					<img src="<?= base_url() ?>assets/img/dosen/<?= $foto ?>" class="img-profile">
					<h1 class="fs15 mbn"><?= $nama ?></h1>
					<h6 class="fs12 mn mt15 mb10"><?= $userid ?></h6>
				</div>
			</div>
		</div>
		<div class="col-md-9">
            <div class="row">
                <div class="col-md-4">
                    <div class="panel panel-tile bg-primary light of-h" id="penelitian-data">
                        <div class="panel-body pn pl20 p5">
                            <div class="icon-bg">
                                <i class="fa fa-flask"></i>
                            </div>
                            <h2 class="mt15 lh15">
                                <b><?= $pn->num_rows() ?></b>
                            </h2>
                            <h5 class="text-muted">Penelitian</h5>
                        </div>
                    </div>
                    <div class="panel panel-tile bg-default dark of-h" id="penelitian-link">
                        <div class="panel-body pn pl20 p5">
                            <h5 class="text-muted fs12">Lihat Selanjutnya</h5>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="panel panel-tile bg-warning light of-h" id="pengabdian-data">
                        <div class="panel-body pn pl20 p5">
                            <div class="icon-bg">
                                <i class="fa fa-leaf"></i>
                            </div>
                            <h2 class="mt15 lh15">
                                <b><?= $pg->num_rows() ?></b>
                            </h2>
                            <h5 class="text-muted">Pengabdian</h5>
                        </div>
                    </div>
                    <div class="panel panel-tile bg-default dark of-h" id="pengabdian-link">
                        <div class="panel-body pn pl20 p5">
                            <h5 class="text-muted fs12">Lihat Selanjutnya</h5>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="panel panel-tile bg-danger light of-h" id="tanggungan-data">
                        <div class="panel-body pn pl20 p5">
                            <div class="icon-bg">
                                <i class="fa fa-edit"></i>
                            </div>
                            <h2 class="mt15 lh15">
                                <b><?= $tanggungan->num_rows() ?></b>
                            </h2>
                            <h5 class="text-muted">Tanggungan</h5>
                        </div>
                    </div>
                    <div class="panel panel-tile bg-default dark of-h" id="tanggungan-link">
                        <div class="panel-body pn pl20 p5">
                            <h5 class="text-muted fs12">Lihat Selanjutnya</h5>
                        </div>
                    </div>
                </div>
            </div>
		</div>
	</div>
</section>

<?= $footer ?>

<script>
    jQuery(document).ready(function() {
        $("#penelitian-link").on("click", function() {
            window.location.href = "<?= site_url('penelitian') ?>";
        });

        $("#pengabdian-link").on("click", function() {
            window.location.href = "<?= site_url('pengabdian') ?>";
        });

        $("#tanggungan-link").on("click", function() {
            window.location.href = "<?= site_url('tanggungan') ?>";
        });
    });
</script>