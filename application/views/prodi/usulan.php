<?php echo $header ?>

<style>
	#alert-danger, #alert-success {
		display: none;
	}
</style>

<header id="topbar">
	<div class="topbar-left">
		<ol class="breadcrumb">
			<li class="crumb-active">
				<a href="#">Usulan Kegiatan</a>
			</li>
			<li class="crumb-trail">
				<span class="glyphicon glyphicon-home"></span>
			</li>
		</ol>
	</div>
</header>
<section id="content" class="animated fadeIn">
	<div class="row">
		<div class="col-md-12">
			<div class="alert alert-success light alert-dismissable" id="alert-success"></div>
			<div class="alert alert-danger light alert-dismissable" id="alert-danger"></div>
			<div class="panel panel-default mn">
				<div class="panel-heading">
					<span class="panel-title">Data Usulan</span>
				</div>
				<div class="panel-body">
					<table id="usulan" width="100%" class="table table-striped table-hover table-bordered table-responsive">
						<thead>
							<tr>
								<th>No</th>
								<th>Periode</th>
								<th>Judul Usulan</th>
								<th>Jenis Usulan</th>
								<th>Pelaksanaan</th>
								<th>File Pendukung</th>
								<th>Status</th>
								<th width="50"></th>
							</tr>
						</thead>
						<tbody>
							<?php $no=1; foreach($usulan as $row) { ?>
								<tr>
									<td><?= $no++ ?></td>
									<td><?= $row->nama_periode ?></td>
									<td><?= $row->judul ?></td>
									<td><?= $row->jenis ?></td>
									<td>
										<?php
											if($row->tgl_mulai_berlaku == $row->tgl_selesai_berlaku) {
												echo $this->AppModel->DateIndo($row->tgl_mulai_berlaku);
											} else {
												echo $this->AppModel->DateIndo($row->tgl_mulai_berlaku)." s/d ".$this->AppModel->DateIndo($row->tgl_selesai_berlaku);
											}
										?>
									</td>
									<td>
										<?php if($row->usulan != NULL) { ?>
											<button type="button" class="btn btn-rounded btn-success btn-xs" onclick="downloadFile('<?= $row->usulan ?>'); return false;">
												Download
											</button>
										<?php } else { ?>
											<button type="button" class="btn btn-rounded btn-danger btn-xs" disabled>
												Tidak Tersedia
											</button>
										<?php } ?>
									</td>
									<td>
										<?php 
											if($row->accepted == 1) {
												$tahap = "success";
												$tahap_txt = "Disetujui";
											} else if($row->accepted == 2) {
												$tahap = "warning";
												$tahap_txt = "Disetujui dengan Revisi";
											} else if($row->accepted == 3) {
												$tahap = "danger";
												$tahap_txt = "Ditolak";
											} else {
												$tahap = "primary";
												$tahap_txt = "Menunggu";
											}
										?>
										<span class="label label-md bg-<?= $tahap ?>">
											<?= $tahap_txt ?>
										</span>
									</td>
									<td>
										<a href="<?= site_url(strtolower($row->jenis)."/detail/".$row->id) ?>" class="btn btn-info btn-xs">Detail</a>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	
	<div id="modalDetailUsulan" class="popup-basic bg-none mfp-with-anim mfp-hide" style="max-width: 550px">
		<div class="panel">
			<div class="panel-heading">
				<span class="panel-title"> Detail Usulan</span>
			</div>
			<form class="form-horizontal" id="form_usulan" method="post">
				<div class="panel-body">
					<div id="data_detail"></div>
				</div>
				<div class="panel-footer text-right">
					<button class="btn btn-primary btn-sm">Submit</button>
				</div>
			</form>
		</div>
	</div>
</section>

<?php echo $footer ?>

<script>
	jQuery(document).ready(function() {
		<?php
			$stts_input = $this->session->flashdata("status");
			$msg_input = $this->session->flashdata("message");
			
			if($stts_input == "sukses") { ?>
				$("#alert-success").text("<?= $msg_input ?>");
				$('#alert-success').fadeToggle();
				$("#alert-success").delay(4000).fadeOut();
		<?php } else if($stts_input == "gagal") { ?>
				$("#alert-danger").text("<?= $msg_input ?>");
				$('#alert-danger').fadeToggle();
				$("#alert-danger").delay(4000).fadeOut();
		<?php } ?>

		$('#usulan').dataTable({
			"columnDefs": [
				{"orderable": false, "targets": 3}
			]
		});
	});

	function detailUsulan(val) {
		$.magnificPopup.open({
			removalDelay: 500, //delay removal by X to allow out-animation,
			items: {
				src: "#modalDetailUsulan"
			},
			callbacks: {
				beforeOpen: function(e) {
					var Animation = "mfp-flipInX";
					this.st.mainClass = Animation;
				}
			},
			midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
		});

		$.ajax({
			url: "<?php echo site_url('usulan/get_data_usulan/') ?>"+val,
			dataType: "JSON",
			success: function(data) {
				$("#data_detail").html(data.output);
				$("#form_usulan").attr("action", "<?php echo site_url('usulan/ubah_status_usulan/') ?>"+val);
			}
		});
	}
</script>