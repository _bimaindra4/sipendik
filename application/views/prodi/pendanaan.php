<?php echo $header ?>

<header id="topbar">
	<div class="topbar-left">
		<ol class="breadcrumb">
			<li class="crumb-active">
				<a href="#">Pendanaan Kegiatan</a>
			</li>
			<li class="crumb-trail">
				<span class="glyphicon glyphicon-home"></span>
			</li>
		</ol>
	</div>
</header>
<section id="content" class="animated fadeIn">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default mn mb10">
				<div class="panel-heading">
					<span class="panel-title">Filter Pendanaan</span>
				</div>
				<div class="panel-body">
					<form method="post" id="filter_pendanaan" class="form-horizontal">
						<div class="form-body">
							<div class="form-group">
								<label class="control-label col-md-2">Periode</label>
								<div class="col-md-4">
									<select class="form-control" name="periode" id="periode">
										<option value="">-- Semua Periode --</option>
										<?php foreach($periode->result() as $row) { ?> 
											<option value="<?= $row->id ?>"><?= $row->nama ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-2">Pilih Kegiatan</label>
								<div class="col-md-4">
									<select id="kegiatan" name="kegiatan" class="form-control" required>
										<option value="1">Penelitian</option>
										<option value="2">Pengabdian</option>
									</select>
								</div>
							</div>
							<div class="col-md-offset-2 col-md-2" style="padding-left: 5px">
								<button class="btn btn-primary " id="filter_pendanaan">Filter</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3">
			<div id="kategori_pendanaan"></div>
		</div>
		<div class="col-md-9">
			<div class="panel panel-default mn mb10">
				<div class="panel-heading">
					<span class="panel-title">Filter Pendanaan</span>
				</div>
				<div class="panel-body">
					<div id="tabel_pendanaan"></div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php echo $footer ?>

<script>
	jQuery(document).ready(function() {
		$('#pendanaan').dataTable({
            dom: 'Bfrtip',
			buttons: [
				{ extend: 'excel', className: 'btn btn-primary mb20' },
				{ extend: 'pdf', className: 'btn btn-primary mb20', exportOptions: { columns: [0,1,2,3,4,5] } },
				{ extend: 'print', className: 'btn btn-primary mb20' }
			]
		});

		$('#filter_pendanaan').submit(function(e) {
			e.preventDefault();

			$.ajax({
				url: "<?= site_url('pendanaan/get_filter_pendanaan') ?>",
				type: "POST",
				dataType: "JSON",
				data: $("#filter_pendanaan").serialize(),
				success: function(data) {
					$("#tabel_pendanaan").html(data.output);
					$("#kategori_pendanaan").html(data.pendanaan);
					$("#pendanaan").DataTable({
						dom: 'Bfrtip',
						buttons: [
							{ extend: 'excel', className: 'btn btn-primary mb20' },
							{ extend: 'pdf', className: 'btn btn-primary mb20', exportOptions: { columns: [0,1,2,3,4,5] } },
							{ extend: 'print', className: 'btn btn-primary mb20' }
						]
					});
				}
			});
		});
	});
</script>