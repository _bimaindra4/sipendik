<?php echo $header ?>

<style>
	.diffstat {
		font-size: 12px;
		font-weight: 600;
		color: #586069;
		white-space: nowrap;
		cursor: default;
	}

	.diffstat-block-added {
		background-color: #28a745;
		outline: 1px solid #28a745;
	}

	.diffstat-block-deleted {
		background-color: #d73a49;
		outline: 1px solid #d73a49;
	}

	.diffstat-block-added:hover {
		background-color: #28a745;
		outline: 1px solid #111;
	}

	.diffstat-block-deleted:hover {
		background-color: #d73a49;
		outline: 1px solid #111;
	}

	.diffstat-block-added, .diffstat-block-deleted {
		display: inline-block;
		width: 12px;
		height: 12px;
		margin-left: 1px;
    	outline-offset: -1px;
	}
</style>

<header id="topbar">
	<div class="topbar-left">
		<ol class="breadcrumb">
			<li class="crumb-active">
				<a href="#">Produktivitas Dosen</a>
			</li>
			<li class="crumb-trail">
				<span class="glyphicon glyphicon-home"></span>
			</li>
		</ol>
	</div>
</header>
<section id="content" class="animated fadeIn">
	<div class="row">
		<div class="col-md-4">
			<div class="panel panel-default mn">
				<div class="panel-heading">
					<span class="panel-title">Keterangan</span>
				</div>
				<div class="panel-body">
					<p>Data yang ditampilkan berupa data 5 tahun terakhir, masing-masing berupa kegiatan penelitian dan pengabdian kepada masyarakat</p>
				</div>
			</div>
		</div>
		<div class="col-md-8">
			<div class="panel panel-default mn">
				<div class="panel-heading">
					<span class="panel-title">Tabel Produktivitas Dosen</span>
				</div>
				<div class="panel-body">
					<table class="table table-striped table-hover table-bordered table-responsive">
						<thead>
							<tr>
								<th width="50">No</th>
								<th>NIP</th>
								<th width="300">Nama Dosen</th>
								<th>Kegiatan</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<?php $no=1; foreach($produktivitas as $row) { ?>
								<tr>
									<td rowspan="2"><?= $no++ ?></td>
									<td rowspan="2"><?= $row['nip'] ?></td>
									<td rowspan="2"><?= $row['nama'] ?></td>
									<td>Penelitian</td>
									<td>
										<span class="diffstat">
											<?php 
												foreach($row["penelitian"] as $pn) { 
													$data_tooltip = $pn['periode'].' ('.$pn['total'].')';
													if($pn["total"] > 0) {
														echo '<span class="diffstat-block-added" data-placement="top" title="'.$data_tooltip.'"></span>';
													} else {
														echo '<span class="diffstat-block-deleted" data-placement="top" title="'.$data_tooltip.'"></span>';
													}
												}
											?> 
										</span>
									</td>
								</tr>
								<tr>
									<td>Pengabdian</td>
									<td>
										<span class="diffstat">
											<?php 
												foreach($row["pengabdian"] as $pn) { 
													$data_tooltip = $pn['periode'].' ('.$pn['total'].')';
													if($pn["total"] > 0) {
														echo '<span class="diffstat-block-added" data-placement="top" title="'.$data_tooltip.'"></span>';
													} else {
														echo '<span class="diffstat-block-deleted" data-placement="top" title="'.$data_tooltip.'"></span>';
													}
												}
											?> 
										</span>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
			
		</div>
	</div>
</section>

<?php echo $footer ?>