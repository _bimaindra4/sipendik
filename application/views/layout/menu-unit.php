<li <?= (($active_menu == "beranda") ? 'class="active"' : '') ?>>
	<a href="<?php echo site_url('dashboard') ?>">
		<span class="glyphicons glyphicons-home"></span>
		<span class="sidebar-title">Beranda</span>
	</a>
</li>
<li <?= (($active_menu == "dosen") ? 'class="active"' : '') ?>>
	<a href="<?php echo site_url('dosen') ?>">
		<span class="glyphicons glyphicons-user"></span>
		<span class="sidebar-title">Data Dosen</span>
	</a>
</li>
<li <?= (($active_menu == "penelitian") ? 'class="active"' : '') ?>>
	<a href="<?php echo site_url('penelitian') ?>">
		<span class="glyphicons glyphicons-globe"></span>
		<span class="sidebar-title">Data Penelitian</span>
	</a>
</li>
<li <?= (($active_menu == "pengabdian") ? 'class="active"' : '') ?>>
	<a href="<?php echo site_url('pengabdian') ?>">
		<span class="glyphicons glyphicons-leaf"></span>
		<span class="sidebar-title">Data Pengabdian</span>
	</a>
</li>
<li <?= (($active_menu == "usulan") ? 'class="active"' : '') ?>>
	<a href="<?php echo site_url('usulan') ?>">
		<span class="glyphicons glyphicons-book"></span>
		<span class="sidebar-title">Usulan Kegiatan</span>
	</a>
</li>
<li <?= (($active_menu == "luaran") ? 'class="active"' : '') ?>>
	<a href="<?php echo site_url('luaran') ?>">
		<span class="glyphicons glyphicons-share"></span>
		<span class="sidebar-title">Data Luaran</span>
	</a>
</li>
<li <?= (($active_menu == "periode") ? 'class="active"' : '') ?>>
	<a href="<?php echo site_url('periode') ?>">
		<span class="glyphicons glyphicons-cogwheels"></span>
		<span class="sidebar-title">Periode</span>
	</a>
</li>
<li <?= (($active_menu == "pengumuman") ? 'class="active"' : '') ?>>
	<a href="<?php echo site_url('pengumuman') ?>">
		<span class="glyphicons glyphicons-bullhorn"></span>
		<span class="sidebar-title">Pengumuman</span>
	</a>
</li>