<li <?= (($active_menu == "dashboard") ? 'class="active"' : '') ?>>
	<a href="<?php echo site_url('dashboard') ?>">
		<span class="glyphicons glyphicons-home"></span>
		<span class="sidebar-title">Dashboard</span>
	</a>
</li>
<li <?= (($active_menu == "penelitian") ? 'class="active"' : '') ?>>
	<a href="<?php echo site_url('penelitian') ?>">
		<span class="glyphicons glyphicons-globe"></span>
		<span class="sidebar-title">Data Penelitian</span>
	</a>
</li>
<li <?= (($active_menu == "pengabdian") ? 'class="active"' : '') ?>>
	<a href="<?php echo site_url('pengabdian') ?>">
		<span class="glyphicons glyphicons-leaf"></span>
		<span class="sidebar-title">Data Pengabdian</span>
	</a>
</li>
<li>
	<a href="<?php echo site_url('usulan') ?>">
		<span class="glyphicons glyphicons-book"></span>
		<span class="sidebar-title">Usulan Kegiatan</span>
	</a>
</li>
<li <?= (($active_menu == "tanggungan") ? 'class="active"' : '') ?>>
	<a href="<?= site_url('tanggungan') ?>">
		<span class="glyphicons glyphicons-warning_sign"></span>
		<span class="sidebar-title">Data Tanggungan</span>
	</a>
</li>