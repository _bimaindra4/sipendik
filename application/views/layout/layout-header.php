<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title><?= $judul ?></title>
	<meta name="keywords" content="HTML5 Bootstrap 3 Admin Template UI Theme" />
	<meta name="description" content="AdminDesigns - A Responsive HTML5 Admin UI Framework">
	<meta name="author" content="AdminDesigns">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- Font CSS (Via CDN) -->
	<link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800'>
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Roboto:400,500,700,300">

	<!-- Required Plugin CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/vendor/plugins/datepicker/css/bootstrap-datetimepicker.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/vendor/plugins/daterange/daterangepicker.css">
	
	<!-- Theme CSS -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/skin/default_skin/css/theme.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/1.6.2/css/buttons.dataTables.min.css">

	<!-- Vendor CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/vendor/plugins/magnific/magnific-popup.css">

	<!-- Admin Forms CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/admin-tools/admin-forms/css/admin-forms.css">

    <!-- Admin Modals CSS -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/admin-tools/admin-plugins/admin-modal/adminmodal.css">
	
	<!-- Favicon -->
	<link rel="shortcut icon" href="<?php echo base_url() ?>assets/img/favicon.ico">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->

	<style>
		table.dataTable {
			margin-top: 15px !important;
		}

		table.dataTable thead td, table.dataTable tfoot td, table.dataTable.no-footer {
			border: #EEE solid 1px;
		}

		table.dataTable thead th, table.dataTable thead td {
			border: #EEE solid 1px;
		}
		
		.stts-unknown { border-left: 4px solid #888 !important; }
		.stts-usulan { border-left: 4px solid #4a89dc !important; }
		.stts-ongoing { border-left: 4px solid #f6bb42 !important; }
		.stts-selesai { border-left: 4px solid #8cd481 !important; }
		.stts-gagal { border-left: 4px solid #ed7764 !important; }

		.img-profile { 
			width: 30%;
			height: 30%;
			border-radius: 50%; 
			margin-top: 20px;
		}

		ul.sidebar-menu > li:hover { /* li:not(.sidebar-label) */
			background: #DDD;
		}

		ul.sidebar-menu > li.sidebar-label:hover {
			background: #FFF;
		}

		ul.sidebar-menu > li.active {
			background: #DDD;
		}
	</style>
</head>

<body class="blank-page">
	<div id="main">
		<header class="navbar navbar-fixed-top bg-info">
			<div class="navbar-branding dark bg-info">
				<a class="navbar-brand" href="<?php echo site_url('dashboard') ?>"> <b>SIPENDIK</b> STIKI</a>
				<span id="toggle_sidemenu_l" class="glyphicons glyphicons-show_lines"></span>
				<ul class="nav navbar-nav pull-right hidden">
					<li>
						<a href="#" class="sidebar-menu-toggle">
							<span class="octicon octicon-ruby fs20 mr10 pull-right"></span>
						</a>
					</li>
				</ul>
			</div>
			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown dropdown-item-slide">
					<a class="dropdown-toggle pl10 pr10" data-toggle="dropdown" href="#" onclick="removeStatusNotif()">
						<span class="octicon octicon-radio-tower fs18"></span>
						<?php
							if($notifikasi->num_rows() > 0) {
								echo '<span class="badge badge-hero badge-danger">'.$notifikasi->num_rows().'</span>';
							}
						?>
					</a>
					<ul class="dropdown-menu dropdown-hover dropdown-persist pn w350 bg-white animated animated-shorter fadeIn" role="menu">
						<li class="bg-light p8">
							<span class="fw600 pl5 lh30"> Notifikasi</span>
						</li>
						<?php 
							foreach($notifikasi->result() as $row) { 
								if($row->jenis == 1) {
									$url = "penelitian/detail/".$row->ref;
								} else if($row->jenis == 2) {
									$url = "pengabdian/detail/".$row->ref;
								} else {
									$url = "";
								}
						?>
							<li class="p10 br-t item-1">
								<div class="media" style="cursor: pointer" onclick="redirectRiset('<?= $url ?>')">
									<a class="media-left" href="#"> <img src="<?php echo base_url() ?>assets/img/dosen/<?= $row->foto ?>" class="mw40" alt="holder-img"> </a>
									<div class="media-body va-m">
										<h5 class="media-heading mv5"><?= $row->kat ?> 
											<small class="text-muted">- <?= $row->tanggal ?></small> 
										</h5> 
										<?= $row->pesan ?>
									</div>
								</div>
							</li>
						<?php } ?>
					</ul>
				</li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle fw600 p15" data-toggle="dropdown">
						<img src="<?php echo base_url() ?>assets/img/dosen/<?= $foto ?>" alt="avatar" class="mw30 br64 mr15">
						<span><?= $nama ?></span>
						<span class="caret caret-tp hidden-xs"></span>
					</a>
					<ul class="dropdown-menu dropdown-persist pn w250 bg-white" role="menu">
						<li class="br-t of-h">
							<a href="<?= site_url('logout') ?>" class="fw600 p12 animated animated-short fadeInDown">
								<span class="fa fa-power-off pr5"></span> Logout </a>
						</li>
					</ul>
				</li>
			</ul>
		</header>
		<aside id="sidebar_left" class="nano nano-primary sidebar-default sidebar-light">
			<div class="nano-content">
				<ul class="nav sidebar-menu">
					<li class="sidebar-label pt20">Menu</li>
					<?php
						$data['active_menu'] = $active_menu;
						if($role == 1) {
							echo $this->load->view("layout/menu-unit", $data, TRUE);
						} else if($role == 2) {
							echo $this->load->view("layout/menu-dosen", $data, TRUE);
						} else if($role == 3) {
							echo $this->load->view("layout/menu-prodi", $data, TRUE);
						} else if($role == 4) {
							echo $this->load->view("layout/menu-pimpinan", $data, TRUE);
						}
					?>
				</ul>
				<div class="sidebar-toggle-mini">
					<a href="#">
						<span class="fa fa-sign-out"></span>
					</a>
				</div>
			</div>
		</aside>
		<section id="content_wrapper">
			<div id="topbar-dropmenu">
				<div class="topbar-menu row">
					<div class="col-xs-4 col-sm-2">
						<a href="#" class="metro-tile bg-success">
							<span class="metro-icon glyphicons glyphicons-inbox"></span>
							<p class="metro-title">Messages</p>
						</a>
					</div>
					<div class="col-xs-4 col-sm-2">
						<a href="#" class="metro-tile bg-info">
							<span class="metro-icon glyphicons glyphicons-parents"></span>
							<p class="metro-title">Users</p>
						</a>
					</div>
					<div class="col-xs-4 col-sm-2">
						<a href="#" class="metro-tile bg-alert">
							<span class="metro-icon glyphicons glyphicons-headset"></span>
							<p class="metro-title">Support</p>
						</a>
					</div>
					<div class="col-xs-4 col-sm-2">
						<a href="#" class="metro-tile bg-primary">
							<span class="metro-icon glyphicons glyphicons-cogwheels"></span>
							<p class="metro-title">Settings</p>
						</a>
					</div>
					<div class="col-xs-4 col-sm-2">
						<a href="#" class="metro-tile bg-warning">
							<span class="metro-icon glyphicons glyphicons-facetime_video"></span>
							<p class="metro-title">Videos</p>
						</a>
					</div>
					<div class="col-xs-4 col-sm-2">
						<a href="#" class="metro-tile bg-system">
							<span class="metro-icon glyphicons glyphicons-picture"></span>
							<p class="metro-title">Pictures</p>
						</a>
					</div>
				</div>
			</div>
