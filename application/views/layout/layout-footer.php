	</section>
	</div>
	<!-- jQuery -->
	<script type="text/javascript" src="<?php echo base_url() ?>assets/vendor/jquery/jquery-1.11.1.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>assets/vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

	<!-- Bootstrap -->
	<script type="text/javascript" src="<?php echo base_url() ?>assets/js/bootstrap/bootstrap.min.js"></script>

	<!-- Page Plugins via CDN -->
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/globalize/0.1.1/globalize.min.js"></script>
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.3/moment.js"></script>

	<!-- Page Plugins -->
	<script type="text/javascript" src="<?php echo base_url() ?>assets/vendor/plugins/magnific/jquery.magnific-popup.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>assets/vendor/plugins/daterange/daterangepicker.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>assets/vendor/plugins/datepicker/js/bootstrap-datetimepicker.min.js"></script>
	
	<!-- Admin Forms Javascript -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/admin-tools/admin-forms/js/jquery-ui-monthpicker.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/admin-tools/admin-forms/js/jquery-ui-timepicker.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/admin-tools/admin-forms/js/jquery-ui-touch-punch.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>assets/admin-tools/admin-forms/js/jquery.spectrum.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>assets/admin-tools/admin-forms/js/jquery.stepper.min.js"></script>
	
	<!-- Vendor -->
	<script type="text/javascript" src="<?php echo base_url() ?>assets/vendor/plugins/magnific/jquery.magnific-popup.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>assets/vendor/editors/ckeditor/ckeditor.js"></script>

	<!-- Theme Javascript -->
	<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>
	<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>
	<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.6.2/js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.flash.min.js"></script>
	<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
	<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
	<script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
	<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.html5.min.js"></script>
	<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.6.2/js/buttons.print.min.js"></script>
	<script type="text/javascript" charset="utf8" src="https://cdn.rawgit.com/ashl1/datatables-rowsgroup/fbd569b8768155c7a9a62568e66a64115887d7d0/dataTables.rowsGroup.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>assets/js/utility/utility.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>assets/js/main.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>assets/js/demo.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function() {
			"use strict";

			// Init Theme Core
			Core.init();

			// Init Theme Core
			Demo.init();

			$('.tutup').on('click', function() {
				$.magnificPopup.close();
			});

			$('#form_luaran').submit(function(e){
				e.preventDefault(); 
				let luaran = $("#luaran_filter").val();

				$.ajax({
					url: '<?= site_url("luaran/tambah_luaran/".$this->uri->segment(3)) ?>',
					dataType: "JSON",
					type: "POST",
					data: new FormData(this),
					processData: false,
					contentType: false,
					cache: false,
					async: false,
					success: function(data){
						if(data.status == true) {
							$("#form_luaran")[0].reset();
							$("#modalTambahLuaran").magnificPopup("close");
							$("#alert-success").text(data.message);
							$('#alert-success').fadeToggle();
							$("#alert-success").delay(4000).fadeOut();

							$("#luaran_filter_table").val(luaran);
							tabelLuaran(luaran);
						} else if(data.status == false) {
							$("#alert-danger-tbh-luaran").text(data.message);
							$('#alert-danger-tbh-luaran').fadeToggle();
							$("#alert-danger-tbh-luaran").delay(4000).fadeOut();
						}
					}
				});
			});

			$('#form_edit_luaran').submit(function(e){
				e.preventDefault(); 
				let id = $("#id_luaran").val();

				$.ajax({
					url: '<?= site_url("luaran/edit_luaran/") ?>'+id,
					dataType: "JSON",
					type: "POST",
					data: new FormData(this),
					processData: false,
					contentType: false,
					cache: false,
					async: false,
					success: function(data){
						if(data.status == true) {
							$("#form_edit_luaran")[0].reset();
							$("#modalTambahLuaran").magnificPopup("close");
							$("#alert-success").text(data.message);
							$('#alert-success').fadeToggle();
							$("#alert-success").delay(4000).fadeOut();

							$('#data_luaran').dataTable().fnDestroy();
							$('#data_luaran').DataTable({
								"ajax": {
									url: "<?php echo site_url('luaran/get_data_luaran/') ?>"+data.jenis+"/<?= $this->uri->segment(3) ?>",
									type: 'GET'
								}
							});
						} else if(data.status == false) {
							$("#alert-danger-edit-luaran").text(data.message);
							$('#alert-danger-edit-luaran').fadeToggle();
							$("#alert-danger-edit-luaran").delay(4000).fadeOut();
						}
					}
				});
			});

			$("#form_upload_file_lpj").submit(function(e) {
				e.preventDefault(); 
				let jenis = $("#jenis_file").val();
				let kategori = $("#kategori").val();
				let url = "<?= base_url() ?>index.php/"+kategori+"/upload_lpj/<?= $this->uri->segment(3) ?>";
				
				$.ajax({
					url: url,
					dataType: "JSON",
					type: "POST",
					data: new FormData(this),
					processData: false,
					contentType: false,
					cache: false,
					async: false,
					success: function(data) {
						if(data.status == true) {
							$("#form_upload_file_lpj")[0].reset();
							$("#modalUploadFileLPJ").magnificPopup("close");
							$("#alert-success").text(data.message);
							$('#alert-success').fadeToggle();
							$("#alert-success").delay(4000).fadeOut();

							$("#lpj").html(data.output);
						} else if(data.status == false) {
							$("#alert-danger-dokumen").text(data.message);
							$('#alert-danger-dokumen').fadeToggle();
							$("#alert-danger-dokumen").delay(4000).fadeOut();
						}
					}
				});
			});

			$("#form_upload_file_proposal").submit(function(e) {
				e.preventDefault(); 
				let kategori = $("#kategori_prop").val();
				let url = "<?= base_url() ?>index.php/"+kategori+"/upload_usulan/<?= $this->uri->segment(3) ?>";
				
				$.ajax({
					url: url,
					dataType: "JSON",
					type: "POST",
					data: new FormData(this),
					processData: false,
					contentType: false,
					cache: false,
					async: false,
					success: function(data) {
						if(data.status == true) {
							$("#form_upload_file_proposal")[0].reset();
							$("#modalUploadFileProposal").magnificPopup("close");
							$("#alert-success").text(data.message);
							$('#alert-success').fadeToggle();
							$("#alert-success").delay(4000).fadeOut();

							$("#usulan").html(data.output);
						} else if(data.status == false) {
							$("#alert-danger-dokumen").text(data.message);
							$('#alert-danger-dokumen').fadeToggle();
							$("#alert-danger-dokumen").delay(4000).fadeOut();
						}
					}
				});
			});

			$("#form_sk").submit(function(e) {
				e.preventDefault();
				let jenis = $("#jenis_keg").val();

				$.ajax({
					url: "<?= base_url() ?>index.php/"+jenis+"/edit_sk",
					type: "POST",
					dataType: "JSON",
					data: new FormData(this),
					processData: false,
					contentType: false,
					cache: false,
					async: false,
					success: function(data) {
						if(data.status == false) {
							$("#alert-danger-edit-sk").text(data.message);
							$('#alert-danger-edit-sk').fadeToggle();
							$("#alert-danger-edit-sk").delay(4000).fadeOut();
						} else if(data.status == true) {
							$("#modalSKDetail").magnificPopup("close");
							$("#alert-success").text(data.message);
							$('#alert-success').fadeToggle();
							$("#alert-success").delay(4000).fadeOut();

							if(data.output != null) {
								$("#surat-tugas").html(data.output);
							}
						}
					}
				});
			});
		});

		function downloadFile(val) {
			window.open('<?= base_url()."assets/files/" ?>'+val, '_blank');
		}

		function redirectRiset(val) {
			location.href = "<?php echo site_url() ?>"+"/"+val;
		}

		function removeStatusNotif() {
			$.ajax({
				url: "<?php echo site_url('notifikasi/remove_status_notif') ?>",
				dataType: "JSON",
				success: function(data) {}
			});
		}
	</script>
	</body>
</html>