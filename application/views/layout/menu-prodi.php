<li <?= (($active_menu == "beranda") ? 'class="active"' : '') ?>>
	<a href="<?= site_url('dashboard') ?>">
		<span class="glyphicons glyphicons-home"></span>
		<span class="sidebar-title">Beranda</span>
	</a>
</li>
<li <?= (($active_menu == "dosen") ? 'class="active"' : '') ?>>
	<a class="accordion-toggle <?= (($active_menu == "dosen") ? 'menu-open' : '') ?>" href="#">
		<span class="glyphicons glyphicons-user"></span>
		<span class="sidebar-title">Data Dosen</span>
        <span class="caret"></span>
	</a>
	<ul class="nav sub-nav">
		<li>
			<a href="<?= site_url("dosen") ?>">
				<span class="glyphicons glyphicons-group"></span> Data Pengabmas Dosen
			</a>
		</li>
		<li>
			<a href="<?= site_url("dosen/produktivitas") ?>">
				<span class="glyphicons glyphicons-stats"></span> Produktivitas Dosen
			</a>
		</li>
	</ul>
</li>
<li <?= (($active_menu == "penelitian") ? 'class="active"' : '') ?>>
	<a href="<?= site_url('penelitian') ?>">
		<span class="glyphicons glyphicons-globe"></span>
		<span class="sidebar-title">Data Penelitian</span>
	</a>
</li>
<li <?= (($active_menu == "pengabdian") ? 'class="active"' : '') ?>>
	<a href="<?= site_url('pengabdian') ?>">
		<span class="glyphicons glyphicons-leaf"></span>
		<span class="sidebar-title">Data Pengabdian</span>
	</a>
</li>
<li <?= (($active_menu == "usulan") ? 'class="active"' : '') ?>>
	<a class="accordion-toggle <?= (($active_menu == "usulan") ? 'menu-open' : '') ?>" href="#">
		<span class="glyphicons glyphicons-book"></span>
		<span class="sidebar-title">Usulan Kegiatan</span>
        <span class="caret"></span>
	</a>
	<ul class="nav sub-nav">
		<li>
			<a href="<?= site_url('usulan') ?>">
				<span class="glyphicons glyphicons-pen"></span> Usulan Saat Ini 
			</a>
		</li>
		<li>
			<a href="<?= site_url('usulan/histori') ?>">
				<span class="glyphicons glyphicons-text_height"></span> Histori Usulan 
			</a>
		</li>
	</ul>
</li>
<li <?= (($active_menu == "tanggungan") ? 'class="active"' : '') ?>>
	<a href="<?= site_url('tanggungan') ?>">
		<span class="glyphicons glyphicons-warning_sign"></span>
		<span class="sidebar-title">Data Tanggungan Dosen</span>
	</a>
</li>
<li <?= (($active_menu == "pendanaan") ? 'class="active"' : '') ?>>
	<a href="<?= site_url('pendanaan') ?>">
		<span class="glyphicons glyphicons-coins"></span>
		<span class="sidebar-title">Pendanaan Kegiatan</span>
	</a>
</li>