<!DOCTYPE html>
<html>

<head>
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<title>SIPENDIK - LOGIN</title>
	<meta name="keywords" content="HTML5 Bootstrap 3 Admin Template UI Theme" />
	<meta name="description" content="AdminDesigns - A Responsive HTML5 Admin UI Framework">
	<meta name="author" content="AdminDesigns">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- Font CSS (Via CDN) -->
	<link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800'>
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Roboto:400,500,700,300">

	<!-- Theme CSS -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/skin/default_skin/css/theme.css">

	<!-- Admin Forms CSS -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/admin-tools/admin-forms/css/admin-forms.css">

	<!-- Favicon -->
	<link rel="shortcut icon" href="<?php echo base_url() ?>assets/img/favicon.ico">

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<![endif]-->

	<style>
		.text-header-login, 
		.text-header-login:hover, 
		.text-header-login:visited,
		.text-header-login:focus {
			color: white;
			text-decoration: none;
			font-size: 28px;
		}
	</style>
</head>

<body class="external-page sb-l-c sb-r-c">
	<div id="main" class="animated fadeIn">
		<section id="content_wrapper">
			<div id="canvas-wrapper">
				<canvas id="demo-canvas"></canvas>
			</div>
			<section id="content">
				<div class="admin-form theme-info" style="max-width: 500px">
					<div class="row mb15 table-layout">
						<div class="col-xs-12 va-m pln text-center">
							<img src="<?= base_url() ?>assets/img/stiki.png" width="60" height="60" style="margin-bottom: 10px">
							<br>
							<a href="#" class="text-header-login"><b>SIPENDIK</b> STIKI</a></a>
						</div>
					</div>
					<?php 
						$status = $this->session->flashdata('status');
						if($status == "gagal") { ?>
							<div class="alert alert-danger">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								<i class="fa fa-remove pr10"></i>
								<strong>Oh snap!</strong> username dan password Anda salah!
							</div>
					<?php } ?>
					<div class="panel panel-info mt10 br-n">
						<div class="panel-heading heading-border bg-white">
							<div class="section row mn"></div>
						</div>
						<form method="post" action="<?php echo site_url('login/aksi_login') ?>">
							<div class="panel-body bg-light p30">
								<div class="row">
									<div class="col-sm-12">
										<div class="section">
											<label for="username" class="field-label text-muted fs18 mb10">Email</label>
											<label for="username" class="field prepend-icon">
												<input type="email" name="email" id="email" class="gui-input" placeholder="Enter email">
												<label for="username" class="field-icon"><i class="fa fa-user"></i>
												</label>
											</label>
										</div>
										<div class="section">
											<label for="username" class="field-label text-muted fs18 mb10">Password</label>
											<label for="password" class="field prepend-icon">
												<input type="password" name="password" id="password" class="gui-input" placeholder="Enter password">
												<label for="password" class="field-icon"><i class="fa fa-lock"></i>
												</label>
											</label>
										</div>
									</div>
								</div>
							</div>
							<div class="panel-footer clearfix p10 ph15">
								<button type="submit" class="btn btn-primary mr10 pull-right">Login</button>
							</div>
						</form>
					</div>
				</div>
			</section>
		</section>
	</div>

	<!-- jQuery -->
	<script type="text/javascript" src="<?php echo base_url() ?>assets/vendor/jquery/jquery-1.11.1.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>assets/vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

	<!-- Bootstrap -->
	<script type="text/javascript" src="<?php echo base_url() ?>assets/js/bootstrap/bootstrap.min.js"></script>

	<!-- Page Plugins -->
	<script type="text/javascript" src="<?php echo base_url() ?>assets/js/pages/login/EasePack.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>assets/js/pages/login/rAF.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>assets/js/pages/login/TweenLite.min.js"></script>

	<!-- Theme Javascript -->
	<script type="text/javascript" src="<?php echo base_url() ?>assets/js/utility/utility.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>assets/js/main.js"></script>
	<script type="text/javascript" src="<?php echo base_url() ?>assets/js/demo.js"></script>

	<!-- Page Javascript -->
	<script type="text/javascript">
		jQuery(document).ready(function() {
			"use strict";

			// Init CanvasBG and pass target starting location
			CanvasBG.init({
				Loc: {
					x: window.innerWidth / 2,
					y: window.innerHeight / 3.3
				},
			});
		});
	</script>
</body>
</html>
