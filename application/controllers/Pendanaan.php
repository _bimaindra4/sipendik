<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pendanaan extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model("AppModel");
		$this->load->model("SessionModel");
		$this->load->model("NotifikasiModel");
		$this->load->model("PendanaanModel");
		$this->load->model("PeriodeModel");

		$this->sess = $this->SessionModel->GetSession();
		$this->sess_not_con = !$this->sess['session_userid'] && !$this->sess['session_role'];
		$this->roles_user = explode(",", $this->sess['session_role']);
        $this->sess_data = [
			"userid" => $this->sess['session_userid'],
			"role" => $this->sess['session_role'],
			"nama" => $this->sess['session_nama'],
			"email" => $this->sess['session_email'],
			"foto" => $this->sess['session_foto'],
			"notifikasi" => $this->NotifikasiModel->GetNotifikasi($this->sess['session_userid'], $this->sess['session_role'])
		];
	}

	public function index() {
		if($this->sess_not_con) {
			redirect("login");
		} else {
			$this->sess_data['judul'] = "SIPENDIK - Pendanaan Kegiatan";
			$this->sess_data['active_menu'] = 'pendanaan';
			$data['header'] = $this->load->view("layout/layout-header", $this->sess_data, TRUE);
            $data['footer'] = $this->load->view("layout/layout-footer", $this->sess_data, TRUE);

            if(in_array('3', $this->roles_user) || in_array('4', $this->roles_user)) {
                $data['periode'] = $this->PeriodeModel->GetPeriode();
                $this->load->view("prodi/pendanaan", $data);
            }
        }
	}
	
	public function get_filter_pendanaan() {
		$post = $this->input->post();
		// Add case filter for user prodi
		if(in_array('3', $this->roles_user)) {
			$post["prodi"] = $this->sess_data['userid'];
		}

		// Load Kategori
		$output_kat = "";
		$kategori_query = $this->PendanaanModel->GetTotalPendanaanProdi($post);
		foreach($kategori_query as $row_kat) {
			$output_kat .= '
			<div class="panel panel-tile text-primary of-h mb10">
				<div class="panel-body pn br-n pl20 p5">
					<div class="icon-bg">
						<i class="fa fa-money"></i>
					</div>
					<h2 class="mt15 lh15">
						<b>'.$row_kat['jumlah'].'</b>
					</h2>
					<h5 class="text-muted">'.$row_kat['nama'].'</h5>
				</div>
			</div>
			';
		}

		// Load Tabel
		$no = 1;

		$tabel_query = $this->PendanaanModel->GetDataPendanaan($post);
		$output = '
		<table id="pendanaan" class="table table-striped table-hover table-bordered">
			<thead>
				<tr>
					<th>No</th>
					<th width="350">Judul Kegiatan</th>
					<th>Pendanaan</th>
					<th>Jumlah Dana</th>
				</tr>
			</thead>
			<tbody>';
			foreach($tabel_query->result() as $row) {
				$output .= '
				<tr>
					<td>'.$no++.'</td>
					<td>'.$row->judul.'</td>
					<td>'.$row->sumber_dana.'</td>
					<td>Rp. '.number_format($row->dana).'</td>
				</tr>
				';
			}
			$output .= '
			</tbody>
		</table>';

		$data = [
			"output" => $output,
			"pendanaan" => $output_kat
		];
		echo json_encode($data);
	}
}
?>
