<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model("LoginModel");
		$this->load->model("SessionModel");
	}

	public function index() {
		$this->load->view("login");
	}

	public function aksi_login() {
		$email = $this->input->post("email");
		$pass = sha1($this->input->post("password"));

		if($email == "lppm@stiki.ac.id" && $pass == "a69b9ed5f420584b720583ad7aaef02d13502ddc") {
			$this->SessionModel->StoreSession(1, $email, 1, "Unit LPPM", "user.png");
		} else if($email == "ti@stiki.ac.id" && $pass == "5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8") {
			$this->SessionModel->StoreSession("TI-S1", $email, 3, "Kaprodi TI", "user.png");
		} else if($email == "mi@stiki.ac.id" && $pass == "5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8") {
			$this->SessionModel->StoreSession("MI-D3", $email, 3, "Kaprodi MI", "user.png");
		} else if($email == "si@stiki.ac.id" && $pass == "5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8") {
			$this->SessionModel->StoreSession("SI-S1", $email, 3, "Kaprodi SI", "user.png");
		} else if($email == "dkv@stiki.ac.id" && $pass == "5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8") {
			$this->SessionModel->StoreSession("DK-S1", $email, 3, "Kaprodi DKV", "user.png");
		} else if($email == "pimpinan@stiki.ac.id" && $pass == "5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8") {
			$this->SessionModel->StoreSession("pimpinan", $email, 4, "Pimpinan", "user.png");
		} else {
			$check = $this->LoginModel->CheckUser($email, $pass);
			if($check->num_rows() > 0) {
				$row = $check->row();
				$this->SessionModel->StoreSession($row->nip, $row->email, 2, $row->nama, $row->foto);
			} else {
				$this->session->set_flashdata("status", "gagal");
				redirect('login');
			}
		}

		redirect("dashboard");
	}
}
?>
