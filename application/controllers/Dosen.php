<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dosen extends CI_Controller {
	protected $roles_user;

	// session
	protected $sess;
	protected $sess_not_con;
	protected $sess_data;

	public function __construct() {
		parent::__construct();
		$this->load->model("AppModel");
		$this->load->model("SessionModel");
		$this->load->model("DosenModel");
		$this->load->model("NotifikasiModel");

		$this->sess = $this->SessionModel->GetSession();
		$this->sess_not_con = !$this->sess['session_userid'] && !$this->sess['session_role'];
		$this->roles_user = explode(",", $this->sess['session_role']);
        $this->sess_data = [
			"userid" => $this->sess['session_userid'],
			"role" => $this->sess['session_role'],
			"nama" => $this->sess['session_nama'],
			"email" => $this->sess['session_email'],
			"foto" => $this->sess['session_foto'],
			"notifikasi" => $this->NotifikasiModel->GetNotifikasi($this->sess['session_userid'], $this->sess['session_role'])
		];
	}

	public function index() {
		if($this->sess_not_con) {
			redirect("login");
		} else {
			$this->sess_data['judul'] = "SIPENDIK - Data Dosen";
			$this->sess_data['active_menu'] = 'dosen';
			$data['header'] = $this->load->view("layout/layout-header", $this->sess_data, TRUE);
			$data['footer'] = $this->load->view("layout/layout-footer", $this->sess_data, TRUE);

			if(in_array('1', $this->roles_user) || in_array('4', $this->roles_user)) {
				$data['dosen'] = $this->DosenModel->GetDataDosenPPM();
				$this->load->view("unit/dosen", $data);
			} else if(in_array('3', $this->roles_user)) {
				$data['dosen'] = $this->DosenModel->GetDataDosenPPM("prodi.Kode_Prodi", $this->sess_data['userid']);
				$this->load->view("unit/dosen", $data);
			}
		}
	}

	public function detail($nip) {
		if($this->sess_not_con) {
			redirect("login");
		} else {
			$dosen = $this->DosenModel->GetDataDosen("peg.nip",$nip)->row();
			$this->sess_data['judul'] = "SIPENDIK - ".$dosen->nama;
			$this->sess_data['active_menu'] = 'dosen';
			$data['header'] = $this->load->view("layout/layout-header", $this->sess_data, TRUE);
			$data['footer'] = $this->load->view("layout/layout-footer", $this->sess_data, TRUE);

			if(in_array('1', $this->roles_user) || in_array('3', $this->roles_user) || in_array('4', $this->roles_user)) {
				$data['dosen'] = $dosen;
				$data['periode'] = $this->AppModel->GetPeriode();
				$data['pn'] = $this->DosenModel->GetDataDosenPenelitian($nip);
				$data['pg'] = $this->DosenModel->GetDataDosenPengabdian($nip);
				$data['selesai'] = $this->DosenModel->GetDataKegiatanDosenByStatus($nip, "Selesai");
				$data['on_going'] = $this->DosenModel->GetDataKegiatanDosenByStatus($nip, "On Going");
				$data['usulan'] = $this->DosenModel->GetDataKegiatanDosenByStatus($nip, "Usulan");
				$data['tanggungan'] = $this->DosenModel->GetDataKegiatanDosenByStatus($nip, "Tanggungan");
				$this->load->view("unit/dosen-detail", $data);
			}
		}
	}

	public function produktivitas($prodi = NULL) {
		if($this->sess_not_con) {
			redirect("login");
		} else {
			$this->sess_data['judul'] = "SIPENDIK - Produktivitas Dosen";
			$this->sess_data['active_menu'] = 'dosen';
			$data['header'] = $this->load->view("layout/layout-header", $this->sess_data, TRUE);
			$data['footer'] = $this->load->view("layout/layout-footer", $this->sess_data, TRUE);

			if(in_array('3', $this->roles_user)) {
				$data['produktivitas'] = $this->DosenModel->GetProduktivitasDosen($this->sess['session_userid']);
				$this->load->view("prodi/dosen-produktiv", $data);
			} else if(in_array('4', $this->roles_user)) {
				$data['produktivitas'] = $this->DosenModel->GetProduktivitasDosen();
				$this->load->view("pimpinan/dosen-produktiv", $data);
			}
		}
	}

	public function get_data_dosen($nip) {
		$sql = $this->DosenModel->GetDataDosen("peg.nip",$nip);
		echo json_encode($sql->row());
	}

	public function get_penelitian_dosen($nip, $filter = NULL, $filter_value = NULL) {
		$draw = intval($this->input->get("draw"));
      	$start = intval($this->input->get("start"));
	  	$length = intval($this->input->get("length"));
	  
		$no = 1;
		$data = [];
		$sql = $this->DosenModel->GetDataDosenPenelitian($nip,$filter,$filter_value);
		foreach($sql->result() as $row) {
			if($row->tahapan == "Usulan") {
				$tahapan = '<span class="badge badge-primary">'.$row->tahapan.'</span>';
			} else if($row->tahapan == "On Going") {
				$tahapan = '<span class="badge badge-warning">'.$row->tahapan.'</span>';
			} else if($row->tahapan == "Selesai") {
				$tahapan = '<span class="badge badge-success">'.$row->tahapan.'</span>';
			} else if($row->tahapan == "Gagal") {
				$tahapan = '<span class="badge badge-danger">'.$row->tahapan.'</span>';
			}

			if($row->sk == NULL) {
				$sk = '<i class="fa fa-times text-danger"></i>';
			} else {
				$sk = '<i class="fa fa-check text-success"></i>';
			}

			if($row->lpj == NULL) {
				$lpj = '<i class="fa fa-times text-danger"></i>';
			} else {
				$lpj = '<i class="fa fa-check text-success"></i>';
			}

			$data[] = [
				$no++,
				$row->judul,
				$row->jenis,
				$this->AppModel->DateIndo($row->tgl_mulai)." s/d ".$this->AppModel->DateIndo($row->tgl_selesai),
				$sk,
				$lpj,
				$tahapan,
				'<a href="'.site_url('penelitian/detail/'.$row->id).'" class="btn btn-xs btn-primary">
					Detail
				</a>'
			];
		}
		
		$result = array(
			"draw" => $draw,
			"recordsTotal" => $sql->num_rows(),
			"recordsFiltered" => $sql->num_rows(),
			"data" => $data
		);

	   	echo json_encode($result);
   		exit();
	}

	public function get_pengabdian_dosen($nip, $filter = NULL, $filter_value = NULL) {
		$draw = intval($this->input->get("draw"));
		$start = intval($this->input->get("start"));
		$length = intval($this->input->get("length"));
	
		$no = 1;
		$data = [];
		$sql = $this->DosenModel->GetDataDosenPengabdian($nip,$filter,$filter_value);
		foreach($sql->result() as $row) {
			if($row->tgl_mulai == $row->tgl_selesai) {
				$pelaksanaan = $this->AppModel->DateIndo($row->tgl_mulai);
			} else {
				$pelaksanaan = $this->AppModel->DateIndo($row->tgl_mulai)." s/d ".$this->AppModel->DateIndo($row->tgl_selesai);
			}

			if($row->tahapan == "Usulan") {
				$tahapan = '<span class="badge badge-primary">'.$row->tahapan.'</span>';
			} else if($row->tahapan == "On Going") {
				$tahapan = '<span class="badge badge-warning">'.$row->tahapan.'</span>';
			} else if($row->tahapan == "Selesai") {
				$tahapan = '<span class="badge badge-success">'.$row->tahapan.'</span>';
			} else if($row->tahapan == "Gagal") {
				$tahapan = '<span class="badge badge-danger">'.$row->tahapan.'</span>';
			}

			if($row->sk == NULL) {
				$sk = '<i class="fa fa-times text-danger"></i>';
			} else {
				$sk = '<i class="fa fa-check text-success"></i>';
			}

			if($row->lpj == NULL) {
				$lpj = '<i class="fa fa-times text-danger"></i>';
			} else {
				$lpj = '<i class="fa fa-check text-success"></i>';
			}

			$data[] = [
				$no++,
				$row->judul,
				$row->jenis,
				$pelaksanaan,
				$sk,
				$lpj,
				$tahapan,
				'<a href="'.site_url('pengabdian/detail/'.$row->id).'" class="btn btn-xs btn-primary">
					Detail
				</a>'
			];
		}
		
		$result = array(
			"draw" => $draw,
			"recordsTotal" => $sql->num_rows(),
			"recordsFiltered" => $sql->num_rows(),
			"data" => $data
		);

		echo json_encode($result);
		exit();
	}

	public function get_tanggungan_dosen($nip) {
		$draw = intval($this->input->get("draw"));
		$start = intval($this->input->get("start"));
		$length = intval($this->input->get("length"));
	
		$no = 1;
		$data = [];
		$sql = $this->DosenModel->GetDataKegiatanDosenByStatus($nip, "Tanggungan");
		foreach($sql->result() as $row) {
			$url = "";
			if($row->kegiatan == "Penelitian") {
				$url = site_url('penelitian/detail/'.$row->id);
			} else if($row->kegiatan == "Pengabdian") {
				$url = site_url('pengabdian/detail/'.$row->id);
			}

			$data[] = [
				$no++,
				$row->periode,
				$row->judul,
				$row->kegiatan,
				$row->jenis,
				'<a href="'.$url.'" class="btn btn-xs btn-primary">Detail</a>'
			];
		}
		
		$result = array(
			"draw" => $draw,
			"recordsTotal" => $sql->num_rows(),
			"recordsFiltered" => $sql->num_rows(),
			"data" => $data
		);

		echo json_encode($result);
		exit();
	}

	public function cari_data_dosen() {
		$output = "";
		$nama = $this->input->post("nama");
		$sql = $this->DosenModel->GetDataDosenPPM("peg.nama", $nama);
		foreach($sql as $row) {
			$output .= '
			<div class="col-md-4 col-xs-6">
				<div class="panel panel-tile text-center br-a br-light">
					<div class="panel-body bg-light">
						<img src="'.base_url().'assets/img/dosen/'.$row['foto'].'" class="img-profile">
						<h1 class="fs15 mbn">'.$row['nama'].'</h1>
						<h6 class="fs10 mn mt5 mb10">'.$row['nip']." / ".$row['nidn'].'</h6>
						<span class="label label-md bg-success">'.$row['prodi'].'</span>
					</div>
					<div class="panel-footer bg-light dark br-t br-light p12">
						<div class="row">
							<div class="col-md-6">
								<h1 class="fs20 number">'.$row['penelitian'].'</h1>
								<h6 class="fs10 mn mt5">PENELITIAN</h6>
							</div>
							<div class="col-md-6">
								<h1 class="fs20 number">'.$row['pengabdian'].'</h1>
								<h6 class="fs10 mn mt5">PENGABDIAN</h6>
							</div>
						</div>
					</div>
					<a href="'.site_url('dosen/detail/'.$row['nip']).'" class="btn btn-sm btn-info btn-block">Detail</a>
				</div>
			</div>
			';
		}

		$data = ["output" => $output];
		echo json_encode($data);
	}
}
?>
