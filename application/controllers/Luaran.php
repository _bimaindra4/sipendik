<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Luaran extends CI_Controller {
	protected $roles_user;

	// session
	protected $sess;
	protected $sess_not_con;
	protected $sess_data;

	public function __construct() {
		parent::__construct();
		$this->load->model("AppModel");
		$this->load->model("SessionModel");
		$this->load->model("LuaranModel");
		$this->load->model("RisetModel");
		$this->load->model("NotifikasiModel");

		$this->sess = $this->SessionModel->GetSession();
		$this->sess_not_con = !$this->sess['session_userid'] && !$this->sess['session_role'];
		$this->roles_user = explode(",", $this->sess['session_role']);
        $this->sess_data = [
			"userid" => $this->sess['session_userid'],
			"role" => $this->sess['session_role'],
			"nama" => $this->sess['session_nama'],
			"email" => $this->sess['session_email'],
			"foto" => $this->sess['session_foto'],
			"notifikasi" => $this->NotifikasiModel->GetNotifikasi($this->sess['session_userid'], $this->sess['session_role'])
		];
	}

	public function index() {
		if($this->sess_not_con) {
			redirect("login");
		} else {
			$this->sess_data['judul'] = "SIPENDIK - Data Luaran";
			$this->sess_data['active_menu'] = 'luaran';
			$data['header'] = $this->load->view("layout/layout-header", $this->sess_data, TRUE);
			$data['footer'] = $this->load->view("layout/layout-footer", $this->sess_data, TRUE);

			if(in_array('1', $this->roles_user)) {
				$data['periode'] = $this->AppModel->GetPeriode();
				$data['luaran_pn'] = $this->LuaranModel->GetPilihanLuaran(1);
				$data['luaran_pg'] = $this->LuaranModel->GetPilihanLuaran(2);
				$data['opt_jenis_pub'] = $this->AppModel->GetOptionDropdown('[jenis-publikasi]');
				$data['opt_tingkat_pub'] = $this->AppModel->GetOptionDropdown('[tingkat-jurnal]');
				$this->load->view("unit/luaran", $data);
			}
		}
	}

	public function tambah_luaran($id) {
		$config['upload_path'] = "./assets/files";
        $config['allowed_types'] = 'pdf';
        $config['encrypt_name'] = TRUE;
         
        $this->load->library('upload', $config);
        if($this->upload->do_upload("33")) {
        	$data = array('upload_data' => $this->upload->data());
			$image = $data['upload_data']['file_name'];
		} else {
			$image = NULL;
		}

		$p = $this->input->post();
		$luaran = $p['luaran_filter'];
		unset($p['luaran_filter']); // Hapus filter luaran
		
		// Add Root Luaran
		$dataLuaran = [
			"riset" => $id,
			"luaran" => $luaran
		];
		$id_luaran = $this->LuaranModel->TambahLuaran($dataLuaran);

		// Add Luaran Prop
		$dataProp = [];
		foreach($p as $key => $value) {
			$dataProp[] = [
				"luaran" => $id_luaran,
				"prop" => $key,
				"value" => $value
			];
		}

		// Add for Image
		$dataProp[] = [
			"luaran" => $id_luaran,
			"prop" => 33,
			"value" => $image
		];

		$sql = $this->LuaranModel->TambahLuaranProp($dataProp);
		if($sql) {
			$output = [
				"status" => true,
				"message" => "Data luaran berhasil ditambahkan"
			];
		} else {
			$output = [
				"status" => false,
				"message" => "Terjadi kesalahan pada saat input luaran"
			];
		}

		echo json_encode($output);
	}

	public function edit_luaran($id) {
		$jenis = $this->LuaranModel->GetDetailLuaran($id);
		$p = $this->input->post();

		$config['upload_path'] = "./assets/files";
        $config['allowed_types'] = 'pdf';
        $config['encrypt_name'] = TRUE;
         
        $this->load->library('upload', $config);
        if($this->upload->do_upload("33")) {
			$data = array('upload_data' => $this->upload->data());
			$image = $data['upload_data']['file_name'];

			$dataProp = ["value" => $image];
			$dataWhere = ["luaran" => $id, "prop" => 33];
			$sql = $this->LuaranModel->EditLuaran($dataProp, $dataWhere);
		}

		$dataProp = [];
		foreach($p as $key => $value) {
			$dataProp = ["value" => $value];
			$dataWhere = ["luaran" => $id, "prop" => $key];
			$sql = $this->LuaranModel->EditLuaran($dataProp, $dataWhere);
		}

		if($sql) {
			$output = [
				"status" => true,
				"message" => "Data luaran berhasil di edit",
				"jenis" => $jenis->row()->luaran
			];
		} else {
			$output = [
				"status" => false,
				"message" => "Terjadi kesalahan pada saat edit luaran",
				"jenis" => $jenis->row()->luaran
			];
		}

		echo json_encode($output);
	}
	
	public function hapus_luaran($id) {
		$jenis = $this->LuaranModel->GetDetailLuaran($id);
		$sql = $this->LuaranModel->HapusLuaran($id);

		if($sql) {
			//unlink(base_url().'assets/files/'.$files);

			$output = [
				"status" => true,
				"message" => "Data luaran berhasil dihapus",
				"jenis" => $jenis->row()->luaran
			];
		} else {
			$output = [
				"status" => false,
				"message" => "Terjadi kesalahan pada saat hapus luaran",
				"jenis" => NULL
			];
		}

		echo json_encode($output);
	}

	public function get_data_luaran($luaran, $id = "semua", $periode = "semua") {
		$draw = intval($this->input->get("draw"));
      	$start = intval($this->input->get("start"));
	  	$length = intval($this->input->get("length"));
	  
		$no = 1;
		$data = [];

		if($id == "semua") {
			$sql = $this->LuaranModel->GetDataLuaran($luaran,"semua",$periode);
			$editable = true;
		} else {
			$sql = $this->LuaranModel->GetDataLuaran($luaran,$id,$periode);
			$status = $this->RisetModel->GetStatusRiset($id);
			if($status == "Usulan" || $status == "On Going") {
				$editable = true;
			} else {
				$editable = false;
			}
		}

		if($luaran == 1 || $luaran == 5) {
			foreach($sql->result() as $row) {
				if($row->file == NULL) {
					$file = '<i class="fa fa-times text-danger"></i>';
				} else {
					$file = '<button type="button" class="btn btn-rounded btn-success btn-xs" onclick="downloadFile(\''.$row->file.'\'); return false;">
						Download
					</button>';
				}

				$data[] = [
					$no++, 
					$row->nama_jurnal, 
					$row->jenis_publikasi, 
					$row->tingkat_publikasi, 
					$row->edisi, 
					$row->status_jurnal, 
					$file, 
					$this->btn_aksi($row->id, $editable)
				];
			}
		} else if($luaran == 2) {
			foreach($sql->result() as $row) {
				if($row->file == NULL) {
					$file = '<i class="fa fa-times text-danger"></i>';
				} else {
					$file = '<button type="button" class="btn btn-rounded btn-success btn-xs" onclick="downloadFile(\''.$row->file.'\'); return false;">
						Download
					</button>';
				}

				$data[] = [
					$no++, 
					$row->integrasi, 
					$row->bentuk, 
					$file, 
					$this->btn_aksi($row->id, $editable)
				];
			}
		} else if($luaran == 3 || $luaran == 8) {
			foreach($sql->result() as $row) {
				if($row->file == NULL) {
					$file = '<i class="fa fa-times text-danger"></i>';
				} else {
					$file = '<button type="button" class="btn btn-rounded btn-success btn-xs" onclick="downloadFile(\''.$row->file.'\'); return false;">
						Download
					</button>';
				}

				$data[] = [
					$no++, 
					$row->no_sertifikat, 
					$row->nama_hki, 
					$row->jenis_hki, 
					$row->status_hki, 
					$this->btn_aksi($row->id, $editable)
				];
			}
		} else if($luaran == 4) {
			foreach($sql->result() as $row) {
				if($row->file == NULL) {
					$file = '<i class="fa fa-times text-danger"></i>';
				} else {
					$file = '<button type="button" class="btn btn-rounded btn-success btn-xs" onclick="downloadFile(\''.$row->file.'\'); return false;">
						Download
					</button>';
				}

				$data[] = [
					$no++, 
					$row->jenis_luaran, 
					$row->deskripsi, 
					$this->btn_aksi($row->id, $editable)
				];
			}
		} else if($luaran == 6) {
			foreach($sql->result() as $row) {
				if($row->file == NULL) {
					$file = '<i class="fa fa-times text-danger"></i>';
				} else {
					$file = '<button type="button" class="btn btn-rounded btn-success btn-xs" onclick="downloadFile(\''.$row->file.'\'); return false;">
						Download
					</button>';
				}

				$data[] = [
					$no++, 
					$row->judul, 
					$this->AppModel->DateIndo($row->tanggal), 
					$row->jenis_media, 
					$file, 
					$this->btn_aksi($row->id, $editable)
				];
			}
		} else if($luaran == 7) {
			foreach($sql->result() as $row) {
				if($row->file == NULL) {
					$file = '<i class="fa fa-times text-danger"></i>';
				} else {
					$file = '<button type="button" class="btn btn-rounded btn-success btn-xs" onclick="downloadFile(\''.$row->file.'\'); return false;">
						Download
					</button>';
				}

				$data[] = [
					$no++,
					$row->nama_forum,
					$row->judul,
					$row->penyelenggara,
					$file,
					$this->btn_aksi($row->id, $editable)
				];
			}
		} else if($luaran == 9) {
			foreach($sql->result() as $row) {
				if($row->file == NULL) {
					$file = '<i class="fa fa-times text-danger"></i>';
				} else {
					$file = '<button type="button" class="btn btn-rounded btn-success btn-xs" onclick="downloadFile(\''.$row->file.'\'); return false;">
						Download
					</button>';
				}

				$data[] = [
					$no++,
					$row->jenis_luaran,
					$row->nama_luaran,
					$file,
					$this->btn_aksi($row->id, $editable)
				];
			}
		} else if($luaran == 10 || $luaran == 11) {
            foreach($sql->result() as $row) {
				if($row->file == NULL) {
					$file = '<i class="fa fa-times text-danger"></i>';
				} else {
					$file = '<button type="button" class="btn btn-rounded btn-success btn-xs" onclick="downloadFile(\''.$row->file.'\'); return false;">
						Download
					</button>';
				}

				$data[] = [
					$no++,
					$row->no_sertifikat,
					$row->nama_produk,
					$row->lembaga,
					$file,
					$this->btn_aksi($row->id, $editable)
				];
			}
        } else if($luaran == 12) {
			foreach($sql->result() as $row) {
				if($row->file == NULL) {
					$file = '<i class="fa fa-times text-danger"></i>';
				} else {
					$file = '<button type="button" class="btn btn-rounded btn-success btn-xs" onclick="downloadFile(\''.$row->file.'\'); return false;">
						Download
					</button>';
				}

				$data[] = [
					$no++,
					$row->no_badan_hukum,
					$row->nama_mitra,
					$row->bid_usaha,
					$file,
					$this->btn_aksi($row->id, $editable)
				];
			}
        } else if($luaran == 13) {
			foreach($sql->result() as $row) {
				if($row->file == NULL) {
					$file = '<i class="fa fa-times text-danger"></i>';
				} else {
					$file = '<button type="button" class="btn btn-rounded btn-success btn-xs" onclick="downloadFile(\''.$row->file.'\'); return false;">
						Download
					</button>';
				}

				$data[] = [
					$no++,
					$row->judul_buku,
					$row->isbn,
					$row->bid_usaha,
					$file,
					$this->btn_aksi($row->id, $editable)
				];
			}
        } else if($luaran == 14) {
            foreach($sql->result() as $row) {
				if($row->file == NULL) {
					$file = '<i class="fa fa-times text-danger"></i>';
				} else {
					$file = '<button type="button" class="btn btn-rounded btn-success btn-xs" onclick="downloadFile(\''.$row->file.'\'); return false;">
						Download
					</button>';
				}

				$data[] = [
					$no++,
					$row->wirausaha,
					$row->deskripsi,
					$file,
					$this->btn_aksi($row->id, $editable)
				];
			}
        }
		
		$result = array(
			"draw" => $draw,
			"recordsTotal" => $sql->num_rows(),
			"recordsFiltered" => $sql->num_rows(),
			"data" => $data
		);

	   	echo json_encode($result);
		exit();
	}

	public function get_detail_luaran($id) {
		$sql = $this->LuaranModel->GetDetailLuaran($id);

		$output = "<table class='table'>";
		foreach($sql->result() as $row) {
			$output .= "<tr>
				<th width='170'>".$row->prop."</th>
				<td>".$row->value."</td>
			</tr>";
		}
		$output .= "</table>";

		$data = ["output" => $output];
		echo json_encode($data);
	}

	public function get_luaran_edit($id) {
		$form = $this->AppModel->GetPropertiesFormLuaran($id, "edit");
		$output = "";

		foreach($form as $row) {
			$output .= '
				<div class="form-group">
					<label class="col-lg-3 control-label">'.$row['label'].'</label>
					<div class="col-lg-8">
						'.$row['form'].'
					</div>
				</div>
			';
		}

		$data = ["output" => $output];
		echo json_encode($data);
	}

	public function show_filter_luaran_penelitian($id_jenis) {
		$output = '';
		if($id_jenis == 1) {
			$output .= '
			<thead>
				<tr>
					<th width="50">No</th>
					<th width="200">Nama Jurnal / Pros</th>
					<th>Jenis</th>
					<th>Tingkat</th>
					<th>Status</th>
					<th>Edisi</th>
					<th>File</h>
					<th></th>
				</tr>
			</thead>
			<tbody></tbody>
			';
		} else if($id_jenis == 2) {
			$output .= '
			<thead>
				<tr>
					<th>No</th>
					<th>Mata Kuliah</th>
					<th>Bentuk Integrasi</th>
					<th>File</th>
					<th></th>
				</tr>
			</thead>
			<tbody></tbody>
			';
		} else if($id_jenis == 3) {
			$output .= '
			<thead>
				<tr>
					<th>No</th>
					<th>No Pendaftaran</th>
					<th>Nama HKI</th>
					<th>Jenis HKI</th>
					<th>Status</th>
					<th></th>
				</tr>
			</thead>
			<tbody></tbody>
			';
		} else if($id_jenis == 4) {
			$output .= '
			<thead>
				<tr>
					<th>No</th>
					<th>Jenis Luaran</th>
					<th>Deskripsi</th>
					<th></th>
				</tr>
			</thead>
			<tbody></tbody>
			';
		}

		$data['output'] = $output;
		echo json_encode($data);
	}

	public function show_filter_luaran_pengabdian($id_jenis) {
		$output = '';
		if($id_jenis == 5) {
			$output .= '
			<thead>
				<tr>
					<th width="50">No</th>
					<th width="200">Nama Jurnal / Pros</th>
					<th>Jenis</th>
					<th>Tingkat</th>
					<th>Status</th>
					<th>Edisi</th>
					<th>File</h>
					<th></th>
				</tr>
			</thead>
			<tbody></tbody>
			';
		} else if($id_jenis == 6) {
			$output .= '
			<thead>
				<tr>
					<th>No</th>
					<th>Judul</th>
					<th>Tanggal</th>
					<th>Jenis Media</th>
					<th>File</th>
					<th></th>
				</tr>
			</thead>
			<tbody></tbody>
			';
		} else if($id_jenis == 7) {
			$output .= '
			<thead>
				<tr>
					<th>No</th>
					<th>Nama Forum</th>
					<th>Judul</th>
					<th>Penyelenggara</th>
					<th>File</th>
					<th></th>
				</tr>
			</thead>
			<tbody></tbody>
			';
		} else if($id_jenis == 8) {
			$output .= '
			<thead>
				<tr>
					<th>No</th>
					<th>No Pendaftaran</th>
					<th>Judul HKI</th>
					<th>Jenis HKI</th>
					<th>File</th>
					<th></th>
				</tr>
			</thead>
			<tbody></tbody>
			';
		} else if($id_jenis == 9) {
			$output .= '
			<thead>
				<tr>
					<th>No</th>
					<th>Jenis Luaran</th>
					<th>Nama Luaran</th>
					<th>File</th>
					<th></th>
				</tr>
			</thead>
			<tbody></tbody>
			';
		} else if($id_jenis == 10 || $id_jenis == 11) {
			$output .= '
			<thead>
				<tr>
					<th>No</th>
					<th>No Sertifikat</th>
					<th>Nama Produk</th>
					<th>Lembaga</th>
					<th>File</th>
					<th></th>
				</tr>
			</thead>
			<tbody></tbody>
			';
		} else if($id_jenis == 12) {
			$output .= '
			<thead>
				<tr>
					<th>No</th>
					<th>No Badan Hukum</th>
					<th>Nama Mitra</th>
					<th>Bidang Usaha</th>
					<th>File</th>
					<th></th>
				</tr>
			</thead>
			<tbody></tbody>
			';
		} else if($id_jenis == 13) {
			$output .= '
			<thead>
				<tr>
					<th>No</th>
					<th>Judul Buku</th>
					<th>ISBN</th>
					<th>Penerbit</th>
					<th>File</th>
					<th></th>
				</tr>
			</thead>
			<tbody></tbody>
			';
		} else if($id_jenis == 14) {
			$output .= '
			<thead>
				<tr>
					<th>No</th>
					<th>Wirausaha</th>
					<th>Deskripsi</th>
					<th>File</th>
					<th></th>
				</tr>
			</thead>
			<tbody></tbody>
			';
		}

		$data['output'] = $output;
		echo json_encode($data);
	}

	public function show_form_luaran($id_jenis) {
		$output = "";
		$html = $this->AppModel->GetPropertiesFormLuaran($id_jenis);
		foreach($html as $row) {
			$output .= '
				<div class="form-group">
					<label class="col-lg-3 control-label">'.$row['label'].'</label>
					<div class="col-lg-8">
						'.$row['form'].'
					</div>
				</div>
			';
		}
		$data = ["output" => $output];
		echo json_encode($data);
	}

	public function btn_aksi($id,$editable = false) {
		$aksi_btn = '<div class="btn-group">
			<i class="glyphicon glyphicon-cog dropdown-toggle" data-toggle="dropdown" aria-expanded="false"></i>
			<ul class="dropdown-menu pull-right" role="menu">
				<li>
					<a href="#" onclick="detailLuaran('.$id.')">Detail</a>
				</li>';
		if($editable) {
			$aksi_btn .= '
				<li>
					<a href="#" onclick="editLuaran('.$id.')">Edit</a>
				</li>
				<li>
					<a href="#" onclick="hapusLuaran('.$id.')">Hapus</a>
				</li>
			';
		}
		$aksi_btn .= '</ul></div>';

		return $aksi_btn;
	}
}
?>