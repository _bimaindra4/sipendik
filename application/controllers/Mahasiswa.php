<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mahasiswa extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model("MahasiswaModel");
    }
    
	public function get_data_mahasiswa($nrp) {
		$sql = $this->MahasiswaModel->GetMhsByNRP($nrp);
		echo json_encode($sql);
	}
}
?>
