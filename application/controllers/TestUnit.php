<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TestUnit extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->library('unit_test');
        $this->load->model("PenelitianModel");
        $this->load->model("UsulanModel");
        $this->load->model("TanggunganModel");
        $this->load->model("PendanaanModel");
        $this->load->model("DosenModel");
    }

    function test_get_data_penelitian() {
        $test = $this->PenelitianModel->GetDataPenelitian(NULL, 1);
        $ex_result = ["id", "periode", "judul", "jenis", "tgl_mulai_berlaku", "tgl_selesai_berlaku", "dana", "tahapan", "prodi"];
        $ex_field = array_keys($test);
        $test_name = "Test Field Data Penelitian";
        echo $this->unit->run($ex_field, $ex_result, $test_name);
    }

    function test_get_detail_penelitian() {
        $test = $this->PenelitianModel->GetDataPenelitian(NULL, 1);
        $ex_result = [
            "id" => 1,
            "judul" => "Perancangan Animasi Perubahan Bentuk Tokoh Garuda di Indonesia",
            "prodi" => "DK-S1",
            "jenis" => "Penelitian Eksternal"
        ];

        echo $this->unit->run($test->id, $ex_result['id'], "Test Data ID");
        echo $this->unit->run($test->judul, $ex_result['judul'], "Test Data Judul");
        echo $this->unit->run($test->prodi, $ex_result['prodi'], "Test Data Prodi");
        echo $this->unit->run($test->jenis, $ex_result['jenis'], "Test Data Jenis");
    }

    function test_get_usulan_dosen() {
        $filter = ["id" => "010124"];
        $test = $this->UsulanModel->GetDataUsulanByDosen($filter);
        $ex_result = ["id", "tgl_pengajuan", "judul", "jenis_id", "jenis", "tgl_mulai_berlaku", "tgl_selesai_berlaku", "inisiator", "periode", "nama_periode", "usulan", "accepted"];
        $ex_field = array_keys(reset($test));
        $test_name = "Test Field Data Usulan Dosen";
        echo $this->unit->run($ex_field, $ex_result, $test_name);
    }

    function test_get_tanggungan_dosen() {
        $filter = ["prodi" => "TI-S1"];
        $test = $this->TanggunganModel->GetDataTanggungan($filter)->result_array();
        $ex_result = ["id", "judul", "ketua", "kegiatan", "periode", "tahapan", "keterlambatan", "sk", "lpj"];
        $ex_field = array_keys(reset($test));
        $test_name = "Test Field Data Tanggungan Dosen";
        echo $this->unit->run($ex_field, $ex_result, $test_name);
    }

    function test_get_pendanaan_dosen() {
        $filter = [
            "prodi" => "TI-S1",
            "periode" => "7",
            "kegiatan" => "1"
        ];
        $test = $this->PendanaanModel->GetDataPendanaan($filter)->result_array();
        $ex_result = ["judul", "sumber_dana", "dana"];
        $ex_field = array_keys(reset($test));
        $test_name = "Test Field Data Pendanaan Dosen";
        echo $this->unit->run($ex_field, $ex_result, $test_name);
    }

    function test_get_data_dosen() {
        $test = $this->DosenModel->GetDataDosenPPM();
        $ex_result = ["nip", "nidn", "nama", "foto", "prodi", "penelitian", "pengabdian"];
        $ex_field = array_keys(reset($test));
        $test_name = "Test Field Data Dosen";
        echo $this->unit->run($ex_field, $ex_result, $test_name);
    }

    function test_get_produktivitas_dosen() {
        $test = $this->DosenModel->GetProduktivitasDosen();
        $ex_result = ["nip", "nama", "foto", "penelitian", "pengabdian"];
        $ex_field = array_keys(reset($test));
        $test_name = "Test Field Produktivitas Dosen";
        echo $this->unit->run($ex_field, $ex_result, $test_name);
    }
}

?>