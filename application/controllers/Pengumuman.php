<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengumuman extends CI_Controller {
	protected $roles_user;

	// session
	protected $sess;
	protected $sess_not_con;
	protected $sess_data;

	public function __construct() {
		parent::__construct();
		$this->load->model("AppModel");
		$this->load->model("SessionModel");
		$this->load->model("PengumumanModel");
		$this->load->model("NotifikasiModel");

		$this->sess = $this->SessionModel->GetSession();
		$this->sess_not_con = !$this->sess['session_userid'] && !$this->sess['session_role'];
		$this->roles_user = explode(",", $this->sess['session_role']);
        $this->sess_data = [
			"userid" => $this->sess['session_userid'],
			"role" => $this->sess['session_role'],
			"nama" => $this->sess['session_nama'],
			"email" => $this->sess['session_email'],
			"foto" => $this->sess['session_foto'],
			"notifikasi" => $this->NotifikasiModel->GetNotifikasi($this->sess['session_userid'], $this->sess['session_role'])
		];
	}

	public function index() {
		if($this->sess_not_con) {
			redirect("login");
		} else {
			$this->sess_data['judul'] = "SIPENDIK - Pengumuman";
			$this->sess_data['active_menu'] = 'pengumuman';
			$data['header'] = $this->load->view("layout/layout-header", $this->sess_data, TRUE);
			$data['footer'] = $this->load->view("layout/layout-footer", $this->sess_data, TRUE);

			if(in_array('1', $this->roles_user)) {
				$data['pengumuman'] = $this->PengumumanModel->GetPengumuman();
				$this->load->view("unit/pengumuman", $data);
			}
		}
	}

	public function tambah() {
		if($this->sess_not_con) {
			redirect("login");
		} else {
			if(in_array('1', $this->roles_user)) {
				$data = [
					"judul" => $this->input->post("judul"),
					"isi" => $this->input->post("isi"),
					"kategori" => $this->input->post("kategori"),
					"Created_By" => "Unit LPPM",
					"Created_Date" => $this->AppModel->DateTimeNow()
				];

				$sql = $this->PengumumanModel->TambahPengumuman($data);
				redirect("pengumuman");
			}
		}
	}

	public function edit($id) {
		if($this->sess_not_con) {
			redirect("login");
		} else {
			if(in_array('1', $this->roles_user)) {
				$data = [
					"judul" => $this->input->post("judul"),
					"isi" => $this->input->post("isi"),
					"kategori" => $this->input->post("kategori")
				];

				$sql = $this->PengumumanModel->EditPengumuman($data, $id);
				redirect("pengumuman");
			}
		}
	}

	public function hapus($id) {
		if($this->sess_not_con) {
			redirect("login");
		} else {
			if(in_array('1', $this->roles_user)) {
				$sql = $this->PengumumanModel->HapusPengumuman($id);
				redirect("pengumuman");
			}
		}
	}

	public function get_data_pengumuman($id) {
		$sql = $this->PengumumanModel->GetPengumuman($id);
		echo json_encode($sql);
	}
}
?>