<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tanggungan extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model("AppModel");
		$this->load->model("SessionModel");
		$this->load->model("NotifikasiModel");
		$this->load->model("TanggunganModel");

		$this->sess = $this->SessionModel->GetSession();
		$this->sess_not_con = !$this->sess['session_userid'] && !$this->sess['session_role'];
		$this->roles_user = explode(",", $this->sess['session_role']);
        $this->sess_data = [
			"userid" => $this->sess['session_userid'],
			"role" => $this->sess['session_role'],
			"nama" => $this->sess['session_nama'],
			"email" => $this->sess['session_email'],
			"foto" => $this->sess['session_foto'],
			"notifikasi" => $this->NotifikasiModel->GetNotifikasi($this->sess['session_userid'], $this->sess['session_role'])
		];
	}

	public function index() {
		if($this->sess_not_con) {
			redirect("login");
		} else {
			$this->sess_data['judul'] = "SIPENDIK - Data Tanggungan Dosen";
			$this->sess_data['active_menu'] = 'tanggungan';
			$data['header'] = $this->load->view("layout/layout-header", $this->sess_data, TRUE);
            $data['footer'] = $this->load->view("layout/layout-footer", $this->sess_data, TRUE);

            if(in_array('2', $this->roles_user)) {
				$data['tanggungan'] = $this->TanggunganModel->GetTanggunganDosen($this->sess_data['userid']);
                $this->load->view("dosen/tanggungan", $data);
            } else if(in_array('3', $this->roles_user)) {
                $data['tanggungan'] = $this->TanggunganModel->GetTanggunganProdi($this->sess_data['userid']);
                $this->load->view("prodi/tanggungan", $data);
            } else if(in_array('4', $this->roles_user)) {
                $data['tanggungan'] = $this->TanggunganModel->GetDataTanggungan();
                $this->load->view("prodi/tanggungan", $data);
			}
        }
    }
}
?>
