<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notifikasi extends CI_Controller {
	protected $roles_user;

	// session
	protected $sess;
	protected $sess_not_con;
	protected $sess_data;

	public function __construct() {
		parent::__construct();
		$this->load->model("AppModel");
        $this->load->model("SessionModel");
        $this->load->model("NotifikasiModel");

		$this->sess = $this->SessionModel->GetSession();
		$this->sess_not_con = !$this->sess['session_userid'] && !$this->sess['session_role'];
		$this->roles_user = explode(",", $this->sess['session_role']);
        $this->sess_data = [
			"userid" => $this->sess['session_userid'],
			"role" => $this->sess['session_role'],
			"nama" => $this->sess['session_nama'],
			"email" => $this->sess['session_email'],
			"foto" => $this->sess['session_foto'],
		];
	}

	public function remove_status_notif() {
        $data = [
            "user_id_to" => $this->sess_data['userid'],
            "user_role_to" => $this->sess_data['role']
        ];

        $sql = $this->NotifikasiModel->RemoveStatusNotifikasi($data);

        echo json_encode($sql);
	}
}
?>
