<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	protected $roles_user;

	// session
	protected $sess;
	protected $sess_not_con;
	protected $sess_data;

	public function __construct() {
		parent::__construct();
		$this->load->model("AppModel");
		$this->load->model("SessionModel");
		$this->load->model("DosenModel");
		$this->load->model("PengabdianModel");
		$this->load->model("PenelitianModel");
		$this->load->model("UsulanModel");
		$this->load->model("NotifikasiModel");
		$this->load->model("TanggunganModel");
		$this->load->model("PengumumanModel");

		$this->sess = $this->SessionModel->GetSession();
		$this->sess_not_con = !$this->sess['session_userid'] && !$this->sess['session_role'];
		$this->roles_user = explode(",", $this->sess['session_role']);
        $this->sess_data = [
			"userid" => $this->sess['session_userid'],
			"role" => $this->sess['session_role'],
			"nama" => $this->sess['session_nama'],
			"email" => $this->sess['session_email'],
			"foto" => $this->sess['session_foto'],
			"notifikasi" => $this->NotifikasiModel->GetNotifikasi($this->sess['session_userid'], $this->sess['session_role'])
		];
	}

	public function index() {
		if($this->sess_not_con) {
			redirect("login");
		} else {
			$this->sess_data['judul'] = "SIPENDIK - Dashboard";
			$this->sess_data['active_menu'] = 'beranda';
			$data['header'] = $this->load->view("layout/layout-header", $this->sess_data, TRUE);
			$data['footer'] = $this->load->view("layout/layout-footer", $this->sess_data, TRUE);
			$data['pengumuman'] = $this->PengumumanModel->GetPengumuman();

			if(in_array('1', $this->roles_user)) {
				$data['pn'] = $this->PenelitianModel->GetDataPenelitian();
				$data['pg'] = $this->PengabdianModel->GetAllDataPengabdian();
				$data['usulan'] = $this->UsulanModel->GetDataUsulan();
				$this->load->view("unit/index", $data);
			} else if(in_array('2', $this->roles_user)) {
				$data['pn'] = $this->DosenModel->GetDataDosenPenelitian($this->sess_data['userid']);
				$data['pg'] = $this->DosenModel->GetDataDosenPengabdian($this->sess_data['userid']);
				$data['tanggungan'] = $this->DosenModel->GetDataKegiatanDosenByStatus($this->sess_data['userid'], "Tanggungan");
				$this->load->view("dosen/index", $data);
			} else if(in_array('3', $this->roles_user)) {
				$filter['prodi'] = [ $this->sess_data['userid'] ];
				$data['pn'] = $this->PenelitianModel->GetDataPenelitian($filter);
				$data['pg'] = $this->PengabdianModel->GetAllDataPengabdian($filter);
				$data['tanggungan'] = $this->TanggunganModel->GetTanggunganProdi($this->sess_data['userid']);
				$this->load->view("prodi/index", $data);
			} else if(in_array('4', $this->roles_user)) {
				$data['pn'] = $this->PenelitianModel->GetDataPenelitian();
				$data['pg'] = $this->PengabdianModel->GetAllDataPengabdian();
				$data['tanggungan'] = $this->TanggunganModel->GetDataTanggungan();
				$this->load->view("pimpinan/index", $data);
			}
		}
	}
}
?>
