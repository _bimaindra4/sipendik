<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usulan extends CI_Controller {
	protected $roles_user;

	// session
	protected $sess;
	protected $sess_not_con;
	protected $sess_data;

	public function __construct() {
		parent::__construct();
		$this->load->model("AppModel");
		$this->load->model("SessionModel");
		$this->load->model("UsulanModel");
		$this->load->model("PenelitianModel");
		$this->load->model("PengabdianModel");
		$this->load->model("NotifikasiModel");

		$this->sess = $this->SessionModel->GetSession();
		$this->sess_not_con = !$this->sess['session_userid'] && !$this->sess['session_role'];
		$this->roles_user = explode(",", $this->sess['session_role']);
        $this->sess_data = [
			"userid" => $this->sess['session_userid'],
			"role" => $this->sess['session_role'],
			"nama" => $this->sess['session_nama'],
			"email" => $this->sess['session_email'],
			"foto" => $this->sess['session_foto'],
			"notifikasi" => $this->NotifikasiModel->GetNotifikasi($this->sess['session_userid'], $this->sess['session_role'])
		];
	}

	public function index() {
		if($this->sess_not_con) {
			redirect("login");
		} else {
			$this->sess_data['judul'] = "SIPENDIK - Usulan Kegiatan";
			$this->sess_data['active_menu'] = 'usulan';
			$data['header'] = $this->load->view("layout/layout-header", $this->sess_data, TRUE);
			$data['footer'] = $this->load->view("layout/layout-footer", $this->sess_data, TRUE);

			if(in_array('1', $this->roles_user)) {
				$data['usulan'] = $this->UsulanModel->GetDataUsulan();
				$this->load->view("unit/usulan", $data);
			} else if(in_array('2', $this->roles_user)) {
				$filter = ["inisiator" => $this->sess_data['userid']];
				$data['usulan'] = $this->UsulanModel->GetDataUsulan($filter);
				$this->load->view("dosen/usulan", $data);
			} else if(in_array('3', $this->roles_user)) {
				$filter = ["prodi" => $this->sess_data['userid']];
				$data['usulan'] = $this->UsulanModel->GetDataUsulan($filter);
				$this->load->view("prodi/usulan", $data);
			} else if(in_array('4', $this->roles_user)) {
				$data['usulan'] = $this->UsulanModel->GetDataUsulan();
				$this->load->view("pimpinan/usulan", $data);
			}
		}
	}

	public function histori() {
		if($this->sess_not_con) {
			redirect("login");
		} else {
			$this->sess_data['judul'] = "SIPENDIK - Histori Usulan";
			$this->sess_data['active_menu'] = 'usulan';
			$data['header'] = $this->load->view("layout/layout-header", $this->sess_data, TRUE);
			$data['footer'] = $this->load->view("layout/layout-footer", $this->sess_data, TRUE);

			if(in_array('3', $this->roles_user)) {
				$data['usulan'] = $this->UsulanModel->GetHistoriUsulan($this->sess_data['userid']);
				$this->load->view("prodi/usulan_histori", $data);
			} else if(in_array('4', $this->roles_user)) {
				$data['usulan'] = $this->UsulanModel->GetHistoriUsulan();
				$this->load->view("prodi/usulan_histori", $data);
			}
		}
	}

	public function get_data_usulan($id) {
		$output = "";
		$filter = ["id" => $id];
		$usulan = $this->UsulanModel->GetDataUsulan($filter);
		if($usulan->jenis_id == 1) {
			$pn = $this->PenelitianModel->GetDataPenelitian(NULL, $id)->row();
			$output .= '
			<table class="table">
				<tr>
					<th width="200">Judul Penelitian</th>
					<td>'.$pn->judul.'</td>
				</tr>
				<tr>
					<th width="200">Jenis Penelitian</th>
					<td>'.$pn->jenis.'</td>
				</tr>
				<tr>
					<th>Pelaksanaan Penelitian</th>
					<td>'.$this->AppModel->DateIndo($pn->tgl_mulai_berlaku).' - '.$this->AppModel->DateIndo($pn->tgl_selesai_berlaku).'</td>
				</tr>
				<tr>
					<th>Bidang Penelitian</th>
					<td>'.$pn->bidang_penelitian.'</td>
				</tr>
				<tr>
					<th>Tujuan Sosial Ekonomi</th>
					<td>'.$pn->tujuan_penelitian.'</td>
				</tr>
				<tr>
					<th>Sumber Dana</th>
					<td>'.$pn->sumber_dana.'</td>
				</tr>
				<tr>
					<th>Institusi Sumber Dana</th>
					<td>'.$pn->institusi_sumber_dana.'</td>
				</tr>
				<tr>
					<th>Jumlah Dana</th>
					<td>Rp. '.number_format($pn->dana).'</td>
				</tr>
				<tr>
					<th>Inisiator</th>
					<td>'.$pn->inisiator.'</td>
				</tr>
			</table>
			';
		} else if($usulan->jenis_id == 2) {
			$pg = $this->PengabdianModel->GetDetailDataPengabdian($id);
			
			if($pg->tgl_mulai_berlaku == $pg->tgl_selesai_berlaku) {
				$tanggal = $this->AppModel->DateIndo($pg->tgl_mulai_berlaku);
			} else {
				$tanggal = $this->AppModel->DateIndo($pg->tgl_mulai_berlaku)." s/d ".$this->AppModel->DateIndo($pg->tgl_selesai_berlaku);
			}

			$output .= '
			<table class="table">
				<tr>
					<th width="200">Judul Pengabdian</th>
					<td>'.$pg->judul.'</td>
				</tr>
				<tr>
					<th width="200">Jenis Kegiatan</th>
					<td>'.$pg->jenis_keg.'</td>
				</tr>
				<tr>
					<th>Tingkat</th>
					<td>'.$pg->tingkat.'</td>
				</tr>
				<tr>
					<th>Jumlah Dana</th>
					<td>'."Rp. ".number_format($pg->dana).'</td>
				</tr>
				<tr>
					<th>Sumber Dana</th>
					<td>'.$pg->sumber_dana.'</td>
				</tr>
				<tr>
					<th>Waktu Kegiatan</th>
					<td>'.$tanggal.'</td>
				</tr>
				<tr>
					<th>Sumber Daya Iptek</th>
					<td>'.$pg->sdi.'</td>
				</tr>
				<tr>
					<th>Inisiator</th>
					<td>'.$pg->inisiator.'</td>
				</tr>
			</table>
			';
		}

		$data = ["output" => $output];
		echo json_encode($data);
	}

	public function ubah_status_usulan($id) {
		if($this->sess_not_con) {
			redirect("login");
		} else {
			if(in_array('1', $this->roles_user)) {
				$filter = ["id" => $id];
				$usulan = $this->UsulanModel->GetDataUsulan($filter);
				$stts = $this->input->post("status");
				$revisi = $this->input->post("revisi");

				$data = ["accepted" => $stts];
				$sql = $this->UsulanModel->UbahStatusAccepted($data, $usulan->periode, $id);

				if($stts == 2) {
					// Tambah revisi
					$data = [
						"riset" => $id,
						"keterangan" => $revisi,
						"tanggal" => date("Y-m-d"),
						"status" => 1
					];
					$this->PenelitianModel->TambahRevisiPenelitian($data);
				}

				if($sql) {
					$this->session->set_flashdata("status", "sukses");
					$this->session->set_flashdata("message", "Data usulan berhasil diproses");

					// Notifikasi
					$dataNotif = [
						"user_id_from" => 1,
						"user_role_from" => 1,
						"user_id_to" => $usulan->nip_inisiator,
						"user_role_to" => 2,
						"jenis" => "hasil_usulan",
						"reference" => $usulan->id,
						"hasil" => $stts
					];
					$this->NotifikasiModel->TambahNotifikasi($dataNotif);
				} else {
					$this->session->set_flashdata("status", "gagal");
					$this->session->set_flashdata("message", "Terjadi kesalahan pada saat proses data");
				}
				
				redirect("usulan");
			}
		}
	} 
}
?>