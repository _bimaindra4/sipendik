<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penelitian extends CI_Controller {
	protected $roles_user;

	// session
	protected $sess;
	protected $sess_not_con;
	protected $sess_data;

	public function __construct() {
		parent::__construct();
		$this->load->model("AppModel");
		$this->load->model("SessionModel");
		$this->load->model("LuaranModel");
		$this->load->model("DosenModel");
		$this->load->model("PenelitianModel");
		$this->load->model("PeriodeModel");
		$this->load->model("NotifikasiModel");

		$this->sess = $this->SessionModel->GetSession();
		$this->sess_not_con = !$this->sess['session_userid'] && !$this->sess['session_role'];
		$this->roles_user = explode(",", $this->sess['session_role']);
        $this->sess_data = [
			"userid" => $this->sess['session_userid'],
			"role" => $this->sess['session_role'],
			"nama" => $this->sess['session_nama'],
			"email" => $this->sess['session_email'],
			"foto" => $this->sess['session_foto'],
			"notifikasi" => $this->NotifikasiModel->GetNotifikasi($this->sess['session_userid'], $this->sess['session_role'])
		];
	}

	public function index() {
		if($this->sess_not_con) {
			redirect("login");
		} else {
			$this->sess_data['judul'] = "SIPENDIK - Data Penelitian";
			$this->sess_data['active_menu'] = 'penelitian';
			$data['header'] = $this->load->view("layout/layout-header", $this->sess_data, TRUE);
			$data['footer'] = $this->load->view("layout/layout-footer", $this->sess_data, TRUE);

			if(in_array('1', $this->roles_user)) {
				$data['penelitian'] = $this->PenelitianModel->GetDataPenelitian();
				$data['prodi'] = $this->AppModel->GetDataProdi();
				$data['periode'] = $this->AppModel->GetPeriode();
				$data['tahapan'] = $this->AppModel->GetDataTahapan();
				$data['jenis'] = $this->AppModel->GetOptionDropdown('[jenis-penelitian]');
				$this->load->view("unit/penelitian", $data);
			} else if(in_array('2', $this->roles_user)) {
				$data['penelitian'] = $this->DosenModel->GetDataDosenPenelitian($this->sess_data['userid']);
				$data['prodi'] = $this->AppModel->GetDataProdi();
				$data['periode'] = $this->AppModel->GetPeriode();
				$data['tahapan'] = $this->AppModel->GetDataTahapan();
				$data['jenis'] = $this->AppModel->GetOptionDropdown('[jenis-penelitian]');
				$this->load->view("dosen/penelitian", $data);
			} else if(in_array('3', $this->roles_user)) {
				$filter['prodi'] = [ $this->sess_data['userid'] ];
				$data['penelitian'] = $this->PenelitianModel->GetDataPenelitian($filter);
				$data['periode'] = $this->AppModel->GetPeriode();
				$data['tahapan'] = $this->AppModel->GetDataTahapan();
				$data['jenis'] = $this->AppModel->GetOptionDropdown('[jenis-penelitian]');
				$this->load->view("prodi/penelitian", $data);
			} else if(in_array('4', $this->roles_user)) {
				$data['penelitian'] = $this->PenelitianModel->GetDataPenelitian();
				$data['periode'] = $this->AppModel->GetPeriode();
				$data['tahapan'] = $this->AppModel->GetDataTahapan();
				$data['jenis'] = $this->AppModel->GetOptionDropdown('[jenis-penelitian]');
				$this->load->view("pimpinan/penelitian", $data);
			}
		}
	}

	public function detail($id) {
		if($this->sess_not_con) {
			redirect("login");
		} else {
			$pn = $this->PenelitianModel->GetDataPenelitian(NULL, $id)->row();
			$this->sess_data['judul'] = "SIPENDIK - ".$pn->judul;
			$this->sess_data['active_menu'] = 'penelitian';
			$data['header'] = $this->load->view("layout/layout-header", $this->sess_data, TRUE);
			$data['footer'] = $this->load->view("layout/layout-footer", $this->sess_data, TRUE);
			
			$data['pn'] = $pn;
			$data['luaran_penelitian'] = $this->LuaranModel->GetPilihanLuaran(1);
			$data['person_dosen_prop'] = $this->AppModel->GetPropertiesFormJenis(3);
			$data['person_mhs_prop'] = $this->AppModel->GetPropertiesFormJenis(4);
			$data['publikasi_prop'] = $this->AppModel->GetPropertiesFormLuaran(1);
			$data['integrasi_prop'] = $this->AppModel->GetPropertiesFormLuaran(2);
			$data['hki_prop'] = $this->AppModel->GetPropertiesFormLuaran(3);
			$data['lainnya_prop'] = $this->AppModel->GetPropertiesFormLuaran(4);
			$data['person_dosen'] = $this->PenelitianModel->GetPersonilPenelitian($id,"Pegawai");
			$data['person_mhs'] = $this->PenelitianModel->GetPersonilPenelitian($id,"Mahasiswa");
			$data['riset_tahap'] = $this->PenelitianModel->GetTahapanPenelitian($id);
			$data['periode_after'] = $this->PeriodeModel->GetPeriodeAfter($id);
			$data['periode_riset'] = $this->PeriodeModel->GetPeriodeRiset($id);
			$data['unit'] = $this->AppModel->GetDataUnit();
			$data['pegawai'] = $this->AppModel->GetDataPegawai();
			$data['tahapan'] = $this->AppModel->GetDataTahapan();
			$data['dokumen'] = $this->PenelitianModel->GetDokumenPenelitian($id);

			if(in_array('1', $this->roles_user)) {
				$data['kontributor'] = "LPPM";
				$this->load->view("unit/penelitian-detail", $data);
			} else if(in_array('2', $this->roles_user)) {
				$kontri = $this->DosenModel->GetDosenStatusAnggota($id, $this->sess_data["userid"]);
				$data['kontributor'] = $kontri;
				$this->load->view("unit/penelitian-detail", $data);
			} else if(in_array('3', $this->roles_user) || in_array('4', $this->roles_user)) {
				$data['kontributor'] = NULL;
				$this->load->view("unit/penelitian-detail", $data);
			}
		}
	}

	public function tambah() {
		if($this->sess_not_con) {
			redirect("login");
		} else {
			$this->sess_data['judul'] = "SIPENDIK - Tambah Data Penelitian";
			$this->sess_data['active_menu'] = 'penelitian';
			$data['header'] = $this->load->view("layout/layout-header", $this->sess_data, TRUE);
			$data['footer'] = $this->load->view("layout/layout-footer", $this->sess_data, TRUE);

			if(in_array('1', $this->roles_user)) {
				$data['prop'] = $this->AppModel->GetPropertiesFormJenis(1);
				$data['prodi'] = $this->AppModel->GetDataProdi();
				$data['periode'] = $this->PeriodeModel->GetPeriode();
				$this->load->view("unit/penelitian-tambah", $data);
			} else if(in_array('2', $this->roles_user)) {
				$data['prop'] = $this->AppModel->GetPropertiesFormJenis(1);
				$data['prodi'] = $this->AppModel->GetDataProdi();
				$data['periode'] = $this->PeriodeModel->GetPeriodeAktiv();
				$this->load->view("unit/penelitian-tambah", $data);
			}
		}
	}

	public function tambah_proses() {
		if($this->sess_not_con) {
			redirect("login");
		} else {
			if(in_array('1', $this->roles_user) || in_array('2', $this->roles_user)) {
				$uploadedFile = false;
				$config['upload_path'] = "./assets/files";
				$config['allowed_types'] = 'pdf';
				$config['encrypt_name'] = TRUE;
				
				$this->load->library('upload', $config);
				if($this->upload->do_upload("proposal")) {
					$data = array('upload_data' => $this->upload->data());
					$file = $data['upload_data']['file_name'];
					$uploaded = TRUE;
				} else {
					$file = NULL;
					$uploaded = FALSE;
				}

				$p = $this->input->post();

				$prodi = "";
				foreach($p["prodi"] as $row) {
					$prodi .= $row.",";
				}
				$prodi = rtrim($prodi,",");

				// Accepted & Initiator
				$accepted = ($this->sess_data['role'] == 1 ? 1 : NULL);
				$initiator = ($this->sess_data['role'] == 1 ? NULL : $this->sess_data['userid']);

				// Insert data riset
				$data = [
					"judul" => $p[1],
					"jenis" => 1,
					"inisiator" => $initiator,
					"person_ref" => "dosen",
					"prodi" => $prodi,
					"accepted" => $accepted,
					"usulan" => $file,
					"Created_Date" => $this->AppModel->DateTimeNow()
				];
				$riset_id = $this->PenelitianModel->TambahPenelitian($data);

				// Insert Kontributor Ketua for Initiator
				if($this->sess_data['role'] == 2) {
					$dataKetua = [
						"riset" => $riset_id,
						"person" => $this->sess_data['userid'],
						"person_ref" => "Pegawai",
						"jenis" => "Ketua"
					];
					$sql = $this->PenelitianModel->TambahPersonil($dataKetua);
				}

				// Insert Tahapan Periode
				$dataTahap = [
					"riset" => $riset_id,
					"periode" => $p["periode"],
					"tahapan" => 1
				];
				$sql = $this->PenelitianModel->TambahPenelitianTahap($dataTahap);

				unset($p["prodi"]);
				unset($p["periode"]);

				// Insert riset properti
				foreach($p as $key => $value) {
					$dataProp[] = [
						"riset" => $riset_id,
						"prop" => $key,
						"value" => $value
					];
				}
				$sql = $this->PenelitianModel->TambahPenelitianProp($dataProp);

				// Insert riset SK
				$exp_date = explode(" - ", $this->input->post("3"));
				$tgl_mulai = date("Y-m-d", strtotime($exp_date[0]));
				$tgl_selesai = date("Y-m-d", strtotime($exp_date[1]));

				$dataSK = [
					"riset" => $riset_id,
					"no_sk" => NULL,
					"tentang" => NULL,
					"tgl_ditetapkan" => NULL,
					"tgl_mulai_berlaku" => $tgl_mulai,
					"tgl_selesai_berlaku" => $tgl_selesai,
					"penerbit" => NULL
				];
				$sql = $this->PenelitianModel->TambahPenelitianSK($dataSK);

				if($sql) {
					// Notifikasi
					if($this->sess_data['role'] == 2) {
						$dataNotif = [
							"user_id_from" => $this->sess_data['userid'],
							"user_role_from" => 2,
							"user_id_to" => 1,
							"user_role_to" => 1,
							"jenis" => "usulan_pn",
							"reference" => $riset_id
						];
						$this->NotifikasiModel->TambahNotifikasi($dataNotif);
					}
					
					if($uploaded == TRUE) {
						$msg_upload = ", dan file berhasil di upload";
					} else {
						$msg_upload = ", tetapi file proposal tidak berhasil di upload";
					}

					$this->session->set_flashdata("status", "sukses");
					$this->session->set_flashdata("message", "Data penelitian berhasil ditambahkan".$msg_upload);
					redirect('penelitian/detail/'.$riset_id);
				} else {
					$this->session->set_flashdata("status", "gagal");
					$this->session->set_flashdata("message", "Terjadi kesalahan pada saat input data");
					redirect('penelitian/tambah');
				}
			}
		}
	}

	public function edit($id) {
		if($this->sess_not_con) {
			redirect("login");
		} else {
			$this->sess_data['judul'] = "SIPENDIK - Edit Data Penelitian";
			$this->sess_data['active_menu'] = 'penelitian';
			$data['header'] = $this->load->view("layout/layout-header", $this->sess_data, TRUE);
			$data['footer'] = $this->load->view("layout/layout-footer", $this->sess_data, TRUE);

			if(in_array('1', $this->roles_user) || in_array('2', $this->roles_user)) {
				$data['prop'] = $this->AppModel->GetPropertiesFormJenis($id, "edit");
				$data['pn'] = $this->PenelitianModel->GetDataPenelitian(NULL, $id)->row();
				$data['prodi'] = $this->AppModel->GetDataProdi();
				$data['periode'] = $this->AppModel->GetPeriode();
				$this->load->view("unit/penelitian-edit", $data);
			}
		}
	}

	public function edit_proses($id) {
		if($this->sess_not_con) {
			redirect("login");
		} else {
			if(in_array('1', $this->roles_user) || in_array('2', $this->roles_user)) {
				$p = $this->input->post();

				$prodi = "";
				foreach($p["prodi"] as $row) {
					$prodi .= $row.",";
				}
				$prodi = rtrim($prodi,",");

				// Edit data riset
				$data = [
					"judul" => $p[1],
					"prodi" => $prodi
				];
				$sql = $this->PenelitianModel->EditPenelitian($data, $id);

				unset($p["prodi"]);
				
				// Cek jika ada skim
				if(array_key_exists(73, $p)) {
					$dataSkim[] = [
						"riset" => $id,
						"prop" => 73,
						"value" => $p[73]
					];

					$this->PenelitianModel->TambahPenelitianProp($dataSkim);
				} else {
					$this->db->delete("tb_rst_tr_riset_props", ["riset" => $id, "prop" => 73]);
				}

				// Edit riset properti
				foreach($p as $key => $value) {
					$dataProp = ["value" => $value];
					$dataWhere = ["riset" => $id, "prop" => $key];
					$sql = $this->PenelitianModel->EditPenelitianProp($dataProp, $dataWhere);
				}

				// Insert riset SK
				$exp_date = explode(" - ", $p[3]);
				$tgl_mulai = date("Y-m-d", strtotime($exp_date[0]));
				$tgl_selesai = date("Y-m-d", strtotime($exp_date[1]));

				$dataSK = [
					"tgl_mulai_berlaku" => $tgl_mulai,
					"tgl_selesai_berlaku" => $tgl_selesai,
				];
				$sql = $this->PenelitianModel->EditDataSK($dataSK, ["riset" => $id]);

				if($sql) {
					$this->session->set_flashdata("status", "sukses");
					$this->session->set_flashdata("message", "Data penelitian berhasil di edit");
					redirect('penelitian');
				} else {
					$this->session->set_flashdata("status", "gagal");
					$this->session->set_flashdata("message", "Terjadi kesalahan pada saat edit data");
					redirect('penelitian/edit/'.$id);
				}
			}
		}
	}

	public function hapus($id) {
		if($this->sess_not_con) {
			redirect("login");
		} else {
			if(in_array('1', $this->roles_user) || in_array('2', $this->roles_user)) {
				$sql = $this->PenelitianModel->HapusPenelitian($id);
				if($sql) {
					$this->session->set_flashdata("status", "sukses");
					$this->session->set_flashdata("message", "Data penelitian berhasil di hapus");
					redirect('penelitian');
				} else {
					$this->session->set_flashdata("status", "gagal");
					$this->session->set_flashdata("message", "Terjadi kesalahan pada saat edit data");
					redirect('penelitian');
				}
			}
		}
	}

	public function get_filter_penelitian() {
		$no = 1;
		$post = $this->input->post();
		$editable = TRUE;

		// Add case filter for user prodi
		if(in_array('3', $this->roles_user)) {
			$editable = FALSE;
			$post["prodi"] = [ $this->sess_data['userid'] ];
		}

		// Add case filter nip for user prodi and unit lppm
		if(in_array('2', $this->roles_user)) {
			$thead_anggota = '<th>Status Anggota</th>';
		} else {
			unset($post["nip"]);
			$thead_anggota = '';
		}

		$query = $this->PenelitianModel->GetDataPenelitian($post);
		$output = '
		<table id="penelitian" class="table table-striped table-hover table-bordered">
			<thead>
				<tr>
					<th>No</th>
					<th>Periode</th>
					<th width="250">Judul Kegiatan</th>
					<th>Jenis</th>
					<th>Tanggal</th>
					<th>Jumlah Dana</th>
					'.$thead_anggota.'
					<th style="width: 20px !important"></th>
				</tr>
			</thead>
			<tbody>';
			foreach($query->result() as $row) {
				$config = json_decode($row->config); 

				if($editable) {
					$editable_btn = '
					<li>
						<a href="'.site_url('penelitian/edit/'.$row->id).'">Edit</a>
					</li>
					<li>
						<a href="#" class="hapus-data" data-effect="mfp-flipInX" data-id="'.$row->id.'">Hapus</a>
					</li>
					';
				} else {
					$editable_btn = '';
				}

				if(in_array('2', $this->roles_user)) {
					$tbody_anggota = '<td>'.$row->status_anggota.'</td>';
				} else {
					$tbody_anggota = '';
				}

				$output .= '
				<tr>
					<td class="'.$config->label.'">'.$no++.'</td>
					<td>'.$row->periode.'</td>
					<td>'.$row->judul.'</td>
					<td>'.strtoupper(substr($row->jenis, 11)).'</td>
					<td>'.$this->AppModel->DateIndo($row->tgl_mulai_berlaku)." - ".$this->AppModel->DateIndo($row->tgl_selesai_berlaku).'</td>
					<td>Rp. '.number_format($row->dana).'</td>
					'.$tbody_anggota.'
					<td>
						<div class="btn-group">
							<i class="glyphicon glyphicon-cog dropdown-toggle" data-toggle="dropdown" aria-expanded="false"></i>
							<ul class="dropdown-menu pull-right" role="menu">
								<li>
									<a href="'.site_url('penelitian/detail/'.$row->id).'">Detail</a>
								</li>
								'.$editable_btn.'
							</ul>
						</div>
					</td>
				</tr>
				';
			}
			$output .= '
			</tbody>
		</table>';

		$data = ["output" => $output];
		echo json_encode($data);
	}

	public function tambah_personil_dosen($id) {
		$status_data = 0;
		$f = $this->input->post();
		$jenis = $this->AppModel->GetOptionValue($f[18]);
		$person = $f[15];
		if($jenis == "Ketua") {
			$init =  $this->PenelitianModel->CheckKetuaPenelitian($id);
			if($init) {
				$data = [
					"riset" => $id,
					"person" => $person,
					"person_ref" => "Pegawai",
					"jenis" => $jenis,
				];
		
				$sql = $this->PenelitianModel->TambahPersonil($data);
				$status_data = ($sql ? 1 : 2);
			} else {
				$status_data = 3;
			}
		} else if($jenis == "Anggota") {
			$data = [
				"riset" => $id,
				"person" => $person,
				"person_ref" => "Pegawai",
				"jenis" => $jenis,
			];
	
			$sql = $this->PenelitianModel->TambahPersonil($data);
			$status_data = ($sql ? 1 : 2);
		}

		if($status_data == 1) {
			$output = [
				"status" => true,
				"message" => "Data personil berhasil ditambahkan",
				"person_count" => $this->PenelitianModel->GetPersonilPenelitian($id,"Pegawai")->num_rows()
			];
		} else if($status_data == 2) {
			$output = [
				"status" => false,
				"message" => "Terjadi kesalahan pada saat input personil",
				"person_count" => $this->PenelitianModel->GetPersonilPenelitian($id,"Pegawai")->num_rows()
			];
		} else if($status_data == 3) {
			$output = [
				"status" => false,
				"message" => "Ketua peneliti sudah tersedia",
				"person_count" => $this->PenelitianModel->GetPersonilPenelitian($id,"Pegawai")->num_rows()
			];
		}

		echo json_encode($output);
	}

	public function tambah_personil_mhs($id) { 
		$f = $this->input->post();
		$data = [
			"riset" => $id,
			"person" => $f[19],
			"person_ref" => "Mahasiswa",
			"jenis" => "Anggota",
		];

		$sql = $this->PenelitianModel->TambahPersonil($data);
		if($sql) {
			$output = [
				"status" => true,
				"message" => "Data personil berhasil ditambahkan",
				"person_count" => $this->PenelitianModel->GetPersonilPenelitian($id,"Mahasiswa")->num_rows()
			];
		} else {
			$output = [
				"status" => false,
				"message" => "Terjadi kesalahan pada saat input personil",
				"person_count" => $this->PenelitianModel->GetPersonilPenelitian($id,"Mahasiswa")->num_rows()
			];
		}

		echo json_encode($output);
	}

	public function tambah_revisi($id) {
		$data = [
			"riset" => $id,
			"keterangan" => $this->input->post("deskripsi"),
			"tanggal" => date("Y-m-d")
		];

		$sql = $this->PenelitianModel->TambahRevisiPenelitian($data);
		if($sql) {
			$output = [
				"status" => true,
				"message" => "Revisi berhasil ditambahkan",
			];
		} else {
			$output = [
				"status" => false,
				"message" => "Terjadi kesalahan pada saat input revisi",
			];
		}

		echo json_encode($output);
	}

	public function tambah_dokumen($id) {
		$config['upload_path'] = "./assets/files";
		$config['allowed_types'] = 'pdf';
		$config['encrypt_name'] = TRUE;
		
		$this->load->library('upload', $config);
		if($this->upload->do_upload("dokumen")) {
			$output = "";
			$data = array('upload_data' => $this->upload->data());
			$file = $data['upload_data']['file_name'];

			$data = [
				"riset" => $id,
				"nama" => $this->input->post("nama"),
				"file" => $file
			];
			$sql = $this->PenelitianModel->TambahDokumen($data);

			$dokumen = $this->PenelitianModel->GetDokumenPenelitian($id);
			foreach($dokumen->result() as $row) {
				$output .= '
				<div class="col-sm-3 col-md-3">
					<div class="panel panel-tile text-center br-a">
						<div class="panel-heading hidden">
							<span class="panel-title"><i class="fa fa-pencil"></i> Title</span>
						</div>
						<div class="panel-body">
							<h1 class="fs60 mbn mtn text-success"><i class="fa fa-check"></i></h1>
							<h6 class="fs12">'.strtoupper($row->nama).'</h6>
						</div>
						<div class="panel-footer bg-success light br-n p12" style="cursor: pointer" onclick="downloadFile(\''.strtoupper($row->file).'\')">
							<span class="fs11">
								<i class="fa fa-arrow-down mr5"></i><b> DOWNLOAD</b>
							</span>
						</div>
					</div>
				</div>';
			}

			if($sql) {
				$output = [
					"status" => true,
					"status" => "Berhasil menambahkan dokumen",
					"output" => $output
				];
			} else {
				$output = [
					"status" => false,
					"status" => "Terjadi kesalahan pada saat upload data",
					"output" => NULL
				];
			}
		} else {
			$output = [
				"status" => false,
				"status" => "Terjadi kesalahan pada saat upload data",
				"output" => NULL
			];
		}

		echo json_encode($output);
	}

	public function tambah_status_penelitian($id) {
		$periode = $this->input->post("periode");
		$stts = $this->input->post("status");

		$data = [
			"riset" => $id,
			"periode" => $periode,
			"tahapan" => $stts 
		];
		
		$sql = $this->PenelitianModel->TambahStatusPenelitian($data);
		
		if($sql) {
			$data = [
				"status" => true,
				"message" => "Status penelitian berhasil di tambah",
			];
		} else {
			$data = [
				"status" => false,
				"message" => "Terjadi kesalahan pada saat tambah status penelitian",
			];
		}

		echo json_encode($data);
	}

	public function edit_dokumen($id) {
		$output = "";
		$uploaded = FALSE;
		$config['upload_path'] = "./assets/files";
		$config['allowed_types'] = 'pdf';
		$config['encrypt_name'] = TRUE;
		
		$this->load->library('upload', $config);
		if($this->upload->do_upload("dokumen_ed")) {
			$uploaded = TRUE;
			$data = array('upload_data' => $this->upload->data());
			$file = $data['upload_data']['file_name'];
			$data = [
				"nama" => $this->input->post("nama_dokumen_ed"),
				"file" => $file
			];
		}
		
		if($uploaded) {
			$sql = $this->PenelitianModel->EditDokumen($data,$id);
			$dokumen = $this->PenelitianModel->GetDokumenPenelitian($id);
			foreach($dokumen->result() as $row) {
				$output .= '
				<div class="col-sm-3 col-md-3">
					<div class="panel panel-tile text-center br-a">
						<div class="panel-heading hidden">
							<span class="panel-title"><i class="fa fa-pencil"></i> Title</span>
						</div>
						<div class="panel-body">
							<h1 class="fs60 mbn mtn text-success"><i class="fa fa-check"></i></h1>
							<h6 class="fs12">'.strtoupper($row->nama).'</h6>
						</div>
						<div class="row">
							<div class="col-md-4" style="padding-right: 0px">
								<div class="panel-footer bg-primary light br-n p12" style="cursor: pointer" onclick="editDokumen(\''.$row->id.'\')">
									<span class="fs11"><b><i class="fa fa-edit"></i></b></span>
								</div>
							</div>
							<div class="col-md-4" style="padding: 0px">
								<div class="panel-footer bg-success light br-n p12" style="cursor: pointer" onclick="downloadFile(\''.$row->file.'\')">
									<span class="fs11"><b><i class="fa fa-arrow-down"></i></b></span>
								</div>
							</div>
							<div class="col-md-4" style="padding-left: 0px">
								<div class="panel-footer bg-danger light br-n p12" style="cursor: pointer" onclick="hapusDokumen(\''.$row->id.'\')">
									<span class="fs11"><b><i class="fa fa-times"></i></b></span>
								</div>
							</div>
						</div>
					</div>
				</div>';
			}

			if($sql) {
				$output = [
					"status" => true,
					"message" => "Edit dokumen berhasil",
					"output" => $output
				];
			} else {
				$output = [
					"status" => false,
					"message" => "Terjadi kesalahan pada saat edit dokumen",
					"output" => NULL
				];
			}
		} else {
			$output = [
				"status" => false,
				"message" => "Terjadi kesalahan pada saat edit dokumen",
				"output" => NULL
			];
		}
		

		echo json_encode($output);
	}

	public function hapus_dokumen($id) {
		$output = '';
		$sql = $this->PenelitianModel->HapusDokumen($id);
		$dokumen = $this->PenelitianModel->GetDokumenPenelitian($id);
		foreach($dokumen->result() as $row) {
			$output .= '
			<div class="col-sm-3 col-md-3">
				<div class="panel panel-tile text-center br-a">
					<div class="panel-heading hidden">
						<span class="panel-title"><i class="fa fa-pencil"></i> Title</span>
					</div>
					<div class="panel-body">
						<h1 class="fs60 mbn mtn text-success"><i class="fa fa-check"></i></h1>
						<h6 class="fs12">'.strtoupper($row->nama).'</h6>
					</div>
					<div class="row">
						<div class="col-md-4" style="padding-right: 0px">
							<div class="panel-footer bg-primary light br-n p12" style="cursor: pointer" onclick="editDokumen(\''.$row->id.'\')">
								<span class="fs11"><b><i class="fa fa-edit"></i></b></span>
							</div>
						</div>
						<div class="col-md-4" style="padding: 0px">
							<div class="panel-footer bg-success light br-n p12" style="cursor: pointer" onclick="downloadFile(\''.$row->file.'\')">
								<span class="fs11"><b><i class="fa fa-arrow-down"></i></b></span>
							</div>
						</div>
						<div class="col-md-4" style="padding-left: 0px">
							<div class="panel-footer bg-danger light br-n p12" style="cursor: pointer" onclick="hapusDokumen(\''.$row->id.'\')">
								<span class="fs11"><b><i class="fa fa-times"></i></b></span>
							</div>
						</div>
					</div>
				</div>
			</div>';
		}

		if($sql) {
			$data = [
				"status" => true,
				"message" => "Hapus dokumen berhasil",
				"output" => $output
			];
		} else {
			$data = [
				"status" => false,
				"message" => "Terjadi kesalahan pada saat hapus dokumen",
				"output" => NULL
			];
		}

		echo json_encode($data);
	}

	public function edit_sk() {
		$uploadedFile = false;
		$config['upload_path'] = "./assets/files";
        $config['allowed_types'] = 'pdf';
        $config['encrypt_name'] = TRUE;
         
		$this->load->library('upload', $config);
		if($this->upload->do_upload("file")) {
			$data = array('upload_data' => $this->upload->data());
			$file = $data['upload_data']['file_name'];
			$uploadedFile = true;
			$html = '
			<div class="panel-heading hidden">
				<span class="panel-title"><i class="fa fa-pencil"></i> Title</span>
			</div>
			<div class="panel-body" id="sk-detail" style="cursor: pointer">
				<h1 class="fs60 mbn mtn text-success"><i class="fa fa-check"></i></h1>
				<h6 class="fs12">SURAT TUGAS</h6>
			</div>
			<div class="row">
				<div class="col-md-6" style="padding-right: 0px">
					<div class="panel-footer bg-primary light br-n p12 upload-laporan" style="cursor: pointer">
						<span class="fs11"><b>EDIT</b></span>
					</div>
				</div>
				<div class="col-md-6" style="padding-left: 0px">
					<div class="panel-footer bg-success light br-n p12" style="cursor: pointer" onclick="downloadFile(\''.$file.'\')">
						<span class="fs11"><b>DOWNLOAD</b></span>
					</div>
				</div>
			</div>
			';
 		} else {
			$file = NULL;
			$html = NULL;
		}
		
		$p = $this->input->post();
		$id = $p["id_sk_riset"];
		$exp_tgl = explode(" - ", $p["masa_berlaku"]);
		$data = [
			"no_sk" => $p["no_sk"],
			"tentang" => $p["tentang"],
			"tgl_ditetapkan" => date("Y-m-d", strtotime($p["tgl_ditetapkan"])),
			"tgl_mulai_berlaku" => date("Y-m-d", strtotime($exp_tgl[0])),
			"tgl_selesai_berlaku" => date("Y-m-d", strtotime($exp_tgl[1])),
			"penerbit" => $p["penerbit"],
			"pejabat_ttd" => $p["pegawai"],
			"file" => $file
		];

		$sql = $this->PenelitianModel->EditDataSK($data, ["id" => $id]);
		if($sql) {
			$output = [
				"status" => true,
				"message" => "Data SK berhasil di update",
				"output" => $html
			];
		} else {
			$output = [
				"status" => false,
				"message" => "Terjadi kesalahan pada saat update SK",
				"output" => NULL
			];
		}

		echo json_encode($output);
	}

	public function edit_status_penelitian($id) {
		$periode = $this->input->post("periode");
		$stts = $this->input->post("status");
		$data = ["tahapan" => $stts];
		$sql = $this->PenelitianModel->UpdateStatusPenelitian($data,$id,$periode);

		if($stts == 1) {
			$text_res = "Usulan";
			$color_txt = "primary";
		} else if($stts == 2) {
			$text_res = "On Going";
			$color_txt = "warning";
		} else if($stts == 3) {
			$text_res = "Selesai";
			$color_txt = "success";
		} else if($stts == 4) {
			$text_res = "Gagal";
			$color_txt = "danger";
		}

		if($sql) {
			$data = [
				"status" => true,
				"message" => "Status penelitian berhasil di ubah",
				"status_res" => '<span class="label label-md bg-'.$color_txt.'">'.strtoupper($text_res).'</span>',
			];
		} else {
			$data = [
				"status" => false,
				"message" => "Terjadi kesalahan pada saat ubah status penelitian",
				"status_res" => NULL,
			];
		}

		echo json_encode($data);
	}

	public function upload_lpj($riset) {
		$config['upload_path'] = "./assets/files";
        $config['allowed_types'] = 'pdf';
        $config['encrypt_name'] = TRUE;
         
        $this->load->library('upload', $config);
        if($this->upload->do_upload("file")) {
			$data = array('upload_data' => $this->upload->data());

			$file = $data['upload_data']['file_name'];
			$sql = $this->PenelitianModel->UploadFileLPJ($riset,$file);
			$html = '
			<div class="panel-heading hidden">
				<span class="panel-title"><i class="fa fa-pencil"></i> Title</span>
			</div>
			<div class="panel-body" id="sk-detail" style="cursor: pointer">
				<h1 class="fs60 mbn mtn text-success"><i class="fa fa-check"></i></h1>
				<h6 class="fs12">LAPORAN AKHIR</h6>
			</div>
			<div class="row">
				<div class="col-md-6" style="padding-right: 0px">
					<div class="panel-footer bg-primary light br-n p12 upload-laporan" style="cursor: pointer">
						<span class="fs11"><b>EDIT</b></span>
					</div>
				</div>
				<div class="col-md-6" style="padding-left: 0px">
					<div class="panel-footer bg-success light br-n p12" style="cursor: pointer" onclick="downloadFile(\''.$file.'\')">
						<span class="fs11"><b>DOWNLOAD</b></span>
					</div>
				</div>
			</div>';

			if($sql) {
				$output = [
					"status" => true,
					"message" => "File LPJ berhasil di upload",
					"output" => $html
				];
			} else {
				$output = [
					"status" => false,
					"message" => "Terjadi kesalahan pada saat upload",
					"output" => NULL
				];
			}
		} else {
			$output = [
				"status" => false,
				"message" => "Terjadi kesalahan pada saat upload",
				"output" => NULL
			];
		}

		echo json_encode($output);
	}

	public function upload_usulan($riset) {
		$config['upload_path'] = "./assets/files";
        $config['allowed_types'] = 'pdf';
        $config['encrypt_name'] = TRUE;
         
        $this->load->library('upload', $config);
        if($this->upload->do_upload("file")) {
			$data = array('upload_data' => $this->upload->data());

			$file = $data['upload_data']['file_name'];
			$sql = $this->PenelitianModel->UploadFileProposal($riset,$file);
			$html = '
			<div class="panel-heading hidden">
				<span class="panel-title"><i class="fa fa-pencil"></i> Title</span>
			</div>
			<div class="panel-body" id="upload-proposal" style="cursor: pointer">
				<h1 class="fs60 mbn mtn text-success"><i class="fa fa-check"></i></h1>
				<h6 class="fs12">PROPOSAL PENELITIAN</h6>
			</div>
			<div class="row">
				<div class="col-md-6" style="padding-right: 0px">
					<div class="panel-footer bg-primary light br-n p12 upload-proposal" style="cursor: pointer">
						<span class="fs11"><b>EDIT</b></span>
					</div>
				</div>
				<div class="col-md-6" style="padding-left: 0px">
					<div class="panel-footer bg-success light br-n p12" style="cursor: pointer" onclick="downloadFile(\''.$file.'\')">
						<span class="fs11"><b>DOWNLOAD</b></span>
					</div>
				</div>
			</div>';

			if($sql) {
				$output = [
					"status" => true,
					"message" => "File Proposal berhasil di upload",
					"output" => $html
				];
			} else {
				$output = [
					"status" => false,
					"message" => "Terjadi kesalahan pada saat upload",
					"output" => NULL
				];
			}
		} else {
			$output = [
				"status" => false,
				"message" => "Terjadi kesalahan pada saat upload",
				"output" => NULL
			];
		}

		echo json_encode($output);
	}

	public function hapus_personil($id,$person) {
		// Get Person Ref
		$ps = $this->PenelitianModel->GetDetailPersonilPenelitian($id,$person);
		$sql = $this->PenelitianModel->HapusPersonil($id,$person);
		if($sql) {
			$output = [
				"status" => true,
				"message" => "Data personil berhasil dihapus",
				"person_count" => $this->PenelitianModel->GetPersonilPenelitian($id, $ps->person_ref)->num_rows(),
				"person_ref" => $ps->person_ref
			];
		} else {
			$output = [
				"status" => false,
				"message" => "Terjadi kesalahan pada saat hapus personil",
				"person_count" => $this->PenelitianModel->GetPersonilPenelitian($id, $ps->person_ref)->num_rows(),
				"person_ref" => $ps->person_ref
			];
		}

		echo json_encode($output);
	}

	public function get_subbidang_penelitian($id) {
		$output = '';
		$output .= '<select class="form-control mt10" id="subbidpen" name="5">';

		// Showing option
		$options = $this->AppModel->GetOptionDropdown('[subbidang-penelitian]',$id);
		$output .= '<option>-- Pilih --</option>';
		foreach($options as $option) {
			$output .= '<option value="'.$option->id.'">'.$option->value.'</option>';
		}
		$output .= '</select>';
		$data = ['output' => $output];
		echo json_encode($data);
	}

	public function get_skim_penelitian($id) {
		$output = '';

		// Showing option
		$options = $this->AppModel->GetOptionDropdown('[skim-penelitian]',$id);
		if(count($options) > 0) {
			$output .= '<select class="form-control mt10" id="skimpenelitian" name="73">';
			$output .= '<option>-- Pilih Skim --</option>';
			foreach($options as $option) {
				$output .= '<option value="'.$option->id.'">'.$option->value.'</option>';
			}
			$output .= '</select>';
		}
		
		$data = ['output' => $output];
		echo json_encode($data);
	}

	public function get_subtujuan_sosial($id) {
		$output = '';
		$output .= '<select class="form-control mt10" id="subtujuan" name="7">';

		// Showing option
		$options = $this->AppModel->GetOptionDropdown('[subtujuan-sosial]',$id);
		$output .= '<option>-- Pilih --</option>';
		foreach($options as $option) {
			$output .= '<option value="'.$option->id.'">'.$option->value.'</option>';
		}
		$output .= '</select>';
		$data = ['output' => $output];
		echo json_encode($data);
	}

	public function get_personil_penelitian($id,$ref) {
		$draw = intval($this->input->get("draw"));
      	$start = intval($this->input->get("start"));
	  	$length = intval($this->input->get("length"));
	  
		$no = 1;
		$data = [];
		$sql = $this->PenelitianModel->GetPersonilPenelitian($id,$ref);
		$status = $this->PenelitianModel->GetDataPenelitian(NULL, $id)->row()->status;
		if($ref == "Pegawai") {
			foreach($sql->result() as $row) {
				$person = [
					$no++,
					$row->personil,
					$row->prodi,
					$row->status
				];

				if($status == "Usulan" || $status == "On Going") {
					array_push($person, '<button class="btn btn-xs btn-danger" onclick="hapusPersonil(\''.$row->identitas.'\')">Hapus</button>');
				}

				$data[] = $person;
			}
		} else if($ref == "Mahasiswa") {
			foreach($sql->result() as $row) {
				$person = [
					$no++,
					$row->identitas,
					$row->personil,
					$row->prodi
				];

				if($status == "Usulan" || $status == "On Going") {
					array_push($person, '<button class="btn btn-xs btn-danger" onclick="hapusPersonil(\''.$row->identitas.'\')">Hapus</button>');
				}

				$data[] = $person;
			}
		}
		
		$result = array(
			"draw" => $draw,
			"recordsTotal" => $sql->num_rows(),
			"recordsFiltered" => $sql->num_rows(),
			"data" => $data
		);

	   	echo json_encode($result);
   		exit();
	}

	public function get_data_revisi($id) {
		$draw = intval($this->input->get("draw"));
      	$start = intval($this->input->get("start"));
	  	$length = intval($this->input->get("length"));
	  
		$no = 1;
		$data = [];
		$sql = $this->PenelitianModel->GetRevisiPenelitian($id);
		foreach($sql->result() as $row) {
			$data[] = [
				$no++,
				$row->keterangan,
				$this->AppModel->DateIndo($row->tanggal),
			];
		}
		
		$result = array(
			"draw" => $draw,
			"recordsTotal" => $sql->num_rows(),
			"recordsFiltered" => $sql->num_rows(),
			"data" => $data
		);

	   	echo json_encode($result);
   		exit();
	}

	public function get_data_sk($id) {
		$sql = $this->PenelitianModel->GetDataSK($id)->row();

		$sql->tgl_mulai_berlaku = date("m/d/Y", strtotime($sql->tgl_mulai_berlaku));
		$sql->tgl_selesai_berlaku = date("m/d/Y", strtotime($sql->tgl_selesai_berlaku));
		$sql->tanggal = $sql->tgl_mulai_berlaku." - ".$sql->tgl_selesai_berlaku;
		
		echo json_encode($sql);
	}

	public function get_child_penelitian($id) {
		$sql = $this->AppModel->GetOptionDropdown("[skim-penelitian]",$id);
		$status = FALSE;
		
		if(count($sql) > 0) {
			$output = '<div class="form-group"><label for="" class="control-label mt15">Skim Penelitian</label><select class="form-control" name="skim">';
			foreach($sql as $row) {
				$status = TRUE;
				$output .= '<option value="'.$row->id.'">'.$row->value.'</option>';
			}
			$output .= '</select></div>';
		} else {
			$output = "";
		}

		$data = [
			"status" => $status,
			"output" => $output
		];
		echo json_encode($data);
	}
}
?>