<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Periode extends CI_Controller {
	protected $roles_user;

	// session
	protected $sess;
	protected $sess_not_con;
	protected $sess_data;

	public function __construct() {
		parent::__construct();
		$this->load->model("AppModel");
		$this->load->model("SessionModel");
		$this->load->model("PeriodeModel");
		$this->load->model("NotifikasiModel");

		$this->sess = $this->SessionModel->GetSession();
		$this->sess_not_con = !$this->sess['session_userid'] && !$this->sess['session_role'];
		$this->roles_user = explode(",", $this->sess['session_role']);
        $this->sess_data = [
			"userid" => $this->sess['session_userid'],
			"role" => $this->sess['session_role'],
			"nama" => $this->sess['session_nama'],
			"email" => $this->sess['session_email'],
			"foto" => $this->sess['session_foto'],
			"notifikasi" => $this->NotifikasiModel->GetNotifikasi($this->sess['session_userid'], $this->sess['session_role'])
		];
    }

    public function index() {
        if($this->sess_not_con) {
			redirect("login");
		} else {
			$this->sess_data['judul'] = "SIPENDIK - Periode";
			$this->sess_data['active_menu'] = 'periode';
			$data['header'] = $this->load->view("layout/layout-header", $this->sess_data, TRUE);
			$data['footer'] = $this->load->view("layout/layout-footer", $this->sess_data, TRUE);

			if(in_array('1', $this->roles_user)) {
                $data['periode'] = $this->PeriodeModel->GetPeriode();
                $this->load->view("unit/periode", $data);
            }
        }
	}

	public function data_report($id) {
		if($this->sess_not_con) {
			redirect("login");
		} else {
			$this->sess_data['judul'] = "SIPENDIK - Data Report Periode";
			$this->sess_data['active_menu'] = 'periode';
			$data['header'] = $this->load->view("layout/layout-header", $this->sess_data, TRUE);
			$data['footer'] = $this->load->view("layout/layout-footer", $this->sess_data, TRUE);

			if(in_array('1', $this->roles_user)) {
				$data["stts_pn"] = $this->PeriodeModel->GetJumlahStatusKegiatan($id, 1);
				$data["stts_pg"] = $this->PeriodeModel->GetJumlahStatusKegiatan($id, 2);
				$data["prodi_pn"] = $this->PeriodeModel->GetJumlahProdiKegiatan($id, 1);
				$data["prodi_pg"] = $this->PeriodeModel->GetJumlahProdiKegiatan($id, 2);
				$data["dana_pn"] = $this->PeriodeModel->GetJumlahSumberDanaKegiatan($id, 1);
				$data["dana_pg"] = $this->PeriodeModel->GetJumlahSumberDanaKegiatan($id, 2);
				$data["jenis_pn"] = $this->PeriodeModel->GetJumlahJenisKegiatan($id, 1);
				$data["jenis_pg"] = $this->PeriodeModel->GetJumlahJenisKegiatan($id, 2);
				$data['produktifitas'] = $this->PeriodeModel->GetDosenProduktif($id);
				$this->load->view("unit/periode-data-report", $data);
            }
        }
	}

	public function tambah_periode() {
		$exp_tgl = explode(" - ", $this->input->post("masa_berlaku"));
		$data = [
			"parent" => NULL,
			"nama" => $this->input->post("nama"),
			"tgl_mulai" => date("Y-m-d", strtotime($exp_tgl[0])),
			"tgl_selesai" => date("Y-m-d", strtotime($exp_tgl[1])),
			"tags" => NULL,
			"isAktif" => "NO"
		];

		$sql = $this->PeriodeModel->TambahPeriode($data);
		if($sql) {
			$output = [
				"status" => true,
				"message" => "Data periode berhasil ditambahkan"
			];
		} else {
			$output = [
				"status" => false,
				"message" => "Terjadi kesalahan pada saat input periode"
			];
		}

		echo json_encode($output);
	}

	public function edit_periode($id) {
		$exp_tgl = explode(" - ", $this->input->post("masa_berlaku_ed"));
		$data = [
			"nama" => $this->input->post("nama_ed"),
			"tgl_mulai" => date("Y-m-d", strtotime($exp_tgl[0])),
			"tgl_selesai" => date("Y-m-d", strtotime($exp_tgl[1])),
		];

		$sql = $this->PeriodeModel->EditPeriode($data, ["id" => $id]);
		if($sql) {
			$output = [
				"status" => true,
				"message" => "Data periode berhasil di edit"
			];
		} else {
			$output = [
				"status" => false,
				"message" => "Terjadi kesalahan pada saat edit periode"
			];
		}

		echo json_encode($output);
	}

	public function hapus_periode($id) {
		$sql = $this->PeriodeModel->HapusPeriode($id);
		if($sql) {
			$output = [
				"status" => true,
				"message" => "Data periode berhasil di hapus"
			];
		} else {
			$output = [
				"status" => false,
				"message" => "Terjadi kesalahan pada saat hapus periode"
			];
		}

		echo json_encode($output);
	}

	public function nonaktif_periode($id) {
		$sql = $this->PeriodeModel->EditPeriode(["isAktif" => "NO"], ["id" => $id, "isAktif" => "YES"]);
		if($sql) {
			$output = [
				"status" => true,
				"message" => "Data periode berhasil di aktifkan"
			];
		} else {
			$output = [
				"status" => false,
				"message" => "Terjadi kesalahan pada saat pengaktifan periode"
			];
		}

		echo json_encode($output);
	}

	public function aktivasi_periode($id) {
		$sql = $this->PeriodeModel->EditPeriode(["isAktif" => "YES"], ["id" => $id, "isAktif" => "NO"]);
		if($sql) {
			$output = [
				"status" => true,
				"message" => "Data periode berhasil di nonaktifkan"
			];
		} else {
			$output = [
				"status" => false,
				"message" => "Terjadi kesalahan pada saat me-nonaktifan periode"
			];
		}

		echo json_encode($output);
	}
	
	public function get_data_periode() {
		$draw = intval($this->input->get("draw"));
      	$start = intval($this->input->get("start"));
	  	$length = intval($this->input->get("length"));
	  
		$no = 1;
		$data = [];
		$sql = $this->PeriodeModel->GetPeriode();
		foreach($sql->result() as $row) {
			$tgl_awal = $this->AppModel->DateIndo(date("Y-m-d", strtotime($row->tgl_mulai)));
			$tgl_akhir = $this->AppModel->DateIndo(date("Y-m-d", strtotime($row->tgl_selesai)));

			if($row->isAktif == "YES") {
				$status = '<span class="label label-md bg-success">AKTIF</span>';
				$act_button = '<li><a href="#" onclick="nonaktivPeriode('.$row->id.')">Non-Aktifkan</a></li>';
			} else if($row->isAktif == "NO") {
				$status = '<span class="label label-md bg-danger">NONAKTIF</span>';
				$act_button = '<li><a href="#" onclick="aktivasiPeriode('.$row->id.')">Aktifkan</a></li>';
			}

			$data[] = [
				$no++,
				$row->nama,
				$status,
				$tgl_awal,
				$tgl_akhir,
				'<div class="btn-group">
					<i class="glyphicon glyphicon-cog dropdown-toggle" data-toggle="dropdown" aria-expanded="false"></i>
					<ul class="dropdown-menu pull-right" role="menu">
						'.$act_button.'
						<li>
							<a href="'.site_url('periode/data_report/'.$row->id).'">Data Report</a>
						</li>
						<li>
							<a href="#" onclick="editPeriode('.$row->id.')">Edit</a>
						</li>
						<li>
							<a href="#" onclick="hapusPeriode('.$row->id.')">Hapus</a>
						</li>
					</ul>
				</div>'
			];
		}
		
		$result = array(
			"draw" => $draw,
			"recordsTotal" => $sql->num_rows(),
			"recordsFiltered" => $sql->num_rows(),
			"data" => $data
		);

	   	echo json_encode($result);
   		exit();
	}

	public function get_detail_periode($id) {
		$sql = $this->PeriodeModel->GetPeriode($id);
		$row = $sql->row();
		$row->tgl_mulai = date("m/d/Y", strtotime($row->tgl_mulai));
		$row->tgl_selesai = date("m/d/Y", strtotime($row->tgl_selesai));
		$row->tanggal = $row->tgl_mulai." - ".$row->tgl_selesai;
		echo json_encode($row);
	}

	public function get_periode_aktiv() {
		$sql = $this->PeriodeModel->GetPeriodeAktiv()->row();
		$data = ["result" => $sql->id];
		echo json_encode($data);
	}
}