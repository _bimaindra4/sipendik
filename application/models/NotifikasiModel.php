<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class NotifikasiModel extends CI_Model {
	public function __construct() {
        parent::__construct();
        $this->load->model("AppModel");
    }

    public function GetNotifikasi($iduser,$role) {
        $q = "
        SELECT
            CASE
                WHEN notif.user_role_from = 2 THEN peg.foto
                ELSE 'user.png'
            END AS foto,
            CASE
                WHEN notif.kategori_notifikasi = 'usulan_pn' THEN 'Usulan Penelitian'
                WHEN notif.kategori_notifikasi = 'usulan_pg' THEN 'Usulan Pengabdian'
                WHEN notif.kategori_notifikasi = 'hasil_usulan' THEN 'Hasil Usulan'
                ELSE '???'
            END AS kat,
            DATE_format(notif.Created_Date, '%d/%m/%Y') AS tanggal,
            notif.pesan,
            notif.referensi_id AS ref,
            riset.jenis,
            notif.user_id_from
        FROM tb_rst_tr_notifikasi notif
        JOIN tb_rst_tr_riset riset ON notif.referensi_id = riset.id
        LEFT JOIN tb_peg_rf_pegawai peg ON notif.user_id_from = peg.nip
        WHERE notif.status_lihat = 'belum' AND notif.user_id_to = '$iduser' AND notif.user_role_to = '$role'";

        $sql = $this->db->query($q);
        return $sql;
    }

    public function TambahNotifikasi($data) {
        // Ambil nama lengkap dosen + gelar
        $this->db->select("CONCAT(IFNULL(gelar_depan,''), nama, gelar_belakang) AS nama");
        $this->db->where("nip", $data['user_id_from']);
        $sql = $this->db->get("tb_peg_rf_pegawai");

        if($sql->num_rows() != 0) {
            $nama = $sql->row()->nama;
        } else {
            $nama = "";
        }

        $kat = $data['jenis'];
        if($kat == "usulan_pn") {
            $pesan = "Terdapat usulan penelitian baru dari ".$nama;
        } else if($kat == "usulan_pg") {
            $pesan = "Terdapat usulan pengabdian baru dari ".$nama;
        } else if($kat == "hasil_usulan") {
            if($data['hasil'] == 1) {
                $pesan = "Selamat!, usulan Anda berhasil disetujui oleh LPPM";
            } else if($data['hasil'] == 2) {
                $pesan = "Selamat!, usulan Anda berhasil disetujui oleh LPPM, tetapi dengan catatan revisi";
            } else if($data['hasil'] == 3) {
                $pesan = "Mohon maaf, usulan Anda ditolak oleh LPPM";
            }
        } else {
            $pesan = "????";
        }

        $data = [
            "user_id_from" => $data['user_id_from'],
            "user_role_from" => $data['user_role_from'],
            "user_id_to" => $data['user_id_to'],
            "user_role_to" => $data['user_role_to'],
            "pesan" => $pesan,
            "kategori_notifikasi" => $data['jenis'],
            "referensi_id" => $data['reference'],
            "status_lihat" => "belum",
            "Created_Date" => $this->AppModel->DateTimeNow()
        ];

        $sql = $this->db->insert("tb_rst_tr_notifikasi", $data);
        return $sql;
    }

    public function RemoveStatusNotifikasi($data) {
        $sql = $this->db->update("tb_rst_tr_notifikasi", ["status_lihat" => "sudah"], $data);
        return $sql;
    } 
}