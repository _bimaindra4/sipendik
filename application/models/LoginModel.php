<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginModel extends CI_Model {
	public function __construct() {
		parent::__construct();
    }
    
    public function CheckUser($email, $pass) {
        $this->db->select("nip, CONCAT(IFNULL(gelar_depan,''), nama, gelar_belakang) AS nama, email, foto");
        $this->db->where("email", $email);
        $this->db->where("sha1(nip)", $pass);

        $sql = $this->db->get("tb_peg_rf_pegawai");
        return $sql;
    }
}