<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LuaranModel extends CI_Model {
	public function __construct() {
		parent::__construct();
    }

    function GetPilihanLuaran($id) {
        $this->db->select("id, luaran");
        $this->db->where("jenis", $id);
        $sql = $this->db->get("tb_rst_rf_jenis_luaran");
        
        return $sql->result();
    }

    function GetDataLuaran($luaran, $riset = "semua", $periode = "semua") {
        $clause_riset = ($riset != "semua" ? "AND rlu.riset = $riset" : "");
        if($periode != "semua") {
            $join_periode = 'JOIN tb_rst_tr_riset_tahapan rst_tahap ON rlu.riset = rst_tahap.riset';
            $clause_periode = 'AND rst_tahap.periode = '.$periode;
        } else {
            $join_periode = "";
            $clause_periode = "";
        }

        if($luaran == 1 || $luaran == 5) {
            $q = '
                SELECT
                    rlu.id,
                    rlu_prop_24.value AS nama_jurnal,
                    opt_jenis.value AS jenis_publikasi,
                    opt_tingkat.value AS tingkat_publikasi,
                    CONCAT("Vol. ",rlu_prop_25.value," No. ",rlu_prop_26.value," Th. ",rlu_prop_27.value) AS edisi,
                    opt_status.value AS status_jurnal,
                    rlu_prop_33.value AS file
                FROM tb_rst_rf_luaran rlu
                JOIN tb_rst_tr_riset_luaran rlu_prop_24 ON rlu.id = rlu_prop_24.luaran AND rlu_prop_24.prop = 24
                JOIN tb_rst_tr_riset_luaran rlu_prop_25 ON rlu.id = rlu_prop_25.luaran AND rlu_prop_25.prop = 25
                JOIN tb_rst_tr_riset_luaran rlu_prop_26 ON rlu.id = rlu_prop_26.luaran AND rlu_prop_26.prop = 26
                JOIN tb_rst_tr_riset_luaran rlu_prop_27 ON rlu.id = rlu_prop_27.luaran AND rlu_prop_27.prop = 27
                LEFT JOIN tb_rst_tr_riset_luaran rlu_prop_33 ON rlu.id = rlu_prop_33.luaran AND rlu_prop_33.prop = 33
                LEFT JOIN (
                    tb_rst_tr_riset_luaran rlu_prop_22 INNER JOIN
                    tb_rst_rf_options opt_tingkat ON rlu_prop_22.value = opt_tingkat.id AND rlu_prop_22.prop = 22
                ) ON rlu.id = rlu_prop_22.luaran
                LEFT JOIN (
                    tb_rst_tr_riset_luaran rlu_prop_32 INNER JOIN
                    tb_rst_rf_options opt_status ON rlu_prop_32.value = opt_status.id AND rlu_prop_32.prop = 32
                ) ON rlu.id = rlu_prop_32.luaran
                LEFT JOIN (
                    tb_rst_tr_riset_luaran rlu_prop_71 INNER JOIN
                    tb_rst_rf_options opt_jenis ON rlu_prop_71.value = opt_jenis.id AND rlu_prop_71.prop = 71
                ) ON rlu.id = rlu_prop_71.luaran '.$join_periode.'
                WHERE rlu.luaran = '.$luaran.' '.$clause_riset.' '.$clause_periode.' 
                GROUP BY rlu.id
            ';
        } else if($luaran == 2) {
            $q = '
                SELECT
                    rlu.id,
                    rlu_prop_34.value AS integrasi,
                    rlu_prop_35.value AS bentuk,
	                rlu_prop_33.value AS file
                FROM tb_rst_rf_luaran rlu
                JOIN tb_rst_tr_riset_luaran rlu_prop_34 ON rlu.id = rlu_prop_34.luaran AND rlu_prop_34.prop = 34
                JOIN tb_rst_tr_riset_luaran rlu_prop_35 ON rlu.id = rlu_prop_35.luaran AND rlu_prop_35.prop = 35
                LEFT JOIN tb_rst_tr_riset_luaran rlu_prop_33 ON rlu.id = rlu_prop_33.luaran AND rlu_prop_33.prop = 33
                WHERE rlu.luaran = '.$luaran.' '.$clause_riset.'
                GROUP BY rlu.id
            ';
        } else if($luaran == 3 || $luaran == 8) {
            $q = '
                SELECT
                    rlu.id,
                    rlu_prop_36.value AS no_sertifikat,
                    rlu_prop_37.value AS nama_hki,
                    opt_jenis.value AS jenis_hki,
                    opt_status.value AS status_hki,
	                rlu_prop_33.value AS file
                FROM tb_rst_rf_luaran rlu
                JOIN tb_rst_tr_riset_luaran rlu_prop_36 ON rlu.id = rlu_prop_36.luaran AND rlu_prop_36.prop = 36
                JOIN tb_rst_tr_riset_luaran rlu_prop_37 ON rlu.id = rlu_prop_37.luaran AND rlu_prop_37.prop = 37
                LEFT JOIN tb_rst_tr_riset_luaran rlu_prop_33 ON rlu.id = rlu_prop_33.luaran AND rlu_prop_33.prop = 33
                LEFT JOIN (
                    tb_rst_tr_riset_luaran rlu_prop_38 INNER JOIN
                    tb_rst_rf_options opt_jenis ON rlu_prop_38.value = opt_jenis.id AND rlu_prop_38.prop = 38
                ) ON rlu.id = rlu_prop_38.luaran
                LEFT JOIN (
                    tb_rst_tr_riset_luaran rlu_prop_39 INNER JOIN
                    tb_rst_rf_options opt_status ON rlu_prop_39.value = opt_status.id AND rlu_prop_39.prop = 39
                ) ON rlu.id = rlu_prop_39.luaran
                WHERE rlu.luaran = '.$luaran.' '.$clause_riset.'
                GROUP BY rlu.id
            ';
        } else if($luaran == 4) {
            $q = '
                SELECT
                    rlu.id,
                    rlu_prop_40.value AS jenis_luaran,
                    rlu_prop_41.value AS deskripsi,
	                rlu_prop_33.value AS file
                FROM tb_rst_rf_luaran rlu
                JOIN tb_rst_tr_riset_luaran rlu_prop_40 ON rlu.id = rlu_prop_40.luaran AND rlu_prop_40.prop = 40
                JOIN tb_rst_tr_riset_luaran rlu_prop_41 ON rlu.id = rlu_prop_41.luaran AND rlu_prop_41.prop = 41
                LEFT JOIN tb_rst_tr_riset_luaran rlu_prop_33 ON rlu.id = rlu_prop_33.luaran AND rlu_prop_33.prop = 33
                WHERE rlu.luaran = '.$luaran.' '.$clause_riset.'
                GROUP BY rlu.id
            ';
        } else if($luaran == 6) {
            $q = '
                SELECT
                    rlu.id,
                    rlu_prop_23.value AS judul,
                    rlu_prop_49.value AS tanggal,
                    opt_jenis.value AS jenis_media,
	                rlu_prop_33.value AS file
                FROM tb_rst_rf_luaran rlu
                JOIN tb_rst_tr_riset_luaran rlu_prop_23 ON rlu.id = rlu_prop_23.luaran AND rlu_prop_23.prop = 23
                JOIN tb_rst_tr_riset_luaran rlu_prop_49 ON rlu.id = rlu_prop_49.luaran AND rlu_prop_49.prop = 49
                LEFT JOIN tb_rst_tr_riset_luaran rlu_prop_33 ON rlu.id = rlu_prop_33.luaran AND rlu_prop_33.prop = 33
                LEFT JOIN (
                    tb_rst_tr_riset_luaran rlu_prop_50 INNER JOIN
                    tb_rst_rf_options opt_jenis ON rlu_prop_50.value = opt_jenis.id AND rlu_prop_50.prop = 50
                ) ON rlu.id = rlu_prop_50.luaran
                WHERE rlu.luaran = '.$luaran.' '.$clause_riset.'
                GROUP BY rlu.id
            ';
        } else if($luaran == 7) {
            $q = '
                SELECT
                    rlu.id,
                    rlu_prop_55.value AS nama_forum,
                    rlu_prop_23.value AS judul,
                    rlu_prop_56.value AS penyelenggara,
	                rlu_prop_33.value AS file
                FROM tb_rst_rf_luaran rlu
                JOIN tb_rst_tr_riset_luaran rlu_prop_55 ON rlu.id = rlu_prop_55.luaran AND rlu_prop_55.prop = 55
                JOIN tb_rst_tr_riset_luaran rlu_prop_23 ON rlu.id = rlu_prop_23.luaran AND rlu_prop_23.prop = 23
                JOIN tb_rst_tr_riset_luaran rlu_prop_56 ON rlu.id = rlu_prop_56.luaran AND rlu_prop_56.prop = 23
                LEFT JOIN tb_rst_tr_riset_luaran rlu_prop_33 ON rlu.id = rlu_prop_33.luaran AND rlu_prop_33.prop = 33
                WHERE rlu.luaran = '.$luaran.' '.$clause_riset.'
                GROUP BY rlu.id
            ';
        } else if($luaran == 9) {
            $q = '
                SELECT
                    rlu.id,
                    opt_luaran.value AS jenis_luaran,
                    rlu_prop_60.value AS nama_luaran,
	                rlu_prop_33.value AS file
                FROM tb_rst_rf_luaran rlu
                JOIN tb_rst_tr_riset_luaran rlu_prop_60 ON rlu.id = rlu_prop_60.luaran AND rlu_prop_60.prop = 60
                LEFT JOIN tb_rst_tr_riset_luaran rlu_prop_33 ON rlu.id = rlu_prop_33.luaran AND rlu_prop_33.prop = 33
                LEFT JOIN (
                    tb_rst_tr_riset_luaran rlu_prop_59 INNER JOIN
                    tb_rst_rf_options opt_luaran ON rlu_prop_59.value = opt_luaran.id AND rlu_prop_59.prop = 59
                ) ON rlu.id = rlu_prop_59.luaran
                WHERE rlu.luaran = '.$luaran.' '.$clause_riset.'
                GROUP BY rlu.id
            ';
        } else if($luaran == 10 || $luaran == 11) {
            $q = '
                SELECT
                    rlu.id,
                    rlu_prop_63.value AS no_sertifikat,
                    rlu_prop_61.value AS nama_produk,
                    rlu_prop_62.value AS lembaga,
	                rlu_prop_33.value AS file
                FROM tb_rst_rf_luaran rlu
                JOIN tb_rst_tr_riset_luaran rlu_prop_63 ON rlu.id = rlu_prop_63.luaran AND rlu_prop_63.prop = 63
                JOIN tb_rst_tr_riset_luaran rlu_prop_61 ON rlu.id = rlu_prop_61.luaran AND rlu_prop_61.prop = 61
                JOIN tb_rst_tr_riset_luaran rlu_prop_62 ON rlu.id = rlu_prop_62.luaran AND rlu_prop_62.prop = 62
                LEFT JOIN tb_rst_tr_riset_luaran rlu_prop_33 ON rlu.id = rlu_prop_33.luaran AND rlu_prop_33.prop = 33
                WHERE rlu.luaran = '.$luaran.' '.$clause_riset.'
                GROUP BY rlu.id
            ';
        } else if($luaran == 12) {
            $q = '
                SELECT
                    rlu.id,
                    rlu_prop_65.value AS no_badan_hukum,
                    rlu_prop_64.value AS nama_mitra,
                    rlu_prop_46.value AS bid_usaha,
                    rlu_prop_33.value AS file
                FROM tb_rst_rf_luaran rlu
                JOIN tb_rst_tr_riset_luaran rlu_prop_65 ON rlu.id = rlu_prop_65.luaran AND rlu_prop_65.prop = 65
                JOIN tb_rst_tr_riset_luaran rlu_prop_64 ON rlu.id = rlu_prop_64.luaran AND rlu_prop_64.prop = 64
                JOIN tb_rst_tr_riset_luaran rlu_prop_46 ON rlu.id = rlu_prop_46.luaran AND rlu_prop_46.prop = 46
                LEFT JOIN tb_rst_tr_riset_luaran rlu_prop_33 ON rlu.id = rlu_prop_33.luaran AND rlu_prop_33.prop = 33
                WHERE rlu.luaran = '.$luaran.' '.$clause_riset.'
                GROUP BY rlu.id
            ';
        } else if($luaran == 13) {
            $q = '
                SELECT
                    rlu.id,
                    rlu_prop_67.value AS judul_buku,
                    rlu_prop_58.value AS isbn,
                    rlu_prop_69.value AS bid_usaha,
                    rlu_prop_33.value AS file
                FROM tb_rst_rf_luaran rlu
                JOIN tb_rst_tr_riset_luaran rlu_prop_67 ON rlu.id = rlu_prop_67.luaran AND rlu_prop_67.prop = 67
                JOIN tb_rst_tr_riset_luaran rlu_prop_58 ON rlu.id = rlu_prop_58.luaran AND rlu_prop_58.prop = 58
                JOIN tb_rst_tr_riset_luaran rlu_prop_69 ON rlu.id = rlu_prop_69.luaran AND rlu_prop_69.prop = 69
                LEFT JOIN tb_rst_tr_riset_luaran rlu_prop_33 ON rlu.id = rlu_prop_33.luaran AND rlu_prop_33.prop = 33
                WHERE rlu.luaran = '.$luaran.' '.$clause_riset.'
                GROUP BY rlu.id
            ';
        } else if($luaran == 14) {
            $q = '
                SELECT
                    rlu.id,
                    rlu_prop_70.value AS wirausaha,
                    rlu_prop_41.value AS deskripsi,
                    rlu_prop_33.value AS file
                FROM tb_rst_rf_luaran rlu
                JOIN tb_rst_tr_riset_luaran rlu_prop_70 ON rlu.id = rlu_prop_70.luaran AND rlu_prop_70.prop = 70
                JOIN tb_rst_tr_riset_luaran rlu_prop_41 ON rlu.id = rlu_prop_41.luaran AND rlu_prop_41.prop = 41
                LEFT JOIN tb_rst_tr_riset_luaran rlu_prop_33 ON rlu.id = rlu_prop_33.luaran AND rlu_prop_33.prop = 33
                WHERE rlu.luaran = '.$luaran.' '.$clause_riset.'
                GROUP BY rlu.id
            ';
        }

        $sql = $this->db->query($q);
        return $sql;
    }

    function GetDetailLuaran($id) {
        $this->db->select("riset");
        $this->db->where("id", $id);
        $sql = $this->db->get("tb_rst_rf_luaran")->row();

        $q = '
        SELECT 
            luaran.luaran,
            prop.prop, 
            prop.type,
            CASE 
                WHEN prop.type = "option" THEN option.value
                WHEN prop.type != "option" THEN rlu.value
            END AS value
        FROM tb_rst_tr_riset_luaran rlu
        JOIN tb_rst_rf_prop prop ON rlu.prop = prop.id
        LEFT JOIN tb_rst_rf_options option ON rlu.value = option.id
        JOIN tb_rst_rf_luaran luaran ON rlu.luaran = luaran.id
        WHERE luaran.riset = '.$sql->riset.' AND rlu.luaran = '.$id.'
        ';
        $sql = $this->db->query($q);

        return $sql;
    }

    function TambahLuaran($data) {
        $sql = $this->db->insert("tb_rst_rf_luaran", $data);
        return $this->db->insert_id();
    }

    function TambahLuaranProp($data) {
        $sql = $this->db->insert_batch("tb_rst_tr_riset_luaran", $data);
        return $sql;
    }

    function EditLuaran($data,$where) {
        $sql = $this->db->update("tb_rst_tr_riset_luaran", $data, $where);
        return $sql;
    }

    function HapusLuaran($id) {
        $sql = $this->db->delete("tb_rst_tr_riset_luaran", ["luaran" => $id]);
        $sql = $this->db->delete("tb_rst_rf_luaran", ["id" => $id]);
		return $sql;
    }
}