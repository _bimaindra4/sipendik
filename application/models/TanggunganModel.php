<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TanggunganModel extends CI_Model {
	public function __construct() {
		parent::__construct();
    }

    function GetDataTanggungan($filter = NULL) {
        if(isset($filter)) {
            $clause_dosen = "";
            $clause_prodi = "";

            if(isset($filter['dosen'])) {
                $dosen = $filter['dosen'];
                $clause_dosen = 'AND kontri.person = "'.$dosen.'"';
            }

            if(isset($filter['prodi'])) {
                $prodi = $filter['prodi'];
                $clause_prodi = 'AND rst.prodi LIKE "%'.$prodi.'%"';
            }

            $clause_filter = $clause_dosen." ".$clause_prodi;
        } else {
            $clause_filter = "";
        }

        $q = '
        SELECT
            rst.id AS id,
            rst.judul AS judul,
            CONCAT(IFNULL(pegawai.gelar_depan,""), pegawai.nama, pegawai.gelar_belakang) AS ketua,
            jenis.jenis AS kegiatan,
            periode.nama AS periode,
            tahapan.tahapan,
            DATEDIFF(NOW(), rst_sk.tgl_selesai_berlaku) AS keterlambatan,
            rst_sk.file AS sk,
            rst.laporan_akhir AS lpj
        FROM tb_rst_tr_riset rst
        JOIN tb_rst_tr_riset_sk rst_sk ON rst.id = rst_sk.riset
        JOIN tb_rst_tr_riset_props rst_prop ON rst.id = rst_prop.riset
        JOIN tb_rst_tr_riset_tahapan rst_tahap ON rst.id = rst_tahap.riset
        JOIN tb_rst_rf_jenis jenis ON rst.jenis = jenis.id
        JOIN tb_rst_rf_tahapan tahapan ON rst_tahap.tahapan = tahapan.id
        JOIN tb_rst_tr_periode periode ON rst_tahap.periode = periode.id
        JOIN tb_rst_tr_kontributor kontri ON rst.id = kontri.riset
        JOIN tb_peg_rf_pegawai pegawai ON kontri.person = pegawai.nip
        LEFT JOIN (
            tb_rst_tr_riset_props rst_prop_2 INNER JOIN
            tb_rst_rf_options opt_jenis_pn ON rst_prop_2.value = opt_jenis_pn.id AND rst_prop_2.prop = 2
        ) ON rst.id = rst_prop_2.riset
        LEFT JOIN (
            tb_rst_tr_riset_props rst_prop_72 INNER JOIN
            tb_rst_rf_options opt_jenis_pg ON rst_prop_72.value = opt_jenis_pg.id AND rst_prop_72.prop = 72
        ) ON rst.id = rst_prop_72.riset
        WHERE rst_sk.tgl_selesai_berlaku < NOW() AND tahapan.tahapan = "On Going" AND rst_tahap.id = (
            SELECT rst_tahap.id
            FROM tb_rst_tr_riset_tahapan rst_tahap
            JOIN tb_rst_tr_periode periode ON rst_tahap.periode = periode.id
            WHERE rst_tahap.riset = rst.id
            ORDER BY periode.urutan DESC
            LIMIT 1
        ) '.$clause_filter.'
        GROUP BY rst.id
        ';
        $sql = $this->db->query($q);

        return $sql;
    }

    function GetTanggunganDosen($nip) {
        $q = '
        SELECT
            rst.id AS id,
            rst.judul AS judul,
            CONCAT(IFNULL(pegawai.gelar_depan,""), pegawai.nama, pegawai.gelar_belakang) AS ketua,
            jenis.jenis AS kegiatan,
            periode.nama AS periode,
            tahapan.tahapan,
            DATEDIFF(NOW(), rst_sk.tgl_selesai_berlaku) AS keterlambatan,
            rst_sk.file AS sk,
            rst.laporan_akhir AS lpj
        FROM tb_rst_tr_riset rst
        JOIN tb_rst_tr_riset_sk rst_sk ON rst.id = rst_sk.riset
        JOIN tb_rst_tr_riset_props rst_prop ON rst.id = rst_prop.riset
        JOIN tb_rst_tr_riset_tahapan rst_tahap ON rst.id = rst_tahap.riset
        JOIN tb_rst_rf_jenis jenis ON rst.jenis = jenis.id
        JOIN tb_rst_rf_tahapan tahapan ON rst_tahap.tahapan = tahapan.id
        JOIN tb_rst_tr_periode periode ON rst_tahap.periode = periode.id
        JOIN tb_rst_tr_kontributor kontri ON rst.id = kontri.riset
        JOIN tb_peg_rf_pegawai pegawai ON kontri.person = pegawai.nip
        LEFT JOIN (
            tb_rst_tr_riset_props rst_prop_2 INNER JOIN
            tb_rst_rf_options opt_jenis_pn ON rst_prop_2.value = opt_jenis_pn.id AND rst_prop_2.prop = 2
        ) ON rst.id = rst_prop_2.riset
        LEFT JOIN (
            tb_rst_tr_riset_props rst_prop_72 INNER JOIN
            tb_rst_rf_options opt_jenis_pg ON rst_prop_72.value = opt_jenis_pg.id AND rst_prop_72.prop = 72
        ) ON rst.id = rst_prop_72.riset
        WHERE rst_sk.tgl_selesai_berlaku < NOW() AND tahapan.tahapan = "On Going" AND kontri.person = "'.$nip.'" AND rst_tahap.id = (
            SELECT rst_tahap.id
            FROM tb_rst_tr_riset_tahapan rst_tahap
            JOIN tb_rst_tr_periode periode ON rst_tahap.periode = periode.id
            WHERE rst_tahap.riset = rst.id
            ORDER BY periode.urutan DESC
            LIMIT 1
        )
        GROUP BY rst.id
        ';
        $sql = $this->db->query($q);

        return $sql;
    }

    function GetTanggunganProdi($prodi) {
        $q = '
        SELECT
            rst.id AS id,
            rst.judul AS judul,
            CONCAT(IFNULL(pegawai.gelar_depan,""), pegawai.nama, pegawai.gelar_belakang) AS ketua,
            jenis.jenis AS kegiatan,
            periode.nama AS periode,
            tahapan.tahapan,
            DATEDIFF(NOW(), rst_sk.tgl_selesai_berlaku) AS keterlambatan,
            rst_sk.file AS sk,
            rst.laporan_akhir AS lpj
        FROM tb_rst_tr_riset rst
        JOIN tb_rst_tr_riset_sk rst_sk ON rst.id = rst_sk.riset
        JOIN tb_rst_tr_riset_props rst_prop ON rst.id = rst_prop.riset
        JOIN tb_rst_tr_riset_tahapan rst_tahap ON rst.id = rst_tahap.riset
        JOIN tb_rst_rf_jenis jenis ON rst.jenis = jenis.id
        JOIN tb_rst_rf_tahapan tahapan ON rst_tahap.tahapan = tahapan.id
        JOIN tb_rst_tr_periode periode ON rst_tahap.periode = periode.id
        JOIN tb_rst_tr_kontributor kontri ON rst.id = kontri.riset AND kontri.jenis = "Ketua"
		JOIN tb_peg_rf_pegawai pegawai ON kontri.person = pegawai.nip
        LEFT JOIN (
            tb_rst_tr_riset_props rst_prop_2 INNER JOIN
            tb_rst_rf_options opt_jenis_pn ON rst_prop_2.value = opt_jenis_pn.id AND rst_prop_2.prop = 2
        ) ON rst.id = rst_prop_2.riset
        LEFT JOIN (
            tb_rst_tr_riset_props rst_prop_72 INNER JOIN
            tb_rst_rf_options opt_jenis_pg ON rst_prop_72.value = opt_jenis_pg.id AND rst_prop_72.prop = 72
        ) ON rst.id = rst_prop_72.riset
        WHERE rst_sk.tgl_selesai_berlaku < NOW() AND tahapan.tahapan = "On Going" AND rst.prodi LIKE "%'.$prodi.'%" AND rst_tahap.id = (
            SELECT rst_tahap.id
            FROM tb_rst_tr_riset_tahapan rst_tahap
            JOIN tb_rst_tr_periode periode ON rst_tahap.periode = periode.id
            WHERE rst_tahap.riset = rst.id
            ORDER BY periode.urutan DESC
            LIMIT 1
        )
        GROUP BY rst.id
        ';
        $sql = $this->db->query($q);

        return $sql;
    }
}
?>