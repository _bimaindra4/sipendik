<?php
class SessionModel extends CI_Model {
	public function __construct() {
		parent::__construct();
	}

	function GetSession() {
		$data['session_userid'] = $this->session->userdata('session_userid');
		$data['session_role'] = $this->session->userdata('session_role');
		$data['session_nama'] = $this->session->userdata('session_nama');
		$data['session_email'] = $this->session->userdata('session_email');
		$data['session_foto'] = $this->session->userdata('session_foto');
		
		return $data;
	}

	function StoreSession($userid,$email,$role,$nama,$foto) {
		$this->session->set_userdata('session_userid', $userid);
		$this->session->set_userdata('session_role', $role);
		$this->session->set_userdata('session_nama', $nama);
		$this->session->set_userdata('session_email', $email);
		$this->session->set_userdata('session_foto', $foto);
	}

	function DestroySession() {
		$this->session->unset_userdata('session_userid');
		$this->session->unset_userdata('session_role');
		$this->session->unset_userdata('session_nama');
		$this->session->unset_userdata('session_email');
		$this->session->unset_userdata('session_foto');
	}
}
