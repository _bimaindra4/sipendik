<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DosenModel extends CI_Model {
	public function __construct() {
		parent::__construct();
    }

    function GetDataDosen($key = NULL, $value = NULL) {
        if($key != NULL && $value != NULL) {
            $this->db->select("peg.nip, CONCAT(IFNULL(peg.gelar_depan,''), peg.nama, peg.gelar_belakang) AS nama, peg.foto, dosen.nidn, dosen.jabatan_akademik, prodi.Nama_Prodi AS prodi");
            if($key == "peg.nama" || $key == "prodi.Kode_Prodi") {
                $this->db->like($key, $value);
            } else {
                $this->db->where($key, $value);
            }

            $this->db->join("tb_akd_tr_dosen dosen", "peg.nip = dosen.nip", "left");
            $this->db->join("tb_akd_rf_prodi prodi", "dosen.Kode_Prodi = prodi.Kode_Prodi", "left");

            $sql = $this->db->get("tb_peg_rf_pegawai peg");
            return $sql;
        } else {
            $this->db->select("peg.nip, CONCAT(IFNULL(peg.gelar_depan,''), peg.nama, peg.gelar_belakang) AS nama, peg.foto, dosen.nidn, prodi.Nama_Prodi AS prodi");
            $this->db->join("tb_akd_tr_dosen dosen", "peg.nip = dosen.nip", "left");
            $this->db->join("tb_akd_rf_prodi prodi", "dosen.Kode_Prodi = prodi.Kode_Prodi", "left");
            $this->db->order_by("peg.nama", "ASC");

            $sql = $this->db->get("tb_peg_rf_pegawai peg");
            return $sql;
        }
    }

    function GetDataDosenPPM($key = NULL, $value = NULL) {
        $dosen = $this->GetDataDosen($key,$value);
        $output = [];

        foreach($dosen->result() as $row) {
            $data["nip"] = $row->nip;
            $data["nidn"] = $row->nidn;
            $data["nama"] = $row->nama;
            $data["foto"] = $row->foto;
            $data["prodi"] = $row->prodi;
            $data["penelitian"] = $this->GetDataDosenPenelitian($row->nip)->num_rows();
            $data["pengabdian"] = $this->GetDataDosenPengabdian($row->nip)->num_rows();

            $output[] = $data;
        }

        return $output;
    }

    function GetDataDosenPenelitian($nip, $filter = NULL, $filter_value = NULL) {
        if($filter == "periode" && $filter_value != "semua") {
            $clause_periode = " AND periode.id='$filter_value'";
        } else {
            $clause_periode = "";
        }
        
        $q = '
        SELECT
            rst.id AS id,
            periode.id AS id_periode,
			periode.nama AS periode,
            rst.judul AS judul,
            opt_jenis.value AS jenis,
            rst_sk.tgl_mulai_berlaku AS tgl_mulai,
            rst_sk.tgl_selesai_berlaku AS tgl_selesai,
            rst_prop_10.value AS dana,
            tahapan.tahapan,
            kontri.jenis AS status_anggota,
            tahapan.config,
            rst_sk.file AS sk,
            rst.laporan_akhir AS lpj
        FROM tb_rst_tr_riset rst
        JOIN tb_rst_tr_riset_sk rst_sk ON rst.id = rst_sk.riset
        JOIN tb_rst_tr_riset_props rst_prop ON rst.id = rst_prop.riset
		JOIN tb_rst_tr_riset_tahapan rst_tahap ON rst.id = rst_tahap.riset
		JOIN tb_rst_rf_tahapan tahapan ON rst_tahap.tahapan = tahapan.id
		JOIN tb_rst_tr_periode periode ON rst_tahap.periode = periode.id
        LEFT JOIN (
            tb_rst_tr_riset_props rst_prop_2 INNER JOIN
            tb_rst_rf_options opt_jenis ON rst_prop_2.value = opt_jenis.id AND rst_prop_2.prop = 2
        ) ON rst.id = rst_prop_2.riset
        JOIN tb_rst_tr_riset_props rst_prop_10 ON rst.id = rst_prop_10.riset AND rst_prop_10.prop = 10
        JOIN tb_rst_tr_kontributor kontri ON rst.id = kontri.riset
        WHERE rst.jenis = 1 AND kontri.person = "'.$nip.'" '.$clause_periode.' AND rst_tahap.id = (
            SELECT rst_tahap.id
            FROM tb_rst_tr_riset_tahapan rst_tahap
            JOIN tb_rst_tr_periode periode ON rst_tahap.periode = periode.id
            WHERE rst_tahap.riset = rst.id
            ORDER BY periode.urutan DESC
            LIMIT 1
        )
        GROUP BY rst.id';

        $sql = $this->db->query($q);
        return $sql;
    }

    function GetDataDosenPengabdian($nip, $filter = NULL, $filter_value = NULL) {
        if($filter == "periode" && $filter_value != "semua") {
            $clause_periode = " AND periode.id='$filter_value'";
        } else {
            $clause_periode = "";
        }

        $q = '
        SELECT
            rst.id,
            periode.id AS id_periode,
			periode.nama AS periode,
            rst.judul,
            opt_jenis.value AS jenis,
            rst_sk.tgl_mulai_berlaku AS tgl_mulai,
            rst_sk.tgl_selesai_berlaku AS tgl_selesai,
            rst_prop_10.value AS dana,
            tahapan.tahapan,
            kontri.jenis AS status_anggota,
            tahapan.config,
            rst_sk.file AS sk,
            rst.laporan_akhir AS lpj
        FROM tb_rst_tr_riset rst
        JOIN tb_rst_tr_riset_sk rst_sk ON rst.id = rst_sk.riset
        JOIN tb_rst_tr_riset_props rst_prop ON rst.id = rst_prop.riset
		JOIN tb_rst_tr_riset_tahapan rst_tahap ON rst.id = rst_tahap.riset
		JOIN tb_rst_rf_tahapan tahapan ON rst_tahap.tahapan = tahapan.id
		JOIN tb_rst_tr_periode periode ON rst_tahap.periode = periode.id
        LEFT JOIN (
            tb_rst_tr_riset_props rst_prop_72 INNER JOIN
            tb_rst_rf_options opt_jenis ON rst_prop_72.value = opt_jenis.id AND rst_prop_72.prop = 72
        ) ON rst.id = rst_prop_72.riset
        JOIN tb_rst_tr_riset_props rst_prop_10 ON rst.id = rst_prop_10.riset AND rst_prop_10.prop = 10
        JOIN tb_rst_tr_kontributor kontri ON rst.id = kontri.riset
        WHERE rst.jenis = 2 AND kontri.person = "'.$nip.'" '.$clause_periode.' AND rst_tahap.id = (
            SELECT rst_tahap.id
            FROM tb_rst_tr_riset_tahapan rst_tahap
            JOIN tb_rst_tr_periode periode ON rst_tahap.periode = periode.id
            WHERE rst_tahap.riset = rst.id
            ORDER BY periode.urutan DESC
            LIMIT 1
        )
        GROUP BY rst.id
        ';

        $sql = $this->db->query($q);
        return $sql;
    }

    function GetDataKegiatanDosenByStatus($nip,$stts) {
        $clause_st = "";
        if($stts == "Tanggungan") {
            $clause_st = 'AND rst_sk.tgl_selesai_berlaku < NOW() AND tahapan.tahapan = "On Going"';
        } else {
            $clause_st = 'AND tahapan.tahapan = "'.$stts.'"';
        }

        $q = '
        SELECT
            rst.id AS id,
            rst.judul AS judul,
            jenis.jenis AS kegiatan,
            CASE
                WHEN rst.jenis = 1 THEN opt_jenis_pn.value
                WHEN rst.jenis = 2 THEN opt_jenis_pg.value
            END AS jenis,
            periode.nama AS periode,
            tahapan.tahapan
        FROM tb_rst_tr_riset rst
        JOIN tb_rst_tr_riset_sk rst_sk ON rst.id = rst_sk.riset
        JOIN tb_rst_tr_riset_props rst_prop ON rst.id = rst_prop.riset
        JOIN tb_rst_tr_riset_tahapan rst_tahap ON rst.id = rst_tahap.riset
        JOIN tb_rst_rf_jenis jenis ON rst.jenis = jenis.id
        JOIN tb_rst_rf_tahapan tahapan ON rst_tahap.tahapan = tahapan.id
        JOIN tb_rst_tr_periode periode ON rst_tahap.periode = periode.id
        JOIN tb_rst_tr_kontributor kontri ON rst.id = kontri.riset
        LEFT JOIN (
            tb_rst_tr_riset_props rst_prop_2 INNER JOIN
            tb_rst_rf_options opt_jenis_pn ON rst_prop_2.value = opt_jenis_pn.id AND rst_prop_2.prop = 2
        ) ON rst.id = rst_prop_2.riset
        LEFT JOIN (
            tb_rst_tr_riset_props rst_prop_72 INNER JOIN
            tb_rst_rf_options opt_jenis_pg ON rst_prop_72.value = opt_jenis_pg.id AND rst_prop_72.prop = 72
        ) ON rst.id = rst_prop_72.riset
        WHERE kontri.person = "'.$nip.'" '.$clause_st.' AND rst_tahap.id = (
            SELECT rst_tahap.id
            FROM tb_rst_tr_riset_tahapan rst_tahap
            JOIN tb_rst_tr_periode periode ON rst_tahap.periode = periode.id
            WHERE rst_tahap.riset = rst.id
            ORDER BY periode.urutan DESC
            LIMIT 1
        )
        GROUP BY rst.id';

        $sql = $this->db->query($q);
        return $sql;
    }

    function GetDosenStatusAnggota($riset,$nip) {
        $q = '
            SELECT kontri.jenis 
            FROM tb_rst_tr_kontributor kontri
            WHERE kontri.riset = '.$riset.' AND kontri.person = "'.$nip.'"
        ';

        $sql = $this->db->query($q);
        return $sql->row();
    }

    function GetProduktivitasDosen($prodi = NULL) {
        $output = [];
        $dosen = $this->GetDataDosen();
        if($prodi != NULL) {
            $dosen = $this->GetDataDosen("prodi.Kode_Prodi", $prodi);
        }
        
        foreach($dosen->result() as $row_dosen) {
            $data_dosen['nip'] = $row_dosen->nip;
            $data_dosen['nama'] = $row_dosen->nama;
            $data_dosen['foto'] = $row_dosen->foto;
            $data_dosen['penelitian'] = $this->GetProduktivitasPenelitian($row_dosen->nip);
            $data_dosen['pengabdian'] = $this->GetProduktivitasPengabdian($row_dosen->nip);

            $output[] = $data_dosen;
        }

        return $output;
    }

    function GetProduktivitasPenelitian($nip) {
        $CI =& get_instance();
        $CI->load->model('PeriodeModel');
        $periode = $this->PeriodeModel->GetPeriodeLimitForProduktivitas();

        $output = [];

        foreach($periode as $row_periode) {
            $this->db->select("periode.nama, COUNT(kontri.person) AS total");
            $this->db->join("tb_rst_tr_riset rst", "kontri.riset = rst.id");
            $this->db->join("tb_rst_tr_riset_tahapan rst_tahap", "rst.id = rst_tahap.riset");
            $this->db->join("tb_rst_tr_periode periode", "rst_tahap.periode = periode.id");
            $this->db->where("rst.jenis", 1);
            $this->db->where_not_in("rst_tahap.tahapan", [1,4]);
            $this->db->where("periode.id", $row_periode->id);
            $this->db->where("kontri.person", $nip);
            $total_riset = $this->db->get("tb_rst_tr_kontributor kontri")->row();

            $data_periode["periode"] = $total_riset->nama;
            $data_periode["total"] = $total_riset->total; 

            $output[] = $data_periode;
        }

        return $output;
    }

    function GetProduktivitasPengabdian($nip) {
        $CI =& get_instance();
        $CI->load->model('PeriodeModel');
        $periode = $this->PeriodeModel->GetPeriodeLimitForProduktivitas();

        $output = [];

        foreach($periode as $row_periode) {
            $this->db->select("periode.nama, COUNT(kontri.person) AS total");
            $this->db->join("tb_rst_tr_riset rst", "kontri.riset = rst.id");
            $this->db->join("tb_rst_tr_riset_tahapan rst_tahap", "rst.id = rst_tahap.riset");
            $this->db->join("tb_rst_tr_periode periode", "rst_tahap.periode = periode.id");
            $this->db->where("rst.jenis", 2);
            $this->db->where_not_in("rst_tahap.tahapan", [1,4]);
            $this->db->where("periode.id", $row_periode->id);
            $this->db->where("kontri.person", $nip);
            $total_riset = $this->db->get("tb_rst_tr_kontributor kontri")->row();

            $data_periode["periode"] = $total_riset->nama;
            $data_periode["total"] = $total_riset->total; 

            $output[] = $data_periode;
        }

        return $output;
    }
}