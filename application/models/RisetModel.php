<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RisetModel extends CI_Model {
	public function __construct() {
		parent::__construct();
    }

    function GetStatusRiset($id) {
        $q = "
        SELECT tahapan.tahapan AS status
        FROM tb_rst_tr_riset rst
        JOIN tb_rst_tr_riset_tahapan rst_tahap ON rst.id = rst_tahap.riset
        JOIN tb_rst_rf_tahapan tahapan ON rst_tahap.tahapan = tahapan.id
        WHERE rst.id = '$id' AND rst_tahap.id = (
			SELECT MAX(id)
			FROM tb_rst_tr_riset_tahapan rst_tahap
			WHERE rst_tahap.riset = rst.id
		)
        ";

        $sql = $this->db->query($q);
		return $sql->row()->status;
    }
}