<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PendanaanModel extends CI_Model {
	public function __construct() {
		parent::__construct();
	}
	
	function GetDataPendanaan($filter) {
		// Klausa tahapan kegiatan yang paling akhir
		$last_step_clause = "
			AND rst_tahap.id = (
				SELECT rst_tahap.id
				FROM tb_rst_tr_riset_tahapan rst_tahap
				JOIN tb_rst_tr_periode periode ON rst_tahap.periode = periode.id
				WHERE rst_tahap.riset = rst.id
				ORDER BY periode.urutan DESC
				LIMIT 1
			)
		";

		if($filter != NULL) {
			// Filter Prodi
			$prodi_clause = "";
			if(isset($filter['prodi'])) {
				$prodi = $filter['prodi'];
				$prodi_clause = "AND rst.prodi LIKE '%$prodi%'";
			}

			// Filter Periode
			$periode_clause = "";
			if(isset($filter['periode'])) {
				$periode = $filter["periode"];
				if($periode != "") {
					$periode_clause = "AND periode.id='$periode'";
					$last_step_clause = "";
				}
			}

			// Filter Kegiatan
			$kegiatan_clause = "";
			if(isset($filter['kegiatan'])) {
				$kegiatan = $filter["kegiatan"];
				$kegiatan_clause = "AND rst.jenis='$kegiatan'";

				// Sumber Dana
				if($kegiatan == 1) {
					$sd_select = "opt_isd.value AS sumber_dana,";
					$sd_join = "LEFT JOIN (
									tb_rst_tr_riset_props rst_prop_9 INNER JOIN
									tb_rst_rf_options opt_isd ON rst_prop_9.value = opt_isd.id AND rst_prop_9.prop = 9
								) ON rst.id = rst_prop_9.riset";
				} else if($kegiatan == 2) {
					$sd_select = "opt_sd.value AS sumber_dana,";
					$sd_join = "LEFT JOIN (
									tb_rst_tr_riset_props rst_prop_13 INNER JOIN
									tb_rst_rf_options opt_sd ON rst_prop_13.value = opt_sd.id AND rst_prop_13.prop = 13
								) ON rst.id = rst_prop_13.riset";
				}
			}

			$filter = $prodi_clause." ".$periode_clause." ".$kegiatan_clause;
		} else {
			$filter = "";
		}

		$q = '
		SELECT
			rst.judul AS judul,
			'.$sd_select.'
			rst_prop_10.value AS dana
		FROM tb_rst_tr_riset rst
		JOIN tb_rst_tr_riset_props rst_prop ON rst.id = rst_prop.riset
		JOIN tb_rst_tr_riset_tahapan rst_tahap ON rst.id = rst_tahap.riset
		JOIN tb_rst_rf_tahapan tahapan ON rst_tahap.tahapan = tahapan.id
		JOIN tb_rst_tr_periode periode ON rst_tahap.periode = periode.id
		'.$sd_join.'
		JOIN tb_rst_tr_riset_props rst_prop_10 ON rst.id = rst_prop_10.riset AND rst_prop_10.prop = 10
		WHERE rst.accepted = 1 '.$filter.' '.$last_step_clause.'
		GROUP BY rst.id
		';

		$sql = $this->db->query($q);
		return $sql;
	}

	function GetTotalPendanaanProdi($filter) {
		$data = [];
		$filter_clause = "";
		$keg = $filter['kegiatan'];

		// Klausa tahapan kegiatan yang paling akhir
		$last_step_clause = "
			AND rst_tahap.id = (
				SELECT rst_tahap.id
				FROM tb_rst_tr_riset_tahapan rst_tahap
				JOIN tb_rst_tr_periode periode ON rst_tahap.periode = periode.id
				WHERE rst_tahap.riset = rst.id
				ORDER BY periode.urutan DESC
				LIMIT 1
			)
		";
		
		if($filter != NULL) {
			$prodi_clause = "";
			$periode_clause = "";

			// Filter Prodi
			if(isset($filter['prodi'])) {
				$prodi = $filter['prodi'];
				$prodi_clause = "AND rst.prodi LIKE '%$prodi%'";
			}

			// Filter Periode
			if(isset($filter['periode'])) {
				$periode = $filter["periode"];
				if($periode != "") {
					$periode_clause = "AND rst_tahap.periode='$periode'";
					$last_step_clause = "";
				}
			}

			$filter_clause = $prodi_clause." ".$periode_clause;
		}

		if($keg == 1) {
			$q = '
			SELECT opt.id, opt.value
			FROM tb_rst_rf_options opt
			LEFT JOIN tb_rst_tr_riset_props rst_prop ON opt.id = rst_prop.value
			WHERE opt.tags = "[institusi-sumber-dana]"
			GROUP BY opt.id
			';
			
			$sql = $this->db->query($q);
			foreach($sql->result() as $row) {
				$q = '
				SELECT COUNT(rst.id) AS jumlah
				FROM tb_rst_tr_riset rst
				JOIN tb_rst_tr_riset_tahapan rst_tahap ON rst.id = rst_tahap.riset
				JOIN tb_rst_tr_riset_props rst_prop ON rst.id = rst_prop.riset
				WHERE rst_prop.prop = 9 AND rst_prop.value = '.$row->id.' '.$filter_clause.' '.$last_step_clause;
				$sql = $this->db->query($q);
				$sumber_dana = $sql->row();

				$dana['nama'] = $row->value;
				$dana['jumlah'] = $sumber_dana->jumlah;

				$data[] = $dana;
			}
		} else if($keg == 2) {
			$q = '
			SELECT opt.id, opt.value
			FROM tb_rst_rf_options opt
			LEFT JOIN tb_rst_tr_riset_props rst_prop ON opt.id = rst_prop.value
			WHERE opt.tags = "[sumber-dana-pg]"
			GROUP BY opt.id
			';

			$sql = $this->db->query($q);
			foreach($sql->result() as $row) {
				$q = '
				SELECT COUNT(rst.id) AS jumlah
				FROM tb_rst_tr_riset rst
				JOIN tb_rst_tr_riset_tahapan rst_tahap ON rst.id = rst_tahap.riset
				JOIN tb_rst_tr_riset_props rst_prop ON rst.id = rst_prop.riset
				WHERE rst_prop.prop = 13 AND rst_prop.value = '.$row->id.' '.$filter_clause.' '.$last_step_clause;
				$sql = $this->db->query($q);
				$sumber_dana = $sql->row();

				$dana['nama'] = $row->value;
				$dana['jumlah'] = $sumber_dana->jumlah;

				$data[] = $dana;
			}
		}

		return $data;
	}
}
?>