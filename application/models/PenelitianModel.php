<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PenelitianModel extends CI_Model {
	public function __construct() {
		parent::__construct();
	}

	public function GetDataPenelitian($filter = NULL, $id = NULL) {
		$join_kontri = 'JOIN tb_rst_tr_kontributor kontri ON rst.id = kontri.riset';
		$anggota_select = "";
	
		// Filter id
		if($id != NULL) {
			$filter_id = "AND rst.id='$id'";
		} else {
			$filter_id = "AND rst.accepted = 1";
		}
	
		// Filter mahasiswa
		if(isset($filter["mhs"])) {
			$join_mhs = 'AND kontri.person_ref = "Mahasiswa"';
		} else {
			$join_mhs = '';
			$join_kontri = '';
		}
	
		// Klausa tahapan kegiatan yang paling akhir
		$last_step_clause = "
			AND rst_tahap.id = (
				SELECT rst_tahap.id
				FROM tb_rst_tr_riset_tahapan rst_tahap
				JOIN tb_rst_tr_periode periode ON rst_tahap.periode = periode.id
				WHERE rst_tahap.riset = rst.id
				ORDER BY periode.urutan DESC
				LIMIT 1
			)
		";
	
		if($filter != NULL) {
			$waktu_clause = "";
	
			// Filter waktu / periode
			if(isset($filter["pilihan_waktu"])) {
				$pilihan_waktu = $filter["pilihan_waktu"];
				if($pilihan_waktu == "periode") {
					$periode = $filter["periode"];
					if($periode != "") {
						$waktu_clause = "AND periode.id='$periode'";
						$last_step_clause = "";
					}
				} else if($pilihan_waktu == "tanggal") {
					$tanggal = $filter["tanggal"];
					if($tanggal != "") {
						$exp_tanggal = explode(" - ", $tanggal);
						$tgl_awal = date("Y-m-d", strtotime($exp_tanggal[0]));
						$tgl_akhir = date("Y-m-d", strtotime($exp_tanggal[1]));
						$waktu_clause = "AND rst_sk.tgl_mulai_berlaku BETWEEN '$tgl_awal' AND '$tgl_akhir'";
					}
				}
			}
	
			// Filter jenis penelitian
			if(isset($filter["jenis_penelitian"])) {
				$jenis = $filter["jenis_penelitian"];
				if($jenis != "semua") {
					$jenis_clause = "AND opt_jenis.id='$jenis'";
					if($jenis == 2) {
						if(isset($filter["skim"])) {
							$skim = $filter["skim"];
							$skim_clause = " AND opt_skim.id='$skim'";
						} else {
							$skim_clause = "";
						}
	
						$jenis_clause .= $skim_clause;
					}					
				} else {
					$jenis_clause = "";
				}
			} else {
				$jenis_clause = "";
			}
			
			// Filter status penelitian
			if(isset($filter["status_penelitian"])) {
				$stts_pen = $filter["status_penelitian"];
				if($stts_pen != "semua") {
					$status_clause = "AND tahapan.id='$stts_pen'";
				} else {
					$status_clause = "";
				}
			} else {
				$status_clause = "";
			}
	
			// Filter status anggota
			if(isset($filter['status_anggota'])) {
				$stts_anggota = $filter['status_anggota'];
				if($stts_anggota != "semua") {
					$anggota_select = "kontri.jenis AS status_anggota,";
					$anggota_clause = "AND kontri.jenis='".$stts_anggota."'";
					$join_kontri = 'JOIN tb_rst_tr_kontributor kontri ON rst.id = kontri.riset';
				} else {
					$anggota_select = "kontri.jenis AS status_anggota,";
					$anggota_clause = "";
					$join_kontri = 'JOIN tb_rst_tr_kontributor kontri ON rst.id = kontri.riset';
				}
			} else {
				$anggota_clause = "";
			}
			
			// Filter prodi
			if(isset($filter["prodi"])) {
				$prodi_clause = "AND (";
				foreach($filter["prodi"] as $prodi) {
					$prodi_clause .= "prodi LIKE '%$prodi%' OR ";
				}
	
				$prodi_clause = rtrim($prodi_clause, "OR ").")";
			} else {
				$prodi_clause = "";
			}
	
			// Filter dosen
			if(isset($filter["nip"])) {
				$dosen_clause = 'AND kontri.person = "'.$filter['nip'].'"';
			} else {
				$dosen_clause = '';
			}
	
			$filter_clause = $waktu_clause." ".$jenis_clause." ".$status_clause." ".$anggota_clause." ".$prodi_clause." ".$dosen_clause;
		} else {
			$filter_clause = "";
		}
	
		$q = '
		SELECT
			rst.id AS id,
			periode.nama AS periode,
			rst.judul AS judul,
			rst.prodi,
			opt_jenis.value AS jenis,
			IFNULL(opt_skim.id, NULL) AS id_skim,
			CASE
				WHEN opt_jenis.value = "Penelitian Eksternal" THEN opt_skim.id
				ELSE NULL
			END AS skim,
			rst_tahap.id AS id_tahap,
			tahapan.tahapan AS status,
			periode.id AS periode_id,
			periode.nama AS periode,
			rst_sk.tgl_mulai_berlaku,
			rst_sk.tgl_selesai_berlaku,
			opt_subbid.id AS subbid,
			opt_subtujuan.id AS subtujuan,
			CONCAT(opt_bid.value," - ",opt_subbid.value) AS bidang_penelitian,
			CONCAT(opt_tujuan.value," - ",opt_subtujuan.value) AS tujuan_penelitian,
			opt_sd.value AS sumber_dana,
			opt_isd.value AS institusi_sumber_dana,
			rst_prop_10.value AS dana,
			rst.usulan,
			rst_sk.file AS surat_tugas,
			rst.laporan_akhir AS lpj,
			rst.inisiator AS id_inisiator,
			CONCAT(IFNULL(pegawai.gelar_depan,""), pegawai.nama, pegawai.gelar_belakang) AS inisiator,
			tahapan.tahapan,
			tahapan.config,
			'.$anggota_select.'
			rst.prodi
		FROM tb_rst_tr_riset rst
		JOIN tb_rst_tr_riset_sk rst_sk ON rst.id = rst_sk.riset
		JOIN tb_rst_tr_riset_props rst_prop ON rst.id = rst_prop.riset
		LEFT JOIN tb_peg_rf_pegawai pegawai ON rst.inisiator = pegawai.nip
		LEFT JOIN (
			tb_rst_tr_riset_props rst_prop_2 INNER JOIN
			tb_rst_rf_options opt_jenis ON rst_prop_2.value = opt_jenis.id AND rst_prop_2.prop = 2
		) ON rst.id = rst_prop_2.riset
		LEFT JOIN (
			tb_rst_tr_riset_props rst_prop_4 INNER JOIN
			tb_rst_rf_options opt_bid ON rst_prop_4.value = opt_bid.id AND rst_prop_4.prop = 4
		) ON rst.id = rst_prop_4.riset
		LEFT JOIN (
			tb_rst_tr_riset_props rst_prop_5 INNER JOIN
			tb_rst_rf_options opt_subbid ON rst_prop_5.value = opt_subbid.id AND rst_prop_5.prop = 5
		) ON rst.id = rst_prop_5.riset
		LEFT JOIN (
			tb_rst_tr_riset_props rst_prop_6 INNER JOIN
			tb_rst_rf_options opt_tujuan ON rst_prop_6.value = opt_tujuan.id AND rst_prop_6.prop = 6
		) ON rst.id = rst_prop_6.riset
		LEFT JOIN (
			tb_rst_tr_riset_props rst_prop_7 INNER JOIN
			tb_rst_rf_options opt_subtujuan ON rst_prop_7.value = opt_subtujuan.id AND rst_prop_7.prop = 7
		) ON rst.id = rst_prop_7.riset
		LEFT JOIN (
			tb_rst_tr_riset_props rst_prop_8 INNER JOIN
			tb_rst_rf_options opt_sd ON rst_prop_8.value = opt_sd.id AND rst_prop_8.prop = 8
		) ON rst.id = rst_prop_8.riset
		LEFT JOIN (
			tb_rst_tr_riset_props rst_prop_9 INNER JOIN
			tb_rst_rf_options opt_isd ON rst_prop_9.value = opt_isd.id AND rst_prop_9.prop = 9
		) ON rst.id = rst_prop_9.riset
		LEFT JOIN (
			tb_rst_tr_riset_props rst_prop_73 INNER JOIN
			tb_rst_rf_options opt_skim ON rst_prop_73.value = opt_skim.id AND rst_prop_73.prop = 73
		) ON rst.id = rst_prop_73.riset
		JOIN tb_rst_tr_riset_props rst_prop_10 ON rst.id = rst_prop_10.riset AND rst_prop_10.prop = 10
		JOIN tb_rst_tr_riset_tahapan rst_tahap ON rst.id = rst_tahap.riset
		JOIN tb_rst_rf_tahapan tahapan ON rst_tahap.tahapan = tahapan.id
		JOIN tb_rst_tr_periode periode ON rst_tahap.periode = periode.id
		'.$join_kontri.' '.$join_mhs.'
		WHERE rst.jenis = 1 '.$filter_id.' '.$filter_clause.' '.$last_step_clause.'
		GROUP BY rst.id
		';
	
		$sql = $this->db->query($q);
		return $sql;
	}

	public function GetPersonilPenelitian($id,$ref) {
		$q = '
		SELECT
			CASE
				WHEN kontri.person_ref = "Pegawai" THEN pegawai.nip
				WHEN kontri.person_ref = "Mahasiswa" THEN mhs.NRP
			END AS identitas,
			CASE
				WHEN kontri.person_ref = "Pegawai" THEN CONCAT(IFNULL(pegawai.gelar_depan,""), pegawai.nama, pegawai.gelar_belakang)
				WHEN kontri.person_ref = "Mahasiswa" THEN mhs.Nama_Mhs
			END AS personil,
			CASE
				WHEN kontri.person_ref = "Pegawai" THEN prodi_dos.Nama_Prodi
				WHEN kontri.person_ref = "Mahasiswa" THEN prodi_mhs.Nama_Prodi
			END AS prodi,
			kontri.jenis AS status
		FROM tb_rst_tr_kontributor kontri
		LEFT JOIN tb_akd_rf_mahasiswa mhs ON kontri.person = mhs.NRP
		LEFT JOIN tb_peg_rf_pegawai pegawai ON kontri.person = pegawai.nip
		LEFT JOIN tb_akd_tr_dosen dosen ON pegawai.nip = dosen.NIP
		LEFT JOIN tb_akd_rf_prodi prodi_dos ON dosen.Kode_Prodi = prodi_dos.Kode_Prodi
		LEFT JOIN tb_akd_rf_prodi prodi_mhs ON mhs.Kode_Prodi = prodi_mhs.Kode_Prodi
		WHERE kontri.riset='.$id.' AND kontri.person_ref = "'.$ref.'"
		';

		$sql = $this->db->query($q);
		return $sql;		
	}

	public function GetDetailPersonilPenelitian($id,$person) {
		$this->db->select("person_ref, jenis");
		$this->db->where("riset", $id);
		$this->db->where("person", $person);
		$sql = $this->db->get("tb_rst_tr_kontributor");

		return $sql->row();
	}

	public function GetRevisiPenelitian($id) {
		$this->db->select("keterangan, tanggal");
		$this->db->where("riset", $id);
		$sql = $this->db->get("tb_rst_tr_riset_revisi");

		return $sql;
	}

	public function GetDataSK($id) {
		$this->db->select("id, no_sk, tentang, tgl_ditetapkan, tgl_mulai_berlaku, 
						   tgl_selesai_berlaku, penerbit, pejabat_ttd");
		$this->db->where("riset", $id);
		$sql = $this->db->get("tb_rst_tr_riset_sk");

		return $sql;
	}

	public function GetDokumenPenelitian($id) {
		$this->db->select("id, nama, file");
		$this->db->where("riset", $id);

		$sql = $this->db->get("tb_rst_tr_riset_dokumen");
		return $sql;
	}

	public function GetTahapanPenelitian($id) {
		$this->db->select("rst_tahap.id, periode.nama, tahapan.tahapan");
		$this->db->join("tb_rst_tr_periode periode", "rst_tahap.periode = periode.id");
		$this->db->join("tb_rst_rf_tahapan tahapan", "rst_tahap.tahapan = tahapan.id");
		$this->db->where("rst_tahap.riset", $id);

		$sql = $this->db->get("tb_rst_tr_riset_tahapan rst_tahap");
		return $sql;
	}
	
	public function TambahPenelitian($data) {
		$this->db->insert("tb_rst_tr_riset", $data);
		return $this->db->insert_id();
	}

	public function TambahPenelitianProp($data) {
		$sql = $this->db->insert_batch("tb_rst_tr_riset_props", $data);
		return $sql;
	}

	public function TambahPenelitianSK($data) {
		$sql = $this->db->insert("tb_rst_tr_riset_sk", $data);
		return $sql;
	}

	public function TambahPenelitianTahap($data) {
		$sql = $this->db->insert("tb_rst_tr_riset_tahapan", $data);
		return $sql;
	}

	public function TambahPersonil($data) {
		$sql = $this->db->insert("tb_rst_tr_kontributor", $data);
		return $sql;
	}

	public function TambahDokumen($data) {
		$sql = $this->db->insert("tb_rst_tr_riset_dokumen", $data);
		return $sql;
	}

	public function TambahRevisiPenelitian($data) {
		$sql = $this->db->insert("tb_rst_tr_riset_revisi", $data);
		return $sql;
	}

	public function TambahStatusPenelitian($data) {
		$sql = $this->db->insert("tb_rst_tr_riset_tahapan", $data);
		return $sql;
	}

	public function EditPenelitian($data, $id) {
		$sql = $this->db->update("tb_rst_tr_riset", $data, ["id" => $id]);
		return $sql;
	}

	public function EditPenelitianProp($data, $where) {
		$sql = $this->db->update("tb_rst_tr_riset_props", $data, $where);
		return $sql;
	}

	public function UpdateStatusPenelitian($data,$id,$periode) {
		$sql = $this->db->update("tb_rst_tr_riset_tahapan", $data, ["riset" => $id, "periode" => $periode]);
		return $sql;
	}

	public function EditDataSK($data,$where) {
		$sql = $this->db->update("tb_rst_tr_riset_sk", $data, $where);
		return $sql;
	}

	public function EditDokumen($data,$id) {
		$sql = $this->db->update("tb_rst_tr_riset_dokumen", $data, ["id" => $id]);
		return $sql;
	}

	public function CheckKetuaPenelitian($id) {
		$this->db->select("person");
		$this->db->where("riset", $id);
		$this->db->where("jenis", "Ketua");
		$sql = $this->db->get("tb_rst_tr_kontributor");

		$init = $sql->num_rows();
		if($init > 0) {
			return false;
		} else {
			return true;
		}
	}

	public function HapusPenelitian($id) {
		$sql = $this->db->delete("tb_rst_tr_riset", ["id" => $id]);
		return $sql;
	}

	public function HapusPersonil($id,$person) {
		$sql = $this->db->delete("tb_rst_tr_kontributor", ["riset" => $id, "person" => $person]);
		return $sql;
	}

	public function HapusDokumen($id) {
		$sql = $this->db->delete("tb_rst_tr_riset_dokumen", ["id" => $id]);
		return $sql;
	}

	public function UploadFileLPJ($riset,$file) {
		$sql = $this->db->update("tb_rst_tr_riset", ["laporan_akhir" => $file], ["id" => $riset]);
		return $sql;
	}

	public function UploadFileProposal($riset,$file) {
		$sql = $this->db->update("tb_rst_tr_riset", ["usulan" => $file], ["id" => $riset]);
		return $sql;
	}
}