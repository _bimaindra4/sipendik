<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UsulanModel extends CI_Model {
	public function __construct() {
		parent::__construct();
    }
    
    public function GetDataUsulan($filter = NULL) {
        $clause_tahapan = 'AND rst_tahap.tahapan = 1';
        $clause_filter = '';
        
        if($filter != NULL) {
            $clause_tahapan = '';
            $filter_id = '';
            $filter_inisiator = '';
            $filter_prodi = '';

            if(isset($filter['id'])) {
                $id = $filter['id'];
                $id_riset = $filter['id'];
                $filter_riset = 'AND rst.id="'.$id.'"';
            }

            if(isset($filter['inisiator'])) {
                $init = $filter['inisiator'];
                $filter_inisiator = 'AND rst.inisiator="'.$init.'"';
            }

            if(isset($filter['prodi'])) {
                $prodi = $filter['prodi'];
                $filter_prodi = 'AND rst.prodi LIKE "%'.$prodi.'%" AND rst_tahap.id = (
                    SELECT rst_tahap.id
                    FROM tb_rst_tr_riset_tahapan rst_tahap
                    JOIN tb_rst_tr_periode periode ON rst_tahap.periode = periode.id
                    WHERE rst_tahap.riset = rst.id
                    ORDER BY periode.urutan DESC
                    LIMIT 1
                )';
            }

            $clause_filter = $filter_id.' '.$filter_inisiator.' '.$filter_prodi;
        }

        $q = '
        SELECT
            rst.id,
            rst.Created_Date AS tgl_pengajuan,
            rst.judul,
            jenis.id AS jenis_id,
            jenis.jenis,
            rst_sk.tgl_mulai_berlaku,
            rst_sk.tgl_selesai_berlaku,
            CONCAT(IFNULL(pegawai.gelar_depan,""), pegawai.nama, pegawai.gelar_belakang) AS inisiator,
            rst.inisiator AS nip_inisiator,
            rst_tahap.periode,
            periode.nama AS nama_periode,
            rst.usulan,
            rst.accepted
        FROM tb_rst_tr_riset rst
        JOIN tb_rst_rf_jenis jenis ON rst.jenis = jenis.id
        JOIN tb_rst_tr_riset_sk rst_sk ON rst.id = rst_sk.riset
        JOIN tb_rst_tr_riset_tahapan rst_tahap ON rst.id = rst_tahap.riset
        JOIN tb_rst_tr_periode periode ON rst_tahap.periode = periode.id
        LEFT JOIN tb_peg_rf_pegawai pegawai ON rst.inisiator = pegawai.nip
        WHERE rst.accepted IS NULL '.$clause_tahapan.' '.$clause_filter;
        $sql = $this->db->query($q);

        return (isset($filter['id']) ? $sql->row() : $sql->result());
    }

    public function GetHistoriUsulan($prodi = NULL) {
        if($prodi != NULL) {
            $prodi_clause = 'AND rst.prodi LIKE "%'.$prodi.'%"';
        } else {
            $prodi_clause = '';
        }

        $q = '
        SELECT
            rst.id,
            rst.Created_Date AS tgl_pengajuan,
            rst.judul,
            jenis.id AS jenis_id,
            jenis.jenis,
            rst_sk.tgl_mulai_berlaku,
            rst_sk.tgl_selesai_berlaku,
            CONCAT(IFNULL(pegawai.gelar_depan,""), pegawai.nama, pegawai.gelar_belakang) AS inisiator,
            rst_tahap.periode,
            periode.nama AS nama_periode,
            rst.usulan,
            rst.accepted
        FROM tb_rst_tr_riset rst
        JOIN tb_rst_rf_jenis jenis ON rst.jenis = jenis.id
        JOIN tb_rst_tr_riset_sk rst_sk ON rst.id = rst_sk.riset
        JOIN tb_rst_tr_riset_tahapan rst_tahap ON rst.id = rst_tahap.riset
        JOIN tb_rst_tr_periode periode ON rst_tahap.periode = periode.id
        LEFT JOIN tb_peg_rf_pegawai pegawai ON rst.inisiator = pegawai.nip
        WHERE rst.accepted IS NOT NULL '.$prodi_clause.' AND rst_tahap.id = (
			SELECT rst_tahap.id
			FROM tb_rst_tr_riset_tahapan rst_tahap
			JOIN tb_rst_tr_periode periode ON rst_tahap.periode = periode.id
			WHERE rst_tahap.riset = rst.id
			ORDER BY periode.urutan DESC
			LIMIT 1
		)';
        $sql = $this->db->query($q);

        return $sql->result();
    }

    public function UbahStatusAccepted($data,$periode,$id) {
        if($data['accepted'] != 3) {
            // Update status form Usulan to On Going
            $this->db->update("tb_rst_tr_riset_tahapan", ["tahapan" => 2], ["riset" => $id, "periode" => $periode]);
        }

        $sql = $this->db->update("tb_rst_tr_riset", $data, ["id" => $id]);
        return $sql;
    }
}