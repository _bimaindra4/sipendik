<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AppModel extends CI_Model {
	public function __construct() {
		parent::__construct();
	}

	function DateTimeNow() {
		date_default_timezone_set("Asia/Jakarta");
		return date("Y-m-d H:i:s");
	}

	function BulanIndo($val="") {
		if($val == "") {
			$month = [
				'-- Bulan --' => 0,
				'Januari' => 1,
				'Februari' => 2,
				'Maret' => 3,
				'April' => 4,
				'Mei' => 5,
				'Juni' => 6,
				'Juli' => 7,
				'Agustus' => 8,
				'September' => 9,
				'Oktober' => 10,
				"November" => 11,
				"Desember" => 12
			];

			return $month;
		} else {
			$month = [
				0 => '-- Bulan --',
				1 => 'Januari',
				2 => 'Februari',
				3 => 'Maret',
				4 => 'April',
				5 => 'Mei',
				6 => 'Juni',
				7 => 'Juli',
				8 => 'Agustus',
				9 => 'September',
				10 => 'Oktober',
				11 => 'November',
				12 => 'Desember'
			];

			return $month[$val];
		}
	}

	function HariIndo() {
		$hari = [
			'-- Hari --' => 0,
			'Senin' => 'senin',
			'Selasa' => 'selasa',
			'Rabu' => 'rabu',
			'Kamis' => 'kamis',
			'Jumat' => 'jumat',
			'Sabtu' => 'sabtu',
			'Minggu' => 'minggu'
		];

		return $hari;
	}

	function DateIndo($data) {
		if($data == NULL || $data == "") {
			return "";
		} else {
			$x_data = explode("-", $data);
			$bulan = $this->BulanIndo((int) $x_data[1]);
			$output = $x_data[2]." ".$bulan." ".$x_data[0];
			
			return $output;
		}
	}

	function TimeIndo($data) {
		if($data == NULL || $data == "") {
			return "";
		} else {
			$time = explode(":", $data);
			$output = $time[0].":".$time[1];

			return $output;
		}
	}

	function DateTimeIndo($data) {
		$x_data = explode(" ", $data);
		$date = $this->DateIndo($x_data[0]);
		$time = $this->TimeIndo($x_data[1]);
		$output = $date." - ".$time;

		return $output;
	}

	function ConvertDay($hari) {
		if($hari > 7) {
			if($hari > 30) {
				if($hari > 365) {
					$output = floor($hari / 365)." Tahun";
				} else {
					$output = floor($hari / 30)." Bulan";
				}
			} else {
				$output = floor($hari / 7)." Minggu";
			}
		} else {
			$output = $hari." Hari";
		}
		
		return $output;
	}

	function GetOptionDropdown($tag, $parent = NULL) {
		$this->db->select("id, value");
		$this->db->like("tags", $tag);

		if($parent != NULL) {
			$this->db->where("parent_id", $parent);
		}

		$this->db->order_by("urutan", "ASC");
		$sql = $this->db->get("tb_rst_rf_options");

		return $sql->result();
	}

	function GetOptionValue($id) {
		$this->db->select("value");
		$this->db->where("id", $id);
		$sql = $this->db->get("tb_rst_rf_options");

		return $sql->row()->value;
	}
	
	function GetPropertiesFormJenis($id, $type="form") {
		if($type == "form") {
			$this->db->select("prop.id, prop.prop, prop.type, prop.tags, prop.configs");
			$this->db->join("tb_rst_rf_prop prop", "jprop.prop = prop.id");
			$this->db->join("tb_rst_rf_jenis jenis", "jprop.jenis = jenis.id");
			$this->db->where("jenis.id", $id);
			$this->db->where("prop.type !=", "suboption");
			$this->db->order_by("jprop.urutan", "ASC");
			$sql = $this->db->get("tb_rst_rf_jenis_props jprop");
		} else if($type == "edit") {
			$this->db->select("prop.id, prop.prop, prop.type, prop.tags, rst_prop.value, prop.configs");
			$this->db->join("tb_rst_rf_jenis_props jprop", "prop.id = jprop.prop");
			$this->db->join("tb_rst_tr_riset_props rst_prop", "prop.id = rst_prop.prop", "LEFT");
			$this->db->where("rst_prop.riset", $id);
			$this->db->where("prop.type !=", "suboption");
			$this->db->group_by("jprop.prop");
			$this->db->order_by("jprop.urutan", "ASC");
			$sql = $this->db->get("tb_rst_rf_prop prop");
		}

		foreach($sql->result() as $row) {
			$value = (isset($row->value) ? $row->value : NULL);
			$form[] = [
				"label" => $row->prop,
				"form" => $this->ConvertToFormHTML($row->id, $row->type, $row->prop, $row->tags, $row->configs, $value)
			];
		}

		return $form;
	}

	function GetPropertiesFormLuaran($id, $type="form") {
		if($type == "form") {
			$this->db->select("prop.id, prop.prop, prop.type, prop.tags, prop.configs");
			$this->db->join("tb_rst_rf_prop prop", "lprop.prop = prop.id");
			$this->db->join("tb_rst_rf_jenis_luaran luaran", "lprop.luaran = luaran.id");
			$this->db->where("luaran.id", $id);
			$this->db->where("prop.type !=", "suboption");
			$this->db->order_by("urutan", "ASC");

			$sql = $this->db->get("tb_rst_rf_luaran_props lprop");
		} else if($type == "edit") {
			$this->db->select("prop.id, prop.prop, prop.type, prop.tags, prop.configs, rst_luaran.value");
			$this->db->join("tb_rst_rf_luaran luaran", "rst_luaran.luaran = luaran.id");
			$this->db->join("tb_rst_rf_prop prop", "rst_luaran.prop = prop.id");
			$this->db->where("luaran.id", $id);
			$this->db->group_by("prop.id");
			$this->db->order_by("rst_luaran.id", "ASC");

			$sql = $this->db->get("tb_rst_tr_riset_luaran rst_luaran");
		}
		
		foreach($sql->result() as $row) {
			$value = (isset($row->value) ? $row->value : NULL);
			$form[] = [
				"id" => $row->id,
				"label" => $row->prop,
				"form" => $this->ConvertToFormHTML($row->id, $row->type, $row->prop, $row->tags, $row->configs, $value)
			];
		}

		return $form;
	}

	function GetDataUnit() {
		$this->db->select("unit_id, nama_unit");
		$sql = $this->db->get("tb_peg_rf_unit");

		return $sql->result();
	}

	function GetDataPegawai() {
		$this->db->select("nip, CONCAT(IFNULL(gelar_depan,''), nama, gelar_belakang) AS nama");
		$sql = $this->db->get("tb_peg_rf_pegawai");

		return $sql->result();
	}

	function GetPeriode() {
		$this->db->select("id, nama, isAktif");
		$sql = $this->db->get("tb_rst_tr_periode");

		return $sql->result();
	}

	function GetDataProdi() {
		$this->db->select("Kode_Prodi, Nama_Prodi");
		$sql = $this->db->get("tb_akd_rf_prodi");

		return $sql->result();
	}

	function GetDataTahapan() {
		$this->db->select("id, tahapan");
		$sql = $this->db->get("tb_rst_rf_tahapan");

		return $sql->result();
	}

	function ConvertToFormHTML($id,$jenis,$prop,$tags,$configs = NULL,$value = NULL) {
		$output = '';
		$config_array = json_decode($configs);
		$val = ($value != NULL ? 'value="'.$value.'"' : '');

		if($jenis == "text" || $jenis == "daterange") {
			$output .= '<input type="text" class="form-control" name="'.$id.'" '.$val;
			if($configs != NULL) {
				foreach($config_array->form_option as $key => $value) {
					$output .= $key."='".$value."' ";
				}
			}
			$output = rtrim($output, " ");
			$output .= '>';
		} else if($jenis == "file") {
			$output .= '<input type="file" class="form-control" name="'.$id.'" ';
			if($configs != NULL) {
				foreach($config_array->form_option as $key => $value) {
					$output .= $key.'="'.$value.'" ';
				}
			}
			$output = rtrim($output, " ");
			$output .= '>';
		} else if($jenis == "number") {
			$output .= '<input type="number" class="form-control" name="'.$id.'" '.$val;
			if($configs != NULL) {
				foreach($config_array->form_option as $key => $value) {
					$output .= $key.'="'.$value.'" ';
				}
			}
			$output = rtrim($output, " ");
			$output .= '>';
		} else if($jenis == "date") {
			$output .= '<input type="date" class="form-control" name="'.$id.'" '.$val;
			if($configs != NULL) {
				foreach($config_array->form_option as $key => $value) {
					$output .= $key.'="'.$value.'" ';
				}
			}
			$output = rtrim($output, " ");
			$output .= '>';
		} else if($jenis == "option") {
			$output .= '<select class="form-control" name="'.$id.'" ';
			if($configs != NULL) {
				foreach($config_array->form_option as $key => $val) {
					$output .= $key.'="'.$val.'" ';
				}
			}
			
			$output = rtrim($output, " ");
			$output .= '>';

			// Showing option
			$options = $this->GetOptionDropdown($tags);
			$output .= ($value == NULL ? '<option>-- Pilih --</option>' : '');
			foreach($options as $option) {
				$selected = ($option->id == $value ? "selected" : "");
				$output .= '<option value="'.$option->id.'" '.$selected.'>'.$option->value.'</option>';
			}
			$output .= '</select>';
		} else if($jenis == "checkbox") {
			$output .= '<div class="checkbox-custom mb5" style="padding-top: 10px"><input type="checkbox" name="'.$id.'" ';
			if($configs != NULL) {
				foreach($config_array->form_option as $key => $value) {
					$output .= $key.'="'.$value.'" ';
				}
			}
			$output = rtrim($output, " ");
			$output .= '>';

			$output .= '<label for="checkboxDefault3">'.$config_array->label.'</label>';
			$output .= '</div>';
		} else if($jenis == "textarea") {
			$output .= '<textarea class="form-control" name="'.$id.'" ';
			if($configs != NULL) {
				foreach($config_array->form_option as $key => $val) {
					$output .= $key.'="'.$val.'" ';
				}
			}
			$output = rtrim($output, " ");
			$output .= '>'.$value.'</textarea>';
		}

		return $output;
	}
}
?>
