<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PengumumanModel extends CI_Model {
	public function __construct() {
		parent::__construct();
    }
    
    public function GetPengumuman($id = NULL) {
        $clause_where = ($id != NULL ? " WHERE id='$id'" : "");
        $q = '
        SELECT id, judul, isi, kategori, Created_By, Created_Date
        FROM tb_rst_rf_pengumuman '.$clause_where;

        $sql = $this->db->query($q);
        return ($id != NULL ? $sql->row() : $sql->result());
    }

    public function TambahPengumuman($data) {
        $sql = $this->db->insert("tb_rst_rf_pengumuman", $data);
        return $sql;
    }

    public function EditPengumuman($data,$id) {
        $sql = $this->db->update("tb_rst_rf_pengumuman", $data, ["id" => $id]);
        return $sql;
    }

    public function HapusPengumuman($id) {
        $sql = $this->db->delete("tb_rst_rf_pengumuman", ["id" => $id]);
        return $sql;
    }
}