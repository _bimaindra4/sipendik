<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PengabdianModel extends CI_Model {
	public function __construct() {
		parent::__construct();
	}

	public function GetAllDataPengabdian($filter = NULL) {
		$join_kontri = 'JOIN tb_rst_tr_kontributor kontri ON rst.id = kontri.riset';
		$anggota_select = "";

		// Filter mahasiswa
		if(isset($filter["mhs"])) {
			$join_mhs = 'AND kontri.person_ref = "Mahasiswa"';
		} else {
			$join_mhs = '';
			$join_kontri = '';
		}

		// Klausa tahapan yang paling akhir
		$last_step_clause = "
			AND rst_tahap.id = (
				SELECT rst_tahap.id
				FROM tb_rst_tr_riset_tahapan rst_tahap
				JOIN tb_rst_tr_periode periode ON rst_tahap.periode = periode.id
				WHERE rst_tahap.riset = rst.id
				ORDER BY periode.urutan DESC
				LIMIT 1
			)
		";
		
		if($filter != NULL) {
			// Filter waktu / periode
			if(isset($filter["pilihan_waktu"])) {
				$pilihan_waktu = $filter["pilihan_waktu"];
				if($pilihan_waktu == "periode") {
					$periode = $filter["periode"];
					if($periode != "") {
						$waktu_clause = "AND periode.id='$periode'";
						$last_step_clause = "";
					} else {
						$waktu_clause = "";
					}
				} else if($pilihan_waktu == "tanggal") {
					$tanggal = $filter["tanggal"];
					$exp_tanggal = explode(" - ", $tanggal);
					$tgl_awal = date("Y-m-d", strtotime($exp_tanggal[0]));
					$tgl_akhir = date("Y-m-d", strtotime($exp_tanggal[1]));
					$waktu_clause = "AND rst_sk.tgl_mulai_berlaku BETWEEN '$tgl_awal' AND '$tgl_akhir'";
				} else {
					$waktu_clause = "";
				}
			} else {
				$waktu_clause = "";
			}

			// Filter jenis pengabdian
			if(isset($filter["jenis_pengabdian"])) {
				$jenis = $filter["jenis_pengabdian"];
				if($jenis != "semua") {
					$jenis_clause = "AND opt_jenis.id='$jenis'";
					if($jenis == 2) {
						if(isset($filter["skim"])) {
							$skim = $filter["skim"];
							$skim_clause = " AND opt_skim.id='$skim'";
						} else {
							$skim_clause = "";
						}

						$jenis_clause .= $skim_clause;	
					}
				} else {
					$jenis_clause = "";
				}
			} else {
				$jenis_clause = "";
			}

			// Filter status pengabdian
			if(isset($filter["status_pengabdian"])) {
				$stts_pen = $filter["status_pengabdian"];
				if($stts_pen != "semua") {
					$status_clause = "AND tahapan.id='$stts_pen'";
				} else {
					$status_clause = "";
				}
			} else {
				$status_clause = "";
			}

			// Filter status anggota
			if(isset($filter['status_anggota'])) {
				$stts_anggota = $filter['status_anggota'];
				if($stts_anggota != "semua") {
					$anggota_select = "kontri.jenis AS status_anggota,";
					$anggota_clause = "AND kontri.jenis='".$stts_anggota."'";
					$join_kontri = 'JOIN tb_rst_tr_kontributor kontri ON rst.id = kontri.riset';
				} else {
					$anggota_select = "kontri.jenis AS status_anggota,";
					$anggota_clause = "";
					$join_kontri = 'JOIN tb_rst_tr_kontributor kontri ON rst.id = kontri.riset';
				}
			} else {
				$anggota_clause = "";
			}

			// Filter prodi
			if(isset($filter["prodi"])) {
				$prodi_clause = "AND (";
				foreach($filter["prodi"] as $prodi) {
					$prodi_clause .= "prodi LIKE '%$prodi%' OR ";
				}

				$prodi_clause = rtrim($prodi_clause, "OR ").")";
			} else {
				$prodi_clause = "";
			}

			// Filter dosen
			if(isset($filter["nip"])) {
				$dosen_clause = 'AND kontri.person = "'.$filter['nip'].'"';
			} else {
				$dosen_clause = '';
			}
			
			$filter_clause = $waktu_clause." ".$jenis_clause." ".$status_clause." ".$anggota_clause." ".$prodi_clause." ".$dosen_clause;
		} else {
			$filter_clause = "";
		}

		$q = '
		SELECT
			rst.id,
			periode.nama AS periode,
			rst.judul,
			opt_jenis.value AS jenis,
			rst_sk.tgl_mulai_berlaku,
			rst_sk.tgl_selesai_berlaku,
			rst_prop_10.value AS dana,
			tahapan.tahapan,
			'.$anggota_select.'
			tahapan.config
		FROM tb_rst_tr_riset rst
		JOIN tb_rst_tr_riset_sk rst_sk ON rst.id = rst_sk.riset
		JOIN tb_rst_tr_riset_props rst_prop ON rst.id = rst_prop.riset
		JOIN tb_rst_tr_riset_tahapan rst_tahap ON rst.id = rst_tahap.riset
		JOIN tb_rst_rf_tahapan tahapan ON rst_tahap.tahapan = tahapan.id
		JOIN tb_rst_tr_periode periode ON rst_tahap.periode = periode.id
		JOIN tb_rst_tr_kontributor kontri ON rst.id = kontri.riset '.$join_mhs.'
		LEFT JOIN (
			tb_rst_tr_riset_props rst_prop_72 INNER JOIN
			tb_rst_rf_options opt_jenis ON rst_prop_72.value = opt_jenis.id AND rst_prop_72.prop = 72
		) ON rst.id = rst_prop_72.riset
		JOIN tb_rst_tr_riset_props rst_prop_10 ON rst.id = rst_prop_10.riset AND rst_prop_10.prop = 10
		WHERE rst.jenis = 2 AND rst.accepted = 1 '.$filter_clause.' '.$last_step_clause.'
		GROUP BY rst.id
		';
		$sql = $this->db->query($q);
		return $sql;
	}

	public function GetDetailDataPengabdian($id) {
		$q = "
		SELECT
			rst.id,
			rst.judul,
			rst.prodi,
			opt_jenis.value AS jenis,
			CASE
				WHEN opt_jenis.value = 'Penelitian Eksternal' THEN opt_skim.value
				ELSE NULL
			END AS skim,
			rst_tahap.id AS id_tahap,
			tahapan.tahapan AS status,
			periode.id AS periode_id,
			periode.nama AS periode,
			opt_jenis_keg.value AS jenis_keg,
			opt_tingkat.value AS tingkat,
			rst_prop_10.value AS dana,
			opt_sumber_dana.value AS sumber_dana,
			rst_sk.tgl_mulai_berlaku,
			rst_sk.tgl_selesai_berlaku,
			opt_sdi.value AS sdi,
			rst.usulan AS usulan,
			rst_sk.file AS surat_tugas,
			rst.laporan_akhir AS lpj,
			CONCAT(IFNULL(pegawai.gelar_depan,''), pegawai.nama, pegawai.gelar_belakang) AS inisiator,
			rst.inisiator AS id_inisiator
		FROM tb_rst_tr_riset rst
		JOIN tb_rst_tr_riset_sk rst_sk ON rst.id = rst_sk.riset
		JOIN tb_rst_tr_riset_props rst_prop ON rst.id = rst_prop.riset
        LEFT JOIN tb_peg_rf_pegawai pegawai ON rst.inisiator = pegawai.nip
		LEFT JOIN (
			tb_rst_tr_riset_props rst_prop_72 INNER JOIN
			tb_rst_rf_options opt_jenis ON rst_prop_72.value = opt_jenis.id AND rst_prop_72.prop = 72
		) ON rst.id = rst_prop_72.riset
		LEFT JOIN (
			tb_rst_tr_riset_props rst_prop_11 INNER JOIN
			tb_rst_rf_options opt_jenis_keg ON rst_prop_11.value = opt_jenis_keg.id AND rst_prop_11.prop = 11
		) ON rst.id = rst_prop_11.riset
		LEFT JOIN (
			tb_rst_tr_riset_props rst_prop_12 INNER JOIN
			tb_rst_rf_options opt_tingkat ON rst_prop_12.value = opt_tingkat.id AND rst_prop_12.prop = 12
		) ON rst.id = rst_prop_12.riset
		LEFT JOIN (
			tb_rst_tr_riset_props rst_prop_13 INNER JOIN
			tb_rst_rf_options opt_sumber_dana ON rst_prop_13.value = opt_sumber_dana.id AND rst_prop_13.prop = 13
		) ON rst.id = rst_prop_13.riset
		LEFT JOIN (
			tb_rst_tr_riset_props rst_prop_14 INNER JOIN
			tb_rst_rf_options opt_sdi ON rst_prop_14.value = opt_sdi.id AND rst_prop_14.prop = 14
		) ON rst.id = rst_prop_14.riset
		LEFT JOIN (
			tb_rst_tr_riset_props rst_prop_73 INNER JOIN
			tb_rst_rf_options opt_skim ON rst_prop_73.value = opt_skim.id AND rst_prop_73.prop = 73
		) ON rst.id = rst_prop_73.riset
		JOIN tb_rst_tr_riset_props rst_prop_10 ON rst.id = rst_prop_10.riset AND rst_prop_10.prop = 10
		JOIN tb_rst_tr_riset_tahapan rst_tahap ON rst.id = rst_tahap.riset
		JOIN tb_rst_rf_tahapan tahapan ON rst_tahap.tahapan = tahapan.id
		JOIN tb_rst_tr_periode periode ON rst_tahap.periode = periode.id
		WHERE rst.id = ".$id." AND rst_tahap.id = (
			SELECT MAX(id)
			FROM tb_rst_tr_riset_tahapan rst_tahap
			WHERE rst_tahap.riset = rst.id
		)
		GROUP BY rst.id
		";
		$sql = $this->db->query($q);
		return $sql->row();
	}

	public function GetPersonilPengabdian($id,$ref) {
		$q = '
		SELECT
			CASE
				WHEN kontri.person_ref = "Pegawai" THEN pegawai.nip
				WHEN kontri.person_ref = "Mahasiswa" THEN mhs.NRP
				WHEN kontri.person_ref = "Staf" THEN kontri.person
				WHEN kontri.person_ref = "Alumni" THEN kontri.person
			END AS identitas,
			CASE
				WHEN kontri.person_ref = "Pegawai" THEN CONCAT(IFNULL(pegawai.gelar_depan,""), pegawai.nama, pegawai.gelar_belakang)
				WHEN kontri.person_ref = "Mahasiswa" THEN mhs.Nama_Mhs
				WHEN kontri.person_ref = "Staf" THEN kontri.person
				WHEN kontri.person_ref = "Alumni" THEN kontri.person
			END AS personil,
			CASE
				WHEN kontri.person_ref = "Pegawai" THEN prodi_dos.Nama_Prodi
				WHEN kontri.person_ref = "Mahasiswa" THEN prodi_mhs.Nama_Prodi
			END AS prodi,
			kontri.jenis AS status
		FROM tb_rst_tr_kontributor kontri
		LEFT JOIN tb_akd_rf_mahasiswa mhs ON kontri.person = mhs.NRP
		LEFT JOIN tb_peg_rf_pegawai pegawai ON kontri.person = pegawai.nip
		LEFT JOIN tb_akd_tr_dosen dosen ON pegawai.nip = dosen.NIP
		LEFT JOIN tb_akd_rf_prodi prodi_dos ON dosen.Kode_Prodi = prodi_dos.Kode_Prodi
		LEFT JOIN tb_akd_rf_prodi prodi_mhs ON mhs.Kode_Prodi = prodi_mhs.Kode_Prodi
		WHERE kontri.riset='.$id.' AND kontri.person_ref = "'.$ref.'"
		';

		$sql = $this->db->query($q);
		return $sql;		
	}

	public function GetMitraPengabdian($id, $id_mitra = NULL) {
		$this->db->select("mitra.id, mitra.nama_mitra, mitra.jenis_mitra AS id_jenis_mitra, opt_jenis.value AS jenis_mitra,
						   mitra.bidang_mitra, mitra.dana_pendamping, mitra.omzet");
		$this->db->join("tb_rst_rf_options opt_jenis", "mitra.jenis_mitra = opt_jenis.id");
		$this->db->where("mitra.riset", $id);
		if($id_mitra != NULL) {
			$this->db->where("mitra.id", $id_mitra);
		}
		
		$sql = $this->db->get("tb_rst_tr_riset_mitra mitra");

		return $sql;
	}

	public function GetDetailPersonilPengabdian($id,$person) {
		$this->db->select("person_ref, jenis");
		$this->db->where("riset", $id);
		$this->db->where("person", $person);
		$sql = $this->db->get("tb_rst_tr_kontributor");

		return $sql->row();
	}

	public function GetRevisiPengabdian($id) {
		$this->db->select("keterangan, tanggal");
		$this->db->where("riset", $id);
		$sql = $this->db->get("tb_rst_tr_riset_revisi");

		return $sql;
	}

	public function GetDataSK($id) {
		$this->db->select("id, no_sk, tentang, tgl_ditetapkan, tgl_mulai_berlaku, 
						   tgl_selesai_berlaku, penerbit, pejabat_ttd");
		$this->db->where("riset", $id);
		$sql = $this->db->get("tb_rst_tr_riset_sk");

		return $sql;
	}

	public function GetDokumenPengabdian($riset,$id = NULL) {
		$this->db->select("id, nama, file");
		$this->db->where("riset", $riset);
		($id != NULL ? $this->db->where("id", $id) : "");

		$sql = $this->db->get("tb_rst_tr_riset_dokumen");
		return $sql;
	}

	public function GetIDPengabdianFromMitra($id) {
		$this->db->select("riset");
		$this->db->where("id", $id);
		$sql = $this->db->get("tb_rst_tr_riset_mitra");

		return $sql->row()->riset;
	}

	public function GetTahapanPengabdian($id) {
		$this->db->select("rst_tahap.id, periode.nama, tahapan.tahapan");
		$this->db->join("tb_rst_tr_periode periode", "rst_tahap.periode = periode.id");
		$this->db->join("tb_rst_rf_tahapan tahapan", "rst_tahap.tahapan = tahapan.id");
		$this->db->where("rst_tahap.riset", $id);

		$sql = $this->db->get("tb_rst_tr_riset_tahapan rst_tahap");
		return $sql;
	}
	
	public function TambahPengabdian($data) {
		$this->db->insert("tb_rst_tr_riset", $data);
		return $this->db->insert_id();
	}

	public function TambahPengabdianProp($data) {
		$sql = $this->db->insert_batch("tb_rst_tr_riset_props", $data);
		return $sql;
	}

	public function TambahPengabdianSK($data) {
		$sql = $this->db->insert("tb_rst_tr_riset_sk", $data);
		return $sql;
	}

	public function TambahPengabdianTahap($data) {
		$sql = $this->db->insert("tb_rst_tr_riset_tahapan", $data);
		return $sql;
	}

	public function TambahPersonil($data) {
		$sql = $this->db->insert("tb_rst_tr_kontributor", $data);
		return $sql;
	}

	public function TambahMitra($data) {
		$sql = $this->db->insert("tb_rst_tr_riset_mitra", $data);
		return $sql;
	}

	public function TambahRevisiPengabdian($data) {
		$sql = $this->db->insert("tb_rst_tr_riset_revisi", $data);
		return $sql;
	}

	public function TambahStatusPengabdian($data) {
		$sql = $this->db->insert("tb_rst_tr_riset_tahapan", $data);
		return $sql;
	}

	public function TambahDokumen($data) {
		$sql = $this->db->insert("tb_rst_tr_riset_dokumen", $data);
		return $sql;
	}

	public function EditPengabdian($data, $id) {
		$sql = $this->db->update("tb_rst_tr_riset", $data, ["id" => $id]);
		return $sql;
	}

	public function EditPengabdianProp($data, $where) {
		$sql = $this->db->update("tb_rst_tr_riset_props", $data, $where);
		return $sql;
	}

	public function EditDataSK($data,$where) {
		$sql = $this->db->update("tb_rst_tr_riset_sk", $data, $where);
		return $sql;
	}

	public function EditMitraPengabdian($data,$id) {
		$sql = $this->db->update("tb_rst_tr_riset_mitra", $data, ["id" => $id]);
		return $sql;
	}

	public function EditDokumen($data,$id) {
		$sql = $this->db->update("tb_rst_tr_riset_dokumen", $data, ["id" => $id]);
		return $sql;
	}

	public function UpdateStatusPengabdian($data,$id,$periode) {
		$sql = $this->db->update("tb_rst_tr_riset_tahapan", $data, ["riset" => $id, "periode" => $periode]);
		return $sql;
	}

	public function CheckKetuaPengabdian($id) {
		$this->db->select("person");
		$this->db->where("riset", $id);
		$this->db->where("jenis", "Ketua");
		$sql = $this->db->get("tb_rst_tr_kontributor");

		$init = $sql->num_rows();
		if($init > 0) {
			return false;
		} else {
			return true;
		}
	}

	public function HapusPengabdian($id) {
		$sql = $this->db->delete("tb_rst_tr_riset", ["id" => $id]);
		return $sql;
	}

	public function HapusPersonil($id,$person) {
		$sql = $this->db->delete("tb_rst_tr_kontributor", ["riset" => $id, "person" => $person]);
		return $sql;
	}

	public function HapusMitra($id) {
		$sql = $this->db->delete("tb_rst_tr_riset_mitra", ["id" => $id]);
		return $sql;
	}

	public function HapusDokumen($id) {
		$sql = $this->db->delete("tb_rst_tr_riset_dokumen", ["id" => $id]);
		return $sql;
	}

	public function UploadFileLPJ($riset,$file) {
		$sql = $this->db->update("tb_rst_tr_riset", ["laporan_akhir" => $file], ["id" => $riset]);
		return $sql;
	}

	public function UploadFileUsulan($riset,$file) {
		$sql = $this->db->update("tb_rst_tr_riset", ["usulan" => $file], ["id" => $riset]);
		return $sql;
	}
}