<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MahasiswaModel extends CI_Model {
	public function __construct() {
		parent::__construct();
    }

    function GetMhsByNRP($id) {
        $this->db->select("Nama_Mhs, Kode_Prodi AS prodi");
        $this->db->where("NRP", $id);

        $sql = $this->db->get("tb_akd_rf_mahasiswa");
        return $sql->row();
    }
}