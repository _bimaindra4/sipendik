<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PeriodeModel extends CI_Model {
	public function __construct() {
		parent::__construct();
	}

	public function GetPeriode($id = NULL) {
		$this->db->select("id, nama, isAktif, tgl_mulai, tgl_selesai");
		($id != NULL ? $this->db->where("id", $id) : "");
		$this->db->order_by("urutan", "ASC");
		$sql = $this->db->get("tb_rst_tr_periode");

		return $sql;
	}

	public function GetPeriodeLimitForProduktivitas() {
		$total_periode = $this->GetPeriode()->num_rows();
		
		$this->db->order_by("urutan", "ASC");
		$this->db->limit(10, ($total_periode - 10));
		$sql = $this->db->get("tb_rst_tr_periode");

		return $sql->result();
	}

	public function GetPeriodeAktiv() {
		$this->db->select("id, nama, isAktif");
		$this->db->where("isAktif", "YES");

		$sql = $this->db->get("tb_rst_tr_periode");
		return $sql;
	}

	public function GetJumlahStatusKegiatan($periode,$keg) {
		$q = '
		SELECT DISTINCT
			(
				SELECT COUNT(*) 
				FROM tb_rst_tr_riset_tahapan rst_tahap 
				JOIN tb_rst_tr_riset rst ON rst_tahap.riset = rst.id 
				WHERE rst_tahap.tahapan = 1 AND rst_tahap.periode = '.$periode.' AND rst.jenis = '.$keg.'
			) AS usulan,
			(
				SELECT COUNT(*) 
				FROM tb_rst_tr_riset_tahapan rst_tahap 
				JOIN tb_rst_tr_riset rst ON rst_tahap.riset = rst.id 
				WHERE rst_tahap.tahapan = 2 AND rst_tahap.periode = '.$periode.' AND rst.jenis = '.$keg.'
			) AS on_going,
			(
				SELECT COUNT(*) 
				FROM tb_rst_tr_riset_tahapan rst_tahap 
				JOIN tb_rst_tr_riset rst ON rst_tahap.riset = rst.id 
				WHERE rst_tahap.tahapan = 3 AND rst_tahap.periode = '.$periode.' AND rst.jenis = '.$keg.'
			) AS selesai,
			(
				SELECT COUNT(*) 
				FROM tb_rst_tr_riset_tahapan rst_tahap 
				JOIN tb_rst_tr_riset rst ON rst_tahap.riset = rst.id 
				WHERE rst_tahap.tahapan = 4 AND rst_tahap.periode = '.$periode.' AND rst.jenis = '.$keg.'
			) AS gagal
		FROM tb_rst_tr_riset
		';

		$sql = $this->db->query($q);
		return $sql->row();
	}

	public function GetJumlahProdiKegiatan($periode,$keg) {
		$q = '
		SELECT DISTINCT
			(
				SELECT COUNT(*) 
				FROM tb_rst_tr_riset rst 
				JOIN tb_rst_tr_riset_tahapan rst_tahap ON rst.id = rst_tahap.riset
				WHERE rst.prodi LIKE "%DK-S1%" AND rst_tahap.periode = '.$periode.' AND rst.jenis = '.$keg.'
			) AS dkv,
			(
				SELECT COUNT(*) 
				FROM tb_rst_tr_riset rst 
				JOIN tb_rst_tr_riset_tahapan rst_tahap ON rst.id = rst_tahap.riset
				WHERE rst.prodi LIKE "%MI-D3%" AND rst_tahap.periode = '.$periode.' AND rst.jenis = '.$keg.'
			) AS mi,
			(
				SELECT COUNT(*) 
				FROM tb_rst_tr_riset rst 
				JOIN tb_rst_tr_riset_tahapan rst_tahap ON rst.id = rst_tahap.riset
				WHERE rst.prodi LIKE "%SI-S1%" AND rst_tahap.periode = '.$periode.' AND rst.jenis = '.$keg.'
			) AS si,
			(
				SELECT COUNT(*) 
				FROM tb_rst_tr_riset rst 
				JOIN tb_rst_tr_riset_tahapan rst_tahap ON rst.id = rst_tahap.riset
				WHERE rst.prodi LIKE "%TI-S1%" AND rst_tahap.periode = '.$periode.' AND rst.jenis = '.$keg.'
			) AS ti
		FROM tb_rst_tr_riset
		';

		$sql = $this->db->query($q);
		return $sql->row();
	}

	public function GetJumlahSumberDanaKegiatan($periode,$keg) {
		$data = [];
		if($keg == 1) {
			$q = '
			SELECT opt.id, opt.value
			FROM tb_rst_rf_options opt
			LEFT JOIN tb_rst_tr_riset_props rst_prop ON opt.id = rst_prop.value
			WHERE opt.tags = "[institusi-sumber-dana]"
			GROUP BY opt.id
			';
			
			$sql = $this->db->query($q);
			foreach($sql->result() as $row) {
				$q = '
				SELECT COUNT(rst.id) AS jumlah
				FROM tb_rst_tr_riset rst
				JOIN tb_rst_tr_riset_tahapan rst_tahap ON rst.id = rst_tahap.riset
				JOIN tb_rst_tr_riset_props rst_prop ON rst.id = rst_prop.riset
				WHERE rst_tahap.periode = '.$periode.' AND rst_prop.prop = 9 AND rst_prop.value = '.$row->id.'
				';
				$sql = $this->db->query($q);
				$sumber_dana = $sql->row();

				$dana['nama'] = $row->value;
				$dana['jumlah'] = $sumber_dana->jumlah;

				$data[] = $dana;
			}
		} else if($keg == 2) {
			$q = '
			SELECT opt.id, opt.value
			FROM tb_rst_rf_options opt
			LEFT JOIN tb_rst_tr_riset_props rst_prop ON opt.id = rst_prop.value
			WHERE opt.tags = "[sumber-dana-pg]"
			GROUP BY opt.id
			';

			$sql = $this->db->query($q);
			foreach($sql->result() as $row) {
				$q = '
				SELECT COUNT(rst.id) AS jumlah
				FROM tb_rst_tr_riset rst
				JOIN tb_rst_tr_riset_tahapan rst_tahap ON rst.id = rst_tahap.riset
				JOIN tb_rst_tr_riset_props rst_prop ON rst.id = rst_prop.riset
				WHERE rst_tahap.periode = '.$periode.' AND rst_prop.prop = 13 AND rst_prop.value = '.$row->id.'
				';
				$sql = $this->db->query($q);
				$sumber_dana = $sql->row();

				$dana['nama'] = $row->value;
				$dana['jumlah'] = $sumber_dana->jumlah;

				$data[] = $dana;
			}
		}

		return $data;
	}

	public function GetJumlahJenisKegiatan($periode,$keg) {
		$data = [];
		if($keg == 1) {
			$q = '
			SELECT opt.id, opt.value
			FROM tb_rst_rf_options opt
			LEFT JOIN tb_rst_tr_riset_props rst_prop ON opt.id = rst_prop.value
			WHERE opt.tags = "[jenis-penelitian]"
			GROUP BY opt.id
			';
			
			$sql = $this->db->query($q);
			foreach($sql->result() as $row) {
				$q = '
				SELECT COUNT(rst.id) AS jumlah
				FROM tb_rst_tr_riset rst
				JOIN tb_rst_tr_riset_tahapan rst_tahap ON rst.id = rst_tahap.riset
				JOIN tb_rst_tr_riset_props rst_prop ON rst.id = rst_prop.riset
				WHERE rst_tahap.periode = '.$periode.' AND rst_prop.prop = 2 AND rst_prop.value = '.$row->id.'
				';
				$sql = $this->db->query($q);
				$jenis = $sql->row();

				$dana['nama'] = $row->value;
				$dana['jumlah'] = $jenis->jumlah;

				$data[] = $dana;
			}
		} else if($keg == 2) {
			$q = '
			SELECT opt.id, opt.value
			FROM tb_rst_rf_options opt
			LEFT JOIN tb_rst_tr_riset_props rst_prop ON opt.id = rst_prop.value
			WHERE opt.tags = "[jenis-pengabdian]"
			GROUP BY opt.id
			';

			$sql = $this->db->query($q);
			foreach($sql->result() as $row) {
				$q = '
				SELECT COUNT(rst.id) AS jumlah
				FROM tb_rst_tr_riset rst
				JOIN tb_rst_tr_riset_tahapan rst_tahap ON rst.id = rst_tahap.riset
				JOIN tb_rst_tr_riset_props rst_prop ON rst.id = rst_prop.riset
				WHERE rst_tahap.periode = '.$periode.' AND rst_prop.prop = 72 AND rst_prop.value = '.$row->id.'
				';
				$sql = $this->db->query($q);
				$jenis = $sql->row();

				$dana['nama'] = $row->value;
				$dana['jumlah'] = $jenis->jumlah;

				$data[] = $dana;
			}
		}

		return $data;
	}

	public function GetDosenProduktif($periode) {
		$CI =& get_instance();
		$CI->load->model('DosenModel');

		$output = [];
		$dosen = $this->DosenModel->GetDataDosen();

		foreach($dosen->result() as $row) {
			$data["nama"] = $row->nama;
			$data["penelitian"] = $this->DosenModel->GetDataDosenPenelitian($row->nip,"periode",$periode)->num_rows();
			$data["pengabdian"] = $this->DosenModel->GetDataDosenPengabdian($row->nip,"periode",$periode)->num_rows();
			
			$output[] = $data;
		}

		return $output;
	}

	public function GetPeriodeAfter($riset) {
		$q = "
		SELECT id, nama FROM tb_rst_tr_periode
		WHERE urutan > (
			SELECT periode.urutan
			FROM tb_rst_tr_riset_tahapan rst_tahap
			JOIN tb_rst_tr_periode periode ON rst_tahap.periode = periode.id
			WHERE rst_tahap.riset = ".$riset."
			ORDER BY periode.urutan DESC
			LIMIT 1
		)";

		$sql = $this->db->query($q);
		return $sql->result();
	}

	public function GetPeriodeRiset($riset) {
		$this->db->select("rst_tahap.periode AS id, periode.nama");
		$this->db->join("tb_rst_tr_periode periode", "rst_tahap.periode = periode.id");
		$this->db->where("rst_tahap.riset", $riset);
		$this->db->order_by("periode.urutan", "ASC");

		$sql = $this->db->get("tb_rst_tr_riset_tahapan rst_tahap");
		return $sql->result();
	}

	public function TambahPeriode($data) {
		$sql = $this->db->insert("tb_rst_tr_periode", $data);
		return $sql;	
	}

	public function EditPeriode($data, $where) {
		$sql = $this->db->update("tb_rst_tr_periode", $data, $where);
		return $sql;
	}

	public function HapusPeriode($id) {
		$sql = $this->db->delete("tb_rst_tr_periode", ["id" => $id]);
		return $sql;
	}
	
}