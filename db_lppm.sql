/*
 Navicat Premium Data Transfer

 Source Server         : Localhost
 Source Server Type    : MySQL
 Source Server Version : 100411
 Source Host           : localhost:3306
 Source Schema         : db_lppm

 Target Server Type    : MySQL
 Target Server Version : 100411
 File Encoding         : 65001

 Date: 13/11/2020 14:44:22
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tb_akd_rf_mahasiswa
-- ----------------------------
DROP TABLE IF EXISTS `tb_akd_rf_mahasiswa`;
CREATE TABLE `tb_akd_rf_mahasiswa`  (
  `NRP` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `Kode_Prodi` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Nama_Mhs` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Id_File_Photo` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Status_Akademis` varchar(2) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Email` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Created_App` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Created_By` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Created_Date` datetime(0) NULL DEFAULT NULL,
  `Modified_App` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Modified_By` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Modified_Date` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`NRP`) USING BTREE,
  INDEX `Kode_Prodi`(`Kode_Prodi`) USING BTREE,
  CONSTRAINT `tb_akd_rf_mahasiswa_ibfk_1` FOREIGN KEY (`Kode_Prodi`) REFERENCES `tb_akd_rf_prodi` (`Kode_Prodi`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_akd_rf_mahasiswa
-- ----------------------------
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('111110411', 'TI-S1', 'Agung Prasetyo', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('121110582', 'TI-S1', 'Sigit Teguh Prakoso', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('131110679', 'TI-S1', 'Muhammad Arif Subkhan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('141111003', 'TI-S1', 'Yoppy Pangestu', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('141111015', 'TI-S1', 'Yosua Kristanto', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('141111016', 'TI-S1', 'Febri Yohanes Aldi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('141111059', 'TI-S1', 'M. Firmansyah Alnaufal G.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('141111077', 'TI-S1', 'Willyanto Sutikno', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('141111102', 'TI-S1', 'Khoirun Nafisah', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('142111005', 'DK-S1', 'Perisal', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('142111019', 'DK-S1', 'Iqbal Rizky Maulana', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('142111040', 'DK-S1', 'Daniel Widianto', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('142111053', 'DK-S1', 'Faris Ma\'ruf', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('151111007', 'TI-S1', 'Daniel Eko P. T.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('151111010', 'TI-S1', 'Yosiasi Suparno', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('151111011', 'TI-S1', 'Yohanes Dwi Listio', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('151111014', 'TI-S1', 'Arrizky Rahmat Alfiansyah', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('151111023', 'TI-S1', 'Baiq Farida Nolawangi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('151111027', 'TI-S1', 'Akmalul Hikam', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('151111041', 'TI-S1', 'Ninda Zulistyaningsih', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('151111045', 'TI-S1', 'Donny Febrian H', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('151111048', 'TI-S1', 'Imam Baihaqi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('151111050', 'TI-S1', 'Vina Dwi Elviani', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('151111058', 'TI-S1', 'Eko Teguh Prasetyo', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('151111081', 'TI-S1', 'Rafi Pratama Aji', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('151111091', 'TI-S1', 'Gustav Arthur Sebastian', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('151111114', 'TI-S1', 'Muhsi Chakra Dhis\'tar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('151111119', 'TI-S1', 'M. Alim Arif H.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('151111121', 'TI-S1', 'Muhammad Tegar Maha Putra', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('151111123', 'TI-S1', 'Jangkung Ari Mukti', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('151111125', 'TI-S1', 'M. Hafidh Bahrul M.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('151122018', 'MI-D3', 'Khairunnisa', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('151131002', 'SI-S1', 'Dyan Bentar Bhaswara Siwi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('151131014', 'SI-S1', 'Fahmi Khudzaifi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('151131017', 'SI-S1', 'Muhammad Rizki Wicaksono', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('151221010', 'SI-S1', 'Dian Wahyu Prasetyo', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('151221019', 'MI-D3', 'Arif Rahman Husin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('152111007', 'DK-S1', 'A. Wiwit Eka Wulandari', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('152111026', 'DK-S1', 'Adi Febrianto Tiantomo', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('152111027', 'DK-S1', 'Rizky Fahrul Rozy Mradipta', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('152111037', 'DK-S1', 'Charles Andre Hartono', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('152111042', 'DK-S1', 'Stefanus Ericky Dewangga', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('152111047', 'DK-S1', 'Yahya Khalim Abdurrahman', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('152111056', 'DK-S1', 'Dimas Setiawan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('161111010', 'TI-S1', 'Deby Nawang Sari', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('161111029', 'TI-S1', 'Naila Raudlatul F.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('161111044', 'TI-S1', 'Hizkia Luke Susanto', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('161111060', 'TI-S1', 'Alfian Noor Sofyan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('161111068', 'TI-S1', 'Achmad Yunus', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('161111070', 'TI-S1', 'M. Bima Indra Kusuma', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('161111071', 'TI-S1', 'Vicky Fajar Ibrahim', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('161111076', 'TI-S1', 'Fajar Abifian Al Ghiffari', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('161116041', 'TI-S1', 'Anggi Yobelia A.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('161131008', 'SI-S1', 'Adi Dharma Putra', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('161221017', 'MI-D3', 'Hafit Syams Widyatama', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('161221018', 'MI-D3', 'Rizki Yogi Prasetyo', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('161221021', 'MI-D3', 'Galang Mahesta', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('162111015', 'DK-S1', 'Tony Wahyudi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('162111023', 'DK-S1', 'Christian Axel Sander', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('162111033', 'DK-S1', 'Meutia Tivani', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('162111040', 'DK-S1', 'Wulandari Dwi Puspitasari', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('162111075', 'DK-S1', 'Anita Wulan Suci', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('171111020', 'TI-S1', 'Ade Ramadhana Pratama', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('171111022', 'TI-S1', 'Suhendra', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('171111023', 'TI-S1', 'Panji Iman Baskoro', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('171111024', 'TI-S1', 'Danu Kuncoro Aji', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('171111076', 'TI-S1', 'Ronald Arrival Fajar', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('171111109', 'TI-S1', 'Nanda Bima Mahendra', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('171111112', 'TI-S1', 'Ahmad Masrud Mubarok', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('171131002', 'SI-S1', 'Rizky Satrio Wibowo', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('171131005', 'SI-S1', 'M. Choirul Putra Pratama', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('171131006', 'SI-S1', 'Gracia Stefy Angela', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('171221016', 'MI-D3', 'Ruben Redo Julius Wardoyo', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('172111025', 'DK-S1', 'Ghulam Najmudin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('181111015', 'TI-S1', 'Kurniat Idaman Sukur Telaumbanua', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('181111031', 'TI-S1', 'Yusan Abid Janitra', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('181111078', 'TI-S1', 'Bima Reynaldi', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('181221029', 'MI-D3', 'Donella Monica Ilham', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('182111006', 'DK-S1', 'Delita Rahmanda Sari', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('182111048', 'DK-S1', 'Pujo Adi Prastyo', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_akd_rf_mahasiswa` VALUES ('191111052', 'TI-S1', 'Hikmah', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for tb_akd_rf_prodi
-- ----------------------------
DROP TABLE IF EXISTS `tb_akd_rf_prodi`;
CREATE TABLE `tb_akd_rf_prodi`  (
  `Kode_Prodi` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `Jenjang` varchar(4) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Nama_Prodi` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Created_By` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Created_Date` datetime(0) NULL DEFAULT NULL,
  `Modified_By` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Modified_Date` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`Kode_Prodi`) USING BTREE,
  INDEX `FJenjangProdi`(`Jenjang`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_akd_rf_prodi
-- ----------------------------
INSERT INTO `tb_akd_rf_prodi` VALUES ('DK-S1', 'S1', 'Desain Komunikasi Visual', 'erni@stiki.ac.id', '2014-08-14 09:05:44', 'adrian', '2015-06-22 08:58:05');
INSERT INTO `tb_akd_rf_prodi` VALUES ('MI-D3', 'D3', 'Manajemen Informatika', 'adrian', '2014-05-20 09:14:51', 'adrian', '2015-06-22 08:58:11');
INSERT INTO `tb_akd_rf_prodi` VALUES ('SI-S1', 'S1', 'Sistem Informasi', 'erni@stiki.ac.id', '2015-08-05 15:35:41', NULL, NULL);
INSERT INTO `tb_akd_rf_prodi` VALUES ('TI-S1', 'S1', 'Teknik Informatika', 'adrian', '2014-05-20 09:14:51', 'adrian', '2017-12-13 11:02:28');

-- ----------------------------
-- Table structure for tb_akd_tr_dosen
-- ----------------------------
DROP TABLE IF EXISTS `tb_akd_tr_dosen`;
CREATE TABLE `tb_akd_tr_dosen`  (
  `NIP` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nidn` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Kode_Prodi` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `mulai_semester` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `pendidikan_tertinggi` varchar(3) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `jabatan_akademik` enum('Tenaga Pengajar','Asisten Ahli','Lektor','Lektor Kepala','Guru Besar') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Created_By` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Created_Date` datetime(0) NULL DEFAULT NULL,
  `Modified_By` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Modified_Date` datetime(0) NULL DEFAULT NULL,
  INDEX `NIP`(`NIP`) USING BTREE,
  INDEX `FProdiDosen`(`Kode_Prodi`) USING BTREE,
  INDEX `FJenjang`(`pendidikan_tertinggi`) USING BTREE,
  CONSTRAINT `FProdiDosen` FOREIGN KEY (`Kode_Prodi`) REFERENCES `tb_akd_rf_prodi` (`Kode_Prodi`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FdosenPegawai` FOREIGN KEY (`NIP`) REFERENCES `tb_peg_rf_pegawai` (`nip`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_akd_tr_dosen
-- ----------------------------
INSERT INTO `tb_akd_tr_dosen` VALUES ('010001', '0710085301', 'TI-S1', NULL, 'S2', 'Lektor', 'adrian', '2014-08-06 15:43:33', 'udin@stiki.ac.id', '2018-10-24 08:46:16');
INSERT INTO `tb_akd_tr_dosen` VALUES ('010004', '0704095201', 'TI-S1', NULL, NULL, 'Lektor', 'adrian', NULL, NULL, NULL);
INSERT INTO `tb_akd_tr_dosen` VALUES ('010034', '0707077201', 'MI-D3', NULL, NULL, 'Asisten Ahli', 'adrian', NULL, NULL, NULL);
INSERT INTO `tb_akd_tr_dosen` VALUES ('010038', '0712087102', 'TI-S1', NULL, NULL, 'Lektor', 'adrian', NULL, NULL, NULL);
INSERT INTO `tb_akd_tr_dosen` VALUES ('010040', '0729127301', 'MI-D3', NULL, NULL, 'Asisten Ahli', 'adrian', NULL, NULL, NULL);
INSERT INTO `tb_akd_tr_dosen` VALUES ('010041', '0012057201', 'TI-S1', NULL, NULL, 'Asisten Ahli', 'adrian', NULL, NULL, NULL);
INSERT INTO `tb_akd_tr_dosen` VALUES ('010045', '0724027202', 'TI-S1', NULL, NULL, 'Asisten Ahli', 'adrian', NULL, NULL, NULL);
INSERT INTO `tb_akd_tr_dosen` VALUES ('010050', '0701047502', 'SI-S1', NULL, NULL, 'Lektor', 'adrian', NULL, NULL, NULL);
INSERT INTO `tb_akd_tr_dosen` VALUES ('010052', '0722037101', 'TI-S1', NULL, NULL, 'Asisten Ahli', 'adrian', NULL, NULL, NULL);
INSERT INTO `tb_akd_tr_dosen` VALUES ('010063', '0020027802', 'TI-S1', NULL, NULL, 'Lektor', 'adrian', NULL, NULL, NULL);
INSERT INTO `tb_akd_tr_dosen` VALUES ('010067', '0701016902', 'SI-S1', NULL, NULL, 'Asisten Ahli', 'adrian', NULL, NULL, NULL);
INSERT INTO `tb_akd_tr_dosen` VALUES ('010077', '0702027201', 'TI-S1', NULL, NULL, 'Lektor', 'adrian', NULL, NULL, NULL);
INSERT INTO `tb_akd_tr_dosen` VALUES ('010078', '0716017601', 'TI-S1', NULL, NULL, 'Tenaga Pengajar', 'adrian', NULL, NULL, NULL);
INSERT INTO `tb_akd_tr_dosen` VALUES ('010081', '0731108002', 'DK-S1', NULL, NULL, 'Tenaga Pengajar', 'adrian', NULL, NULL, NULL);
INSERT INTO `tb_akd_tr_dosen` VALUES ('010094', '0709107702', 'TI-S1', NULL, NULL, 'Tenaga Pengajar', 'adrian', NULL, NULL, NULL);
INSERT INTO `tb_akd_tr_dosen` VALUES ('010096', '0725057705', 'MI-D3', NULL, NULL, 'Tenaga Pengajar', 'adrian', NULL, NULL, NULL);
INSERT INTO `tb_akd_tr_dosen` VALUES ('010106', '0727078503', 'SI-S1', NULL, NULL, 'Asisten Ahli', 'adrian', NULL, NULL, NULL);
INSERT INTO `tb_akd_tr_dosen` VALUES ('010123', '0712038002', 'DK-S1', NULL, NULL, 'Tenaga Pengajar', 'adrian', NULL, NULL, NULL);
INSERT INTO `tb_akd_tr_dosen` VALUES ('010124', '0715118901', 'TI-S1', NULL, NULL, 'Asisten Ahli', 'adrian', NULL, NULL, NULL);
INSERT INTO `tb_akd_tr_dosen` VALUES ('010128', '0706128703', 'TI-S1', NULL, NULL, 'Asisten Ahli', 'adrian', NULL, NULL, NULL);
INSERT INTO `tb_akd_tr_dosen` VALUES ('010130', '0731038803', 'MI-D3', NULL, NULL, 'Asisten Ahli', 'adrian', NULL, NULL, NULL);
INSERT INTO `tb_akd_tr_dosen` VALUES ('010134', '0712108805', 'SI-S1', NULL, NULL, 'Tenaga Pengajar', 'adrian', NULL, NULL, NULL);
INSERT INTO `tb_akd_tr_dosen` VALUES ('010152', '0702069101', 'SI-S1', NULL, NULL, 'Asisten Ahli', 'adrian', NULL, NULL, NULL);
INSERT INTO `tb_akd_tr_dosen` VALUES ('010153', '0701068605', 'DK-S1', NULL, NULL, 'Tenaga Pengajar', 'adrian', NULL, NULL, NULL);
INSERT INTO `tb_akd_tr_dosen` VALUES ('010154', '0723029202', 'SI-S1', NULL, NULL, 'Asisten Ahli', 'adrian', NULL, NULL, NULL);
INSERT INTO `tb_akd_tr_dosen` VALUES ('010158', '0714069004', 'DK-S1', NULL, NULL, 'Tenaga Pengajar', 'adrian', NULL, NULL, NULL);
INSERT INTO `tb_akd_tr_dosen` VALUES ('010163', '0709089102', 'TI-S1', NULL, NULL, 'Tenaga Pengajar', 'adrian', NULL, NULL, NULL);
INSERT INTO `tb_akd_tr_dosen` VALUES ('010167', '0708108503', 'DK-S1', NULL, NULL, 'Tenaga Pengajar', 'adrian', NULL, NULL, NULL);
INSERT INTO `tb_akd_tr_dosen` VALUES ('010174', '0724058704', 'SI-S1', NULL, NULL, 'Tenaga Pengajar', 'adrian', NULL, NULL, NULL);
INSERT INTO `tb_akd_tr_dosen` VALUES ('010175', '0705129101', 'TI-S1', NULL, NULL, 'Tenaga Pengajar', 'adrian', NULL, NULL, NULL);
INSERT INTO `tb_akd_tr_dosen` VALUES ('040016', '0706018203', 'TI-S1', NULL, NULL, 'Asisten Ahli', 'adrian', NULL, NULL, NULL);
INSERT INTO `tb_akd_tr_dosen` VALUES ('040077', '0703066801', 'MI-D3', NULL, NULL, 'Tenaga Pengajar', 'adrian', NULL, NULL, NULL);
INSERT INTO `tb_akd_tr_dosen` VALUES ('040086', '0725099103', 'DK-S1', NULL, NULL, 'Tenaga Pengajar', 'adrian', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for tb_peg_rf_pegawai
-- ----------------------------
DROP TABLE IF EXISTS `tb_peg_rf_pegawai`;
CREATE TABLE `tb_peg_rf_pegawai`  (
  `nip` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT 'NIP containt XY_ are old data used for migration',
  `nama` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `gelar_depan` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `gelar_belakang` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `foto` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `aktif` enum('YA','TIDAK') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'YA',
  `created_app` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_by` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_date` datetime(0) NULL DEFAULT NULL,
  `modified_app` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `modified_by` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `modified_date` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`nip`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_peg_rf_pegawai
-- ----------------------------
INSERT INTO `tb_peg_rf_pegawai` VALUES ('010001', 'LN. Andoyo', 'Ir. ', ', M.T', 'andoyo@stiki.ac.id', '010001.jpg', 'YA', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_peg_rf_pegawai` VALUES ('010004', 'Indra Soegiharto', 'Dipl. Ing. ', ', S.H., MBA', 'indra.s@stiki.ac.id', '010004.jpg', 'YA', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_peg_rf_pegawai` VALUES ('010034', 'Anita', NULL, ', S.Kom., M.T', 'ant@stiki.ac.id', '010034.jpg', 'YA', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_peg_rf_pegawai` VALUES ('010038', 'Evy Poerbaningtyas', 'Dr. ', ', S.Si., M.T', 'evip@stiki.ac.id', '010038.jpg', 'YA', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_peg_rf_pegawai` VALUES ('010040', 'Sugeng Widodo', NULL, ', S.Kom., M.Kom', 'sugeng@stiki.ac.id', '010040.jpg', 'YA', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_peg_rf_pegawai` VALUES ('010041', 'Jozua F. Palandi', NULL, ', S.Kom., M.Kom', 'jozua@stiki.ac.id', '010041.jpg', 'YA', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_peg_rf_pegawai` VALUES ('010045', 'Laila Isyriyah', NULL, ', S.Kom., M.Kom', 'laila@stiki.ac.id', '010045.jpg', 'YA', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_peg_rf_pegawai` VALUES ('010050', 'Eva Handriyantini', 'Dr. ', ', S.Kom., M.MT', 'eva@stiki.ac.id', '010050.jpg', 'YA', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_peg_rf_pegawai` VALUES ('010052', 'Daniel Rudiaman Sijabat', NULL, ', S.T., M.Kom', 'daniel232@stiki.ac.id', '010052.jpg', 'YA', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_peg_rf_pegawai` VALUES ('010063', 'Diah Arifah Prastiningtyas', NULL, ', S.Kom., M.T', 'diah@stiki.ac.id', '010063.jpg', 'YA', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_peg_rf_pegawai` VALUES ('010067', 'Setiabudi Sakaria', NULL, ', S.Kom., M.Kom', 'setiabudi@stiki.ac.id', '010067.jpg', 'YA', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_peg_rf_pegawai` VALUES ('010077', 'Subari', NULL, ', S.Kom., M.Kom', 'subari@stiki.ac.id', '010077.jpg', 'YA', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_peg_rf_pegawai` VALUES ('010078', 'Zusana Eko Pudyastuti', NULL, ', S.S., M.Pd', 'zusanacr@stiki.ac.id', '010078.jpg', 'YA', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_peg_rf_pegawai` VALUES ('010081', 'Saiful Yahya', NULL, ', S.Sn, M.T', 'saiful@stiki.ac.id', '010081.jpg', 'YA', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_peg_rf_pegawai` VALUES ('010094', 'Mukhlis Amien', NULL, ', S.Kom., M.Kom', 'mukhlis@stiki.ac.id', '010094.jpg', 'YA', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_peg_rf_pegawai` VALUES ('010096', 'Meivi Kartikasari', NULL, ', S.Kom., M.T', 'meivi.k@stiki.ac.id', '010096.jpg', 'YA', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_peg_rf_pegawai` VALUES ('010106', 'Koko Wahyu Prasetyo', NULL, ', S.Kom., M.T.I', 'koko@stiki.ac.id', '010106.jpg', 'YA', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_peg_rf_pegawai` VALUES ('010123', 'Mahendra Wibawa', NULL, ', S.Sn., M.Pd', 'mahendra@stiki.ac.id', '010123.jpg', 'YA', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_peg_rf_pegawai` VALUES ('010124', 'Siti Aminah', NULL, ', S.Si., M.Pd', 'siti@stiki.ac.id', '010124.jpg', 'YA', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_peg_rf_pegawai` VALUES ('010128', 'Nira Radita', NULL, ', S.Pd., M.Pd', 'nira@stiki.ac.id', '010128.jpg', 'YA', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_peg_rf_pegawai` VALUES ('010130', 'Yekti Asmoro Kanthi', NULL, ', S.Si., M.AB', 'yekti@stiki.ac.id', '010130.jpg', 'YA', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_peg_rf_pegawai` VALUES ('010134', 'Chaulina Alfianti Oktavia', NULL, ', S.Kom., M.T', 'chaulina@stiki.ac.id', '010134.jpg', 'YA', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_peg_rf_pegawai` VALUES ('010152', 'Addin Aditya', NULL, ', S.Kom., M.Kom', 'addin@stiki.ac.id', '010152.jpg', 'YA', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_peg_rf_pegawai` VALUES ('010153', 'Ahmad Zakiy Ramadhan', NULL, ', S.Sn., M.Sn', 'zakiramadhan@stiki.ac.id', '010153.jpg', 'YA', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_peg_rf_pegawai` VALUES ('010154', 'Febry Eka Purwiantono', NULL, ', S.Kom., M.Kom', 'febry@stiki.ac.id', '010154.jpg', 'YA', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_peg_rf_pegawai` VALUES ('010158', 'Rahmat Kurniawan', NULL, ', S.Pd., M.Pd', 'rahmat@stiki.ac.id', '010158.jpg', 'YA', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_peg_rf_pegawai` VALUES ('010163', 'Bagus Kristomoyo Kristanto', NULL, ', S.Kom., M.MT', 'bagus.kristanto@stiki.ac.id', '010163.jpg', 'YA', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_peg_rf_pegawai` VALUES ('010167', 'Adita Ayu Kusumasari', NULL, ', S.Sn., M.Sn', 'adita.kusumasari@stiki.ac.id', '010167.jpg', 'YA', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_peg_rf_pegawai` VALUES ('010174', 'Adnan Zulkarnain', NULL, ', S.Kom., M.M.S.I', 'adnan.zulkarnain@stiki.ac.id', '010174.jpg', 'YA', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_peg_rf_pegawai` VALUES ('010175', 'Arif Tirtana', NULL, ', S.Kom., M.Kom', 'arif.tirtana@stiki.ac.id', '010175.jpg', 'YA', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_peg_rf_pegawai` VALUES ('040016', 'Rakhmad Maulidi', NULL, ', S.Kom., M.Kom', 'maulidi@stiki.ac.id', '040016.jpg', 'YA', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_peg_rf_pegawai` VALUES ('040077', 'Windarini Cahyadiana', NULL, ', S.E., M.M', 'windarini@stiki.ac.id', '040077.jpg', 'YA', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_peg_rf_pegawai` VALUES ('040086', 'Rina Nur Fitri', NULL, ', S.Pd., M.Pd', 'rina.nurfitri@stiki.ac.id', '040086.jpg', 'YA', NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for tb_peg_rf_unit
-- ----------------------------
DROP TABLE IF EXISTS `tb_peg_rf_unit`;
CREATE TABLE `tb_peg_rf_unit`  (
  `unit_id` int(20) NOT NULL AUTO_INCREMENT,
  `parent_unit` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nama_unit` varchar(150) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nip_puk` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `keterangan` varchar(85) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_app` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_by` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_date` datetime(0) NULL DEFAULT NULL,
  `modified_app` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `modified_by` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `modified_date` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`unit_id`) USING BTREE,
  INDEX `nip_puk`(`nip_puk`) USING BTREE,
  CONSTRAINT `tb_peg_rf_unit_ibfk_1` FOREIGN KEY (`nip_puk`) REFERENCES `tb_peg_rf_pegawai` (`nip`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 31 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_peg_rf_unit
-- ----------------------------
INSERT INTO `tb_peg_rf_unit` VALUES (1, NULL, 'KTI', '010106', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_peg_rf_unit` VALUES (2, NULL, 'AKADEMIK', '010052', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_peg_rf_unit` VALUES (3, '2', 'PRODI TI', '040016', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_peg_rf_unit` VALUES (4, '2', 'PRODI MI', '010034', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_peg_rf_unit` VALUES (5, '2', 'PRODI DKV', '010081', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_peg_rf_unit` VALUES (6, '2', 'PRODI SI', '010034', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_peg_rf_unit` VALUES (7, '2', 'BAA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_peg_rf_unit` VALUES (8, NULL, 'LPPM', '010077', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_peg_rf_unit` VALUES (9, NULL, 'PPTIK', '010096', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for tb_rst_rf_jenis
-- ----------------------------
DROP TABLE IF EXISTS `tb_rst_rf_jenis`;
CREATE TABLE `tb_rst_rf_jenis`  (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `keterangan` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `isAktif` enum('YES','NO') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'YES',
  `tags` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Created_App` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Created_By` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Created_Date` datetime(0) NULL DEFAULT NULL,
  `Modified_App` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Modified_By` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Modified_Date` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_rst_rf_jenis
-- ----------------------------
INSERT INTO `tb_rst_rf_jenis` VALUES (1, 'Penelitian', NULL, 'YES', '[riset-kelompok]', 'db', 'adrian', '2019-02-19 14:22:54', NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_jenis` VALUES (2, 'Pengabdian', NULL, 'YES', '[riset-kelompok]', 'db', 'adrian', '2019-02-19 14:22:54', NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_jenis` VALUES (3, 'Personil Dosen', NULL, 'YES', '[personil]', 'db', 'adrian', '2020-08-10 22:19:55', NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_jenis` VALUES (4, 'Personil Mahasiswa', NULL, 'YES', '[personil]', 'db', 'adrian', '2020-08-10 22:19:57', NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_jenis` VALUES (5, 'Personil Staf', NULL, 'YES', '[personil]', 'db', 'adrian', '2020-08-10 22:19:57', NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_jenis` VALUES (6, 'Personil Alumni', NULL, 'YES', '[personil]', 'db', 'adrian', '2020-08-10 22:19:57', NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_jenis` VALUES (7, 'Mitra Pengabdian', NULL, 'YES', '[mitra]', 'db', 'adrian', '2020-08-10 22:19:57', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for tb_rst_rf_jenis_luaran
-- ----------------------------
DROP TABLE IF EXISTS `tb_rst_rf_jenis_luaran`;
CREATE TABLE `tb_rst_rf_jenis_luaran`  (
  `id` int(11) NOT NULL,
  `jenis` int(11) NULL DEFAULT NULL,
  `luaran` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `keterangan` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `isAktif` enum('YES','NO') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'YES',
  `tags` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Created_App` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Created_By` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Created_Date` datetime(0) NULL DEFAULT NULL,
  `Modified_App` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Modified_By` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Modified_Date` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `tb_rst_rf_jenis_luaran_ibfk_1`(`jenis`) USING BTREE,
  CONSTRAINT `tb_rst_rf_jenis_luaran_ibfk_1` FOREIGN KEY (`jenis`) REFERENCES `tb_rst_rf_jenis` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_rst_rf_jenis_luaran
-- ----------------------------
INSERT INTO `tb_rst_rf_jenis_luaran` VALUES (1, 1, 'Publikasi', NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_jenis_luaran` VALUES (2, 1, 'Integrasi dalam Pembelajaran', NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_jenis_luaran` VALUES (3, 1, 'Hak Kekayaan Intelektual', NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_jenis_luaran` VALUES (4, 1, 'Luaran Lainnya', NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_jenis_luaran` VALUES (5, 2, 'Publikasi di Jurnal', NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_jenis_luaran` VALUES (6, 2, 'Publikasi di Media Massa', NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_jenis_luaran` VALUES (7, 2, 'Pemakalah Forum Ilmiah', NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_jenis_luaran` VALUES (8, 2, 'Hak Kekayaan Intelektual', NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_jenis_luaran` VALUES (9, 2, 'Luaran IPTEK Lainnya', NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_jenis_luaran` VALUES (10, 2, 'Produk Terstandarisasi', NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_jenis_luaran` VALUES (11, 2, 'Produk Tersertifikasi', NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_jenis_luaran` VALUES (12, 2, 'Mitra Berbadan Hukum', NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_jenis_luaran` VALUES (13, 2, 'Buku', NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_jenis_luaran` VALUES (14, 2, 'Wirausaha Baru Mandiri', NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for tb_rst_rf_jenis_props
-- ----------------------------
DROP TABLE IF EXISTS `tb_rst_rf_jenis_props`;
CREATE TABLE `tb_rst_rf_jenis_props`  (
  `jenis` int(6) NOT NULL,
  `prop` int(6) NOT NULL,
  `config` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `urutan` int(11) NULL DEFAULT NULL,
  `tags` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `isAktif` enum('YES','NO') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'YES',
  `Created_App` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Created_By` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Created_Date` datetime(0) NULL DEFAULT NULL,
  `Modified_App` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Modified_By` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Modified_Date` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`jenis`, `prop`) USING BTREE,
  INDEX `prop`(`prop`) USING BTREE,
  CONSTRAINT `tb_rst_rf_jenis_props_ibfk_2` FOREIGN KEY (`prop`) REFERENCES `tb_rst_rf_prop` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_rst_rf_jenis_props
-- ----------------------------
INSERT INTO `tb_rst_rf_jenis_props` VALUES (1, 1, NULL, 1, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_jenis_props` VALUES (1, 2, NULL, 2, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_jenis_props` VALUES (1, 3, NULL, 4, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_jenis_props` VALUES (1, 4, NULL, 5, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_jenis_props` VALUES (1, 5, NULL, 6, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_jenis_props` VALUES (1, 6, NULL, 7, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_jenis_props` VALUES (1, 7, NULL, 8, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_jenis_props` VALUES (1, 8, NULL, 9, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_jenis_props` VALUES (1, 9, NULL, 10, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_jenis_props` VALUES (1, 10, NULL, 11, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_jenis_props` VALUES (1, 73, NULL, 3, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_jenis_props` VALUES (2, 1, NULL, 4, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_jenis_props` VALUES (2, 3, NULL, 7, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_jenis_props` VALUES (2, 10, NULL, 5, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_jenis_props` VALUES (2, 11, NULL, 2, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_jenis_props` VALUES (2, 12, NULL, 3, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_jenis_props` VALUES (2, 13, NULL, 6, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_jenis_props` VALUES (2, 14, NULL, 8, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_jenis_props` VALUES (2, 72, NULL, 1, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_jenis_props` VALUES (3, 15, NULL, 1, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_jenis_props` VALUES (3, 16, NULL, 2, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_jenis_props` VALUES (3, 17, NULL, 3, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_jenis_props` VALUES (3, 18, NULL, 4, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_jenis_props` VALUES (4, 19, NULL, 1, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_jenis_props` VALUES (4, 20, NULL, 2, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_jenis_props` VALUES (4, 21, NULL, 3, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_jenis_props` VALUES (5, 42, NULL, 1, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_jenis_props` VALUES (6, 43, NULL, 1, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_jenis_props` VALUES (7, 44, NULL, 1, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_jenis_props` VALUES (7, 45, NULL, 2, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_jenis_props` VALUES (7, 46, NULL, 3, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_jenis_props` VALUES (7, 47, NULL, 4, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_jenis_props` VALUES (7, 48, NULL, 5, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for tb_rst_rf_luaran
-- ----------------------------
DROP TABLE IF EXISTS `tb_rst_rf_luaran`;
CREATE TABLE `tb_rst_rf_luaran`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `riset` int(11) NULL DEFAULT NULL,
  `luaran` int(11) NULL DEFAULT NULL,
  `keterangan` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `isAktif` enum('YES','NO') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'YES',
  `tags` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Created_App` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Created_By` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Created_Date` datetime(0) NULL DEFAULT NULL,
  `Modified_App` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Modified_By` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Modified_Date` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `riset`(`riset`) USING BTREE,
  INDEX `luaran`(`luaran`) USING BTREE,
  CONSTRAINT `tb_rst_rf_luaran_ibfk_1` FOREIGN KEY (`riset`) REFERENCES `tb_rst_tr_riset` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tb_rst_rf_luaran_ibfk_2` FOREIGN KEY (`luaran`) REFERENCES `tb_rst_rf_jenis_luaran` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 23 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_rst_rf_luaran
-- ----------------------------
INSERT INTO `tb_rst_rf_luaran` VALUES (1, 14, 1, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran` VALUES (2, 15, 2, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran` VALUES (3, 22, 9, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran` VALUES (4, 28, 9, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran` VALUES (5, 29, 9, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran` VALUES (6, 30, 9, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran` VALUES (7, 31, 9, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran` VALUES (8, 32, 9, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran` VALUES (9, 33, 9, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran` VALUES (10, 50, 9, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran` VALUES (11, 52, 9, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran` VALUES (12, 53, 9, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran` VALUES (13, 55, 9, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran` VALUES (14, 60, 9, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran` VALUES (15, 61, 6, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran` VALUES (16, 62, 9, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran` VALUES (17, 64, 9, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran` VALUES (18, 74, 6, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran` VALUES (19, 75, 9, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran` VALUES (20, 90, 1, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran` VALUES (21, 90, 2, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran` VALUES (22, 94, 1, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for tb_rst_rf_luaran_props
-- ----------------------------
DROP TABLE IF EXISTS `tb_rst_rf_luaran_props`;
CREATE TABLE `tb_rst_rf_luaran_props`  (
  `luaran` int(11) NULL DEFAULT NULL,
  `prop` int(11) NULL DEFAULT NULL,
  `config` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `urutan` int(11) NULL DEFAULT NULL,
  `tags` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `isAktif` enum('YES','NO') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'YES',
  `Created_App` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Created_By` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Created_Date` datetime(0) NULL DEFAULT NULL,
  `Modified_App` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Modified_By` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Modified_Date` datetime(0) NULL DEFAULT NULL,
  INDEX `prop_ke_luaran`(`prop`) USING BTREE,
  INDEX `luaran_ke_luaran`(`luaran`) USING BTREE,
  CONSTRAINT `luaran_ke_luaran` FOREIGN KEY (`luaran`) REFERENCES `tb_rst_rf_jenis_luaran` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `prop_ke_luaran` FOREIGN KEY (`prop`) REFERENCES `tb_rst_rf_prop` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_rst_rf_luaran_props
-- ----------------------------
INSERT INTO `tb_rst_rf_luaran_props` VALUES (1, 22, NULL, 2, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (1, 23, NULL, 3, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (1, 24, NULL, 4, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (1, 25, NULL, 5, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (1, 26, NULL, 6, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (1, 27, NULL, 7, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (1, 28, NULL, 8, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (1, 29, NULL, 9, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (1, 30, NULL, 10, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (1, 31, NULL, 11, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (1, 32, NULL, 12, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (1, 33, NULL, 13, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (5, 22, NULL, 2, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (5, 23, NULL, 3, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (5, 24, NULL, 4, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (5, 25, NULL, 5, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (5, 26, NULL, 6, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (5, 27, NULL, 7, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (5, 28, NULL, 8, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (5, 29, NULL, 9, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (5, 30, NULL, 10, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (5, 31, NULL, 11, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (5, 32, NULL, 12, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (5, 33, NULL, 13, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (2, 33, NULL, 3, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (2, 34, NULL, 1, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (2, 35, NULL, 2, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (3, 33, NULL, 5, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (3, 36, NULL, 1, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (3, 37, NULL, 2, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (3, 38, NULL, 3, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (3, 39, NULL, 4, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (8, 33, NULL, 5, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (8, 36, NULL, 1, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (8, 37, NULL, 2, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (8, 38, NULL, 3, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (8, 39, NULL, 4, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (4, 33, NULL, 3, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (4, 40, NULL, 1, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (4, 41, NULL, 2, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (6, 23, NULL, 2, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (6, 25, NULL, 5, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (6, 26, NULL, 6, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (6, 28, NULL, 7, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (6, 31, NULL, 8, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (6, 33, NULL, 11, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (6, 49, NULL, 1, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (6, 50, NULL, 3, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (6, 51, NULL, 4, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (6, 52, NULL, 9, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (6, 53, NULL, 10, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (7, 75, NULL, 5, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (7, 23, NULL, 2, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (7, 33, NULL, 9, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (7, 53, NULL, 8, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (7, 54, NULL, 1, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (7, 55, NULL, 3, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (7, 56, NULL, 4, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (7, 57, NULL, 6, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (7, 58, NULL, 7, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (9, 33, NULL, 4, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (9, 41, NULL, 3, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (9, 59, NULL, 1, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (9, 60, NULL, 2, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (10, 33, NULL, 4, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (10, 61, NULL, 1, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (10, 62, NULL, 2, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (10, 63, NULL, 3, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (11, 33, NULL, 4, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (11, 61, NULL, 1, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (11, 62, NULL, 2, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (11, 63, NULL, 3, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (12, 33, NULL, 5, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (12, 46, NULL, 2, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (12, 62, NULL, 3, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (12, 64, NULL, 1, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (12, 65, NULL, 4, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (13, 33, NULL, 6, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (13, 58, NULL, 3, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (13, 66, NULL, 1, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (13, 67, NULL, 2, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (13, 68, NULL, 4, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (13, 69, NULL, 5, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (14, 33, NULL, 3, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (14, 41, NULL, 2, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (14, 70, NULL, 1, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (1, 71, NULL, 1, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_luaran_props` VALUES (5, 71, NULL, 1, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for tb_rst_rf_options
-- ----------------------------
DROP TABLE IF EXISTS `tb_rst_rf_options`;
CREATE TABLE `tb_rst_rf_options`  (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `value` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `urutan` int(11) NULL DEFAULT NULL,
  `kategori` enum('jenis_penelitian','bidang_penelitian','subbidang_penelitian','tujuan_sosial','subtujuan_sosial','sumber_dana','institusi_sumber_dana','jenis_kegiatan','penyelenggaraaan','sumber_dana_pg','sumber_daya_iptek','mitra','status_anggota','tingkat_jurnal','status_pengajuan','jenis_hki','status_hki','jenis_media','jenis_publikasi','jenis_pengabdian','tingkat_pemakalah','luaran_iptek','skim_penelitian','skim_pengabdian') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `parent_id` int(3) NULL DEFAULT NULL,
  `tags` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `isAktif` enum('YES','NO') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'YES',
  `Created_App` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Created_By` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Created_Date` datetime(0) NULL DEFAULT NULL,
  `Modified_App` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Modified_By` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Modified_Date` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 413 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_rst_rf_options
-- ----------------------------
INSERT INTO `tb_rst_rf_options` VALUES (1, 'Penelitian Mandiri', 1, 'jenis_penelitian', NULL, '[jenis-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (2, 'Penelitian Eksternal', 2, 'jenis_penelitian', NULL, '[jenis-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (3, 'Penelitian Internal', 3, 'jenis_penelitian', NULL, '[jenis-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (4, 'Natural Science', 1, 'bidang_penelitian', NULL, '[bidang-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (5, 'Engineering Technology', 2, 'bidang_penelitian', NULL, '[bidang-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (6, 'Agricultural and Environmental Sciences', 3, 'bidang_penelitian', NULL, '[bidang-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (7, 'Medical Sciences', 4, 'bidang_penelitian', NULL, '[bidang-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (8, 'Social Sciences', 5, 'bidang_penelitian', NULL, '[bidang-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (9, 'Humanities', 6, 'bidang_penelitian', NULL, '[bidang-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (10, 'Mathematical Sciences', 1, 'subbidang_penelitian', 4, '[subbidang-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (11, 'Physical Sciences', 2, 'subbidang_penelitian', 4, '[subbidang-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (12, 'Chemical Sciences', 3, 'subbidang_penelitian', 4, '[subbidang-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (13, 'Earth Sciences', 4, 'subbidang_penelitian', 4, '[subbidang-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (14, 'Biological Sciences', 5, 'subbidang_penelitian', 4, '[subbidang-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (15, 'Information, Computing, and Communication Sciences', 6, 'subbidang_penelitian', 4, '[subbidang-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (16, 'Industrial Biotechnology and Food Sciences', 1, 'subbidang_penelitian', 5, '[subbidang-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (17, 'Aerospace Engineering', 2, 'subbidang_penelitian', 5, '[subbidang-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (18, 'Manufacturing Engineering', 3, 'subbidang_penelitian', 5, '[subbidang-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (19, 'Automotive Engineering', 4, 'subbidang_penelitian', 5, '[subbidang-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (20, 'Mechanical and Industrial Engineering', 5, 'subbidang_penelitian', 5, '[subbidang-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (21, 'Chemical Engineering', 6, 'subbidang_penelitian', 5, '[subbidang-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (22, 'Resources Engineering', 7, 'subbidang_penelitian', 5, '[subbidang-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (23, 'Civil Engineering', 8, 'subbidang_penelitian', 5, '[subbidang-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (24, 'Electrical and Electronic Engineering', 9, 'subbidang_penelitian', 5, '[subbidang-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (25, 'Geometric Engineering', 10, 'subbidang_penelitian', 5, '[subbidang-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (26, 'Environmental Engineering', 11, 'subbidang_penelitian', 5, '[subbidang-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (27, 'Maritime Engineering', 12, 'subbidang_penelitian', 5, '[subbidang-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (28, 'Metallurgy', 13, 'subbidang_penelitian', 5, '[subbidang-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (29, 'Material Engineering', 14, 'subbidang_penelitian', 5, '[subbidang-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (30, 'Biomedical Engineering', 15, 'subbidang_penelitian', 5, '[subbidang-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (31, 'Computer Hardware', 16, 'subbidang_penelitian', 5, '[subbidang-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (32, 'Communication Technologies', 17, 'subbidang_penelitian', 5, '[subbidang-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (33, 'Interdisciplinary Engineering', 18, 'subbidang_penelitian', 5, '[subbidang-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (34, 'Agricultural and Vetenary Sciences', 1, 'subbidang_penelitian', 6, '[subbidang-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (35, 'Environmental Sciences', 2, 'subbidang_penelitian', 6, '[subbidang-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (36, 'Architecture Urban Environment and Building', 3, 'subbidang_penelitian', 6, '[subbidang-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (37, 'Medical Sciences', 1, 'subbidang_penelitian', 7, '[subbidang-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (38, 'Public Health and Health Services', 2, 'subbidang_penelitian', 7, '[subbidang-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (39, 'Education', 1, 'subbidang_penelitian', 8, '[subbidang-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (40, 'Economics', 2, 'subbidang_penelitian', 8, '[subbidang-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (41, 'Commerce, Management, Tourism and Services', 3, 'subbidang_penelitian', 8, '[subbidang-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (42, 'Policy and Political Sciences', 4, 'subbidang_penelitian', 8, '[subbidang-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (43, 'Studies in Human Society', 5, 'subbidang_penelitian', 8, '[subbidang-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (44, 'Behavioral and Cognitive Sciences', 6, 'subbidang_penelitian', 8, '[subbidang-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (45, 'Law, Justice, and Law Enforcement', 7, 'subbidang_penelitian', 8, '[subbidang-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (46, 'Journalism, Librarianship and Curatorial Studies', 8, 'subbidang_penelitian', 8, '[subbidang-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (47, 'The Arts', 1, 'subbidang_penelitian', 9, '[subbidang-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (48, 'Language and Culture', 2, 'subbidang_penelitian', 9, '[subbidang-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (49, 'History and Archeology', 3, 'subbidang_penelitian', 9, '[subbidang-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (50, 'Philosophy and Religion', 4, 'subbidang_penelitian', 9, '[subbidang-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (51, 'Visual Communication Design', 5, 'subbidang_penelitian', 9, '[subbidang-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (52, 'Defense', 1, 'tujuan_sosial', NULL, '[tujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (53, 'Plant Production and Plant Primary Products', 2, 'tujuan_sosial', NULL, '[tujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (54, 'Animal Production and Animal Primary Products', 3, 'tujuan_sosial', NULL, '[tujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (55, 'Mineral Resources', 4, 'tujuan_sosial', NULL, '[tujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (56, 'Energy Resources', 5, 'tujuan_sosial', NULL, '[tujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (57, 'Energy Supply', 6, 'tujuan_sosial', NULL, '[tujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (58, 'Manufacturing', 7, 'tujuan_sosial', NULL, '[tujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (59, 'Construction', 8, 'tujuan_sosial', NULL, '[tujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (60, 'Transport', 9, 'tujuan_sosial', NULL, '[tujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (61, 'Information and Communication Services', 10, 'tujuan_sosial', NULL, '[tujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (62, 'Commercial Services', 11, 'tujuan_sosial', NULL, '[tujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (63, 'Economic Framework', 12, 'tujuan_sosial', NULL, '[tujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (64, 'Natural Resources', 13, 'tujuan_sosial', NULL, '[tujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (65, 'Health', 14, 'tujuan_sosial', NULL, '[tujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (66, 'Education and Training', 15, 'tujuan_sosial', NULL, '[tujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (67, 'Social Development and Community Services', 16, 'tujuan_sosial', NULL, '[tujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (68, 'Environmental Knowledge', 17, 'tujuan_sosial', NULL, '[tujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (69, 'Environmental Aspects of Development', 18, 'tujuan_sosial', NULL, '[tujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (70, 'Environmental Management & Other Aspects', 19, 'tujuan_sosial', NULL, '[tujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (71, 'Advancement of Natural Sciences, Technology, and Engineering', 20, 'tujuan_sosial', NULL, '[tujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (72, 'Advancement of Social Sciences and Humanities', 21, 'tujuan_sosial', NULL, '[tujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (73, 'Military and Politics', 1, 'subtujuan_sosial', 52, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (74, 'Military Technology', 2, 'subtujuan_sosial', 52, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (75, 'Military Doctrine, Education, and Training', 3, 'subtujuan_sosial', 52, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (76, 'Military Capabilities', 4, 'subtujuan_sosial', 52, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (77, 'Police and Internal Security', 5, 'subtujuan_sosial', 52, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (78, 'Field Crops', 1, 'subtujuan_sosial', 53, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (79, 'Plantation Crops', 2, 'subtujuan_sosial', 53, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (80, 'Horticultural Crops', 3, 'subtujuan_sosial', 53, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (81, 'Forestry', 4, 'subtujuan_sosial', 53, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (82, 'Primary Products From Plants', 5, 'subtujuan_sosial', 53, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (83, 'By-products Utilization', 6, 'subtujuan_sosial', 53, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (84, 'Herbs, Spices and Medicinal Plants', 7, 'subtujuan_sosial', 53, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (85, 'Livestock', 1, 'subtujuan_sosial', 54, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (86, 'Pasture, Browse and Folder Crops', 2, 'subtujuan_sosial', 54, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (87, 'Fisheries Products', 3, 'subtujuan_sosial', 54, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (88, 'Primary & By-products From Animals', 4, 'subtujuan_sosial', 54, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (89, 'Exploration', 1, 'subtujuan_sosial', 55, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (90, 'Primary Mining and Extraction Processes', 2, 'subtujuan_sosial', 55, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (91, 'First Stage Treatment of Ores and Minerals', 3, 'subtujuan_sosial', 55, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (92, 'Prevention and Treatment of Pollution', 4, 'subtujuan_sosial', 55, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (93, 'Exploration', 1, 'subtujuan_sosial', 56, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (94, 'Mining and Extraction', 2, 'subtujuan_sosial', 56, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (95, 'Preparation and Supply of Energy Source Materials', 3, 'subtujuan_sosial', 56, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (96, 'Non-conventional Energy Resources', 4, 'subtujuan_sosial', 56, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (97, 'Nuclear Energy', 5, 'subtujuan_sosial', 56, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (98, 'Energy Transformation', 1, 'subtujuan_sosial', 57, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (99, 'Renewable Energy', 2, 'subtujuan_sosial', 57, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (100, 'Energy Distribution', 3, 'subtujuan_sosial', 57, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (101, 'Energy Conservation and Efficiency', 4, 'subtujuan_sosial', 57, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (102, 'Energy Issues', 5, 'subtujuan_sosial', 57, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (103, 'Processed Food Products and Beverages', 1, 'subtujuan_sosial', 58, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (104, 'Fiber Processing and Textiles, Footwear and Leather Products', 2, 'subtujuan_sosial', 58, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (105, 'Wood, Wood Products and Paper', 3, 'subtujuan_sosial', 58, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (106, 'Human Pharmaceutical Products', 4, 'subtujuan_sosial', 58, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (107, 'Veterinary Pharmaceutical Products', 5, 'subtujuan_sosial', 58, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (108, 'Agricultural Chemicals', 6, 'subtujuan_sosial', 58, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (109, 'Industrial Chemicals and Related Products', 7, 'subtujuan_sosial', 58, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (110, 'Basic Metal Products (Including Smelting)', 8, 'subtujuan_sosial', 58, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (111, 'Industrial Mineral Products', 9, 'subtujuan_sosial', 58, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (112, 'Fabricated Metal Products', 10, 'subtujuan_sosial', 58, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (113, 'Transport Equipment', 11, 'subtujuan_sosial', 58, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (114, 'Computer Hardware and Electronic Equipment', 12, 'subtujuan_sosial', 58, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (115, 'Communication Equipment', 13, 'subtujuan_sosial', 58, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (116, 'Instrumentation', 14, 'subtujuan_sosial', 58, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (117, 'Machinery and Equipment', 15, 'subtujuan_sosial', 58, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (118, 'Latex Product Industry', 16, 'subtujuan_sosial', 58, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (119, 'Standard Supporting Technologies', 17, 'subtujuan_sosial', 58, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (120, 'Materials Performance and Processes / Analysis', 18, 'subtujuan_sosial', 58, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (121, 'Milling and Process Materials', 19, 'subtujuan_sosial', 58, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (122, 'Synthesis and Design of Fine and Speciality Chemicals', 20, 'subtujuan_sosial', 58, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (123, 'Consumer Products', 21, 'subtujuan_sosial', 58, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (124, 'Planning', 1, 'subtujuan_sosial', 59, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (125, 'Design', 2, 'subtujuan_sosial', 59, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (126, 'Construction Processes', 3, 'subtujuan_sosial', 59, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (127, 'Building Management and Services', 4, 'subtujuan_sosial', 59, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (128, 'Ground Transport', 1, 'subtujuan_sosial', 60, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (129, 'Water Transport', 2, 'subtujuan_sosial', 60, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (130, 'Air & Space Transport', 3, 'subtujuan_sosial', 60, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (131, 'Computer Software and Services', 1, 'subtujuan_sosial', 61, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (132, 'Information Services (Including Library)', 2, 'subtujuan_sosial', 61, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (133, 'Communication Services', 3, 'subtujuan_sosial', 61, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (134, 'Geoinformation Services', 4, 'subtujuan_sosial', 61, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (135, 'Electricity, Gas and Water Services and Utilities', 1, 'subtujuan_sosial', 62, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (136, 'Waste Management and Recycling', 2, 'subtujuan_sosial', 62, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (137, 'Wholesoale and Retail Trade', 3, 'subtujuan_sosial', 62, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (138, 'Finance, Property and Business Services', 4, 'subtujuan_sosial', 62, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (139, 'Tourism', 5, 'subtujuan_sosial', 62, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (140, 'Macroeconomics Issues', 1, 'subtujuan_sosial', 63, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (141, 'Microeconomics Issues', 2, 'subtujuan_sosial', 63, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (142, 'International Trade Issues', 3, 'subtujuan_sosial', 63, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (143, 'Management and Productivity Issues', 4, 'subtujuan_sosial', 63, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (144, 'Measurement Standards and Calibration Services', 5, 'subtujuan_sosial', 63, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (145, 'Commercialization', 6, 'subtujuan_sosial', 63, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (146, 'Socio-economic Development', 7, 'subtujuan_sosial', 63, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (147, 'Economic Development and Environment', 8, 'subtujuan_sosial', 63, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (148, 'Human Resource Management', 9, 'subtujuan_sosial', 63, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (149, 'Water Resources', 1, 'subtujuan_sosial', 64, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (150, 'Biodiversity', 2, 'subtujuan_sosial', 64, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (151, 'Bioactive Product', 3, 'subtujuan_sosial', 64, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (152, 'Industrial Raw Materials', 4, 'subtujuan_sosial', 64, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (153, 'Mineral Resource', 5, 'subtujuan_sosial', 64, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (154, 'Clinical (Organs, Diseases and Conditions)', 1, 'subtujuan_sosial', 65, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (155, 'Public Health', 2, 'subtujuan_sosial', 65, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (156, 'Health and Support Services', 3, 'subtujuan_sosial', 65, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (157, 'Early Childhood and Primary Education', 1, 'subtujuan_sosial', 66, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (158, 'Secondary Education', 2, 'subtujuan_sosial', 66, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (159, 'Tertiary Education', 3, 'subtujuan_sosial', 66, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (160, 'Technical and Further Education', 4, 'subtujuan_sosial', 66, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (161, 'Special Education', 5, 'subtujuan_sosial', 66, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (162, 'Computer Base Teaching and Learning', 6, 'subtujuan_sosial', 66, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (163, 'Education Policy', 7, 'subtujuan_sosial', 66, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (164, 'Teaching', 8, 'subtujuan_sosial', 66, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (165, 'Educational Administration', 9, 'subtujuan_sosial', 66, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (166, 'Community Services', 1, 'subtujuan_sosial', 67, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (167, 'Public Services', 2, 'subtujuan_sosial', 67, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (168, 'Art, Sport and Recreation', 3, 'subtujuan_sosial', 67, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (169, 'International Relation', 4, 'subtujuan_sosial', 67, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (170, 'Ethical Issues', 5, 'subtujuan_sosial', 67, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (171, 'Nation Building', 6, 'subtujuan_sosial', 67, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (172, 'Urban Issues', 7, 'subtujuan_sosial', 67, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (173, 'Climate and Atmosphere', 1, 'subtujuan_sosial', 68, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (174, 'Ocean', 2, 'subtujuan_sosial', 68, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (175, 'Water', 3, 'subtujuan_sosial', 68, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (176, 'Land', 4, 'subtujuan_sosial', 68, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (177, 'Nature Conservation', 5, 'subtujuan_sosial', 68, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (178, 'Social Environment', 6, 'subtujuan_sosial', 68, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (179, 'Social Environment', 7, 'subtujuan_sosial', 68, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (180, 'River and Lake', 8, 'subtujuan_sosial', 68, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (181, 'Plant Production and Plant Primary Products (Including Forestry)', 1, 'subtujuan_sosial', 69, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (182, 'Animal Production and Animal Primary Products (Including Fishing)', 2, 'subtujuan_sosial', 69, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (183, 'Mineral Resources (Excluding Energy)', 3, 'subtujuan_sosial', 69, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (184, 'Energy Resources', 4, 'subtujuan_sosial', 69, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (185, 'Energy Supply', 5, 'subtujuan_sosial', 69, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (186, 'Manufacturing', 6, 'subtujuan_sosial', 69, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (187, 'Construction', 7, 'subtujuan_sosial', 69, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (188, 'Transport', 8, 'subtujuan_sosial', 69, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (189, 'Information and Communication Services', 9, 'subtujuan_sosial', 69, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (190, 'Commercial Services', 10, 'subtujuan_sosial', 69, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (191, 'Environmental Economic Framework', 11, 'subtujuan_sosial', 69, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (192, 'Environmental Management', 1, 'subtujuan_sosial', 70, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (193, 'Waste Management and Recyling', 2, 'subtujuan_sosial', 70, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (194, 'Climate and Weather', 3, 'subtujuan_sosial', 70, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (195, 'Atmosphere (Excl. Climate and Weather)', 4, 'subtujuan_sosial', 70, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (196, 'Marine and Coastal Environment', 5, 'subtujuan_sosial', 70, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (197, 'Fresh Water and Estuarine Environment', 6, 'subtujuan_sosial', 70, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (198, 'Urban and Industrial Environment', 7, 'subtujuan_sosial', 70, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (199, 'Forest and Wooded Lands', 8, 'subtujuan_sosial', 70, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (200, 'Mining Environment', 9, 'subtujuan_sosial', 70, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (201, 'Mathematical Science', 1, 'subtujuan_sosial', 71, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (202, 'Physical Sciences', 2, 'subtujuan_sosial', 71, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (203, 'Chemical Sciences', 3, 'subtujuan_sosial', 71, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (204, 'Earth Sciences', 4, 'subtujuan_sosial', 71, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (205, 'Information, Computer and Communication Technologies', 5, 'subtujuan_sosial', 71, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (206, 'Applied Sciences and Technologies', 6, 'subtujuan_sosial', 71, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (207, 'Engineering Sciences', 7, 'subtujuan_sosial', 71, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (208, 'Biological Sciences', 8, 'subtujuan_sosial', 71, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (209, 'Agricultural Sciences', 9, 'subtujuan_sosial', 71, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (210, 'Medical and Health Sciences', 10, 'subtujuan_sosial', 71, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (211, 'Multimedia', 11, 'subtujuan_sosial', 71, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (212, 'Social Sciences', 1, 'subtujuan_sosial', 72, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (213, 'Humanities', 2, 'subtujuan_sosial', 72, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (214, 'Cyber Law', 3, 'subtujuan_sosial', 72, '[subtujuan-sosial]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (215, 'Dalam Negeri', 1, 'sumber_dana', NULL, '[sumber-dana]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (216, 'Luar Negeri / Asing', 2, 'sumber_dana', NULL, '[sumber-dana]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (217, 'Pemerintah', 1, 'institusi_sumber_dana', NULL, '[institusi-sumber-dana]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (218, 'Swasta / Industri', 2, 'institusi_sumber_dana', NULL, '[institusi-sumber-dana]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (219, 'Lembaga Multilateral', 3, 'institusi_sumber_dana', NULL, '[institusi-sumber-dana]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (220, 'Lembaga Nirlaba', 4, 'institusi_sumber_dana', NULL, '[institusi-sumber-dana]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (221, 'Kegiatan Non-Insidental (1-6 Bulan)', 1, 'jenis_kegiatan', NULL, '[jenis-kegiatan]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (222, 'Kegiatan Insidental (kurang dari 1 Bulan)', 2, 'jenis_kegiatan', NULL, '[jenis-kegiatan]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (223, 'Lokal', 1, 'penyelenggaraaan', NULL, '[penyelenggaraan]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (224, 'Nasional', 2, 'penyelenggaraaan', NULL, '[penyelenggaraan]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (225, 'Internasional', 3, 'penyelenggaraaan', NULL, '[penyelenggaraan]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (226, 'Internal PT', 1, 'sumber_dana_pg', NULL, '[sumber-dana-pg]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (227, 'Pemda', 2, 'sumber_dana_pg', NULL, '[sumber-dana-pg]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (228, 'CSR', 3, 'sumber_dana_pg', NULL, '[sumber-dana-pg]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (229, 'Lainnya Dalam Negeri', 4, 'sumber_dana_pg', NULL, '[sumber-dana-pg]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (230, 'Lainnya Luar Negeri', 5, 'sumber_dana_pg', NULL, '[sumber-dana-pg]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (231, 'Mitra yang Non Produktif', 1, 'mitra', NULL, '[mitra]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (232, 'Mitra yang Produktif (IRT/UMKM)', 2, 'mitra', NULL, '[mitra]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (233, 'Mitra CSR', 3, 'mitra', NULL, '[mitra]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (234, 'Mitra Pemda / Instansi', 4, 'mitra', NULL, '[mitra]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (235, 'Mitra Industri / UKM', 5, 'mitra', NULL, '[mitra]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (236, 'Mitra yang Produktivitasnya Meningkat', 6, 'mitra', NULL, '[mitra]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (237, 'Mitra yang Kualitas Produknya Meningkat', 7, 'mitra', NULL, '[mitra]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (238, 'Mitra yang Berhasil Melakukan Ekspor / Pemasaran Antar Pulau', 8, 'mitra', NULL, '[mitra]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (239, 'Mitra yang Menghasilkan Usahawan Muda', 9, 'mitra', NULL, '[mitra]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (240, 'Mitra yang Omzetnya Meningkat', 10, 'mitra', NULL, '[mitra]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (241, 'Mitra yang Tenaga Kerjanya Meningkat', 11, 'mitra', NULL, '[mitra]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (242, 'Mitra yang Kemampuan Manajemennya Meningkat', 12, 'mitra', NULL, '[mitra]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (243, 'Paten', 1, 'sumber_daya_iptek', NULL, '[sumber-daya-iptek][jenis-hki]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (244, 'Paten Sederhana', 2, 'sumber_daya_iptek', NULL, '[sumber-daya-iptek][jenis-hki]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (245, 'Perlindungan Varietas Tanaman', 3, 'sumber_daya_iptek', NULL, '[sumber-daya-iptek][jenis-hki]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (246, 'Hak Cipta Merek Dagang', 4, 'sumber_daya_iptek', NULL, '[sumber-daya-iptek][jenis-hki]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (247, 'Rahasia Dagang', 5, 'sumber_daya_iptek', NULL, '[sumber-daya-iptek][jenis-hki]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (248, 'Desain Produk Industri', 6, 'sumber_daya_iptek', NULL, '[sumber-daya-iptek][jenis-hki]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (249, 'Indikasi Geografis', 7, 'sumber_daya_iptek', NULL, '[sumber-daya-iptek][jenis-hki]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (250, 'Perlindungan Desain Tata Letak Sirkuit Terpadu', 8, 'sumber_daya_iptek', NULL, '[sumber-daya-iptek][jenis-hki]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (251, 'Teknologi Tepat Guna', 9, 'sumber_daya_iptek', NULL, '[sumber-daya-iptek][jenis-hki][luaran-iptek]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (252, 'Model', 10, 'sumber_daya_iptek', NULL, '[sumber-daya-iptek][luaran-iptek]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (253, 'Purwarupa (Prototipe)', 11, 'sumber_daya_iptek', NULL, '[sumber-daya-iptek][luaran-iptek]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (254, 'Karya Desain / Seni / Kriya / Bangunan dan Arsitektur', 12, 'sumber_daya_iptek', NULL, '[sumber-daya-iptek][luaran-iptek]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (255, 'Rekayasa Sosial', 13, 'sumber_daya_iptek', NULL, '[sumber-daya-iptek][luaran-iptek]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (256, 'Ketua', 1, 'status_anggota', NULL, '[status-anggota]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (257, 'Anggota', 2, 'status_anggota', NULL, '[status-anggota]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (258, 'Internasional', 1, 'tingkat_jurnal', NULL, '[tingkat-jurnal]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (259, 'Nasional Terakreditasi', 2, 'tingkat_jurnal', NULL, '[tingkat-jurnal]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (260, 'Nasional Tidak Terakreditasi', 3, 'tingkat_jurnal', NULL, '[tingkat-jurnal]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (261, 'Pengajuan', 1, 'status_pengajuan', NULL, '[status-pengajuan]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (262, 'On Review', 2, 'status_pengajuan', NULL, '[status-pengajuan]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (263, 'Publish', 3, 'status_pengajuan', NULL, '[status-pengajuan]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (264, 'Terdaftar', 1, 'status_hki', NULL, '[status-hki]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (265, 'Granted', 2, 'status_hki', NULL, '[status-hki]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (266, 'Jurnal', 1, 'jenis_publikasi', NULL, '[jenis-publikasi]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (267, 'Prosiding', 2, 'jenis_publikasi', NULL, '[jenis-publikasi]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (268, 'Pengabdian Mandiri', 1, 'jenis_pengabdian', NULL, '[jenis-pengabdian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (269, 'Pengabdian Internal', 2, 'jenis_pengabdian', NULL, '[jenis-pengabdian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (270, 'Pengabdian Eksternal', 3, 'jenis_pengabdian', NULL, '[jenis-pengabdian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (271, 'Koran', 1, 'jenis_media', NULL, '[jenis-media]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (272, 'Majalah', 2, 'jenis_media', NULL, '[jenis-media]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (273, 'Tabloid', 3, 'jenis_media', NULL, '[jenis-media]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (274, 'Radio', 4, 'jenis_media', NULL, '[jenis-media]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (275, 'Televisi', 5, 'jenis_media', NULL, '[jenis-media]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (276, 'Media Onlne', 6, 'jenis_media', NULL, '[jenis-media]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (277, 'Tingkat Internasional', 1, 'tingkat_pemakalah', NULL, '[tingkat-pemakalah]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (278, 'Tingkat Nasional', 2, 'tingkat_pemakalah', NULL, '[tingkat-pemakalah]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (279, 'Tingkat Regional', 3, 'tingkat_pemakalah', NULL, '[tingkat-pemakalah]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (280, 'Penelitian Unggulan Perguruan Tinggi (PTUP)', 1, 'skim_penelitian', 2, '[skim-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (281, 'Penelitian Hibah Bersaing (PHB)', 2, 'skim_penelitian', 2, '[skim-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (282, 'Penelitian Fundamental (PF)', 3, 'skim_penelitian', 2, '[skim-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (283, 'Penelitian Tim Pascasarjana (PPS)', 4, 'skim_penelitian', 2, '[skim-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (284, 'Penelitian Kerjasama antar Perguruan Tinggi (PEKERTI)', 5, 'skim_penelitian', 2, '[skim-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (285, 'Penelitian Disertasi Doktor (PDD)', 6, 'skim_penelitian', 2, '[skim-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (286, 'Penelitian Dosen Pemula (PDP)', 7, 'skim_penelitian', 2, '[skim-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (287, 'Sistem Pembelajaran Daring Indonesia (SPADA)', 8, 'skim_penelitian', 2, '[skim-penelitian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (288, 'Ipteks bagi Masyarakat (IbM)', 1, 'skim_pengabdian', 270, '[skim-pengabdian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (289, 'Ipteks bagi Kewirausahaan (IbK)', 2, 'skim_pengabdian', 270, '[skim-pengabdian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (290, 'Ipteks bagi Produk Ekspor (IbPE)', 3, 'skim_pengabdian', 270, '[skim-pengabdian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (291, 'Ipteks bagi Inovasi Kreativitas Kampus (IbIKK)', 4, 'skim_pengabdian', 270, '[skim-pengabdian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (292, 'Ipteks bagi Wilayah (IbW)', 5, 'skim_pengabdian', 270, '[skim-pengabdian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (293, 'Ipteks bagi Wilayah antara PT-CSR atau PT-Pemda-CSR (IbWPT)', 6, 'skim_pengabdian', 270, '[skim-pengabdian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (294, 'Hibah Hi-Link', 7, 'skim_pengabdian', 270, '[skim-pengabdian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (295, 'Produk Teknologi yang Didiseminasikan ke Masyarakat', 8, 'skim_pengabdian', 270, '[skim-pengabdian]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (296, 'Mandiri', 6, 'sumber_dana_pg', NULL, '[sumber-dana-pg]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (297, 'DIKTI', 7, 'sumber_dana_pg', NULL, '[sumber-dana-pg]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (298, 'Mandiri', 5, 'institusi_sumber_dana', NULL, '[institusi-sumber-dana]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_options` VALUES (299, 'Internal PT', 5, 'institusi_sumber_dana', NULL, '[institusi-sumber-dana]', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for tb_rst_rf_pengumuman
-- ----------------------------
DROP TABLE IF EXISTS `tb_rst_rf_pengumuman`;
CREATE TABLE `tb_rst_rf_pengumuman`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `isi` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `kategori` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `Created_App` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Created_By` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Created_Date` datetime(0) NULL DEFAULT NULL,
  `Modified_App` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Modified_By` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Modified_Date` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_rst_rf_pengumuman
-- ----------------------------
INSERT INTO `tb_rst_rf_pengumuman` VALUES (1, 'Pemberitahuan Penerimaan Proposal Penelitian dan Pengabdian Tahun Anggaran 2021', '<p>Yth. Bapak/Ibu Dosen</p>\r\n\r\n<p>Sehubungan dengan seleksi pendanaan penelitian dan pengabdian Tahun Anggaran 2021, Direktorat Riset dan Pengabdian Masyarakat (DRPM) Deputi Bidang Penguatan Riset dan Pengembangan Kementerian Riset dan Teknologi/Badan Riset dan Inovasi Nasional dengan ini mengumumkan beberapa hal sebagai berikut:</p>\r\n\r\n<ol>\r\n	<li>Penerimaan proposal akan dilaksanakan pada tanggal&nbsp;<strong>15-29 Oktober 2020</strong>&nbsp;(tidak ada perpanjangan periode mengingat keterbatasan waktu).</li>\r\n	<li>Dana penelitian dan pengabdian pada tahun 2021 akan dialokasikan pada:<br />\r\n	a. Penelitian dan Pengabdian Tahun Jamak Lanjutan;<br />\r\n	b. Penelitian dan Pengabdian Tahun 2020 yang ditunda pelaksanaannya pada tahun 2021;<br />\r\n	c. Penelitian dan Pengabdian hasil seleksi penerimaan proposal (baru)</li>\r\n	<li>Skema penelitian dan pengabdian baru yang akan didanai pada 2021 adalah sebagai berikut:</li>\r\n</ol>\r\n\r\n<p>Skema Penelitian</p>\r\n\r\n<ol>\r\n	<li>Penelitian Dosen Pemula</li>\r\n	<li>Penelitian Disertasi Doktor</li>\r\n	<li>Penelitian Dasar</li>\r\n	<li>Penelitian Terapan</li>\r\n	<li>Penelitian Dasar Unggulan Perguruan Tinggi</li>\r\n	<li>Penelitian Terapan Unggulan Perguruan Tinggi</li>\r\n</ol>\r\n\r\n<p>Skema Pengabdian</p>\r\n\r\n<ol>\r\n	<li>Program Kemitraan Masyarakat</li>\r\n	<li>Program Kemitraan Masyarakat Stimulus</li>\r\n</ol>\r\n\r\n<p>Para peneliti agar melakukan update profile peneliti di Simlitabmas mulai tanggal&nbsp;<strong>9 Oktober 2020</strong>&nbsp;(panduan terlampir).</p>\r\n\r\n<p>Atas perhatiannya kami ucapkan terima kasih.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Salam<br />\r\nLPPM</p>\r\n', 'PPM DIKTI', NULL, 'Unit LPPM', '2020-08-24 11:27:02', NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_pengumuman` VALUES (3, 'Pengumuman Pelaksanaan Insentif Peningkatan Kualitas Jurnal Ilmiah Tahun 2020', '<p>Kepada<br />\r\nYth. Bapak /Ibu Dosen</p>\r\n\r\n<p>Deputi Bidang Penguatan Riset dan Pengembangan, Direktorat Pengelolaan Kekayaan Intelektual Tahun 2020 akan melaksanakan program Insentif Peningkatan Kualitas Jurnal Ilmiah Tahun 2020. Program ini bertujuan untuk mendorong jurnal ilmiah yang terbit di Indonesia untuk meningkatkan kualitas dan peringkat di pengindeksinternasional bereputasi dan mendorong jurnal terakreditasi nasional untuk meningkatkan kualitasnya sehingga dapat terindeks di pengindeks internasional bereputasi.</p>\r\n\r\n<p>Sehubungan dengan hal tersebut kami menginformasikan kepada Saudara terkait jadwal penting pelaksanaan program dimaksud diatassebagai berikut :</p>\r\n\r\n<ul>\r\n	<li>Pengumuman Program Insentif Peningkatan Kualitas Jurnal Ilmiah :&nbsp;<strong>1 Oktober 2020</strong></li>\r\n	<li>Sosialisasi Program Insentif Peningkatan Kualitas Jurnal Ilmiah :&nbsp;<strong>5-9 Oktober 2020</strong></li>\r\n	<li>Pengumpulan Proposal :&nbsp;<strong>5-16 Oktober 2020</strong></li>\r\n	<li>Seleksi Jurnal Penerima Insentif :&nbsp;<strong>19-26 Oktober 2020</strong></li>\r\n	<li>Pengumuman Jurnal Penerima Insentif :&nbsp;<strong>27 Oktober 2020</strong></li>\r\n</ul>\r\n\r\n<p>Panduan pelaksanaan dan pendaftaran usulan insentif peningkatan kualitas jurnal ilmiah tahun 2020 dapat dilakukan melalui laman&nbsp;<a href=\"https://insentifki.ristekbrin.go.id/\">https://insentifki.ristekbrin.go.id</a>&nbsp;(sesuai jadwal yang telah ditetapkan). Informasi lebih lanjut dapat menghubungi Sdr. Qhasim melalui HP 082242427782.</p>\r\n\r\n<p>Atas perhatiannya kami ucapkan terima kasih.</p>\r\n', 'Jurnal Ilmiah', NULL, 'Unit LPPM', '2020-11-10 23:00:03', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for tb_rst_rf_prop
-- ----------------------------
DROP TABLE IF EXISTS `tb_rst_rf_prop`;
CREATE TABLE `tb_rst_rf_prop`  (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `prop` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `type` enum('text','file','db_ref','number','date','daterange','option','option_multi','suboption','textarea','checkbox','radio','multiselect') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `tags` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `configs` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `isAktif` enum('YES','NO') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'YES',
  `Created_App` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Created_By` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Created_Date` datetime(0) NULL DEFAULT NULL,
  `Modified_App` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Modified_By` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Modified_Date` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 78 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_rst_rf_prop
-- ----------------------------
INSERT INTO `tb_rst_rf_prop` VALUES (1, 'Judul Kegiatan', 'text', NULL, '{\"form_option\":{\"placeholder\": \"Judul Kegiatan\"}}', 'YES', 'db', 'adrian', NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (2, 'Jenis Penelitian', 'option', '[jenis-penelitian]', '{\"form_option\":{\"id\":\"jenis_pen\"}}', 'YES', 'db', 'adrian', NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (3, 'Waktu Pelaksanaan', 'daterange', NULL, '{\"form_option\":{\"placeholder\":\"\",\"id\":\"waktu_pelaksanaan\"}}', 'YES', 'db', 'adrian', NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (4, 'Bidang Penelitian', 'option', '[bidang-penelitian]', '{\"form_option\":{\"id\":\"bidpen\"}}', 'YES', 'db', 'adrian', NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (5, 'Subbidang Penelitian', 'suboption', '[subbidang-penelitian]', NULL, 'YES', 'db', 'adrian', NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (6, 'Tujuan Sosial Ekonomi', 'option', '[tujuan-sosial]', '{\"form_option\":{\"id\":\"tujuan_sosnom\"}}', 'YES', 'db', 'adrian', NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (7, 'Subtujuan Sosial Ekonomi', 'suboption', '[subtujuan-sosial]', NULL, 'YES', 'db', 'adrian', NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (8, 'Sumber Dana', 'option', '[sumber-dana]', '{\"form_option\":{\"id\":\"sumber_dana\"}}', 'YES', 'db', 'adrian', NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (9, 'Institusi Sumber Dana', 'option', '[institusi-sumber-dana]', '{\"form_option\":{\"id\":\"ins_sumber_dana\"}}', 'YES', 'db', 'adrian', NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (10, 'Jumlah Dana', 'number', NULL, '{\"form_option\":{\"placeholder\": \"Jumlah Dana\"}}', 'YES', 'db', 'adrian', NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (11, 'Jenis Kegiatan', 'option', '[jenis-kegiatan]', '{\"form_option\":{\"id\":\"jenis_kegiatan\"}}', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (12, 'Penyelenggaraan', 'option', '[penyelenggaraan]', '{\"form_option\":{\"id\":\"penyelenggaraan\"}}', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (13, 'Sumber Dana', 'option', '[sumber-dana-pg]', '{\"form_option\":{\"id\":\"sumber_dana\"}}', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (14, 'Sumber Daya Iptek', 'option', '[sumber-daya-iptek]', '{\"form_option\":{\"id\":\"sumber_daya_iptek\"}}', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (15, 'NIP', 'text', NULL, '{\"form_option\":{\"id\":\"nip\",\"placeholder\": \"NIP\"}}', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (16, 'Nama Dosen', 'text', NULL, '{\"form_option\":{\"id\":\"nama_dosen\",\"readonly\":\"on\",\"style\":\"background-color: #eaeaea\"}}', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (17, 'Program Studi', 'text', NULL, '{\"form_option\":{\"id\":\"prodi_dosen\",\"readonly\":\"on\",\"style\":\"background-color: #eaeaea\"}}', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (18, 'Status', 'option', '[status-anggota]', '{\"form_option\":{\"id\":\"stts_anggota\"}}', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (19, 'NRP', 'text', NULL, '{\"form_option\":{\"id\":\"nrp\",\"placeholder\":\"NRP\"}}', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (20, 'Nama Mahasiswa', 'text', NULL, '{\"form_option\":{\"id\":\"nama_mhs\",\"readonly\":\"on\",\"style\":\"background-color: #eaeaea\"}}', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (21, 'Program Studi', 'text', NULL, '{\"form_option\":{\"id\":\"prodi_mhs\",\"readonly\":\"on\",\"style\":\"background-color: #eaeaea\"}}', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (22, 'Tingkat Jurnal', 'option', '[tingkat-jurnal]', NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (23, 'Judul', 'text', NULL, '{\"form_option\":{\"placeholder\":\"Judul\"}}', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (24, 'Nama Jurnal', 'text', NULL, '{\"form_option\":{\"placeholder\":\"Nama Jurnal\"}}', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (25, 'Volume', 'number', NULL, '{\"form_option\":{\"placeholder\":\"Volume\"}}', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (26, 'Nomor', 'number', NULL, '{\"form_option\":{\"placeholder\":\"Nomor\"}}', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (27, 'Tahun', 'number', NULL, '{\"form_option\":{\"placeholder\":\"Tahun\"}}', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (28, 'Halaman', 'text', NULL, '{\"form_option\":{\"placeholder\":\"Halaman\"}}', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (29, 'E-ISSN', 'text', NULL, '{\"form_option\":{\"placeholder\":\"E-ISSN\"}}', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (30, 'P-ISSN', 'text', NULL, '{\"form_option\":{\"placeholder\":\"P-ISSN\"}}', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (31, 'URL', 'text', NULL, '{\"form_option\":{\"placeholder\":\"URL\"}}', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (32, 'Status', 'option', '[status-pengajuan]', NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (33, 'Upload Bukti', 'file', NULL, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (34, 'Mata Kuliah', 'text', NULL, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (35, 'Bentuk Integrasi', 'text', NULL, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (36, 'No Pendaftaran', 'text', NULL, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (37, 'Judul HKI', 'text', NULL, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (38, 'Jenis HKI', 'option', '[jenis-hki]', NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (39, 'Status', 'option', '[status-hki]', NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (40, 'Jenis Luaran', 'text', NULL, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (41, 'Deskripsi', 'textarea', NULL, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (42, 'Nama Staf', 'text', NULL, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (43, 'Nama Alumni', 'text', NULL, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (44, 'Jenis Mitra', 'option', '[mitra]', NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (45, 'Nama Mitra', 'text', NULL, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (46, 'Bidang Usaha', 'text', NULL, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (47, 'Peningkatan Omzet', 'number', NULL, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (48, 'Dana Pendamping', 'number', NULL, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (49, 'Tanggal Publikasi', 'date', NULL, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (50, 'Jenis Media', 'option', '[jenis-media]', NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (51, 'Nama Media', 'text', NULL, '{\"form_option\":{\"class\":\"form-control\",\"name\":\"nama_media\"}}', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (52, 'Status Berita', 'checkbox', NULL, '{\"form_option\":{\"id\":\"checkboxDefault3\",\"value\":\"1\"},\"label\":\"Artikel / berita ditulis oleh penulis / pelaksana\"}', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (53, 'Penulis', 'text', NULL, '{\"form_option\":{\"class\":\"form-control\",\"name\":\"penulis\"}}', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (54, 'Tingkat Pemakalah', 'option', '[tingkat-pemakalah]', '{\"form_option\":{\"class\":\"form-control\",\"name\":\"tingkat\"}}', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (55, 'Nama Forum', 'text', NULL, '{\"form_option\":{\"class\":\"form-control\",\"name\":\"nama_forum\"}}', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (56, 'Penyelenggara', 'text', NULL, '{\"form_option\":{\"class\":\"form-control\",\"name\":\"penyelenggara\"}}', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (57, 'Tempat Pelaksanaan', 'text', NULL, '{\"form_option\":{\"class\":\"form-control\",\"name\":\"tempat_pelaksana\"}}', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (58, 'ISBN', 'text', NULL, '{\"form_option\":{\"class\":\"form-control\",\"name\":\"isbn\"}}', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (59, 'Jenis Luaran', 'option', '[luaran-iptek]', '{\"form_option\":{\"class\":\"form-control\",\"name\":\"jenis_luaran\"}}', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (60, 'Nama Luaran', 'text', NULL, '{\"form_option\":{\"class\":\"form-control\",\"name\":\"nama_luaran\"}}', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (61, 'Nama Produk', 'text', NULL, '{\"form_option\":{\"class\":\"form-control\",\"name\":\"nama_produk\"}}', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (62, 'Lembaga', 'text', NULL, '{\"form_option\":{\"class\":\"form-control\",\"name\":\"lembaga\"}}', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (63, 'No Sertifikat', 'text', NULL, '{\"form_option\":{\"class\":\"form-control\",\"name\":\"no_sertifikat\"}}', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (64, 'Nama Mitra', 'text', NULL, '{\"form_option\":{\"class\":\"form-control\",\"name\":\"nama_mitra\"}}', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (65, 'No Badan Hukum', 'text', NULL, '{\"form_option\":{\"class\":\"form-control\",\"name\":\"no_badan_hukum\"}}', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (66, 'Jenis Buku', 'text', NULL, '{\"form_option\":{\"class\":\"form-control\",\"name\":\"jenis_buku\"}}', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (67, 'Judul Buku', 'text', NULL, '{\"form_option\":{\"class\":\"form-control\",\"name\":\"judul_buku\"}}', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (68, 'Jumlah Halaman', 'number', NULL, '{\"form_option\":{\"class\":\"form-control\",\"name\":\"jumlah_halaman\"}}', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (69, 'Penerbit', 'text', NULL, '{\"form_option\":{\"class\":\"form-control\",\"name\":\"penerbit\"}}', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (70, 'Nama Wirausahawan', 'text', NULL, '{\"form_option\":{\"class\":\"form-control\",\"name\":\"wirausahawan\"}}', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (71, 'Jenis Publikasi', 'option', '[jenis-publikasi]', NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (72, 'Jenis Pengabdian', 'option', '[jenis-pengabdian]', '{\"form_option\":{\"id\":\"jenis_pen\"}}', 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (73, 'Skim Penelitian', 'suboption', '[skim-penelitian]', NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (74, 'Skim Pengabdian', 'suboption', '[skim-pengabdian]', NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_prop` VALUES (75, 'Waktu Pelaksanaan', 'date', NULL, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for tb_rst_rf_tahapan
-- ----------------------------
DROP TABLE IF EXISTS `tb_rst_rf_tahapan`;
CREATE TABLE `tb_rst_rf_tahapan`  (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `tahapan` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `config` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `tags` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `isAktif` enum('YES','NO') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'YES',
  `Created_App` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Created_By` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Created_Date` datetime(0) NULL DEFAULT NULL,
  `Modified_App` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Modified_By` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Modified_Date` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_rst_rf_tahapan
-- ----------------------------
INSERT INTO `tb_rst_rf_tahapan` VALUES (1, 'Usulan', '{\"label\":\"stts-usulan\"}', NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_tahapan` VALUES (2, 'On Going', '{\"label\":\"stts-ongoing\"}', NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_tahapan` VALUES (3, 'Selesai', '{\"label\":\"stts-selesai\"}', NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_tahapan` VALUES (4, 'Gagal', '{\"label\":\"stts-gagal\"}', NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for tb_rst_rf_tahapan_jenis
-- ----------------------------
DROP TABLE IF EXISTS `tb_rst_rf_tahapan_jenis`;
CREATE TABLE `tb_rst_rf_tahapan_jenis`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_riset` int(6) NULL DEFAULT NULL,
  `tahapan` int(6) NULL DEFAULT NULL,
  `urutan` int(11) NULL DEFAULT NULL,
  `config` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `tags` varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Created_App` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Created_By` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Created_Date` datetime(0) NULL DEFAULT NULL,
  `Modified_App` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Modified_By` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Modified_Date` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `jenis_riset`(`jenis_riset`) USING BTREE,
  INDEX `tahapan`(`tahapan`) USING BTREE,
  CONSTRAINT `tb_rst_rf_tahapan_jenis_ibfk_1` FOREIGN KEY (`jenis_riset`) REFERENCES `tb_rst_rf_jenis` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tb_rst_rf_tahapan_jenis_ibfk_2` FOREIGN KEY (`tahapan`) REFERENCES `tb_rst_rf_tahapan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_rst_rf_tahapan_jenis
-- ----------------------------
INSERT INTO `tb_rst_rf_tahapan_jenis` VALUES (1, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_tahapan_jenis` VALUES (2, 1, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_tahapan_jenis` VALUES (3, 1, 3, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_tahapan_jenis` VALUES (4, 1, 4, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_tahapan_jenis` VALUES (5, 2, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_tahapan_jenis` VALUES (6, 2, 2, 2, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_tahapan_jenis` VALUES (7, 2, 3, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_rf_tahapan_jenis` VALUES (8, 2, 4, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for tb_rst_tr_kontributor
-- ----------------------------
DROP TABLE IF EXISTS `tb_rst_tr_kontributor`;
CREATE TABLE `tb_rst_tr_kontributor`  (
  `riset` int(11) NULL DEFAULT NULL,
  `person` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `person_ref` enum('Pegawai','Mahasiswa','Staf','Alumni') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `jenis` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Created_App` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Created_By` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Created_Date` datetime(0) NULL DEFAULT NULL,
  `Modified_App` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Modified_By` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Modifeid_Date` datetime(0) NULL DEFAULT NULL,
  INDEX `sk_riset`(`riset`) USING BTREE,
  INDEX `jenis`(`jenis`) USING BTREE,
  CONSTRAINT `tb_rst_tr_kontributor_ibfk_1` FOREIGN KEY (`riset`) REFERENCES `tb_rst_tr_riset` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_rst_tr_kontributor
-- ----------------------------
INSERT INTO `tb_rst_tr_kontributor` VALUES (1, '010123', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (2, '010038', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (5, '010128', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (5, '010124', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (5, '010130', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (6, '010134', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (6, '040016', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (7, '010081', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (8, '010067', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (9, '010063', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (9, '010052', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (10, '010106', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (11, '010163', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (12, '010152', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (13, '010034', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (14, '010152', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (14, '010154', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (15, '010124', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (15, '010128', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (16, '010130', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (16, '010163', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (16, '161111044', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (16, '171131006', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (16, '171131005', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (14, '142111019', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (14, '152111007', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (14, '152111026', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (14, '152111027', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (14, '152111042', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (14, '152111056', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (15, '161111070', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (15, '152111037', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (17, '010152', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (17, '010154', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (18, '040086', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (18, '010153', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (18, '142111040', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (18, '142111053', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (18, '142111005', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (19, '010152', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (20, '010106', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (20, 'M. Zamroni', 'Staf', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (21, '010067', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (22, '010077', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (22, '010040', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (22, '010067', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (23, '010081', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (24, '010081', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (25, '010154', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (25, '151111081', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (25, '151111011', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (26, '040086', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (28, '010163', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (29, '010077', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (29, '010041', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (29, '151111119', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (29, '151111123', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (29, '161111070', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (30, '010096', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (30, '040016', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (30, '010134', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (30, '010152', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (30, '151131002', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (30, '171111112', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (30, '141111015', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (30, '141111077', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (30, '141111016', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (30, '141111003', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (30, 'Antok Nurwicaksono, A.Md', 'Staf', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (31, '010096', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (31, '010134', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (31, '151131002', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (32, '010096', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (32, '152111047', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (32, 'Siti Sunariyah, S.Pd', 'Staf', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (32, 'Isa Suarti, S.Kom', 'Staf', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (33, '010128', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (33, '010130', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (33, '161111070', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (34, '010134', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (34, '151111011', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (35, '010063', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (36, '010077', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (36, '111110411', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (37, '010041', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (37, '010077', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (38, '010050', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (38, '010034', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (38, '010081', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (38, '151111023', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (38, '151111045', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (38, '151111050', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (38, '151111123', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (38, '151111114', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (38, '151131002', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (38, '151122018', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (38, '131110679', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (39, '010106', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (39, '141111102', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (40, '010052', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (41, '010128', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (41, '161111070', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (42, '010124', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (42, '151111081', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (43, '010063', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (43, '010045', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (44, '010130', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (45, '010041', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (45, '010077', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (46, '010041', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (47, '010041', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (47, '010077', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (48, '010041', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (48, '010077', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (49, '010175', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (50, '010077', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (50, '010040', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (50, '010041', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (50, '161111070', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (50, '152111056', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (50, '161116041', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (50, '181221029', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (51, '010106', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (51, 'Ahmad Rianto, S.Kom', 'Staf', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (51, 'M. Syaifudin Sistiyanto, S.Kom', 'Staf', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (52, '010041', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (52, '010128', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (52, '141111003', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (52, '151111091', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (52, '161111060', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (52, '151111123', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (53, '010077', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (53, '010041', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (53, '161111070', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (54, '040086', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (55, '010077', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (55, '010128', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (55, '010124', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (55, '161111070', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (56, '040016', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (56, '010174', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (56, '010158', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (56, '161111010', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (56, '161111029', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (56, '161111071', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (57, '010106', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (57, '010050', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (57, '010034', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (57, '010163', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (57, '010152', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (57, '010175', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (57, '040016', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (57, '141111102', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (57, '151111119', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (59, '010134', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (59, '151131002', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (59, '171111112', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (59, '151111010', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (59, '151111058', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (59, '151111014', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (59, '151221010', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (59, '161111076', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (59, '161131008', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (59, '161221017', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (59, '161221018', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (59, '161221021', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (59, '171111023', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (59, '171131002', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (59, '171111020', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (60, '010077', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (61, '010052', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (61, '010034', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (61, '040016', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (61, '010081', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (61, '010163', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (61, '010078', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (61, '171111109', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (61, '161111068', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (62, '010096', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (63, '010096', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (64, '010134', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (64, '151111081', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (64, '151111011', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (65, '010163', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (65, '151111011', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (65, '151131014', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (66, '010152', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (68, '010063', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (68, '151131002', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (68, '141111077', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (68, '141111015', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (68, '141111016', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (68, '151111010', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (68, '172111025', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (68, '161221017', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (68, '171111112', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (69, '010163', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (70, '010134', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (71, '010134', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (72, '010124', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (73, '010050', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (74, '010067', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (74, '171111112', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (74, '141111015', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (74, 'Septa Noviana Y, S.Kom', 'Staf', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (75, '010096', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (75, '172111025', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (75, '151131002', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (75, '161221017', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (76, '010096', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (76, '151131002', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (76, '141111015', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (76, '141111016', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (76, '151111010', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (76, '161221017', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (76, '171111112', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (76, 'Antok Nurwicaksono, A.Md.Kom', 'Alumni', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (76, 'Siti Sunariyah, S.Pd', 'Staf', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (77, '010078', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (78, '010163', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (79, '010067', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (80, '010154', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (80, '010077', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (80, '010163', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (80, '010134', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (80, '010041', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (80, '010004', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (80, '010001', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (80, '010167', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (80, '010174', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (80, '010130', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (80, '010106', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (80, '010067', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (80, '010123', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (80, '010096', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (80, '010175', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (80, '040016', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (80, '040086', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (80, '010153', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (80, '010158', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (80, '162111033', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (80, '162111040', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (80, '162111075', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (80, '161111029', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (80, '151111045', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (85, '010041', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (85, '010077', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (86, '010050', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (86, '010134', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (86, '010077', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (87, '010045', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (87, '010063', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (88, '010128', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (88, '010124', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (89, '010154', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (89, '010152', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (89, '181111031', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (89, '181111015', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (90, '010154', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (90, '171111022', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (90, '151111007', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (90, '151111027', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (90, '151111048', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (90, '151111125', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (90, '151221019', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (91, '010134', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (91, '010123', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (92, '010123', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (92, '010134', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (93, '010096', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (93, '010134', 'Pegawai', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (94, '040077', 'Pegawai', 'Ketua', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_kontributor` VALUES (94, '171221016', 'Mahasiswa', 'Anggota', NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for tb_rst_tr_notifikasi
-- ----------------------------
DROP TABLE IF EXISTS `tb_rst_tr_notifikasi`;
CREATE TABLE `tb_rst_tr_notifikasi`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id_from` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `user_role_from` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `user_id_to` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `user_role_to` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `pesan` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `kategori_notifikasi` enum('usulan_pn','usulan_pg','hasil_usulan') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `referensi_id` int(11) NULL DEFAULT NULL,
  `status_lihat` enum('sudah','belum') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `Created_App` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Created_By` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Created_Date` datetime(0) NULL DEFAULT NULL,
  `Modified_App` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Modified_By` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Modified_Date` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `notif_ke_riset`(`referensi_id`) USING BTREE,
  CONSTRAINT `notif_ke_riset` FOREIGN KEY (`referensi_id`) REFERENCES `tb_rst_tr_riset` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_rst_tr_notifikasi
-- ----------------------------
INSERT INTO `tb_rst_tr_notifikasi` VALUES (16, '1', '1', '010124', '2', 'Selamat!, usulan Anda berhasil disetujui oleh LPPM', 'hasil_usulan', 104, 'belum', NULL, NULL, '2020-11-11 00:36:56', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for tb_rst_tr_periode
-- ----------------------------
DROP TABLE IF EXISTS `tb_rst_tr_periode`;
CREATE TABLE `tb_rst_tr_periode`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent` int(11) NULL DEFAULT NULL,
  `nama` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `tgl_mulai` datetime(0) NULL DEFAULT NULL,
  `tgl_selesai` datetime(0) NULL DEFAULT NULL,
  `urutan` int(5) NULL DEFAULT NULL,
  `tags` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `isAktif` enum('YES','NO') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'YES',
  `Created_App` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Created_By` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Created_Date` datetime(0) NULL DEFAULT NULL,
  `Modified_App` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Modified_By` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Modifeid_Date` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `parent`(`parent`) USING BTREE,
  CONSTRAINT `tb_rst_tr_periode_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `tb_rst_tr_periode` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_rst_tr_periode
-- ----------------------------
INSERT INTO `tb_rst_tr_periode` VALUES (1, NULL, '2016-2017 GANJIL', '2016-08-01 10:00:47', '2016-12-31 10:00:56', 100, NULL, 'NO', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_periode` VALUES (2, NULL, '2016-2017 GENAP', '2017-01-01 10:22:44', '2017-07-31 10:22:54', 200, NULL, 'NO', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_periode` VALUES (3, NULL, '2017-2018 GANJIL', '2017-08-01 10:00:47', '2017-12-31 10:00:56', 300, NULL, 'NO', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_periode` VALUES (4, NULL, '2017-2018 GENAP', '2018-01-01 10:22:44', '2018-07-31 10:22:54', 400, NULL, 'NO', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_periode` VALUES (5, NULL, '2018-2019 GANJIL', '2018-08-01 10:00:47', '2018-12-31 10:00:56', 500, NULL, 'NO', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_periode` VALUES (6, NULL, '2018-2019 GENAP', '2019-01-01 10:22:44', '2019-07-31 10:22:54', 600, NULL, 'NO', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_periode` VALUES (7, NULL, '2019-2020 GANJIL', '2019-08-01 00:00:00', '2020-01-31 00:00:00', 700, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_periode` VALUES (8, NULL, '2019-2020 GENAP', '2020-02-01 00:00:00', '2020-07-31 00:00:00', 800, NULL, 'YES', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_periode` VALUES (9, NULL, '2020-2021 GANJIL', '2020-08-01 00:00:00', '2021-01-31 00:00:00', 900, NULL, 'NO', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_periode` VALUES (11, NULL, '2015-2016 GENAP', '2016-02-01 00:00:00', '2016-07-31 00:00:00', 90, NULL, 'NO', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_periode` VALUES (12, NULL, '2015-2016 GANJIL', '2015-08-01 00:00:00', '2016-01-31 00:00:00', 80, NULL, 'NO', NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for tb_rst_tr_riset
-- ----------------------------
DROP TABLE IF EXISTS `tb_rst_tr_riset`;
CREATE TABLE `tb_rst_tr_riset`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `jenis` int(6) NULL DEFAULT NULL,
  `inisiator` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `person_ref` enum('dosen','mahasiswa') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `prodi` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `accepted` int(1) NULL DEFAULT NULL,
  `usulan` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `laporan_akhir` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Created_App` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Created_By` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Created_Date` datetime(0) NULL DEFAULT NULL,
  `Modified_App` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Modified_By` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Modified_Date` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `jenis`(`jenis`) USING BTREE,
  CONSTRAINT `tb_rst_tr_riset_ibfk_1` FOREIGN KEY (`jenis`) REFERENCES `tb_rst_rf_jenis` (`id`) ON DELETE RESTRICT ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 105 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_rst_tr_riset
-- ----------------------------
INSERT INTO `tb_rst_tr_riset` VALUES (1, 'Perancangan Animasi Perubahan Bentuk Tokoh Garuda di Indonesia', 1, NULL, 'dosen', 'DK-S1', 1, '42110725214575de8943656ba1a3394e.pdf', '1cca0f2bcfb6f07d1f19b15711285880.pdf', NULL, NULL, '2020-10-20 21:44:47', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (2, 'Analisa Citra Panas Menggunakan Metode Wavelet dan Statistika dalam Struktur ANN (Artificial Neural Network) pada Kanker Payudara (Tikus Model Kanker)', 1, NULL, 'dosen', 'TI-S1', 1, '6978f6289dd255189368a83bdba9f414.pdf', '6f27acfe1f4a5558da49ea286256a4df.pdf', NULL, NULL, '2020-10-22 10:38:04', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (5, 'Eksperimentasi Pembelajaran Matematika Diskrit Moda Daring pada Program Studi Teknik Informatika', 1, NULL, 'dosen', 'MI-D3,TI-S1', 1, '5d3956f3f6a3fb1e3c141fcfd7ae40d9.pdf', '66e42efac02316d0020e85142fb29b2f.pdf', NULL, NULL, '2020-10-22 19:35:54', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (6, 'Pengembangan Permainan Edukasi Menggunakan Logika Fuzzy untuk Anak Usia Dini', 1, NULL, 'dosen', 'SI-S1,TI-S1', 1, '57420c576fd2423459f1a885e855c56e.pdf', '9121f2518defe94d6b6954c1efb3eae6.pdf', NULL, NULL, '2020-10-22 19:48:43', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (7, 'Infografis Karakter Punokawan', 1, NULL, 'dosen', 'DK-S1', 1, NULL, NULL, NULL, NULL, '2020-10-22 19:58:48', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (8, 'Interaksi Manusia dan Komputer (Buku Ajar)', 1, NULL, 'dosen', 'SI-S1', 1, NULL, NULL, NULL, NULL, '2020-10-22 20:19:28', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (9, 'Implementasi Fuzzy Sugeno dalam Pemilihan Processor', 1, NULL, 'dosen', 'TI-S1', 1, NULL, '840817dd5ed286f44cc275273a71d219.pdf', NULL, NULL, '2020-10-22 20:28:13', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (10, 'Analisis Model Tata Kelola Sistem Informasi Perguruan Tinggi (Studi Kasus: STIKI Malang)', 1, NULL, 'dosen', 'SI-S1', 1, NULL, '86e05d2dd5281fb4bdc856749595f26a.pdf', NULL, NULL, '2020-10-23 10:12:42', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (11, 'Studi Kelayakan Implementasi Sistem Informasi Klinik Mata Mojoagung Menggunakan Information Economics', 1, NULL, 'dosen', 'TI-S1', 1, NULL, '05506c36c9eb4555921fd64fea981c90.pdf', NULL, NULL, '2020-10-23 10:15:38', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (12, 'Pembuatan Aplikasi Dashboard Strategic untuk Perencanaan Kapasitas Pembangkit Listrik yang Terintegrasi di Pulau Madura', 1, NULL, 'dosen', 'SI-S1', 1, NULL, '5374e6219aa28a9edfa2934821ba0903.pdf', NULL, NULL, '2020-10-23 10:33:12', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (13, 'Efektifitas Pemanfaatan Studio TA dalam Menunjang Penyelesaian Tugas Akhir Mahasiswa dengan Menggunakan Model Rasch', 1, NULL, 'dosen', 'MI-D3', 1, NULL, NULL, NULL, NULL, '2020-10-23 10:55:50', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (14, 'Perancangan Identitas Visual Kampung Wisata Keramik Dinoyo Malang', 1, NULL, 'dosen', 'DK-S1', 1, '14fffa3cf190ec88bc929c7eaa581ede.pdf', 'f6d07e4ec7aec48a5a32a828cb7adbbd.pdf', NULL, NULL, '2020-10-23 11:07:00', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (15, 'Pengembangan Modul Pembelajaran Aljabar Linier dan Matriks dengan Pendekatan Inkuiri untuk Mahasiswa Teknik Informatika', 1, NULL, 'dosen', 'TI-S1', 1, '0bc53914834678aad233cedcf7fa58ef.pdf', '266fa4dca623119f512039ea6110d8c8.pdf', NULL, NULL, '2020-10-23 13:25:11', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (16, 'Implementasi Metode North West Corner dan Stepping Stone Pengiriman Barang pada Jasa Ekspedisi', 1, NULL, 'dosen', 'MI-D3,TI-S1', 1, '1b95a4694f1bfcb70d7dc1f36e124d2e.pdf', '7a47caf947fafe55fede3c07dec546c1.pdf', NULL, NULL, '2020-10-23 23:04:02', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (17, 'Penyusunan Kerangka Konseptual Pengukuran Kualitas Sistem Informasi Akademik di Kampus STIKI Malang Berdasarkan Standard ISO 9126', 1, NULL, 'dosen', 'SI-S1', 1, 'd88428c50f5e49a7e4de10511efb0d85.pdf', '931c780c9bf1e8bad43a61a1e3886ace.pdf', NULL, NULL, '2020-10-23 23:23:52', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (18, 'Perancangan Media Promosi Kampung Gunung Wukir Kota Batu', 1, NULL, 'dosen', 'DK-S1', 1, 'ac86e3da72e3f95287b01655dd75a008.pdf', '392ff8bfcae59b480d3d4b8727e08803.pdf', NULL, NULL, '2020-10-23 23:34:45', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (19, 'Analisa Kebutuhan Perangkat Lunak untuk Pengembangan Aplikasi Pemetaan Distribusi UMKM (Usaha Mikro Kecil Menengah) Menggunakan Pendekatan Beriorentasi Objek', 1, NULL, 'dosen', 'SI-S1', 1, NULL, '457540b99126db0d13829abf20c3d738.pdf', NULL, NULL, '2020-10-23 23:43:21', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (20, 'Pengembangan Website Institusi Salesmaxx Academy / iClean', 2, NULL, 'dosen', 'SI-S1', 1, NULL, '76258417564a4c320b5761853e895f6a.pdf', NULL, NULL, '2020-10-23 23:51:19', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (21, 'Pelatihan Pembuatan Foto Produk untuk Meningkatkan Pemasaran pada Website UMKM Kota Malang', 2, NULL, 'dosen', 'SI-S1', 1, NULL, '16c74b58ce6bbb97b79c7de5a8c42486.pdf', NULL, NULL, '2020-10-24 00:32:59', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (22, 'Penguji UKK SMK Mamba\'ul Jadid Gondanglegi', 2, NULL, 'dosen', 'MI-D3,SI-S1,TI-S1', 1, NULL, 'e4f3d66a99b2663f9122c4207219b796.pdf', NULL, NULL, '2020-10-24 00:43:10', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (23, 'Penguji UKK SMK Negeri 2 Blitar', 2, NULL, 'dosen', 'DK-S1', 1, NULL, 'c8f0067275ba58672d19db551f0c527b.pdf', NULL, NULL, '2020-10-24 00:54:07', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (24, 'Ujian Kompetensi Keahlian di SMK Darul Huda Blitar', 2, NULL, 'dosen', 'DK-S1', 1, NULL, '849c297c1162468374a1b9b683175636.pdf', NULL, NULL, '2020-10-24 00:57:42', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (25, 'Workshop Akademik “How to Increase Website Ranking Using Search Engine Optimization”', 2, NULL, 'dosen', 'SI-S1', 1, NULL, 'edd1a04a92a14b822601ca6c55fe1607.pdf', NULL, NULL, '2020-10-24 01:10:23', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (26, 'Juri dan Pembimbing dalam Acara \"Seleksi Bina Kreatifitas Siswa 2018\"', 2, NULL, 'dosen', 'DK-S1', 1, NULL, '7a779e7a8b1a314e156ec9aa8e7504e1.pdf', NULL, NULL, '2020-10-24 01:18:00', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (27, 'Workshop akademik “Tips tips Penting Menjaga Kesehatan bagi Para Pengguna Komputer”', 2, NULL, 'dosen', 'TI-S1', 1, NULL, NULL, NULL, NULL, '2020-10-24 01:24:52', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (28, 'Bakti Sosial Katarak dan Pemeriksaan Mata terhadap Masyarakat yang Membutuhkan', 2, NULL, 'dosen', 'TI-S1', 1, NULL, 'b13c2383f48b435ee0d4b805c38082b9.pdf', NULL, NULL, '2020-10-24 01:29:02', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (29, 'Narasumber pada Acara \"Workshop Registrasi Web Sitasi Internasional\"', 2, NULL, 'dosen', 'TI-S1', 1, NULL, '662c6a5f01570c1e5c02b3535c0bd658.pdf', NULL, NULL, '2020-10-24 01:37:04', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (30, 'Pelatihan Internet Sehat dan Pemanfaatannya untuk Peningkatan Perekonomian Masyarakat', 2, NULL, 'dosen', 'MI-D3,SI-S1,TI-S1', 1, NULL, '97ab5ef0d39d303082b2e178a42b0fa7.pdf', NULL, NULL, '2020-10-24 01:46:24', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (31, 'Pendampingan Pelatihan Pembuatan SIMPEG dan Sosialisasi Penggunaan SIMPEG di BKPP Pulang Pisau', 2, NULL, 'dosen', 'MI-D3,SI-S1', 1, NULL, '4c0c7547d859d3a32fed78c021175706.pdf', NULL, NULL, '2020-10-24 09:44:03', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (32, 'Join Program - SMA Kristen Kalam Kudus (SKKK) Malang (Blender & Bahasa Korea Level 1)', 2, NULL, 'dosen', 'MI-D3', 1, NULL, 'eb9bce2832b843f8115af1d67054443e.pdf', NULL, NULL, '2020-10-24 09:54:31', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (33, 'Pembuatan Website SDN 3 Sukun Malang', 2, NULL, 'dosen', 'MI-D3,TI-S1', 1, NULL, '1dd8cb3c71b64ce6f789e1bfee8a7b30.pdf', NULL, NULL, '2020-10-24 10:02:14', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (34, 'Workshop akademik “Make Awesome Games Using Construct 3”', 2, NULL, 'dosen', 'SI-S1', 1, NULL, 'ba3148715b0111ef6779bb3c993be43e.pdf', NULL, NULL, '2020-10-24 10:05:53', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (35, 'Peningkatan Proses Pembelajaran Siswa Dalam Pemanfaatan Teknologi Informasi di SMAN 1 Sumberpucung Malang', 2, NULL, 'dosen', 'TI-S1', 1, NULL, 'b0da1078852283d1adbe38389ecf1a29.pdf', NULL, NULL, '2020-10-24 10:11:59', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (36, 'Sistem Informasi Pelayanan Pengaduan Pedagang Kaki Lima Berbasis Android (Studi Kasus Satpol PP Pemerintahan Kabupaten Malang)', 1, NULL, 'dosen', 'TI-S1', 1, NULL, '25833d0685ae0eab5da449c13a11f27b.pdf', NULL, NULL, '2020-10-24 10:27:09', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (37, 'Implementasi Teknologi web-RTC dan web-socket untuk Video-Conference dengan Fitur Push-to-Talk', 1, NULL, 'dosen', 'SI-S1', 1, NULL, '0b3f69254e51290af556ff74f7f5bbb8.pdf', NULL, NULL, '2020-10-24 10:44:17', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (38, 'Pembelajaran Berbasis Proyek di STIKI Malang', 1, NULL, 'dosen', 'DK-S1,MI-D3,SI-S1', 1, NULL, 'dad538b334d7f79923637b2c92a474b3.pdf', NULL, NULL, '2020-10-24 10:49:03', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (39, 'Analisis Technology Acceptance Model untuk Mengevaluasi Potensi Penerapan Sistem Presensi Perkuliahan Berbasis Wi-fi Direct', 1, NULL, 'dosen', 'SI-S1', 1, NULL, '6980f8ae89bd70456e38aba25c92ac7a.pdf', NULL, NULL, '2020-10-24 11:09:33', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (40, 'Analisa Kinerja Akademik Mahasiswa dengan Metode Backpropagation', 1, NULL, 'dosen', 'TI-S1', 1, NULL, NULL, NULL, NULL, '2020-10-24 11:13:14', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (41, 'Pengembangan dan Pelaksanaan Perkuliahan Hybris/Blended pada Mata Kuliah Aljabar Linier dan Matriks', 1, NULL, 'dosen', 'TI-S1', 1, NULL, '9ea8f7d590a0820f08bfb42e70ff1c2c.pdf', NULL, NULL, '2020-10-24 11:18:01', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (42, 'Sistem Pembelajaran Daring Mata Kuliah Metode Numerik pada Program Studi Teknik Informatika', 1, NULL, 'dosen', 'TI-S1', 1, NULL, '0876415942192eac788d55a0cde34b26.pdf', NULL, NULL, '2020-10-24 15:17:35', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (43, 'Penyelenggaraan Sistem Pembelajaran Daring (Blended) pada Mata Kuliah Pengolahan Citra Digital', 1, NULL, 'dosen', 'TI-S1', 1, NULL, 'fccd499831b20ebd4c15fb4246f04a31.pdf', NULL, NULL, '2020-10-24 15:21:26', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (44, 'Penyelenggaraan Pembelajaran Blended pada Mata Kuliah Riset Operasi di STIKI Malang', 1, NULL, 'dosen', 'MI-D3', 1, NULL, '91d8460c0160ccd245096e20671b8c33.pdf', NULL, NULL, '2020-10-24 15:26:30', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (45, 'Building Generation of Entrepreneurship to Compete in Facing the ASEAN Economic Communities', 1, NULL, 'dosen', 'TI-S1', 1, NULL, '6b479f78e6aa5f167a1aceb1fba5429c.pdf', NULL, NULL, '2020-10-24 15:36:44', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (46, 'Integration of Character Education in Developing Eglish Syllabus and Lesson Plan of Vocational Education', 1, NULL, 'dosen', 'SI-S1', 1, NULL, '4b3d86e3b7a3babf38145a9a2df1e6d9.pdf', NULL, NULL, '2020-10-24 15:42:07', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (47, 'The Effectiveness Of Batik Training For International Students', 1, NULL, 'dosen', 'TI-S1', 1, NULL, 'a1278cbfb3e3de6701c04875db0bc54b.pdf', NULL, NULL, '2020-10-24 15:46:02', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (48, 'Redesigning CHIML: Orchestration Language for Chimera-Framework', 1, NULL, 'dosen', 'TI-S1', 1, NULL, '9ba129c1c2524acc3aeaa2bca4a4072d.pdf', NULL, NULL, '2020-10-24 16:06:08', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (49, 'Sistem Pendukung Keputusan Kelompok Untuk Menentukan Penerima AGC Award Menggunakan Metode SAW dan Borda', 1, NULL, 'dosen', 'TI-S1', 1, NULL, '285f8afaa75fa79b1aa4a01b078d9fc0.pdf', NULL, NULL, '2020-10-24 16:09:55', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (50, 'Pengembangan Sistem Informasi e-SAMBAT', 2, NULL, 'dosen', 'TI-S1', 1, NULL, 'c0cbe308e49abb483e6ab4b29cdbc59e.pdf', NULL, NULL, '2020-10-24 16:37:41', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (51, 'Pengembangan Aplikasi Perijinan Online Pendirian Lembaga Kesejahteraan Sosial di Dinas Sosial Kota Malang', 2, NULL, 'dosen', 'SI-S1', 1, NULL, 'a58af969c4d899f25595676d4d9324a5.pdf', NULL, NULL, '2020-10-24 18:18:13', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (52, 'Seminar dan Workshop Optimasi Kuliah dengan Kerangka Sistem Pembelajaran Daring (SPADA)', 2, NULL, 'dosen', 'TI-S1', 1, NULL, '59f80c7decee26407404b72b04f8b492.pdf', NULL, NULL, '2020-10-24 18:24:14', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (53, 'Workshop Penulisan Karya Ilmiah dengan Mendeley', 2, NULL, 'dosen', 'TI-S1', 1, NULL, '93b19f828c8a491738bafff631f01db5.pdf', NULL, NULL, '2020-10-24 18:37:21', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (54, 'Juri pada Acara Lomba Mewarna Mamamia (Ibu dan Anak)', 2, NULL, 'dosen', 'DK-S1', 1, NULL, '21923272737065d1f3295a1cd583953d.pdf', NULL, NULL, '2020-10-24 18:44:51', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (55, 'Workshop Penyusunan Proposal Pengabdian kepada Masyarakat Secara Daring Melalui SIMLITABMAS', 2, NULL, 'dosen', 'TI-S1', 1, NULL, '3b3b95867c652ce7d121ba66c500487c.pdf', NULL, NULL, '2020-10-24 20:48:24', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (56, 'Pembuatan Web Madrasah dan Aplikasi Soal Berbasis Komputer Madrasah Ibtidaiyah Nahdlatul Ulama’', 2, NULL, 'dosen', 'DK-S1,SI-S1,TI-S1', 1, NULL, 'f1a5421a799e791719e0d098de378cb9.pdf', NULL, NULL, '2020-10-24 21:02:56', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (57, 'Penyusunan Rencana Induk TIK Kota Malang 2019 - 2023', 2, NULL, 'dosen', 'MI-D3,SI-S1,TI-S1', 1, NULL, 'a5478278be11ad5606182077003d0db4.pdf', NULL, NULL, '2020-10-24 21:19:05', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (59, 'Join Program Pembelajaran Bidang Teknologi Informasi dan Komputer (TIK) di SMA Negeri 1 Sumberpucung Kabupaten Malang', 2, NULL, 'dosen', 'SI-S1', 1, NULL, 'ada9c74d108f2ba9763e7fe8c09c2af3.pdf', NULL, NULL, '2020-10-24 21:43:44', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (60, 'Penguji UKK SMK Mamba\'ul Jadid Gondanglegi', 2, NULL, 'dosen', 'TI-S1', 1, NULL, '2ef1cd192717b76900fe1b5d6bc26e64.pdf', NULL, NULL, '2020-10-25 05:49:37', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (61, '2nd Batch SEA-TVET Student Internship Exchange Programme', 2, NULL, 'dosen', 'DK-S1,MI-D3,TI-S1', 1, NULL, '767de77fdee57833d63baa43b5be3e68.pdf', NULL, NULL, '2020-10-25 06:17:56', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (62, 'Pembinaan Keluarga Sadar Hukum (KADARKUM) di Kelurahan Merjosari Kecamatan Lowokwaru', 2, NULL, 'dosen', 'MI-D3', 1, NULL, '0f9b24f04de5b2dc1d9abcd29016e9f6.pdf', NULL, NULL, '2020-10-25 06:31:19', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (63, 'Pembinaan Keluarga Sadar Hukum (KADARKUM) di Kelurahan Pisang Candi Kecamatan Sukun', 2, NULL, 'dosen', 'MI-D3', 1, NULL, '957c5a8c37f94506e33988a7782c766b.pdf', NULL, NULL, '2020-10-25 06:39:47', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (64, 'Workshop “Chatbot and Chatcommerce using API.ai”', 2, NULL, 'dosen', 'SI-S1', 1, NULL, '7154b1abdb7add867154e3c891d50652.pdf', NULL, NULL, '2020-10-25 06:55:58', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (65, 'Workshop Manajemen Konflik dalam Dunia Pekerjaan', 2, NULL, 'dosen', 'TI-S1', 1, NULL, 'dd4ebd4b6fed0728cc7d92de67b96240.pdf', NULL, NULL, '2020-10-25 08:17:41', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (66, 'Pembinaan Keluarga Sadar Hukum (KADARKUM) di Kecamatan Klojen (Kelurahan Kiduldalem dan Kelurahan Kauman)', 2, NULL, 'dosen', 'SI-S1', 1, NULL, '8f1659a1a2585dba5d0969f2ad3b36b8.pdf', NULL, NULL, '2020-10-25 08:21:19', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (68, 'Kegiatan Join Program Peningkatan Keahlian Bidang TIK di SMAK Frateran Malang', 2, NULL, 'dosen', 'TI-S1', 1, NULL, 'b682e349030e3fc2018eea23f7dcd593.pdf', NULL, NULL, '2020-10-26 08:19:17', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (69, 'Tenaga Ahli di Polsek Klojen pada Tindak Pidana Informasi dan Transaksi Elektronik', 2, NULL, 'dosen', 'TI-S1', 1, NULL, '7a0a644587c4f019796322a7fdf38808.pdf', NULL, NULL, '2020-10-26 08:24:59', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (70, 'Pembinaan Keluarga Sadar Hukum (KADARKUM) di Kelurahan Sawojajar dan Kelurahan Buring', 2, NULL, 'dosen', 'SI-S1', 1, NULL, '07e783841fef9dc77d97ecd1a133294f.pdf', NULL, NULL, '2020-10-26 08:29:19', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (71, 'Pembinaan Keluarga Sadar Hukum (KADARKUM) di Kelurahan Kasin dan Kelurahan Rampal Celaket', 2, NULL, 'dosen', 'SI-S1', 1, NULL, 'bdd37c2f4f5be0206cba28f65bdd8b3f.pdf', NULL, NULL, '2020-10-26 08:32:39', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (72, 'Pembinaan Keluarga Sadar Hukum (KADARKUM) Kelurahan Pandanwangi dan Arjosari Kecamatan Blimbing', 2, NULL, 'dosen', 'TI-S1', 1, NULL, '2e9fe3d6eecca55fa7078004f49dcb36.pdf', NULL, NULL, '2020-10-26 08:36:45', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (73, 'Narasumber Rapat Kerja STIKES Kepanjen Tahun 2018', 2, NULL, 'dosen', 'SI-S1', 1, NULL, 'a60098372d3df168d5153317f631beac.pdf', NULL, NULL, '2020-10-26 08:40:00', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (74, 'Workshop Melek Digital dan Pemberian Bantuan Komputer dalam Semarak Dua Dasawarsa Malang bersama Bentoel', 2, NULL, 'dosen', 'SI-S1', 1, NULL, '3f9ce8f670254000b1981062ef5b58b2.pdf', NULL, NULL, '2020-10-26 08:43:36', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (75, 'Pelatihan Pembuatan Video Pembelajaran bagi Guru SMPN 4 Kepanjen', 2, NULL, 'dosen', 'MI-D3', 1, NULL, '79cbee9d2132f008e8c8483148e1e2c4.pdf', NULL, NULL, '2020-10-26 08:50:07', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (76, 'Join Program - SMA Kristen Kalam Kudus (Adobe Ilustrator dan Bahasa Korea Lv. 2)', 2, NULL, 'dosen', 'MI-D3', 1, NULL, 'd78dd8f9c7f8fe4ea6a24ecee644039f.pdf', NULL, NULL, '2020-10-26 09:00:42', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (77, 'Pelatihan “Tagline” Bahasa Inggris untuk Meningkatkan daya Jual', 2, NULL, 'dosen', 'TI-S1', 1, NULL, '7720ba010f85f582cd761401cd13737e.pdf', NULL, NULL, '2020-10-26 09:10:34', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (78, 'Pemateri pada Seminar Amal untuk Palu dan Donggala dengan Tema ”Menjadi Global Citizen”', 2, NULL, 'dosen', 'TI-S1', 1, NULL, 'a94af531dda0b3ff26613142bc44cc49.pdf', NULL, NULL, '2020-10-26 09:16:05', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (79, 'Penjurian Sosial Adventure and Product Marketing Langsep Chalenge 2018', 2, NULL, 'dosen', 'SI-S1', 1, NULL, '32771daac3245386f3342cd6b366694d.pdf', NULL, NULL, '2020-10-26 09:18:58', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (80, 'Pekan Ilmiah Mahasiswa & SSC VI ”Techno Creative”', 2, NULL, 'dosen', 'DK-S1,MI-D3,SI-S1,TI-S1', 1, NULL, 'd9f79e58efbb4441574a7afab89162ce.pdf', NULL, NULL, '2020-10-26 09:22:57', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (85, 'Prototipe ROIP dengan Menggunakan Web-RTC, Web-USB, dan Arduino Leonardo', 1, NULL, 'dosen', 'TI-S1', 1, NULL, 'e7504d63ae66cd4143b9584a5017fad3.pdf', NULL, NULL, '2020-10-29 21:35:40', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (86, 'Presepsi Siswa dalam Penggunaan Media Pembelajaran berbasis Augmented Reality', 1, NULL, 'dosen', 'SI-S1,TI-S1', 1, NULL, NULL, NULL, NULL, '2020-10-29 21:41:57', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (87, 'Seleksi Dosen dalam Proses Rekrutmen Menggunakan Fuzzy Sugeno', 1, NULL, 'dosen', 'TI-S1', 1, NULL, '1d2eac20d88f6a5b1f976c95aee78365.pdf', NULL, NULL, '2020-10-30 06:35:41', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (88, 'Model Pembelajaran Matematika Berbasis Blended Learning dalam menghadapi Era Revolusi Industri 4.0', 1, NULL, 'dosen', 'TI-S1', 1, NULL, NULL, NULL, NULL, '2020-10-30 06:40:34', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (89, 'Pemanfaatan RFID sebagai Alternatif Absensi Siswa (Studi Kasus: SMK ArRahmah Sukabumi, Jawa Barat)', 1, NULL, 'dosen', 'MI-D3,SI-S1', 1, NULL, 'edb9d3725c9e1af88b35919cc861f553.pdf', NULL, NULL, '2020-10-30 06:55:30', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (90, 'Implementasi Teknik SEO (Search Engine Optimization) untuk Meningkatkan Rank Universitas di Webometrics (studi kasus: STIKI MALANG)', 1, NULL, 'dosen', 'SI-S1', 1, NULL, NULL, NULL, NULL, '2020-10-30 07:02:40', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (91, 'Analisis Kebutuhan Mahasiswa dalam Mengikuti Perkuliahan Musik Digital', 1, NULL, 'dosen', 'DK-S1,SI-S1', 1, NULL, NULL, NULL, NULL, '2020-10-30 10:16:35', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (92, 'Analisis Penggunaan Digital Audio Workstation dalam Pembelajaran Musik Digital', 1, NULL, 'dosen', 'DK-S1,SI-S1', 1, NULL, NULL, NULL, NULL, '2020-10-30 10:27:43', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (93, 'Perancangan Sistem Informasi Kepegawaian pada BKPP Berbasis Website', 1, NULL, 'dosen', 'MI-D3,SI-S1', 1, NULL, NULL, NULL, NULL, '2020-10-30 10:57:01', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (94, 'Pengaruh HardSkill dan SoftSkill terhadap Self Efficacy Mahasiswa', 1, NULL, 'dosen', 'MI-D3', 1, NULL, 'ab000a2c685a32277b783b4a97dc0736.pdf', NULL, NULL, '2020-10-30 12:20:02', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (102, 'Implementasi Algoritma Naive Bayes untuk Memprediksi Lama Masa Studi dan Predikat Kelulusan Mahasiswa', 1, NULL, 'dosen', 'SI-S1,TI-S1', 1, NULL, NULL, NULL, NULL, '2020-11-05 10:50:37', NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset` VALUES (104, 'Korelasi Antara Matematika Dengan Kecerdasan Seseorang', 1, '010124', 'dosen', 'TI-S1', 1, '8f7aaf84b6bcf9d7c6232b3fa32ba674.pdf', NULL, NULL, NULL, '2020-11-10 23:27:48', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for tb_rst_tr_riset_dokumen
-- ----------------------------
DROP TABLE IF EXISTS `tb_rst_tr_riset_dokumen`;
CREATE TABLE `tb_rst_tr_riset_dokumen`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `riset` int(11) NULL DEFAULT NULL,
  `nama` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `file` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `Created_App` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Created_By` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Created_Date` datetime(0) NULL DEFAULT NULL,
  `Modified_App` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Modified_By` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Modified_Date` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `tb_rst_tr_riset_dokumen_ibfk_1`(`riset`) USING BTREE,
  CONSTRAINT `tb_rst_tr_riset_dokumen_ibfk_1` FOREIGN KEY (`riset`) REFERENCES `tb_rst_tr_riset` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of tb_rst_tr_riset_dokumen
-- ----------------------------
INSERT INTO `tb_rst_tr_riset_dokumen` VALUES (2, 14, 'Laporan Kemajuan', '050da8512bebd009eec17c1a3de4aba5.pdf', NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for tb_rst_tr_riset_luaran
-- ----------------------------
DROP TABLE IF EXISTS `tb_rst_tr_riset_luaran`;
CREATE TABLE `tb_rst_tr_riset_luaran`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `luaran` int(11) NULL DEFAULT NULL,
  `prop` int(6) NOT NULL,
  `value` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Created_App` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Created_By` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Created_Date` datetime(0) NULL DEFAULT NULL,
  `Modified_App` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Modified_By` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Modified_Date` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `luaran_to_luaran`(`luaran`) USING BTREE,
  INDEX `prop_to_luaran`(`prop`) USING BTREE,
  CONSTRAINT `prop_to_luaran` FOREIGN KEY (`prop`) REFERENCES `tb_rst_rf_prop` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 147 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_rst_tr_riset_luaran
-- ----------------------------
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (22, 1, 71, '266', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (23, 1, 22, '259', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (24, 1, 23, 'Perancangan Identitas Visual Kampung Keramik Dinoyo Malang', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (25, 1, 24, 'Jurnal Desain Komunikasi Visual Asia (JESKOVSIA)', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (26, 1, 25, '3', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (27, 1, 26, '2', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (28, 1, 27, '2019', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (29, 1, 28, '1-7', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (30, 1, 29, '2580-8397', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (31, 1, 30, '0852-730X', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (32, 1, 31, 'https://jurnal.stmikasia.ac.id/index.php/jeskovsia/index', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (33, 1, 32, '263', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (34, 1, 33, '6b92da1ce801f1e874a4efa036d0c4f9.pdf', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (35, 2, 34, 'Aljabar Linier dan Matriks', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (36, 2, 35, 'Modul', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (37, 2, 33, '963412e6f915f899aed4e7ff3ca98c28.pdf', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (38, 3, 59, '253', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (39, 3, 60, 'Produk Aplikasi Web Untuk Penilaian UKK', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (40, 3, 41, 'Aplikasi Simulasi Pembayaran PLN', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (41, 3, 33, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (42, 4, 59, '255', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (43, 4, 60, 'Design dan Dokumentasi Acara Bakti Sosial', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (44, 4, 41, 'Design dan dokumentasi acara bakti sosial yang diadakan oleh Klinik Mata Mojoagung di 2 tempat', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (45, 4, 33, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (46, 5, 59, '252', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (47, 5, 60, 'Profil Publikasi Peneliti', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (48, 5, 41, 'Profil Publikasi Peneliti (ID) di beberapa situs web sitasi Internasional', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (49, 5, 33, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (50, 6, 59, '255', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (51, 6, 60, 'Modul Pelatihan', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (52, 6, 41, 'Modul pelatihan memaparkan mengenai internet sehat', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (53, 6, 33, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (54, 7, 59, '251', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (55, 7, 60, 'Sistem Kepegawaian (SIMPEG)', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (56, 7, 41, 'SIMPEG merupakan aplikasi sistem kepegawaian online berbasis website yang berfungsi untuk mengelola data pegawai di seluruh unit kerja di lingkungan Kabupaten Pulang Pisau', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (57, 7, 33, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (58, 8, 59, '255', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (59, 8, 60, 'Modul Pembelajaran Blender dan Bahasa Korea Level 1', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (60, 8, 41, '', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (61, 8, 33, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (62, 9, 59, '255', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (63, 9, 60, 'Website Profil SDN 3 Sukun', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (64, 9, 41, '', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (65, 9, 33, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (66, 10, 59, '253', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (67, 10, 60, 'Sistem Informasi e-SAMBAT Kominfo Kota Malang', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (68, 10, 41, 'Web untuk pengaduan secara terbuka masyarakat malang terhadap kerja pemerintah kota.', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (69, 10, 33, '8660f0c693b9baf5ce71dac1a36189d8.pdf', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (70, 11, 59, '255', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (71, 11, 60, 'Media Pembelajaran', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (72, 11, 41, 'Luaran dari kegiatan\r\nworkshop ini adalah rancangan pembelajaran yang sesuai dengan sistem pembelajaran daring Indonesia (SPADA)', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (73, 11, 33, '1136796889b4406e5c19914cd3ca0cae.pdf', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (74, 12, 59, '252', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (75, 12, 60, 'Profil Publikasi Peneliti', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (76, 12, 41, 'Profil Publikasi Peneliti (ID) di Mendeley', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (77, 12, 33, '74d9bccc41e9abf05829085784203ffd.pdf', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (78, 13, 59, '255', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (79, 13, 60, 'Materi Workshop', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (80, 13, 41, 'Pedoman penyusuna proposal pengabdian kepada masyarakat secara daring dan jenis luaran pengabdian', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (81, 13, 33, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (82, 14, 59, '253', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (83, 14, 60, 'Penilaian Kompetensi Siswa peserta UKK', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (84, 14, 41, 'Penilaian berdasarkan kompetensi mata pelajaran Rekayasa Perangkat Lunak untuk Siswa SMK', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (85, 14, 33, 'a58dcb4641fdc6b3413b8bcbfdb149fc.pdf', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (86, 15, 49, '2018-09-25', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (87, 15, 23, 'Magang Internasional, STIKI Gandeng Perguruan Tinggi Filipina', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (88, 15, 50, '276', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (89, 15, 51, 'stiki.ac.id', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (90, 15, 25, '', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (91, 15, 26, '', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (92, 15, 28, '', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (93, 15, 31, 'https://www.stiki.ac.id/magang-internasional-stiki-gandeng-perguruan-tinggi-filipina/', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (94, 15, 53, '', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (95, 15, 33, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (96, 16, 59, '255', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (97, 16, 60, 'Sosialisasi Pembinaan Keluarga Sadar Hukum', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (98, 16, 41, 'Materi disajikan dalam bentuk PPT', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (99, 16, 33, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (100, 17, 59, '255', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (101, 17, 60, 'Materi Chatbot', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (102, 17, 41, 'Materi disajikan dalam bentuk ppt', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (103, 17, 33, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (104, 18, 49, '2018-08-08', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (105, 18, 23, 'Banyak Hoax, Media Mainstream Jadi Acuan', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (106, 18, 50, '276', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (107, 18, 51, 'Malang Post Online', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (108, 18, 25, '1', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (109, 18, 26, '', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (110, 18, 28, '1', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (111, 18, 31, 'https://malang-post.com/berita/kota-malang/banyak-hoax-media-mainstream-jadi-acuan', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (112, 18, 53, '', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (113, 18, 33, '589dd432079f4a4d9c45373850dcfbd9.pdf', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (114, 19, 59, '255', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (115, 19, 60, 'Modul Adobe Premier', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (116, 19, 41, 'Modul untuk membuat video pembelajaran bagi Guru secara sederhana', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (117, 19, 33, '005ca48c3a43c065a4ac93b246cd67a0.pdf', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (118, 20, 71, '266', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (119, 20, 22, '259', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (120, 20, 23, 'Metode Search Engine Optimization (SEO) Untuk Meningkatkan Ranking Webometrics pada Web STIKI Malang', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (121, 20, 24, 'Journal of Information System', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (122, 20, 25, '03', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (123, 20, 26, '02', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (124, 20, 27, '2019', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (125, 20, 28, '111-120', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (126, 20, 29, '2548-3587', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (127, 20, 30, '', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (128, 20, 31, 'http://ejournal-binainsani.ac.id/index.php/ISBI/article/view/1067/950', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (129, 20, 32, '263', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (130, 20, 33, 'b5dad169746de3ba4b364f2288e170db.pdf', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (131, 21, 34, 'Pemrograman Web Lanjut', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (132, 21, 35, 'Materi', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (133, 21, 33, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (134, 22, 71, '266', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (135, 22, 22, '259', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (136, 22, 23, 'Pengaruh Hardskill dan Softskill Terhadap Self Efficacy Mahasiswa', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (137, 22, 24, 'Jurnal PSIKOVIDYA', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (138, 22, 25, '24', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (139, 22, 26, '01', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (140, 22, 27, '2020', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (141, 22, 28, '1-7', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (142, 22, 29, '2502-6925', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (143, 22, 30, '0853-8050', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (144, 22, 31, 'http://psikovidya.wisnuwardhana.ac.id/index.php/psikovidya', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (145, 22, 32, '263', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_luaran` VALUES (146, 22, 33, 'c165baf4dcfc049366fa8b7c04855890.pdf', NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for tb_rst_tr_riset_mitra
-- ----------------------------
DROP TABLE IF EXISTS `tb_rst_tr_riset_mitra`;
CREATE TABLE `tb_rst_tr_riset_mitra`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `riset` int(11) NULL DEFAULT NULL,
  `jenis_mitra` int(11) NULL DEFAULT NULL,
  `nama_mitra` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `bidang_mitra` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `omzet` int(10) NULL DEFAULT NULL,
  `dana_pendamping` int(10) NULL DEFAULT NULL,
  `Created_App` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Created_By` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Created_Date` datetime(0) NULL DEFAULT NULL,
  `Modified_App` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Modified_By` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Modified_Date` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `riset`(`riset`) USING BTREE,
  CONSTRAINT `tb_rst_tr_riset_mitra_ibfk_1` FOREIGN KEY (`riset`) REFERENCES `tb_rst_tr_riset` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 38 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_rst_tr_riset_mitra
-- ----------------------------
INSERT INTO `tb_rst_tr_riset_mitra` VALUES (2, 20, 234, 'Salesmaxx', 'Pemda', 0, 2000000, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_mitra` VALUES (3, 22, 234, 'SMK Mamba’ul Jadid Gondanglegi  Kab. Malang', 'Pendidikan', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_mitra` VALUES (4, 23, 234, 'SMK Negeri 2 Blitar', 'Pendidikan', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_mitra` VALUES (5, 24, 234, 'SMK Darul Huda', 'Pendidikan', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_mitra` VALUES (6, 26, 234, 'SDN Purwodadi I', 'Pendidikan', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_mitra` VALUES (7, 28, 235, 'Klinik Mata Mojoagung', 'Kesehatan Mata', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_mitra` VALUES (8, 29, 231, 'STIKI Malang', 'Pendidikan', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_mitra` VALUES (9, 30, 234, 'Diskominfo Kota Malang', 'Komunikasi dan Informasi', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_mitra` VALUES (10, 31, 234, 'BKPP Pulang Pisau', 'Pemerintah Bagian Kepegawaian', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_mitra` VALUES (11, 32, 231, 'SMAK Kalam Kudus Malang', 'Pendidikan', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_mitra` VALUES (12, 33, 231, 'SDN 3 Sukun Malang', 'Pendidikan', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_mitra` VALUES (13, 35, 231, 'SMAN 1 Sumberpucung Malang', 'Pendidikan', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_mitra` VALUES (14, 50, 234, 'Dinas Komunikasi dan Informatika Kota Malang', 'Komunikasi dan Informatika', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_mitra` VALUES (15, 51, 234, 'Dinas Sosial Kota Malang', 'Pemerintahan', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_mitra` VALUES (16, 52, 231, 'Dosen Perguruan Tinggi', '', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_mitra` VALUES (17, 53, 231, 'STIKI Malang', 'Pendidikan', 0, 300000, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_mitra` VALUES (18, 54, 233, 'JabMart', 'Dagang', 0, 3000000, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_mitra` VALUES (19, 56, 234, 'MINU Polowijen Kota Malang', 'Pendidikan', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_mitra` VALUES (20, 57, 234, 'Pemerintah Kota Malang', 'Pemerintahan', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_mitra` VALUES (22, 59, 234, 'SMAN 1 Sumberpucung', 'Pendidikan', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_mitra` VALUES (23, 60, 234, 'SMK Mamba\'ul Jadid Gondanglegi', 'Pendiidkan', 0, 1920000, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_mitra` VALUES (24, 61, 235, 'Kapanlagi Youniverse', 'Media Online', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_mitra` VALUES (25, 62, 234, 'Kelurahan Merjosari Kecamatan Lowokwaru', '', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_mitra` VALUES (26, 63, 234, 'Kelurahan Pisang Candi Kecamatan Sukun', '', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_mitra` VALUES (27, 64, 231, 'STIKI Malang', 'Pendidikan', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_mitra` VALUES (28, 66, 234, 'Kelurahan Kauman dan Kelurahan Kiduldalem', 'Pemerintahan', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_mitra` VALUES (29, 68, 231, 'SMAK Frateran Malang', 'Pendidikan', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_mitra` VALUES (30, 69, 234, 'Polisi Sektor Klojen', '', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_mitra` VALUES (31, 70, 234, 'Kelurahan Sawojajar dan Buring', '', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_mitra` VALUES (32, 71, 234, 'Kelurahan Kasin dan Kelurahan Rampal Celaket', '', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_mitra` VALUES (33, 72, 234, 'Kelurahan Pandanwangi dan Arjosari', '', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_mitra` VALUES (34, 73, 231, 'STIKES Kepanjen', 'Pendidikan', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_mitra` VALUES (35, 75, 231, 'SMPN 4 Kepanjen', 'Pendidikan', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_mitra` VALUES (36, 76, 231, 'SMAKr Kalam Kudus Malang', 'Pendidikan', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_mitra` VALUES (37, 77, 232, 'Koperasi Serba Usaha Amangtiwi', 'Koperasi', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for tb_rst_tr_riset_props
-- ----------------------------
DROP TABLE IF EXISTS `tb_rst_tr_riset_props`;
CREATE TABLE `tb_rst_tr_riset_props`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `riset` int(11) NOT NULL,
  `prop` int(6) NOT NULL,
  `value` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Created_App` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Created_By` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Created_Date` datetime(0) NULL DEFAULT NULL,
  `Modified_App` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Modified_By` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Modified_Date` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `riset`(`riset`) USING BTREE,
  INDEX `prop`(`prop`) USING BTREE,
  CONSTRAINT `tb_rst_tr_riset_props_ibfk_1` FOREIGN KEY (`riset`) REFERENCES `tb_rst_tr_riset` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tb_rst_tr_riset_props_ibfk_2` FOREIGN KEY (`prop`) REFERENCES `tb_rst_rf_prop` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 946 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_rst_tr_riset_props
-- ----------------------------
INSERT INTO `tb_rst_tr_riset_props` VALUES (1, 1, 1, 'Perancangan Animasi Perubahan Bentuk Tokoh Garuda di Indonesia', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (2, 1, 2, '2', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (3, 1, 3, '03/27/2018 - 11/16/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (4, 1, 4, '9', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (5, 1, 5, '51', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (6, 1, 6, '71', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (7, 1, 7, '211', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (8, 1, 8, '215', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (9, 1, 9, '217', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (10, 1, 10, '2500000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (11, 2, 1, 'Analisa Citra Panas Menggunakan Metode Wavelet dan Statistika dalam Struktur ANN (Artificial Neural Network) pada Kanker Payudara (Tikus Model Kanker)', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (12, 2, 2, '2', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (13, 2, 73, '285', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (14, 2, 3, '03/20/0018 - 11/16/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (15, 2, 4, '4', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (16, 2, 5, '15', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (17, 2, 6, '65', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (18, 2, 7, '156', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (19, 2, 8, '215', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (20, 2, 9, '217', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (21, 2, 10, '51500000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (38, 5, 1, 'Eksperimentasi Pembelajaran Matematika Diskrit Moda Daring pada Program Studi Teknik Informatika', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (39, 5, 2, '2', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (40, 5, 73, '286', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (41, 5, 3, '03/27/2018 - 11/16/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (42, 5, 4, '4', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (43, 5, 5, '10', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (44, 5, 6, '66', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (45, 5, 7, '162', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (46, 5, 8, '215', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (47, 5, 9, '217', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (48, 5, 10, '14000000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (49, 6, 1, 'Pengembangan Permainan Edukasi Menggunakan Logika Fuzzy untuk Anak Usia Dini', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (50, 6, 2, '2', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (51, 6, 73, '286', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (52, 6, 3, '03/27/2018 - 11/16/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (53, 6, 4, '4', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (54, 6, 5, '15', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (55, 6, 6, '66', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (56, 6, 7, '157', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (57, 6, 8, '215', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (58, 6, 9, '217', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (59, 6, 10, '17000000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (60, 7, 1, 'Infografis Karakter Punokawan', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (61, 7, 2, '1', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (62, 7, 3, '07/01/2017 - 07/31/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (63, 7, 4, '9', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (64, 7, 5, '51', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (65, 7, 6, '71', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (66, 7, 7, '211', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (67, 7, 8, '215', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (68, 7, 9, '298', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (69, 7, 10, '0', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (70, 8, 1, 'Interaksi Manusia dan Komputer (Buku Ajar)', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (71, 8, 2, '1', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (72, 8, 3, '01/01/2018 - 01/31/2019', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (73, 8, 4, '4', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (74, 8, 5, '15', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (75, 8, 6, '66', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (76, 8, 7, '160', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (77, 8, 8, '215', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (78, 8, 9, '298', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (79, 8, 10, '0', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (80, 9, 1, 'Implementasi Fuzzy Sugeno dalam Pemilihan Processor', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (81, 9, 2, '1', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (82, 9, 3, '02/01/2018 - 12/31/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (83, 9, 4, '4', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (84, 9, 5, '15', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (85, 9, 6, '71', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (86, 9, 7, '206', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (87, 9, 8, '215', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (88, 9, 9, '298', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (89, 9, 10, '1000000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (90, 10, 1, 'Analisis Model Tata Kelola Sistem Informasi Perguruan Tinggi (Studi Kasus: STIKI Malang)', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (91, 10, 2, '1', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (92, 10, 3, '01/01/2018 - 12/31/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (93, 10, 4, '4', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (94, 10, 5, '15', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (95, 10, 6, '61', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (96, 10, 7, '132', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (97, 10, 8, '215', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (98, 10, 9, '298', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (99, 10, 10, '0', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (100, 11, 1, 'Studi Kelayakan Implementasi Sistem Informasi Klinik Mata Mojoagung Menggunakan Information Economics', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (101, 11, 2, '1', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (102, 11, 3, '05/01/2018 - 07/31/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (103, 11, 4, '4', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (104, 11, 5, '15', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (105, 11, 6, '65', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (106, 11, 7, '156', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (107, 11, 8, '215', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (108, 11, 9, '298', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (109, 11, 10, '810000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (110, 12, 1, 'Pembuatan Aplikasi Dashboard Strategic untuk Perencanaan Kapasitas Pembangkit Listrik yang Terintegrasi di Pulau Madura', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (111, 12, 2, '1', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (112, 12, 3, '01/01/2018 - 06/30/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (113, 12, 4, '4', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (114, 12, 5, '15', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (115, 12, 6, '57', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (116, 12, 7, '101', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (117, 12, 8, '215', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (118, 12, 9, '298', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (119, 12, 10, '2500000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (120, 13, 1, 'Efektifitas Pemanfaatan Studio TA dalam Menunjang Penyelesaian Tugas Akhir Mahasiswa dengan Menggunakan Model Rasch', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (121, 13, 2, '1', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (122, 13, 3, '08/01/2018 - 07/31/2019', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (123, 13, 4, '4', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (124, 13, 5, '15', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (125, 13, 6, '61', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (126, 13, 7, '132', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (127, 13, 8, '215', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (128, 13, 9, '298', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (129, 13, 10, '0', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (130, 14, 1, 'Perancangan Identitas Visual Kampung Wisata Keramik Dinoyo Malang', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (131, 14, 2, '3', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (132, 14, 3, '07/16/2018 - 12/31/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (133, 14, 4, '9', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (134, 14, 5, '51', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (135, 14, 6, '62', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (136, 14, 7, '139', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (137, 14, 8, '215', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (138, 14, 9, '299', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (139, 14, 10, '2500000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (140, 15, 1, 'Pengembangan Modul Pembelajaran Aljabar Linier dan Matriks dengan Pendekatan Inkuiri untuk Mahasiswa Teknik Informatika', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (141, 15, 2, '3', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (142, 15, 3, '07/16/2018 - 12/31/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (143, 15, 4, '4', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (144, 15, 5, '10', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (145, 15, 6, '71', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (146, 15, 7, '201', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (147, 15, 8, '215', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (148, 15, 9, '299', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (149, 15, 10, '2500000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (150, 16, 1, 'Implementasi Metode North West Corner dan Stepping Stone Pengiriman Barang pada Jasa Ekspedisi', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (151, 16, 2, '3', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (152, 16, 3, '07/16/2018 - 12/31/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (153, 16, 4, '4', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (154, 16, 5, '15', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (155, 16, 6, '62', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (156, 16, 7, '137', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (157, 16, 8, '215', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (158, 16, 9, '299', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (159, 16, 10, '2500000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (160, 17, 1, 'Penyusunan Kerangka Konseptual Pengukuran Kualitas Sistem Informasi Akademik di Kampus STIKI Malang Berdasarkan Standard ISO 9126', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (161, 17, 2, '3', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (162, 17, 3, '07/16/2018 - 12/31/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (163, 17, 4, '4', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (164, 17, 5, '15', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (165, 17, 6, '66', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (166, 17, 7, '165', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (167, 17, 8, '215', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (168, 17, 9, '299', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (169, 17, 10, '2500000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (170, 18, 1, 'Perancangan Media Promosi Kampung Gunung Wukir Kota Batu', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (171, 18, 2, '3', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (172, 18, 3, '07/16/2018 - 12/31/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (173, 18, 4, '9', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (174, 18, 5, '51', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (175, 18, 6, '62', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (176, 18, 7, '139', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (177, 18, 8, '215', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (178, 18, 9, '299', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (179, 18, 10, '2500000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (180, 19, 1, 'Analisa Kebutuhan Perangkat Lunak untuk Pengembangan Aplikasi Pemetaan Distribusi UMKM (Usaha Mikro Kecil Menengah) Menggunakan Pendekatan Beriorentasi Objek', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (181, 19, 2, '1', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (182, 19, 3, '07/01/2018 - 12/31/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (183, 19, 4, '4', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (184, 19, 5, '15', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (185, 19, 6, '62', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (186, 19, 7, '137', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (187, 19, 8, '215', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (188, 19, 9, '298', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (189, 19, 10, '2500000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (190, 20, 72, '268', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (191, 20, 11, '221', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (192, 20, 12, '223', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (193, 20, 1, 'Pengembangan Website Institusi Salesmaxx Academy / iClean', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (194, 20, 10, '2000000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (195, 20, 13, '296', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (196, 20, 3, '01/01/2018 - 06/30/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (197, 20, 14, '255', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (198, 21, 72, '268', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (199, 21, 11, '222', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (200, 21, 12, '223', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (201, 21, 1, 'Pelatihan Pembuatan Foto Produk untuk Meningkatkan Pemasaran pada Website UMKM Kota Malang', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (202, 21, 10, '247000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (203, 21, 13, '296', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (204, 21, 3, '05/11/2018 - 05/11/2018 ', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (205, 21, 14, '254', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (206, 22, 72, '268', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (207, 22, 11, '222', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (208, 22, 12, '223', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (209, 22, 1, 'Penguji UKK SMK Mamba\'ul Jadid Gondanglegi', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (210, 22, 10, '1970000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (211, 22, 13, '229', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (212, 22, 3, '02/26/2018 - 03/01/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (213, 22, 14, '253', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (214, 23, 72, '268', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (215, 23, 11, '222', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (216, 23, 12, '223', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (217, 23, 1, 'Penguji UKK SMK Negeri 2 Blitar', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (218, 23, 10, '0', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (219, 23, 13, '229', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (220, 23, 3, '02/21/2018 - 02/22/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (221, 23, 14, '255', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (222, 24, 72, '268', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (223, 24, 11, '222', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (224, 24, 12, '223', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (225, 24, 1, 'Ujian Kompetensi Keahlian di SMK Darul Huda Blitar', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (226, 24, 10, '0', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (227, 24, 13, '229', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (228, 24, 3, '03/03/2018 - 03/04/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (229, 24, 14, '255', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (230, 25, 72, '268', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (231, 25, 11, '222', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (232, 25, 12, '223', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (233, 25, 1, 'Workshop Akademik “How to Increase Website Ranking Using Search Engine Optimization”', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (234, 25, 10, '472000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (235, 25, 13, '226', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (236, 25, 3, '03/16/2018 - 03/16/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (237, 25, 14, '255', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (238, 26, 72, '268', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (239, 26, 11, '222', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (240, 26, 12, '223', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (241, 26, 1, 'Juri dan Pembimbing dalam Acara \"Seleksi Bina Kreatifitas Siswa 2018\"', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (242, 26, 10, '0', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (243, 26, 13, '229', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (244, 26, 3, '03/03/2018 - 03/03/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (245, 26, 14, '255', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (246, 27, 72, '268', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (247, 27, 11, '222', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (248, 27, 12, '223', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (249, 27, 1, 'Workshop akademik “Tips tips Penting Menjaga Kesehatan bagi Para Pengguna Komputer”', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (250, 27, 10, '0', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (251, 27, 13, '226', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (252, 27, 3, '04/06/2018 - 04/06/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (253, 27, 14, '255', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (254, 28, 72, '268', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (255, 28, 11, '221', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (256, 28, 12, '223', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (257, 28, 1, 'Bakti Sosial Katarak dan Pemeriksaan Mata terhadap Masyarakat yang Membutuhkan', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (258, 28, 10, '1000000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (259, 28, 13, '228', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (260, 28, 3, '07/01/2018 - 08/31/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (261, 28, 14, '255', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (262, 29, 72, '268', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (263, 29, 11, '222', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (264, 29, 12, '223', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (265, 29, 1, 'Narasumber pada Acara \"Workshop Registrasi Web Sitasi Internasional\"', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (266, 29, 10, '300000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (267, 29, 13, '226', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (268, 29, 3, '06/29/2018 - 06/29/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (269, 29, 14, '252', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (270, 30, 72, '268', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (271, 30, 11, '222', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (272, 30, 12, '223', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (273, 30, 1, 'Pelatihan Internet Sehat dan Pemanfaatannya untuk Peningkatan Perekonomian Masyarakat', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (274, 30, 10, '9500000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (275, 30, 13, '227', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (276, 30, 3, '07/10/2018 - 07/11/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (277, 30, 14, '255', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (278, 31, 72, '269', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (279, 31, 11, '221', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (280, 31, 12, '224', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (281, 31, 1, 'Pendampingan Pelatihan Pembuatan SIMPEG dan Sosialisasi Penggunaan SIMPEG di BKPP Pulang Pisau', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (282, 31, 10, '42987500', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (283, 31, 13, '227', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (284, 31, 3, '02/01/2018 - 07/31/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (285, 31, 14, '251', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (286, 32, 72, '268', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (287, 32, 11, '221', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (288, 32, 12, '223', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (289, 32, 1, 'Join Program - SMA Kristen Kalam Kudus (SKKK) Malang (Blender & Bahasa Korea Level 1)', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (290, 32, 10, '19500000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (291, 32, 13, '229', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (292, 32, 3, '02/08/2018 - 05/11/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (293, 32, 14, '255', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (294, 33, 72, '268', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (295, 33, 11, '221', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (296, 33, 12, '223', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (297, 33, 1, 'Pembuatan Website SDN 3 Sukun Malang', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (298, 33, 10, '216680', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (299, 33, 13, '296', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (300, 33, 3, '02/01/2018 - 07/31/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (301, 33, 14, '255', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (302, 34, 72, '268', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (303, 34, 11, '222', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (304, 34, 12, '223', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (305, 34, 1, 'Workshop akademik “Make Awesome Games Using Construct 3”', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (306, 34, 10, '0', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (307, 34, 13, '226', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (308, 34, 3, '05/25/2018 - 05/25/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (309, 34, 14, '255', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (310, 35, 72, '268', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (311, 35, 11, '221', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (312, 35, 12, '223', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (313, 35, 1, 'Peningkatan Proses Pembelajaran Siswa Dalam Pemanfaatan Teknologi Informasi di SMAN 1 Sumberpucung Malang', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (314, 35, 10, '0', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (315, 35, 13, '226', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (316, 35, 3, '01/29/2018 - 05/11/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (317, 35, 14, '255', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (318, 36, 1, 'Sistem Informasi Pelayanan Pengaduan Pedagang Kaki Lima Berbasis Android (Studi Kasus Satpol PP Pemerintahan Kabupaten Malang)', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (319, 36, 2, '1', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (320, 36, 3, '08/01/2018 - 12/31/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (321, 36, 4, '4', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (322, 36, 5, '15', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (323, 36, 6, '67', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (324, 36, 7, '167', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (325, 36, 8, '215', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (326, 36, 9, '298', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (327, 36, 10, '1366500', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (328, 37, 1, 'Implementasi Teknologi web-RTC dan web-socket untuk Video-Conference dengan Fitur Push-to-Talk', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (329, 37, 2, '1', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (330, 37, 3, '08/01/2018 - 12/31/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (331, 37, 4, '4', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (332, 37, 5, '15', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (333, 37, 6, '61', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (334, 37, 7, '133', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (335, 37, 8, '215', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (336, 37, 9, '298', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (337, 37, 10, '3876000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (338, 38, 1, 'Pembelajaran Berbasis Proyek di STIKI Malang', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (339, 38, 2, '1', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (340, 38, 3, '02/01/2018 - 01/31/2019', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (341, 38, 4, '4', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (342, 38, 5, '15', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (343, 38, 6, '66', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (344, 38, 7, '164', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (345, 38, 8, '215', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (346, 38, 9, '298', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (347, 38, 10, '30000000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (348, 39, 1, 'Analisis Technology Acceptance Model untuk Mengevaluasi Potensi Penerapan Sistem Presensi Perkuliahan Berbasis Wi-fi Direct', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (349, 39, 2, '1', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (350, 39, 3, '01/01/2019 - 11/30/2019', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (351, 39, 4, '4', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (352, 39, 5, '15', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (353, 39, 6, '61', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (354, 39, 7, '132', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (355, 39, 8, '215', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (356, 39, 9, '298', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (357, 39, 10, '1000000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (358, 40, 1, 'Analisa Kinerja Akademik Mahasiswa dengan Metode Backpropagation', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (359, 40, 2, '1', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (360, 40, 3, '01/01/2019 - 01/31/2020', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (361, 40, 4, '4', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (362, 40, 5, '15', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (363, 40, 6, '61', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (364, 40, 7, '132', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (365, 40, 8, '215', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (366, 40, 9, '298', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (367, 40, 10, '2500000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (368, 41, 1, 'Pengembangan dan Pelaksanaan Perkuliahan Hybris/Blended pada Mata Kuliah Aljabar Linier dan Matriks', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (369, 41, 2, '2', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (370, 41, 3, '08/01/2018 - 01/31/2019', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (371, 41, 4, '8', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (372, 41, 5, '39', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (373, 41, 6, '66', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (374, 41, 7, '162', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (375, 41, 8, '215', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (376, 41, 9, '298', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (377, 41, 10, '10000000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (390, 1, 73, '286', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (391, 41, 73, '287', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (392, 42, 1, 'Sistem Pembelajaran Daring Mata Kuliah Metode Numerik pada Program Studi Teknik Informatika', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (393, 42, 2, '2', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (394, 42, 73, '287', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (395, 42, 3, '08/01/2018 - 01/31/2019', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (396, 42, 4, '8', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (397, 42, 5, '39', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (398, 42, 6, '66', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (399, 42, 7, '162', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (400, 42, 8, '215', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (401, 42, 9, '217', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (402, 42, 10, '10000000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (403, 42, 73, '287', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (404, 43, 1, 'Penyelenggaraan Sistem Pembelajaran Daring (Blended) pada Mata Kuliah Pengolahan Citra Digital', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (405, 43, 2, '2', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (406, 43, 73, '287', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (407, 43, 3, '08/01/2018 - 01/31/2019', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (408, 43, 4, '8', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (409, 43, 5, '39', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (410, 43, 6, '66', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (411, 43, 7, '162', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (412, 43, 8, '215', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (413, 43, 9, '217', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (414, 43, 10, '10000000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (415, 44, 1, 'Penyelenggaraan Pembelajaran Blended pada Mata Kuliah Riset Operasi di STIKI Malang', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (416, 44, 2, '2', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (417, 44, 73, '287', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (418, 44, 3, '08/01/2018 - 01/31/2019', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (419, 44, 4, '8', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (420, 44, 5, '39', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (421, 44, 6, '66', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (422, 44, 7, '162', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (423, 44, 8, '215', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (424, 44, 9, '217', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (425, 44, 10, '10000000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (426, 45, 1, 'Building Generation of Entrepreneurship to Compete in Facing the ASEAN Economic Communities', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (427, 45, 2, '1', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (428, 45, 3, '11/02/2018 - 11/04/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (429, 45, 4, '8', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (430, 45, 5, '41', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (431, 45, 6, '62', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (432, 45, 7, '137', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (433, 45, 8, '215', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (434, 45, 9, '298', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (435, 45, 10, '3850000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (436, 46, 1, 'Integration of Character Education in Developing Eglish Syllabus and Lesson Plan of Vocational Education', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (437, 46, 2, '1', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (438, 46, 3, '08/27/2018 - 08/28/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (439, 46, 4, '8', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (440, 46, 5, '39', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (441, 46, 6, '66', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (442, 46, 7, '165', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (443, 46, 8, '215', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (444, 46, 9, '298', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (445, 46, 10, '5050000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (446, 47, 1, 'The Effectiveness Of Batik Training For International Students', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (447, 47, 2, '1', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (448, 47, 3, '10/17/2018 - 10/18/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (449, 47, 4, '9', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (450, 47, 5, '48', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (451, 47, 6, '66', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (452, 47, 7, '164', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (453, 47, 8, '215', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (454, 47, 9, '298', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (455, 47, 10, '3550000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (456, 48, 1, 'Redesigning CHIML: Orchestration Language for Chimera-Framework', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (457, 48, 2, '1', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (458, 48, 3, '10/17/2018 - 10/18/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (459, 48, 4, '4', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (460, 48, 5, '15', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (461, 48, 6, '71', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (462, 48, 7, '205', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (463, 48, 8, '215', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (464, 48, 9, '298', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (465, 48, 10, '4874000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (466, 49, 1, 'Sistem Pendukung Keputusan Kelompok Untuk Menentukan Penerima AGC Award Menggunakan Metode SAW dan Borda', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (467, 49, 2, '1', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (468, 49, 3, '08/01/2018 - 01/31/2019', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (469, 49, 4, '4', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (470, 49, 5, '15', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (471, 49, 6, '71', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (472, 49, 7, '205', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (473, 49, 8, '215', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (474, 49, 9, '298', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (475, 49, 10, '1000000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (476, 50, 72, '268', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (477, 50, 11, '222', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (478, 50, 12, '223', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (479, 50, 1, 'Pengembangan Sistem Informasi e-SAMBAT', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (480, 50, 10, '3600000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (481, 50, 13, '227', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (482, 50, 3, '07/01/2018 - 10/31/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (483, 50, 14, '253', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (484, 51, 72, '268', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (485, 51, 11, '221', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (486, 51, 12, '223', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (487, 51, 1, 'Pengembangan Aplikasi Perijinan Online Pendirian Lembaga Kesejahteraan Sosial di Dinas Sosial Kota Malang', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (488, 51, 10, '2500000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (489, 51, 13, '227', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (490, 51, 3, '06/01/2018 - 12/31/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (491, 51, 14, '255', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (492, 52, 72, '268', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (493, 52, 11, '222', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (494, 52, 12, '224', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (495, 52, 1, 'Seminar dan Workshop Optimasi Kuliah dengan Kerangka Sistem Pembelajaran Daring (SPADA)', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (496, 52, 10, '10000000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (497, 52, 13, '228', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (498, 52, 3, '08/08/2018 - 08/10/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (499, 52, 14, '255', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (500, 53, 72, '269', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (501, 53, 11, '221', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (502, 53, 12, '223', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (503, 53, 1, 'Workshop Penulisan Karya Ilmiah dengan Mendeley', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (504, 53, 10, '300000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (505, 53, 13, '226', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (506, 53, 3, '08/28/2020 - 08/28/2020', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (507, 53, 14, '252', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (508, 54, 72, '268', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (509, 54, 11, '222', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (510, 54, 12, '223', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (511, 54, 1, 'Juri pada Acara Lomba Mewarna Mamamia (Ibu dan Anak)', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (512, 54, 10, '3000000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (513, 54, 13, '228', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (514, 54, 3, '10/13/2018 - 10/13/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (515, 54, 14, '247', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (516, 55, 72, '268', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (517, 55, 11, '222', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (518, 55, 12, '223', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (519, 55, 1, 'Workshop Penyusunan Proposal Pengabdian kepada Masyarakat Secara Daring Melalui SIMLITABMAS', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (520, 55, 10, '150000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (521, 55, 13, '226', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (522, 55, 3, '10/19/2018 - 10/19/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (523, 55, 14, '255', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (524, 56, 72, '268', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (525, 56, 11, '221', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (526, 56, 12, '223', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (527, 56, 1, 'Pembuatan Web Madrasah dan Aplikasi Soal Berbasis Komputer Madrasah Ibtidaiyah Nahdlatul Ulama’', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (528, 56, 10, '5130000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (529, 56, 13, '229', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (530, 56, 3, '11/26/2018 - 07/01/2019', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (531, 56, 14, '255', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (532, 57, 72, '268', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (533, 57, 11, '222', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (534, 57, 12, '223', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (535, 57, 1, 'Penyusunan Rencana Induk TIK Kota Malang 2019 - 2023', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (536, 57, 10, '5400000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (537, 57, 13, '227', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (538, 57, 3, '08/01/2018 - 01/31/2019', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (539, 57, 14, '255', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (548, 59, 72, '268', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (549, 59, 11, '221', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (550, 59, 12, '223', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (551, 59, 1, 'Join Program Pembelajaran Bidang Teknologi Informasi dan Komputer (TIK) di SMA Negeri 1 Sumberpucung Kabupaten Malang', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (552, 59, 10, '28335000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (553, 59, 13, '229', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (554, 59, 3, '11/12/2018 - 11/24/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (555, 59, 14, '255', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (556, 60, 72, '268', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (557, 60, 11, '222', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (558, 60, 12, '223', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (559, 60, 1, 'Penguji UKK SMK Mamba\'ul Jadid Gondanglegi', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (560, 60, 10, '1920000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (561, 60, 13, '229', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (562, 60, 3, '12/26/2018 - 12/29/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (563, 60, 14, '253', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (564, 61, 72, '269', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (565, 61, 11, '221', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (566, 61, 12, '223', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (567, 61, 1, '2nd Batch SEA-TVET Student Internship Exchange Programme', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (568, 61, 10, '5000000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (569, 61, 13, '226', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (570, 61, 3, '08/01/2018 - 10/31/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (571, 61, 14, '255', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (572, 62, 72, '268', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (573, 62, 11, '222', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (574, 62, 12, '223', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (575, 62, 1, 'Pembinaan Keluarga Sadar Hukum (KADARKUM) di Kelurahan Merjosari Kecamatan Lowokwaru', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (576, 62, 10, '1250000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (577, 62, 13, '227', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (578, 62, 3, '09/19/2018 - 09/19/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (579, 62, 14, '255', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (580, 63, 72, '268', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (581, 63, 11, '222', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (582, 63, 12, '223', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (583, 63, 1, 'Pembinaan Keluarga Sadar Hukum (KADARKUM) di Kelurahan Pisang Candi Kecamatan Sukun', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (584, 63, 10, '1250000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (585, 63, 13, '227', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (586, 63, 3, '10/03/2018 - 10/03/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (587, 63, 14, '255', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (588, 64, 72, '269', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (589, 64, 11, '222', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (590, 64, 12, '223', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (591, 64, 1, 'Workshop “Chatbot and Chatcommerce using API.ai”', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (592, 64, 10, '220000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (593, 64, 13, '226', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (594, 64, 3, '11/09/2018 - 11/09/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (595, 64, 14, '255', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (596, 65, 72, '269', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (597, 65, 11, '222', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (598, 65, 12, '223', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (599, 65, 1, 'Workshop Manajemen Konflik dalam Dunia Pekerjaan', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (600, 65, 10, '270000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (601, 65, 13, '226', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (602, 65, 3, '09/07/2018 - 09/07/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (603, 65, 14, '255', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (604, 66, 72, '268', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (605, 66, 11, '222', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (606, 66, 12, '223', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (607, 66, 1, 'Pembinaan Keluarga Sadar Hukum (KADARKUM) di Kecamatan Klojen (Kelurahan Kiduldalem dan Kelurahan Kauman)', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (608, 66, 10, '1500000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (609, 66, 13, '227', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (610, 66, 3, '01/22/2019 - 01/22/2019', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (611, 66, 14, '255', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (620, 68, 72, '268', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (621, 68, 11, '221', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (622, 68, 12, '223', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (623, 68, 1, 'Kegiatan Join Program Peningkatan Keahlian Bidang TIK di SMAK Frateran Malang', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (624, 68, 10, '43125000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (625, 68, 13, '229', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (626, 68, 3, '08/07/2018 - 11/30/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (627, 68, 14, '255', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (628, 69, 72, '268', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (629, 69, 11, '222', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (630, 69, 12, '223', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (631, 69, 1, 'Tenaga Ahli di Polsek Klojen pada Tindak Pidana Informasi dan Transaksi Elektronik', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (632, 69, 10, '50000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (633, 69, 13, '229', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (634, 69, 3, '10/19/2018 - 11/06/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (635, 69, 14, '255', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (636, 70, 72, '268', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (637, 70, 11, '222', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (638, 70, 12, '223', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (639, 70, 1, 'Pembinaan Keluarga Sadar Hukum (KADARKUM) di Kelurahan Sawojajar dan Kelurahan Buring', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (640, 70, 10, '1250000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (641, 70, 13, '227', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (642, 70, 3, '09/26/2018 - 09/26/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (643, 70, 14, '255', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (644, 71, 72, '268', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (645, 71, 11, '222', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (646, 71, 12, '223', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (647, 71, 1, 'Pembinaan Keluarga Sadar Hukum (KADARKUM) di Kelurahan Kasin dan Kelurahan Rampal Celaket', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (648, 71, 10, '1250000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (649, 71, 13, '227', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (650, 71, 3, '09/05/2018 - 09/05/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (651, 71, 14, '255', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (652, 72, 72, '268', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (653, 72, 11, '222', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (654, 72, 12, '223', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (655, 72, 1, 'Pembinaan Keluarga Sadar Hukum (KADARKUM) Kelurahan Pandanwangi dan Arjosari Kecamatan Blimbing', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (656, 72, 10, '1250000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (657, 72, 13, '227', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (658, 72, 3, '09/12/2018 - 09/12/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (659, 72, 14, '255', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (660, 73, 72, '268', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (661, 73, 11, '222', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (662, 73, 12, '223', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (663, 73, 1, 'Narasumber Rapat Kerja STIKES Kepanjen Tahun 2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (664, 73, 10, '1000000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (665, 73, 13, '296', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (666, 73, 3, '12/12/2018 - 12/12/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (667, 73, 14, '255', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (668, 74, 72, '268', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (669, 74, 11, '222', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (670, 74, 12, '223', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (671, 74, 1, 'Workshop Melek Digital dan Pemberian Bantuan Komputer dalam Semarak Dua Dasawarsa Malang bersama Bentoel', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (672, 74, 10, '300000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (673, 74, 13, '228', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (674, 74, 3, '08/08/2018 - 08/08/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (675, 74, 14, '255', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (676, 75, 72, '268', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (677, 75, 11, '222', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (678, 75, 12, '223', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (679, 75, 1, 'Pelatihan Pembuatan Video Pembelajaran bagi Guru SMPN 4 Kepanjen', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (680, 75, 10, '4850000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (681, 75, 13, '229', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (682, 75, 3, '12/08/2018 - 12/08/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (683, 75, 14, '255', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (684, 76, 72, '268', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (685, 76, 11, '222', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (686, 76, 12, '223', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (687, 76, 1, 'Join Program - SMA Kristen Kalam Kudus (Adobe Ilustrator dan Bahasa Korea Lv. 2)', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (688, 76, 10, '32250000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (689, 76, 13, '229', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (690, 76, 3, '08/10/2018 - 10/18/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (691, 76, 14, '255', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (692, 77, 72, '268', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (693, 77, 11, '222', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (694, 77, 12, '223', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (695, 77, 1, 'Pelatihan “Tagline” Bahasa Inggris untuk Meningkatkan daya Jual', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (696, 77, 10, '500000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (697, 77, 13, '226', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (698, 77, 3, '01/16/2019 - 01/17/2019', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (699, 77, 14, '255', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (700, 78, 72, '268', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (701, 78, 11, '222', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (702, 78, 12, '223', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (703, 78, 1, 'Pemateri pada Seminar Amal untuk Palu dan Donggala dengan Tema ”Menjadi Global Citizen”', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (704, 78, 10, '270000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (705, 78, 13, '226', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (706, 78, 3, '08/12/2018 - 08/12/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (707, 78, 14, '255', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (708, 79, 72, '268', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (709, 79, 11, '222', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (710, 79, 12, '223', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (711, 79, 1, 'Penjurian Sosial Adventure and Product Marketing Langsep Chalenge 2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (712, 79, 10, '125000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (713, 79, 13, '229', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (714, 79, 3, '11/07/2018 - 11/07/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (715, 79, 14, '255', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (716, 80, 72, '269', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (717, 80, 11, '222', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (718, 80, 12, '223', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (719, 80, 1, 'Pekan Ilmiah Mahasiswa & SSC VI ”Techno Creative”', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (720, 80, 10, '41789000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (721, 80, 13, '226', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (722, 80, 3, '10/19/2018 - 10/19/2018', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (723, 80, 14, '255', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (760, 85, 1, 'Prototipe ROIP dengan Menggunakan Web-RTC, Web-USB, dan Arduino Leonardo', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (761, 85, 2, '1', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (762, 85, 3, '02/02/2019 - 01/01/2020', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (763, 85, 4, '4', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (764, 85, 5, '15', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (765, 85, 6, '71', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (766, 85, 7, '205', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (767, 85, 8, '215', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (768, 85, 9, '298', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (769, 85, 10, '3876000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (770, 86, 1, 'Presepsi Siswa dalam Penggunaan Media Pembelajaran berbasis Augmented Reality', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (771, 86, 2, '1', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (772, 86, 3, '02/02/2019 - 01/01/2020', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (773, 86, 4, '4', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (774, 86, 5, '15', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (775, 86, 6, '66', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (776, 86, 7, '162', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (777, 86, 8, '215', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (778, 86, 9, '298', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (779, 86, 10, '0', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (780, 87, 1, 'Seleksi Dosen dalam Proses Rekrutmen Menggunakan Fuzzy Sugeno', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (781, 87, 2, '1', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (782, 87, 3, '02/02/2019 - 12/31/2019', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (783, 87, 4, '4', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (784, 87, 5, '15', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (785, 87, 6, '71', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (786, 87, 7, '205', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (787, 87, 8, '215', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (788, 87, 9, '298', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (789, 87, 10, '1000000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (790, 88, 1, 'Model Pembelajaran Matematika Berbasis Blended Learning dalam menghadapi Era Revolusi Industri 4.0', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (791, 88, 2, '1', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (792, 88, 3, '04/01/2019 - 12/31/2019', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (793, 88, 4, '4', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (794, 88, 5, '10', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (795, 88, 6, '66', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (796, 88, 7, '162', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (797, 88, 8, '215', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (798, 88, 9, '298', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (799, 88, 10, '0', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (800, 89, 1, 'Pemanfaatan RFID sebagai Alternatif Absensi Siswa (Studi Kasus: SMK ArRahmah Sukabumi, Jawa Barat)', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (801, 89, 2, '1', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (802, 89, 3, '04/01/2019 - 12/31/2019', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (803, 89, 4, '4', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (804, 89, 5, '15', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (805, 89, 6, '66', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (806, 89, 7, '165', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (807, 89, 8, '215', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (808, 89, 9, '298', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (809, 89, 10, '2889000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (810, 90, 1, 'Implementasi Teknik SEO (Search Engine Optimization) untuk Meningkatkan Rank Universitas di Webometrics (studi kasus: STIKI MALANG)', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (811, 90, 2, '1', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (812, 90, 3, '04/01/2019 - 12/31/2019', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (813, 90, 4, '4', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (814, 90, 5, '15', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (815, 90, 6, '61', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (816, 90, 7, '132', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (817, 90, 8, '215', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (818, 90, 9, '298', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (819, 90, 10, '3122500', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (820, 91, 1, 'Analisis Kebutuhan Mahasiswa dalam Mengikuti Perkuliahan Musik Digital', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (821, 91, 2, '1', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (822, 91, 3, '04/01/2019 - 12/31/2019', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (823, 91, 4, '9', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (824, 91, 5, '47', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (825, 91, 6, '66', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (826, 91, 7, '164', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (827, 91, 8, '215', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (828, 91, 9, '298', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (829, 91, 10, '0', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (830, 92, 1, 'Analisis Penggunaan Digital Audio Workstation dalam Pembelajaran Musik Digital', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (831, 92, 2, '1', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (832, 92, 3, '04/01/2019 - 12/31/2019', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (833, 92, 4, '9', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (834, 92, 5, '47', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (835, 92, 6, '66', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (836, 92, 7, '164', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (837, 92, 8, '215', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (838, 92, 9, '298', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (839, 92, 10, '0', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (840, 93, 1, 'Perancangan Sistem Informasi Kepegawaian pada BKPP Berbasis Website', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (841, 93, 2, '1', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (842, 93, 3, '04/01/2019 - 12/31/2019', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (843, 93, 4, '4', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (844, 93, 5, '15', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (845, 93, 6, '71', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (846, 93, 7, '205', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (847, 93, 8, '215', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (848, 93, 9, '298', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (849, 93, 10, '0', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (850, 94, 1, 'Pengaruh HardSkill dan SoftSkill terhadap Self Efficacy Mahasiswa', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (851, 94, 2, '1', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (852, 94, 3, '05/01/2019 - 12/31/2019', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (853, 94, 4, '8', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (854, 94, 5, '44', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (855, 94, 6, '67', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (856, 94, 7, '170', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (857, 94, 8, '215', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (858, 94, 9, '298', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (859, 94, 10, '1500000', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (918, 102, 1, 'Implementasi Algoritma Naive Bayes untuk Memprediksi Lama Masa Studi dan Predikat Kelulusan Mahasiswa', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (919, 102, 2, '1', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (920, 102, 3, '02/01/2019 - 07/31/2019', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (921, 102, 4, '4', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (922, 102, 5, '15', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (923, 102, 6, '66', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (924, 102, 7, '165', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (925, 102, 8, '215', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (926, 102, 9, '298', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (927, 102, 10, '2538250', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (936, 104, 1, 'Korelasi Antara Matematika Dengan Kecerdasan Seseorang', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (937, 104, 2, '1', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (938, 104, 3, '01/01/2020 - 12/31/2020', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (939, 104, 4, '4', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (940, 104, 5, '10', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (941, 104, 6, '66', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (942, 104, 7, '163', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (943, 104, 8, '215', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (944, 104, 9, '298', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_props` VALUES (945, 104, 10, '2000000', NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for tb_rst_tr_riset_revisi
-- ----------------------------
DROP TABLE IF EXISTS `tb_rst_tr_riset_revisi`;
CREATE TABLE `tb_rst_tr_riset_revisi`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `riset` int(11) NULL DEFAULT NULL,
  `keterangan` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `tanggal` date NULL DEFAULT NULL,
  `status` enum('proses','selesai') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `Created_App` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Created_By` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Created_Date` datetime(0) NULL DEFAULT NULL,
  `Modified_App` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Modified_By` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Modified_Date` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `riset`(`riset`) USING BTREE,
  CONSTRAINT `tb_rst_tr_riset_revisi_ibfk_1` FOREIGN KEY (`riset`) REFERENCES `tb_rst_tr_riset` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_rst_tr_riset_revisi
-- ----------------------------

-- ----------------------------
-- Table structure for tb_rst_tr_riset_sk
-- ----------------------------
DROP TABLE IF EXISTS `tb_rst_tr_riset_sk`;
CREATE TABLE `tb_rst_tr_riset_sk`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `riset` int(11) NULL DEFAULT NULL,
  `no_sk` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `tentang` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `tgl_ditetapkan` date NULL DEFAULT NULL,
  `tgl_mulai_berlaku` date NULL DEFAULT NULL,
  `tgl_selesai_berlaku` date NULL DEFAULT NULL,
  `penerbit` int(6) NULL DEFAULT NULL,
  `pejabat_ttd` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `file` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Created_App` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Created_By` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Created_Date` datetime(0) NULL DEFAULT NULL,
  `Modified_App` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Modified_By` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Modified_Date` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `riset`(`riset`) USING BTREE,
  INDEX `penerbit`(`penerbit`) USING BTREE,
  INDEX `pejabat_ttd`(`pejabat_ttd`) USING BTREE,
  CONSTRAINT `tb_rst_tr_riset_sk_ibfk_1` FOREIGN KEY (`riset`) REFERENCES `tb_rst_tr_riset` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tb_rst_tr_riset_sk_ibfk_2` FOREIGN KEY (`penerbit`) REFERENCES `tb_peg_rf_unit` (`unit_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `tb_rst_tr_riset_sk_ibfk_3` FOREIGN KEY (`pejabat_ttd`) REFERENCES `tb_peg_rf_pegawai` (`nip`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 105 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_rst_tr_riset_sk
-- ----------------------------
INSERT INTO `tb_rst_tr_riset_sk` VALUES (1, 1, '052/LPPM.05/STIKI/VII/2018', 'Surat Perjanjian Pelaksanaan Penelitian (SP3) Mahendra Wibawa', '2018-07-15', '2018-03-27', '2018-11-16', 8, '010077', 'c86222668ab9e2ed0584772b8564bde9.pdf', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (2, 2, '031/LPPM.05/STIKI/III/2018', 'Surat Perjanjian Pelaksanaan Penelitian (SP3) Evy Poerbaningtyas', '2018-03-26', '2018-03-20', '2018-11-16', 8, '010077', 'd193a6d66d4eb5a3fc92b20ef25bd80b.pdf', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (5, 5, '030/LPPM.05/STIKI/III/2018', 'Surat Perjanjian Pelaksanaan Penelitian (SP3) Nira Radita', '2018-03-26', '2018-03-27', '2018-11-16', 8, '010077', '444f07caf42af101420497794f4e4911.pdf', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (6, 6, '028/LPPM.05/STIKI/III/2018', 'Surat Perjanjian Pelaksaan Penelitian (SP3) Chaulina Alfianti Oktavia', '2018-03-26', '2018-03-27', '2018-11-16', 8, '010077', '496e7fc5add222aeea33c653812ec16c.pdf', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (7, 7, '044/LPPM.11/STIKI/VII/2017', 'Infografis Karakter Punokawan', '2017-07-14', '2017-07-01', '2018-07-31', 8, '010077', '56736b20e1b91f56eb6866a9cbfb26f7.pdf', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (8, 8, '008/LPPM.05/STIKI/I/2018', 'Penyusunan buku ajar \"Interaksi Manusia dan Komputer\"', '2018-01-26', '2018-01-01', '2019-01-31', 8, '010077', 'ceba5a423ca2d67e01a3b0d86da90992.pdf', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (9, 9, '007/LPPM.05/STIKI/I/2018', 'Implementasi Fuzzy Sugeno dalam Pemilihan Processor', '2018-01-26', '2018-02-01', '2018-12-31', 8, '010077', '26a5b260f3e3cbeed329c627f3635385.pdf', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (10, 10, '005/LPPM.05/STIKI/I/2018', 'Analisis Model Tata Kelola Sistem Informasi Perguruan Tinggi (Studi Kasus: STIKI Malang)', '2020-01-26', '2018-01-01', '2018-12-31', 8, '010077', '222ef81a11faff5a8ba21c6f1d1dbf8a.pdf', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (11, 11, '041/LPPM.05/STIKI/V/2018', 'Studi Kelayakan Implementasi Sistem Informasi Klinik Mata Mojoagung Menggunakan Information Economic', '2018-05-08', '2018-05-01', '2018-07-31', 8, '010077', '5f1f463c103e49cfd6cd300e0a97421e.pdf', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (12, 12, '073/LPPM.05/STIKI/XII/2017', 'Pembuatan Aplikasi Dashboard Strategic untuk Perencanaan Kapasitas Pembangkit Listrik yang Terintegr', '2017-12-18', '2018-01-01', '2018-06-30', 8, '010077', 'ec6c2da96d8a227c1b4849f8edbb1183.pdf', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (13, 13, '051/LPPM.05/STIKI/VII/2018', 'Efektifitas Pemanfaatan Studio TA dalam Menunjang Penyelesaian Tugas Akhir Mahasiswa dengan Mengguna', '2018-07-16', '2018-08-01', '2019-07-31', 8, '010077', 'd628f13b9422680d141da5df31c9fcf0.pdf', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (14, 14, '052/LPPM.05/STIKI/VII/2018', 'Surat Perjanjian Pelaksanaan Penelitian (SP3) Mahendra Wibawa', '2018-07-16', '2018-07-16', '2018-12-31', 8, '010077', 'c6f1ab488be4812659cdcfe8756ee634.pdf', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (15, 15, '053/LPPM.05/STIKI/VII/2018', 'Surat Perjanjian Pelaksanaan Penelitian (SP3) Siti Aminah', '2018-07-16', '2018-07-16', '2018-12-31', 8, '010077', '4f9e5af4500c5350ddf5e16261e1a7af.pdf', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (16, 16, '058/LPPM.05/STIKI/2018', 'Surat Perjanjian Pelaksanaan Penelitian (SP3) Yekti Asmoro Kanthi', '2018-07-16', '2018-07-16', '2018-12-31', 8, '010077', 'db42ecd13e42411171eadba5110cc511.pdf', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (17, 17, '059/LPPM.05/STIKI/VII/2018', 'Surat Perjanjian Pelaksanaan Penelitian (SP3) Addin Aditya', '2018-07-16', '2018-07-16', '2018-12-31', 8, '010077', '2783180ce9e3ba29674ff51100edd671.pdf', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (18, 18, '060/LPPM.05/STIKI/VII/2018', 'Surat Perjanjian Pelaksanaan Penelitian (SP3) Rina Nur Fitri', '2018-07-16', '2018-07-16', '2018-12-31', 8, '010077', '981e1a9fcd8ec3fbee7aa95ff1df982e.pdf', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (19, 19, '049/LPPM.05/STIKI/VII/2018', 'Analisa Kebutuhan PErangkat Lunak untuk Pengembangan Aplikasi Pemetaan Distribusi UMKM Menggunakan P', '1970-01-01', '2018-07-01', '2018-12-31', 8, '010077', '6e061cf8234b0a18da3936eeb629a939.pdf', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (20, 20, '004/LPPM.11/STIKI/I/2018', 'Pengembangan Website Institusi Salesmaxx Academy / iClean', '2018-01-26', '2018-01-01', '2018-06-30', 8, '010077', '2f4406fe52f8b14ab2e59d9822034cde.pdf', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (21, 21, '006/LPPM.11/STIKI/I/2018', 'Pelatihan Pembuatan Foto Produk yang Menarik untuk Meningkatkan Pemasaran pada Website UMKM Kota Mal', '2018-01-26', '2018-05-11', '2018-05-11', 8, '010077', '7950df9308f0bd00ad8bd9c533b830d2.pdf', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (22, 22, '010/LPPM.11/STIKI/II/2018', 'Tim Penguji SMK Mamba\'ul Jadid', '2018-01-26', '2018-02-26', '2018-03-01', 8, '010077', '59e8c5ef4636c990f0e44fa70973022f.pdf', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (23, 23, '013/LPPM.11/STIKI/II/2018', 'Ujian Kompetensi Keahlian di SMK Negeri 2 Blitar', '2018-01-31', '2018-02-21', '2018-02-22', 8, '010077', 'cb5eb287f91d2cb2d9bf59b5b2f77c88.pdf', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (24, 24, '020/LPPM.11/STIKI/II/2018', 'Ujian Kompetensi Keahlian di SMK \"Darul Huda\" Blitar', '2018-02-27', '2018-03-03', '2018-03-04', 8, '010077', 'f9cf7ea92c0cd9c67f0bd32d854eab18.pdf', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (25, 25, NULL, NULL, NULL, '2018-03-16', '2018-03-16', 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (26, 26, '034/LPPM.11/STIKI/III/2018', 'Seleksi Bina Kreatifitas Siswa 2018', '1970-01-01', '2018-03-03', '2018-03-03', 8, '010077', 'be18b518d73d44521d94ed8f77b8be98.pdf', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (27, 27, '035/LPPM.11/STIKI/IV/2018', 'Workshop Akademik \"Tips-Tips Penting Menjaga Kesehatan bagi Para Pengguna Komputer\"', '2018-04-04', '2018-04-06', '2018-04-06', 8, '010077', '7ff3a7eccac1b656d8f11fc40e3ed2db.pdf', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (28, 28, '046/LPPM.11/STIKI/V/2018', 'Bakti Sosial Operasi Katarak dan Pemeriksaan Mata terhadap Masyarakat yang Membutuhkan', '2018-05-31', '2018-07-01', '2018-08-31', 8, '010077', '4bc52920ccced0aa290121ef983ab719.pdf', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (29, 29, '048/LPPM.11/STIKI/VI/2018', 'Workshop Registrasi Web Sitasi', '2018-06-28', '2018-06-29', '2018-06-29', 8, '010077', 'e6ddca4eb758cc5bf28b01b284c2ffd2.pdf', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (30, 30, NULL, NULL, NULL, '2018-07-10', '2018-07-11', 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (31, 31, NULL, NULL, NULL, '2018-02-01', '2018-07-31', 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (32, 32, NULL, NULL, NULL, '2018-02-08', '2018-05-11', 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (33, 33, '002/LPPM.11/STIKI/I/2018', 'Pembuatan Website SDN Sukun 3 Malang', '2018-01-22', '2018-02-01', '2018-07-31', 8, '010077', 'a84ba29883971623764ec1c012d14d18.pdf', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (34, 34, NULL, NULL, NULL, '2018-05-25', '2018-05-25', 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (35, 35, NULL, NULL, NULL, '2018-01-29', '2018-05-11', 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (36, 36, '054/LPPM.05/STIKI/VII2018', 'Sistem Informasi Pelayanan Pengaduan Pedagang Kaki Lima Berbasis Android', '2018-07-17', '2018-08-01', '2018-12-31', 8, '010077', '5b0c8cc814abe70a3cf6f45503d52785.pdf', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (37, 37, NULL, NULL, NULL, '2018-08-01', '2018-12-31', 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (38, 38, NULL, NULL, NULL, '2018-02-01', '2019-01-31', 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (39, 39, '001/LPPM.01/STIKI/I/2019', 'Analisis Technology Acceptance Model untuk Mengevaluasi Potensi Penerapan Sistem Presensi Perkuliaha', '2019-01-22', '2019-01-01', '2019-11-30', 8, '010077', 'e38343da8644d249e8422664771b4fd9.pdf', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (40, 40, '004/LPPM.05/STIKI/I/2019', 'Analisa Kinerja Akademik Mahasiswa dengan Metode Backpropagation', '2019-01-24', '2019-01-01', '2020-01-31', 8, '010077', 'c16226d51df5c7588b009aab2f280832.pdf', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (41, 41, NULL, NULL, NULL, '2018-08-01', '2019-01-31', 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (42, 42, NULL, NULL, NULL, '2018-08-01', '2019-01-31', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (43, 43, NULL, NULL, NULL, '2018-08-01', '2019-01-31', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (44, 44, NULL, NULL, NULL, '2018-08-01', '2019-01-31', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (45, 45, NULL, NULL, NULL, '2018-11-02', '2018-11-04', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (46, 46, NULL, NULL, NULL, '2018-08-27', '2018-08-28', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (47, 47, NULL, NULL, NULL, '2018-10-17', '2018-10-18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (48, 48, NULL, NULL, NULL, '2018-10-17', '2018-10-18', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (49, 49, NULL, NULL, NULL, '2018-08-01', '2019-01-31', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (50, 50, '056/LPPM.11/STIKI/VII/2018', 'Pengembangan Sistem Informasi e-SAMBAT', '2018-07-18', '2018-07-01', '2018-10-31', 8, '010077', '406e42056a5b1d1e8b2f27f033e2e758.pdf', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (51, 51, NULL, NULL, NULL, '2018-06-01', '2018-12-31', 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (52, 52, '066/LPPM.11/STIKI/VIII/2018', 'TIM SPADA pada Seminar dan Workshop \"Optimasi Kuliah dengan Kerangka Sistem Pembelajaran Daring Indo', '2018-08-06', '2018-08-08', '2018-08-10', 8, '010077', '4eb615294a10d6cbb57671d1fe812bd5.pdf', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (53, 53, '068/LPPM.11/STIKI/IX/2018', 'Workshop Penulisan Karya Ilmiah dengan Mendeley', '2018-08-24', '2020-08-28', '2020-08-28', 8, '010077', '5a67755c24dc869f3fdc3a37bedf303b.pdf', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (54, 54, '083/LPPM.11/STIKI/IX/2018', 'Lomba Mewarna Mamamia (Ibu dan Anak)', '2018-10-12', '2018-10-13', '2018-10-13', 8, '010077', 'fc1f3169a352830855c129566f75b6dc.pdf', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (55, 55, '085/LPPM.11/STIKI/X/2018', 'Workshop Penyusunan Proposal Pengabdian kepada Masyarakat Secara Daring Melalui SIMLITABMAS', '2018-10-15', '2018-10-19', '2018-10-19', 8, '010077', 'c750a0eb964c6469481b3dd89cb81fbd.pdf', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (56, 56, '100/LPPM.11/STIKI/IX/2018', 'Pembuatan Web Madrasah dan Aplikasi Soal Berbasis Komputer Madrasah Ibtidaiyah Nahdlatul Ulama\'', '2018-10-10', '2018-11-26', '2019-07-01', 8, '010077', 'f0099afe2946b9c361e8672766b743e9.pdf', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (57, 57, '102/LPPM.11/STIKI/VIII/2018', 'Penyususnan Rencana Induk TIK Kota Malang 2019 - 2023', '2018-08-03', '2018-08-01', '2019-01-31', 8, '010077', 'd424c8314abbad23cb0be79d1b5301c8.pdf', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (59, 59, NULL, NULL, NULL, '2018-11-12', '2018-11-24', 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (60, 60, '103/LPPM.11/STIKI/XII/2018', 'Penguji UKK pada SMK Mamba\'ul Jadid', '2018-12-21', '2018-12-26', '2018-12-29', 8, '010077', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (61, 61, NULL, NULL, NULL, '2018-08-01', '2018-10-31', 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (62, 62, NULL, NULL, NULL, '2018-09-19', '2018-09-19', 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (63, 63, NULL, NULL, NULL, '2018-10-03', '2018-10-03', 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (64, 64, NULL, NULL, NULL, '2018-11-09', '2018-11-09', 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (65, 65, NULL, NULL, NULL, '2018-09-07', '2018-09-07', 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (66, 66, NULL, NULL, NULL, '2019-01-22', '2019-01-22', 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (68, 68, NULL, NULL, NULL, '2018-08-07', '2018-11-30', 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (69, 69, NULL, NULL, NULL, '2018-10-19', '2018-11-06', 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (70, 70, NULL, NULL, NULL, '2018-09-26', '2018-09-26', 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (71, 71, NULL, NULL, NULL, '2018-09-05', '2018-09-05', 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (72, 72, NULL, NULL, NULL, '2018-09-12', '2018-09-12', 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (73, 73, NULL, NULL, NULL, '2018-12-12', '2018-12-12', 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (74, 74, NULL, NULL, NULL, '2018-08-08', '2018-08-08', 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (75, 75, NULL, NULL, NULL, '2018-12-08', '2018-12-08', 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (76, 76, NULL, NULL, NULL, '2018-08-10', '2018-10-18', 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (77, 77, NULL, NULL, NULL, '2019-01-16', '2019-01-17', 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (78, 78, NULL, NULL, NULL, '2018-08-12', '2018-08-12', 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (79, 79, NULL, NULL, NULL, '2018-11-07', '2018-11-07', 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (80, 80, NULL, NULL, NULL, '2018-10-19', '2018-10-19', 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (85, 85, NULL, NULL, NULL, '2019-02-02', '2020-01-01', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (86, 86, '005/LPPM.05/STIKI/I/2019', 'Persepsi Siswa dalam Penggunaan Media Pembelajaran Berbasis Augmented Reality', '2019-01-30', '2019-02-02', '2020-01-01', 8, '010077', '4976a393df4f9ad793c3e5dbe0504e43.pdf', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (87, 87, '007/LPPM.05/STIKI/II/2019', 'Seleksi Dosen dalam Proses Rekrutmen Menggunakan Fuzzy Sugeno', '2019-02-01', '2019-02-02', '2019-12-31', 8, '010077', '5f9bb7408b12185d4fb658c2444c41da.pdf', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (88, 88, '029/LPPM.05/STIKI/IV/2019', 'Model Pembelajaran Matematika Berbasis Blended Learning dalam Meningkatkan Communication Skill untuk', '2019-04-24', '2019-04-01', '2019-12-31', 8, '010077', 'a03918cab9208ae8be8e111d93c36c43.pdf', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (89, 89, '031/LPPM.05/STIKI/IV/2019', 'Pemanfaatan RFID Sebagai Alternatif Absensi Siswa (Studi Kasus : SMK Ar-Rahmah Sukabumi, Jawa Barat)', '2019-04-24', '2019-04-01', '2019-12-31', 8, '010077', 'cf953bb589a99bd99d05f15322ef9267.pdf', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (90, 90, NULL, NULL, NULL, '2019-04-01', '2019-12-31', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (91, 91, '033/LPPM.05/STIKI/IV/2019', 'Analisis Kebutuhan Mahasiswa dalam Mengikuti Perkuliahan Musik Digital', '2019-04-25', '2019-04-01', '2019-12-31', 8, '010077', 'ae11ea9e58c40bf3d1864ee729238f69.pdf', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (92, 92, '032/LPPM.05/STIKI/IV/2019', 'Analisis Penggunaan Digital Audio Workstation dalam Pembelajaran Musik Digital', '1970-01-01', '2019-04-01', '2019-12-31', 8, '010077', '4a1d1bb48a6238620e6d6746ec8be618.pdf', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (93, 93, '034/LPPM.05/STIKI/IV/2019', 'Perancangan Sistem Informasi Kepegawaian pada BKPP Berbasis Website', '2019-04-25', '2019-04-01', '2019-12-31', 8, '010077', '80ee26c58a4dc40a50367d48c5031ae7.pdf', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (94, 94, '044/LPPM.05/STIKI/V/2019', 'Perngaruh Hardskill dan Softskill Terhadap Self Efficacy Mahasiswa', '2019-05-23', '2019-05-01', '2019-12-31', 8, '010077', 'a13bac79db604d2a032cbe9cd4d33c11.pdf', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (102, 102, NULL, NULL, NULL, '2019-02-01', '2019-07-31', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_sk` VALUES (104, 104, NULL, NULL, NULL, '2020-01-01', '2020-12-31', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for tb_rst_tr_riset_tahapan
-- ----------------------------
DROP TABLE IF EXISTS `tb_rst_tr_riset_tahapan`;
CREATE TABLE `tb_rst_tr_riset_tahapan`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `riset` int(11) NULL DEFAULT NULL,
  `periode` int(11) NULL DEFAULT NULL,
  `tahapan` int(11) NULL DEFAULT NULL,
  `Created_App` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Created_By` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Created_Date` datetime(0) NULL DEFAULT NULL,
  `Modified_App` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Modified_By` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `Modified_Date` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `riset`(`riset`) USING BTREE,
  INDEX `periode`(`periode`) USING BTREE,
  INDEX `tahapan`(`tahapan`) USING BTREE,
  CONSTRAINT `tb_rst_tr_riset_tahapan_ibfk_1` FOREIGN KEY (`riset`) REFERENCES `tb_rst_tr_riset` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tb_rst_tr_riset_tahapan_ibfk_2` FOREIGN KEY (`periode`) REFERENCES `tb_rst_tr_periode` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `tb_rst_tr_riset_tahapan_ibfk_3` FOREIGN KEY (`tahapan`) REFERENCES `tb_rst_rf_tahapan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 129 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_rst_tr_riset_tahapan
-- ----------------------------
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (1, 1, 4, 2, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (2, 2, 4, 2, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (8, 2, 5, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (9, 1, 5, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (10, 5, 4, 2, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (11, 5, 5, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (12, 6, 4, 2, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (13, 6, 5, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (14, 7, 4, 2, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (15, 8, 4, 2, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (16, 9, 4, 2, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (17, 10, 4, 2, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (18, 11, 4, 2, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (19, 12, 4, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (20, 13, 4, 2, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (21, 14, 4, 2, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (22, 15, 4, 2, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (23, 16, 4, 2, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (24, 17, 4, 2, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (25, 18, 4, 2, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (26, 19, 4, 2, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (27, 19, 5, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (28, 20, 4, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (29, 21, 4, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (30, 22, 4, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (31, 23, 4, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (32, 24, 4, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (33, 25, 4, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (34, 26, 4, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (35, 27, 4, 2, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (36, 28, 4, 2, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (37, 29, 4, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (38, 30, 4, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (39, 31, 4, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (40, 32, 4, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (41, 33, 4, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (42, 34, 4, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (43, 35, 4, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (44, 14, 5, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (45, 15, 5, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (46, 16, 5, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (47, 17, 5, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (48, 18, 5, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (49, 9, 5, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (50, 10, 5, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (51, 11, 5, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (52, 13, 5, 2, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (53, 36, 5, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (54, 37, 5, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (55, 38, 5, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (56, 39, 5, 2, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (57, 40, 5, 2, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (58, 41, 5, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (59, 42, 5, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (60, 43, 5, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (61, 44, 5, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (62, 45, 5, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (63, 46, 5, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (64, 47, 5, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (65, 48, 5, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (66, 49, 5, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (67, 28, 5, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (68, 50, 5, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (69, 51, 5, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (70, 52, 5, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (71, 53, 5, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (72, 54, 5, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (73, 55, 5, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (74, 56, 5, 2, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (75, 56, 6, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (76, 57, 5, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (78, 59, 5, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (79, 60, 5, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (80, 61, 5, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (81, 62, 5, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (82, 63, 5, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (83, 64, 5, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (84, 65, 5, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (85, 66, 5, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (87, 68, 5, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (88, 69, 5, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (89, 70, 5, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (90, 71, 5, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (91, 72, 5, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (92, 73, 5, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (93, 74, 5, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (94, 75, 5, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (95, 76, 5, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (96, 77, 5, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (97, 78, 5, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (98, 79, 5, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (99, 80, 5, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (100, 13, 6, 2, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (101, 39, 6, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (102, 40, 6, 2, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (107, 85, 6, 2, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (108, 85, 7, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (109, 86, 6, 2, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (110, 87, 6, 2, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (111, 87, 7, 3, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (112, 88, 6, 2, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (113, 89, 6, 2, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (114, 90, 6, 2, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (115, 91, 6, 2, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (116, 92, 6, 2, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (117, 93, 6, 2, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (118, 94, 6, 2, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (126, 102, 6, 1, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `tb_rst_tr_riset_tahapan` VALUES (128, 104, 8, 2, NULL, NULL, NULL, NULL, NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
